# HiveMind Client Setup

These instructions have been moved to the [HiveMind wiki](https://kqhivemind.com/wiki).

See one of these specific pages:

 - [Basic Client Setup](https://kqhivemind.com/wiki/Basic_Client_Setup) - first steps
 - [Raspberry Pi Configuration](https://kqhivemind.com/wiki/Raspberry_Pi_Configuration) - to set up a more permanent installation
 - [NFC Reader Configuration](https://kqhivemind.com/wiki/NFC_Reader_Configuration) - to set up a NFC reader box
