#!/usr/bin/env node
const http = require("http");
const request = require("request");
const url = require("url");
const WebSocket = require("ws");
const redis = require("redis");
const uuid = require("uuid/v4");
const { Pool, Client } = require("pg");

function log(message) {
  console.log((new Date()) + " " + message);
}

function heartbeat() {
  this.isAlive = true;
}

function onInterrupt() {
  log("Shutdown requested, waiting for games to finish.");
  wss.shutdownRequested = true;
  tryShutdown();
}

process.on("SIGINT", onInterrupt);

function tryShutdown() {
  wss.clients.forEach((ws) => {
    if(currentGameID[ws.cabinetID] === null) {
      ws.close();
    }
  });

  if(wss.clients.size == 0) {
    log("No clients online. Exiting.");
    server.close();
    process.exit(0);
  }
}

const server = http.createServer(function(request, response) {
  response.writeHead(404);
  response.end();
});

const messageRegex = new RegExp("(\\d+) \\= \\!\\[\\k\\[(\\w+).*?\\]\,v\\[(.*?)\\]\\]\\!");
function parseMessage(message) {
  var result = messageRegex.exec(message);
  if(result) {
    return {
      timestamp: new Date(parseInt(result[1])),
      messageType: result[2],
      values: result[3].split(",")
    };
  }
}

const pgClient = new Pool({
  user: process.env.POSTGRES_USER,
  host: process.env.POSTGRES_HOST,
  database: process.env.POSTGRES_DB,
  password: process.env.POSTGRES_PASSWORD,
});

pgClient.connect();

const redisClient = redis.createClient({host: process.env.REDIS_HOST, password: process.env.REDIS_PASSWORD});

async function startGame(message) {
  var result = await pgClient.query("insert into game_game(start_time, map_name, cabinet_id) values($1, $2, $3) returning id",
                                    [message.timestamp, message.values[0], message.cabinetID]);
  log(`(${message.cabinetName}) Game ${result.rows[0].id} started (${message.values[0]}).`);
  redisClient.publish("gamestate", JSON.stringify({
    "type": "gamestart",
    "cabinet_id": message.cabinetID,
    "cabinet_name": message.cabinetName,
    "scene_name": message.sceneName,
    "game_id": result.rows[0].id,
  }));

  return result.rows[0].id;
}

async function insertMessages(gameID, startTime, messages) {
  await pgClient.query("begin");
  var victoryMessage = null;

  for(var message of messages) {
    if(message.timestamp.getTime() >= startTime.getTime() - 12000) {
      await pgClient.query("insert into game_gameevent(timestamp, event_type, values, game_id, uuid) values($1, $2, $3, $4, $5)",
                           [message.timestamp, message.messageType, message.values, gameID, uuid()]);

      if(message.messageType == "victory") {
        victoryMessage = message;
      }
    }
  }

  await pgClient.query("commit");

  if(victoryMessage !== null) {
    await updateResult(gameID, victoryMessage);
    return true;
  }

  return false;
}

async function updateResult(gameID, victoryMessage) {
  var mapName = null;
  var gameStart = null;

  log(`(${victoryMessage.cabinetName}) Game complete (${victoryMessage.values[0]} ${victoryMessage.values[1]}).`);

  await pgClient.query("begin");

  var result = await pgClient.query("update game_game set end_time = $1, win_condition = $2, winning_team = $3 where id = $4",
                                    [victoryMessage.timestamp, victoryMessage.values[1], victoryMessage.values[0].toLowerCase(), gameID]);
  await pgClient.query("commit");

  redisClient.publish("gamestate", JSON.stringify({
    "type": "gameend",
    "cabinet_name": victoryMessage.cabinetName,
    "scene_name": victoryMessage.sceneName,
    "game_id": gameID,
    "winning_team": victoryMessage.values[0].toLowerCase(),
    "win_condition": victoryMessage.values[1],
  }));
}

const wss = new WebSocket.Server({ server });
wss.shutdownRequested = false;

const interval = setInterval(function ping() {
  wss.clients.forEach(function each(ws) {
    if(!ws.isAlive)
      return ws.terminate();

    ws.isAlive = false;
    ws.ping(() => {});
  });
}, 30000);

const queuedMessages = {};
var currentGameID = {};
var currentGameStart = {};
var lastMessagesInsert = null;

wss.on("connection", ws => {
  if(wss.shutdownRequested) {
    ws.close();
    return;
  }

  log("New connection.");
  ws.isAlive = true;
  ws.sentToken = false;
  ws.cabinetID = null;
  ws.cabinetName = null;
  ws.sceneName = null;

  ws.on("pong", heartbeat);

  ws.on("message", messageString => {
    var message = parseMessage(messageString);
    if(!message)
      return;

    message.cabinetID = ws.cabinetID;
    message.cabinetName = ws.cabinetName;
    message.sceneName = ws.sceneName;

    if(message.messageType == "piOnline") {
      pgClient.query("select c.*, s.name as scene_name from game_cabinetlegacyname cln join game_cabinet c on (cln.cabinet_id = c.id) join game_scene s on (c.scene_id = s.id) where lower(cln.name) = lower($1)", [message.values[1]]).then((result) => {
        if(result.rows.length == 0) {
          log(`Cabinet "${message.values[1]}" connected, not found in database.`);
          return;
        }

        const cabinet = result.rows[0];
        if(cabinet.token != message.values[2]) {
          log(`Invalid token for cabinet "${message.values[1]}".`);
          return;
        }

        log(`Cabinet "${message.values[1]}" (id ${cabinet.id}) online.`);

        ws.cabinetID = cabinet.id;
        ws.cabinetName = message.values[1].toLowerCase();
        ws.sceneName = cabinet.scene_name.toLowerCase();
        ws.sentToken = true;
        currentGameID[ws.cabinetID] = null;
        queuedMessages[ws.cabinetID] = [];
        return;
      });

    } else {
      if(!ws.sentToken)
        return;

      queuedMessages[ws.cabinetID].push(message);
      redisClient.publish("events." + message.sceneName + "." + message.cabinetName, JSON.stringify({
        "event_type": message.messageType,
        "values": message.values,
      }));
    }
  });

  ws.on("close", () => {
    log("Connection closed.");
  });
});

async function processQueue() {
  for(let cabinetID in queuedMessages) {
    const messageList = queuedMessages[cabinetID];

    for(let message of messageList) {
      if(message.messageType == "gamestart") {
        currentGameID[cabinetID] = await startGame(message);
        currentGameStart[cabinetID] = message.timestamp;
      }

      if(message.messageType == "cabinetOffline") {
        currentGameID[cabinetID] = null;
      }
    }

    if(currentGameID[cabinetID] !== null && messageList.length > 0) {
      let insertingMessages = messageList.splice(0, messageList.length);
      let isComplete = await insertMessages(currentGameID[cabinetID], currentGameStart[cabinetID], insertingMessages);

      if(isComplete) {
        currentGameID[cabinetID] = null;
        if(wss.shutdownRequested)
          tryShutdown();
      }
    }
  }

  setTimeout(processQueue, 100);
}

setTimeout(processQueue, 100);
log("Stats listener online.");
server.listen(8080);
