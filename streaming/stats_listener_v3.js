#!/usr/bin/env node
const http = require('http');
const request = require('request');
const url = require('url');
const WebSocket = require('ws');
const redis = require('redis');
const { Pool, Client } = require('pg');
const { format } = require('date-fns');

function log(...message) {
  console.log(format(new Date(), 'yyyy-MM-dd HH:mm:ss'), ...message);
}

function heartbeat() {
  this.isAlive = true;
}

const server = http.createServer(function (request, response) {
  response.writeHead(404);
  response.end();
});

const pgConfig = {
  user: process.env.POSTGRES_USER,
  host: process.env.POSTGRES_HOST,
  database: process.env.POSTGRES_DB,
  password: process.env.POSTGRES_PASSWORD,
  connectionTimeoutMillis: 1000,
  idleTimeoutMillis: 1000,
};
const pgPool = new Pool(pgConfig);

const redisClient = redis.createClient({
  host: process.env.REDIS_HOST,
  password: process.env.REDIS_PASSWORD,
});

const QUERIES = {
  startGame: `INSERT INTO game_game (start_time, map_name, cabinet_id, cabinet_version, uuid, status)
              VALUES ($1, $2, $3, $4, $5, 'incomplete')
              ON CONFLICT (uuid) DO UPDATE SET start_time = $1, map_name = $2, cabinet_id = $3, cabinet_version = $4
              RETURNING id`,

  updateGame: `UPDATE game_game
               SET end_time = $1, win_condition = $2, winning_team = $3, status = 'ready'
               WHERE uuid = $4`,

  getGame: `SELECT *
            FROM game_game
            WHERE uuid = $1`,

  updateScene: `UPDATE game_scene
               SET last_game_time = $1, total_games = total_games + 1
               WHERE name = $2`,

  getCabinet: `SELECT c.*, s.name AS scene_name
               FROM game_cabinet c
                 JOIN game_scene s ON (c.scene_id = s.id)
               WHERE lower(c.name) = lower($1)
                 AND lower(s.name) = lower($2)`,

  insertEvent: `INSERT INTO game_gameevent (uuid, game_uuid, timestamp, event_type, values, game_id)
                VALUES ($1, $2, $3, $4, $5, $6)
                ON CONFLICT (uuid) DO UPDATE SET
                  game_uuid = $2, timestamp = $3, event_type = $4, values = $5, game_id = $6`,

  cabinetOnline: `UPDATE game_cabinet
                  SET client_status = 'online', last_connection = now(), last_connection_ip = $1
                  WHERE id = $2`,

  cabinetOffline: `UPDATE game_cabinet
                   SET client_status = 'offline'
                   WHERE id = $1`,
};

function getCacheKey(message) {
  return `${message.cabinet.sceneName}/${message.cabinet.cabinetName}`;
}

async function startGame(pgClient, cabinetInfo, message) {
  const result = await pgClient.query(QUERIES.startGame, [
    new Date(message.time),
    message.values[0],
    cabinetInfo.cabinetID,
    message.values[4],
    message.gameID,
  ]);

  redisClient.publish(
    'gamestate',
    JSON.stringify({
      type: 'gamestart',
      cabinet_id: cabinetInfo.cabinetID,
      cabinet_name: cabinetInfo.cabinetName,
      scene_name: cabinetInfo.sceneName,
      game_id: result.rows[0].id,
    }),
  );

  const gameInfo = {
    cabinetInfo,
    gameID: result.rows[0].id,
    startTime: new Date(message.time),
  };

  return gameInfo;
}

async function getGameInfo(pgClient, message) {
  if (message.gameID in gameInfoCache) {
    return gameInfoCache[message.gameID];
  }

  const threshold = new Date() - 5000;
  if (gameInfoLastQuery[message.gameID] && gameInfoLastQuery[message.gameID] >= threshold) {
    return null;
  }

  const result = await pgClient.query(QUERIES.getGame, [message.gameID]);
  if (result && result.rows && result.rows.length > 0) {
    const cabinetInfo = await getCabinetInfo(pgClient, message);
    gameInfoCache[message.gameID] = {
      cabinetInfo,
      gameID: result.rows[0].id,
      startTime: result.rows[0].start_time,
    };

    log(`Fetched game ${result.rows[0].id} for UUID ${message.gameID}`);

    delete gameInfoLastQuery[message.gameID];
    return gameInfoCache[message.gameID];
  }

  gameInfoLastQuery[message.gameID] = Date.now();
}

async function updateResult(pgClient, gameInfo, cabinetInfo, gameID, victoryMessage) {
  log(
    `(${cabinetInfo.cabinetName}) Game complete (${victoryMessage.values[0]} ${victoryMessage.values[1]}).`,
  );

  if (gameInfo) {
    gameInfo.isComplete = true;
  }
  const gameEndTime = new Date(victoryMessage.time);
  const result = await pgClient.query(QUERIES.updateGame, [
    gameEndTime,
    victoryMessage.values[1],
    victoryMessage.values[0].toLowerCase(),
    gameID,
  ]);

  const sceneResult = await pgClient.query(QUERIES.updateScene, [
    gameEndTime,
    cabinetInfo.sceneName,
  ]);
}

async function getCabinetInfo(pgClient, message) {
  const cacheKey = getCacheKey(message);
  if (
    !(cacheKey in cabinetInfoCache && cabinetInfoCache[cacheKey].lastUpdate > Date.now() - 60000)
  ) {
    log(`Updating cabinet info for ${cacheKey}...`);
    const result = await pgClient.query(QUERIES.getCabinet, [
      message.cabinet.cabinetName,
      message.cabinet.sceneName,
    ]);

    if (result && result.rows && result.rows.length > 0) {
      let row = result.rows[0];
      cabinetInfoCache[cacheKey] = {
        cabinetID: row.id,
        cabinetName: row.name.toLowerCase(),
        token: row.token,
        sceneName: row.scene_name.toLowerCase(),
        lastUpdate: Date.now(),
      };

      await pgClient.query(QUERIES.cabinetOnline, [message.ws._socket.remoteAddress, row.id]);

      log(`Updated cabinet info for ${cacheKey} (cabinet ID ${row.id})`);
    } else {
      log(`Cabinet ${cacheKey} not found.`);
    }
  }

  return cabinetInfoCache[cacheKey];
}

async function setCabinetOnline(pgClient, message) {
  const cabinetInfo = await getCabinetInfo(pgClient, message);
  log(`Cabinet online: ${cabinetInfo.sceneName}/${cabinetInfo.cabinetName}`);

  await pgClient.query(QUERIES.cabinetOnline, [
    message.ws._socket.remoteAddress,
    cabinetInfo.cabinetID,
  ]);

  redisClient.publish(
    'gamestate',
    JSON.stringify({
      type: 'cabinetOnline',
      cabinet_id: cabinetInfo.cabinetID,
      cabinet_name: cabinetInfo.cabinetName,
      scene_name: cabinetInfo.sceneName,
    }),
  );
}

async function setCabinetOffline(pgClient, cacheKey, reason) {
  if (cacheKey in cabinetInfoCache) {
    const cabinetInfo = cabinetInfoCache[cacheKey];
    log(`Cabinet offline: ${cabinetInfo.sceneName}/${cabinetInfo.cabinetName} - ${reason}`);

    await pgClient.query(QUERIES.cabinetOffline, [cabinetInfo.cabinetID]);

    redisClient.publish(
      'gamestate',
      JSON.stringify({
        type: 'cabinetOffline',
        cabinet_id: cabinetInfo.cabinetID,
        cabinet_name: cabinetInfo.cabinetName,
        scene_name: cabinetInfo.sceneName,
      }),
    );
  }
}

function checkToken(message, cabinetInfo) {
  if (message.cabinet.token != cabinetInfo.token) {
    if (message.ws && message.ws.readyState === WebSocket.OPEN) {
      message.ws.send(
        JSON.stringify({
          error: `Invalid token for cabinet ${cabinetInfo.sceneName}/${cabinetInfo.cabinetName}`,
        }),
      );

      message.ws.close();
    }

    return false;
  }

  return true;
}

const wss = new WebSocket.Server({ server });

const interval = setInterval(() => {
  wss.clients.forEach(ws => {
    if (!ws.isAlive) return ws.terminate();

    ws.isAlive = false;
    ws.ping(() => {});
  });
}, 30000);

const queuedMessages = [];
const queuedMessagesByID = new Set();
const disconnects = [];
const cabinetInfoCache = {};
const gameInfoCache = {};
const gameInfoLastQuery = {};

wss.on('connection', ws => {
  log('New connection.');
  ws.isAlive = true;
  ws.cabinets = new Set();

  ws.on('pong', heartbeat);

  ws.on('message', messageString => {
    var message = JSON.parse(messageString);
    if (!message) return;

    message.ws = ws;
    if (!queuedMessagesByID.has(message.eventID)) {
      queuedMessages.push(message);
      queuedMessagesByID.add(message.eventID);
      ws.cabinets.add(getCacheKey(message));
    }
  });

  ws.on('close', () => {
    log('Connection closed.');
    for (const cabinet of ws.cabinets) {
      disconnects.push(cabinet);
    }
  });
});

async function processQueue(pgClient) {
  if (queuedMessages.length === 0) return;

  const gamesEnded = [];
  await pgClient.query('begin');

  for (const message of queuedMessages) {
    if (message.type == 'gamestart') {
      const cabinetInfo = await getCabinetInfo(pgClient, message);
      if (!checkToken(message, cabinetInfo)) {
        continue;
      }

      const gameInfo = (gameInfoCache[message.gameID] = await startGame(
        pgClient,
        cabinetInfo,
        message,
      ));
      log(
        `Starting new game ${gameInfo.gameID} on ${cabinetInfo.sceneName}/${cabinetInfo.cabinetName}`,
      );
    }

    if (message.type == 'playernames') {
      const cabinetInfo = await getCabinetInfo(pgClient, message);

      redisClient.publish(
        'gamestate',
        JSON.stringify({
          type: 'playernames',
          cabinet_id: cabinetInfo.cabinetID,
          cabinet_name: cabinetInfo.cabinetName,
          scene_name: cabinetInfo.sceneName,
        }),
      );
    }

    if (message.type == 'cabinetOnline') {
      await setCabinetOnline(pgClient, message);
    }

    if (message.type == 'cabinetOffline') {
      const cacheKey = getCacheKey(message);
      await setCabinetOffline(pgClient, cacheKey, `(${message.values[0]}) ${message.values[1]}`);
    }
  }

  await pgClient.query('commit');
  await pgClient.query('begin');

  const messageBatch = queuedMessages.splice(0, Math.min(queuedMessages.length, 100));
  for (const message of messageBatch) {
    let gameInfo = await getGameInfo(pgClient, message);
    const cabinetInfo = await getCabinetInfo(pgClient, message);

    if (!checkToken(message, cabinetInfo)) {
      continue;
    }

    // Handle race condition where gamestart comes into the queue after the previous loop,
    // but before this batch was spliced off
    if (message.type === 'gamestart' && gameInfo === null) {
      log(
        `(${cabinetInfo.sceneName}/${cabinetInfo.cabinetName}) got gamestart in batch with no game associated`,
      );
      gameInfo = gameInfoCache[message.gameID] = await startGame(pgClient, cabinetInfo, message);
      log(
        `Starting new game ${gameInfo.gameID} on ${cabinetInfo.sceneName}/${cabinetInfo.cabinetName}`,
      );
    }

    const messageTime = new Date(message.time);
    await pgClient.query(QUERIES.insertEvent, [
      message.eventID,
      message.gameID,
      messageTime,
      message.type,
      message.values,
      gameInfo?.gameID,
    ]);

    redisClient.publish(
      `events.cabinet.${cabinetInfo.cabinetID}`,
      JSON.stringify({
        event_type: message.type,
        values: message.values,
        uuid: message.eventID,
      }),
    );

    if (message.type == 'victory') {
      await updateResult(pgClient, gameInfo, cabinetInfo, message.gameID, message);
      gamesEnded.push({ message, gameInfo, cabinetInfo });
    }

    message.processed = true;
  }

  const disconnectBatch = disconnects.splice(0, disconnects.length);
  disconnectBatch.forEach(async disconnect => {
    for (const ws of wss.clients) {
      if (ws.isAlive && ws.cabinets.has(disconnect)) {
        return;
      }
    }

    await setCabinetOffline(pgClient, disconnect, 'client disconnected from server');
  });

  await pgClient.query('commit');

  for (const { message, gameInfo, cabinetInfo } of gamesEnded) {
    if (gameInfo) {
      redisClient.publish(
        'gamestate',
        JSON.stringify({
          type: 'gameend',
          cabinet_id: cabinetInfo.cabinetID,
          cabinet_name: cabinetInfo.cabinetName,
          scene_name: cabinetInfo.sceneName,
          game_id: gameInfo.gameID,
          winning_team: message.values[0].toLowerCase(),
          win_condition: message.values[1],
        }),
      );
    }
  }

  for (const message of messageBatch) {
    if (message.processed) {
      if (message.ws && message.ws.readyState === WebSocket.OPEN) {
        message.ws.send(JSON.stringify({ eventID: message.eventID }));
      }
    } else {
      queuedMessages.push(message);
    }
  }
}

function tryProcessQueue() {
  pgPool
    .connect()
    .then(pgClient => {
      processQueue(pgClient)
        .then(() => {})
        .catch(error => {
          log('Error processing queue', error);
          pgClient.query('rollback').catch(() => {});
        })
        .finally(() => {
          pgClient.release();
        });
    })
    .catch(error => {
      log('Error connecting to database', error);
    })
    .finally(() => {
      setTimeout(tryProcessQueue, 100);
    });
}

function cleanup() {
  const lastMessageByGame = {};
  for (const message of queuedMessages) {
    lastMessageByGame[message.gameID] = Math.max(
      new Date(message.time),
      lastMessageByGame[message.gameID] || 0,
    );
  }

  const staleGames = new Set();
  const threshold = new Date() - 30 * 60000;

  for (const [gameID, messageTime] of Object.entries(lastMessageByGame)) {
    if (messageTime < threshold) {
      staleGames.add(gameID);
      log(`Discarding stale game ${gameID}`);
    }
  }

  if (staleGames.size > 0) {
    const messages = queuedMessages.splice(0, queuedMessages.length);
    for (message of messages) {
      if (staleGames.has(message.gameID)) {
        if (message.ws && message.ws.readyState === WebSocket.OPEN) {
          message.ws.send(JSON.stringify({ eventID: message.eventID }));
        }
      } else {
        queuedMessages.push(message);
      }
    }
  }

  log(
    `${queuedMessages.length} in queue, ${Object.keys(cabinetInfoCache).length} cabinets cached, ${
      Object.keys(gameInfoCache).length
    } games cached`,
  );
}

setInterval(cleanup, 30000);

setTimeout(tryProcessQueue, 100);
server.listen(8080);
