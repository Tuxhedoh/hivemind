#!/usr/bin/env node
const http = require("http");
const WebSocket = require("ws");
const redis = require("redis");

function log(message) {
  console.log((new Date()) + " " + message);
}

function heartbeat() {
  this.isAlive = true;
}

const server = http.createServer(function(request, response) {
  response.writeHead(404);
  response.end();
});

const redisClient = redis.createClient({host: process.env.REDIS_HOST, password: process.env.REDIS_PASSWORD});
redisClient.subscribe("gamestate");
redisClient.subscribe("matchstate");
redisClient.subscribe("overlaysettings");

const wss = new WebSocket.Server({ server });

const interval = setInterval(function ping() {
  wss.clients.forEach(function each(ws) {
    if(!ws.isAlive)
      return ws.terminate();

    ws.isAlive = false;
    ws.ping(() => {});
  });
}, 30000);

wss.on("connection", ws => {
  ws.isAlive = true;
  ws.filters = {};
  ws.on("pong", heartbeat);

  ws.on("message", messageString => {
    message = JSON.parse(messageString);
    if(message.scene_name) {
      if(!ws.filters.scenes)
        ws.filters.scenes = new Set();

      ws.filters.scenes.add(message.scene_name.toLowerCase());
    }

    if(message.cabinet_name) {
      if(!ws.filters.cabinets)
        ws.filters.cabinets = new Set();

      ws.filters.cabinets.add(message.cabinet_name.toLowerCase());
    }
  });
});

redisClient.on("message", function(channel, messageString) {
  let message = JSON.parse(messageString);
  log(messageString);

  wss.clients.forEach(function each(ws) {
    if(ws.filters.scenes && !ws.filters.scenes.has(message.scene_name))
      return;
    if(ws.filters.cabinets && !ws.filters.cabinets.has(message.cabinet_name))
      return;

    try {
      ws.send(messageString);
    } catch (err) {
      log(err);
      ws.close();
    }
  });
});

server.listen(8080);
