version: '2.2'
services:
  db:
    image: "postgres:13.2"
    env_file: app.env
    volumes:
      - /var/lib/postgresql/data

  redis:
    image: "redis:5"

  web:
    image: "nginx:1.20"
    volumes:
      - ./static:/usr/share/nginx/html/static:ro
      - ./media:/usr/share/nginx/media:ro
      - ./config/nginx.conf:/etc/nginx/conf.d/default.conf:ro
      - ./config/nginx.common.conf:/etc/nginx/nginx.common.conf:ro
    ports:
      - "8087:80"
    depends_on:
      - app
      - stats_listener

  nextjs:
    build:
      context: .
      dockerfile: Dockerfile.nextjs.local
    volumes:
      - ./nextjs:/src
    command: "yarn run dev"
    environment:
      NODE_PATH: /node/node_modules

  app:
    build:
      context: .
      dockerfile: Dockerfile.app
    depends_on:
      - db
      - redis
    env_file: app.env
    volumes:
      - ./media:/data/media:rw
      - .:/src
      - ./.ipython:/root/.ipython
    environment:
      APP_ENVIRONMENT: dev

  worker:
    build:
      context: .
      dockerfile: Dockerfile.app
    depends_on:
      - db
      - redis
    env_file: app.env
    command: "python /src/server/manage.py runscript worker"
    volumes:
      - ./media:/data/media:rw
      - .:/src
      - ./.ipython:/root/.ipython
    environment:
      APP_ENVIRONMENT: dev

  scheduler:
    build:
      context: .
      dockerfile: Dockerfile.app
    depends_on:
      - db
      - redis
    env_file: app.env
    command: "python /src/server/manage.py runscript scheduler"
    volumes:
      - ./media:/data/media:rw
      - .:/src
      - ./.ipython:/root/.ipython
    environment:
      APP_ENVIRONMENT: dev

  event_manager:
    build:
      context: .
      dockerfile: Dockerfile.app
    depends_on:
      - db
      - redis
    env_file: app.env
    command: "python /src/server/manage.py runscript event_manager"
    volumes:
      - .:/src

  kquity:
    build:
      context: .
      dockerfile: Dockerfile.app
    depends_on:
      - db
      - redis
    env_file: app.env
    command: "python /src/server/manage.py runscript kquity"
    volumes:
      - .:/src

  stats_listener:
    build:
      context: .
      dockerfile: Dockerfile.streaming
    depends_on:
      - db
      - redis
    command: "node stats_listener_v3.js"
    env_file: app.env
    volumes:
      - ./streaming/stats_listener_v3.js:/src/streaming/stats_listener_v3.js

  stats_listener_v3:
    build:
      context: .
      dockerfile: Dockerfile.streaming
    depends_on:
      - db
      - redis
    command: "node --inspect stats_listener_v3.js"
    env_file: app.env
    volumes:
      - ./streaming/stats_listener_v3.js:/src/streaming/stats_listener_v3.js

  gamestate:
    build:
      context: .
      dockerfile: Dockerfile.streaming
    depends_on:
      - redis
    command: "node gamestate.js"
    env_file: app.env
    volumes:
      - ./streaming/gamestate.js:/src/streaming/gamestate.js

  ingame_stats:
    build:
      context: .
      dockerfile: Dockerfile.streaming
    depends_on:
      - redis
    command: "node ingame_stats.js"
    env_file: app.env
    volumes:
      - ./streaming/ingame_stats.js:/src/streaming/ingame_stats.js

  signin:
    build:
      context: .
      dockerfile: Dockerfile.streaming
    depends_on:
      - redis
    command: "node signin.js"
    env_file: app.env
    volumes:
      - ./streaming/signin.js:/src/streaming/signin.js

  event_relay:
    build:
      context: .
      dockerfile: Dockerfile.streaming
    depends_on:
      - redis
    command: "node event_relay.js"
    env_file: app.env
    volumes:
      - ./streaming/event_relay.js:/src/streaming/event_relay.js

  tournament_relay:
    build:
      context: .
      dockerfile: Dockerfile.streaming
    depends_on:
      - redis
    command: "node tournament_relay.js"
    env_file: app.env
    volumes:
      - ./streaming/tournament_relay.js:/src/streaming/tournament_relay.js

  elasticsearch:
    image: docker.elastic.co/elasticsearch/elasticsearch-oss:7.10.2
    env_file: app.env
    restart: always
    environment:
      - "http.host=0.0.0.0"
      - "discovery.type=single-node"
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
      - "cluster.routing.allocation.disk.threshold_enabled=false"

  mongo:
    image: "mongo:4.2"
    env_file: app.env
    restart: always

  graylog:
    image: graylog/graylog:4.2
    env_file: app.env
    restart: always
    ports:
      - "127.0.0.1:12201:12201/udp"
    depends_on:
      - mongo
      - elasticsearch
