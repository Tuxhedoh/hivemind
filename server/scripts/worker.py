import os

import hivemind.worker


def run():
    worker = hivemind.worker.app.Worker(include=hivemind.worker.task_modules)
    worker.start()
