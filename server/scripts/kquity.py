import json
import logging
import pathlib
import threading
import urllib
from datetime import datetime

import lightgbm
from django.db import transaction

from hivemind.client import redis_client
from hivemind.game.models import GameEvent
from hivemind.kquity import map_structure, preprocess

logger = logging.getLogger(__name__)

MODEL_URL = "https://cdn.kqhivemind.com/kquity%2Fmodel.mdl"
MODEL_PATH = "/tmp/model.mdl"

skippable_events = {
    'gameend', 'playernames',
    'reserveMaiden', 'unreserveMaiden',
    'cabinetOnline', 'cabinetOffline',
    'bracket', 'tstart', 'tournamentValidation', 'checkIfTournamentRunning',
    'glance',
}

dispatcher = {
    'berryDeposit': preprocess.BerryDepositEvent,
    'berryKickIn': preprocess.BerryKickInEvent,
    'carryFood': preprocess.CarryFoodEvent,
    'snailEat': preprocess.SnailEatEvent,
    'snailEscape': preprocess.SnailEscapeEvent,
    'getOnSnail': preprocess.GetOnSnailEvent,
    'getOffSnail': preprocess.GetOffSnailEvent,
    'glance': preprocess.GlanceEvent,
    'playerKill': preprocess.PlayerKillEvent,
    'blessMaiden': preprocess.BlessMaidenEvent,
    'useMaiden': preprocess.UseMaidenEvent,
    'spawn': preprocess.SpawnEvent,
    'gamestart': preprocess.GameStartEvent,
    'mapstart': preprocess.MapStartEvent,
    'victory': preprocess.VictoryEvent,
}

def predict_cabinet(model, map_structure_infos, cabinet_id):
    subscriber = redis_client.pubsub()
    subscriber.subscribe(f"events.cabinet.{cabinet_id}")

    game_state = None
    map_info = None

    for message in subscriber.listen():
        if message["type"] == "message":
            data = json.loads(message["data"])

            if data["event_type"] == "gamestart":
                try:
                    game_start = preprocess.GameStartEvent(data["values"])
                    map_info = map_structure_infos.get_map_info(game_start.map, game_start.gold_on_left)
                    game_state = preprocess.GameState(map_info)
                except:
                    pass

                continue

            if game_state is None or data["event_type"] in skippable_events:
                continue

            try:
                event = dispatcher[data["event_type"]](data["values"])
                event.timestamp = datetime.utcnow().timestamp()
                event.modify_game_state(game_state)
                state_vector = preprocess.vectorize_game_state(game_state, event)
                prediction = model.predict([state_vector])
                redis_client.publish(
                    f"predictions.cabinet.{cabinet_id}",
                    json.dumps({"prediction": prediction[0]}),
                )

            except:
                pass

@transaction.atomic
def process_game(model, map_structure_infos, game_id):
    logger.info("Processing win probability for game {}".format(game_id))

    events = GameEvent.objects.select_for_update().filter(game_id=game_id).order_by("timestamp")
    game_state = None
    map_info = None

    for event in events:
        if event.event_type == "gamestart":
            game_start = preprocess.GameStartEvent(event.values)
            game_start.timestamp = event.timestamp.timestamp()
            map_info = map_structure_infos.get_map_info(game_start.map, game_start.gold_on_left)
            game_state = preprocess.GameState(map_info)
            continue

        if game_state is None or event.event_type in skippable_events:
            continue

        try:
            event_obj = dispatcher[event.event_type](event.values)
            event_obj.timestamp = event.timestamp.timestamp()
            event_obj.modify_game_state(game_state)
            state_vector = preprocess.vectorize_game_state(game_state, event_obj)
            prediction = model.predict([state_vector])
            event.win_probability = prediction[0]
        except:
            pass

    GameEvent.objects.bulk_update(events, ["win_probability"], batch_size=100)
    logger.info("Done processing game {}".format(game_id))

def run():
    if not pathlib.Path(MODEL_PATH).is_file():
        urllib.request.urlretrieve(MODEL_URL, MODEL_PATH)

    model = lightgbm.Booster(None, None, MODEL_PATH)
    map_structure_infos = map_structure.MapStructureInfos()

    subscriber = redis_client.pubsub()
    subscriber.subscribe("gamestate")

    cabinets_online = set()

    for message in subscriber.listen():
        if message["type"] != "message":
            continue

        data = json.loads(message["data"])
        if data["type"] in ["gamestart", "playernames", "cabinetOnline"] and data["cabinet_id"] not in cabinets_online:
            cabinets_online.add(data["cabinet_id"])
            thread = threading.Thread(target=predict_cabinet, args=[model, map_structure_infos, data["cabinet_id"]])
            thread.start()

        if data["type"] == "gameend":
            try:
                process_game(model, map_structure_infos, data["game_id"])
            except:
                pass
