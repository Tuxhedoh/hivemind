import json
import logging

import hivemind.game.tasks
from hivemind.client import redis_client

logger = logging.getLogger(__name__)

def run():
    subscriber = redis_client.pubsub()
    subscriber.subscribe("gamestate")

    for message in subscriber.listen():
        try:
            if message["type"] == "message":
                data = json.loads(message["data"])
                if data["type"] == "gameend":
                    hivemind.game.tasks.process_game.delay(data["game_id"])

        except:
            logger.exception("Error in postgame processing")
