import http.client
from datetime import datetime, timedelta

from ..tests import HiveMindTest
from .models import Season


class SeasonTestCase(HiveMindTest):
    def setUp(self):
        super().setUp()

    def test_create_season(self):
        self.client.force_authenticate(self._user)

        data = {
            "name": "Test Season",
            "scene": self._scene.id,
            "start_date": datetime.now().strftime("%Y-%m-%d"),
            "end_date": (datetime.now() + timedelta(days=14)).strftime("%Y-%m-%d"),
            "season_type": Season.SeasonType.SHUFFLE,
        }

        response = self.client.post("/api/league/season/", data)
        self.assertEqual(response.status_code, http.client.CREATED)
        season_id = response.data["id"]

        response = self.client.get("/api/league/season/{}/".format(season_id))
        self.assertEqual(response.data["name"], "Test Season")

    def test_edit_season(self):
        self.client.force_authenticate(self._user)

        data = {
            "name": "Test Season",
            "scene": self._scene.id,
            "start_date": datetime.now().strftime("%Y-%m-%d"),
            "end_date": (datetime.now() + timedelta(days=14)).strftime("%Y-%m-%d"),
            "season_type": Season.SeasonType.SHUFFLE,
        }

        response = self.client.post("/api/league/season/", data)
        self.assertEqual(response.status_code, http.client.CREATED)
        season_id = response.data["id"]

        data["name"] = "Test Season 2"
        response = self.client.put("/api/league/season/{}/".format(season_id), data)
        self.assertEqual(response.status_code, http.client.OK)

        response = self.client.get("/api/league/season/{}/".format(season_id))
        self.assertEqual(response.data["name"], "Test Season 2")


class LeagueTest(HiveMindTest):
    def setUp(self):
        super().setUp()

        self._season = Season(
            name="Test Season",
            scene=self._scene,
            start_date=datetime.now(),
            end_date=datetime.now() + timedelta(days=14),
            season_type=Season.SeasonType.SHUFFLE,
        )
        self._season.save()

        self.client.force_authenticate(self._user)


class EventShuffleTestCase(LeagueTest):
    def test_create_event(self):
        data = {
            "event_date": datetime.now().strftime("%Y-%m-%d"),
            "name": "Test Event",
            "season": self._season.id,
            "cabinet": self._cabinet.id,
            "is_active": False,
            "is_quickplay": False,
            "points_per_round": 2,
            "points_per_match": 1,
            "rounds_per_match": 3,
            "wins_per_match": 0,
            "matches_per_opponent": 1,
        }

        response = self.client.post("/api/league/event/", data)
        self.assertEqual(response.status_code, http.client.CREATED)
