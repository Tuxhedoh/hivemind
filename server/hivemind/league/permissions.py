from django.core.exceptions import BadRequest
from rest_framework.permissions import SAFE_METHODS, BasePermission

from ..constants import HTTPMethod
from ..permissions import SceneAdminOrReadOnly
from .models import Event, Season


class SeasonPermission(SceneAdminOrReadOnly):
    pass


class EventPermission(SceneAdminOrReadOnly):
    def get_scene_from_object(self, obj):
        return obj.season.scene

    def get_scene_id_from_request(self, request):
        try:
            season = Season.objects.get(id=request.data.get("season"))
            return season.scene.id
        except Season.DoesNotExist:
            raise BadRequest("Invalid season")


class PlayerPermission(SceneAdminOrReadOnly):
    pass


class TeamPermission(SceneAdminOrReadOnly):
    def get_scene_from_object(self, obj):
        return obj.event.season.scene

    def get_scene_id_from_request(self, request):
        try:
            event = Event.objects.get(id=request.data.get("event"))
            return event.season.scene.id
        except Event.DoesNotExist:
            raise BadRequest("Invalid event")


class BYOTeamPermission(SceneAdminOrReadOnly):
    def get_scene_from_object(self, obj):
        return obj.season.scene

    def get_scene_id_from_request(self, request):
        try:
            season = Season.objects.get(id=request.data.get("season"))
            return season.scene.id
        except Season.DoesNotExist:
            raise BadRequest("Invalid event")
