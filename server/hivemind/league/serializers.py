from rest_framework import serializers

from ..game.models import Cabinet, Scene
from ..game.serializers import SceneSerializer
from .models import (BYOTeam, Event, Match, Player, QPMatch, Season,
                     SeasonPlace, Team)


class SeasonSerializer(serializers.ModelSerializer):
    event_count = serializers.IntegerField(read_only=True)
    scene = serializers.PrimaryKeyRelatedField(queryset=Scene.objects.all())

    class Meta:
        model = Season
        fields = "__all__"


class EventSerializer(serializers.ModelSerializer):
    name = serializers.CharField(allow_null=True)
    event_number = serializers.IntegerField(read_only=True)
    season = serializers.PrimaryKeyRelatedField(queryset=Season.objects.all())
    cabinet = serializers.PrimaryKeyRelatedField(queryset=Cabinet.objects.all(), allow_null=True)
    unassigned_players = serializers.PrimaryKeyRelatedField(
        queryset=Player.objects.all(),
        many=True,
        required=False,
        allow_empty=True,
    )

    class Meta:
        model = Event
        fields = ["id", "event_date", "name", "season", "cabinet", "is_complete", "is_active",
                  "is_quickplay", "is_playoff", "points_per_round", "points_per_match",
                  "rounds_per_match", "wins_per_match", "qp_matches_per_player", "matches_per_opponent",
                  "event_number", "unassigned_players"]


class MatchSerializer(serializers.ModelSerializer):
    blue_team = Team.default_serializer()()
    gold_team = Team.default_serializer()()
    event = EventSerializer(read_only=True)
    match_number = serializers.IntegerField(read_only=True)

    class Meta:
        model = Match
        fields = "__all__"


class QPMatchSerializer(serializers.ModelSerializer):
    match_number = serializers.IntegerField(read_only=True)

    class Meta:
        model = QPMatch
        fields = "__all__"


class SeasonPlaceSerializer(serializers.ModelSerializer):
    team = BYOTeam.default_serializer()(read_only=True)

    class Meta:
        model = SeasonPlace
        fields = ["place", "team"]


class PlayerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Player
        fields = ["id", "name", "scene"]


class TeamSerializer(serializers.ModelSerializer):
    players = serializers.PrimaryKeyRelatedField(queryset=Player.objects.all(), many=True, allow_empty=True)

    class Meta:
        model = Team
        fields = ["__all__"]
