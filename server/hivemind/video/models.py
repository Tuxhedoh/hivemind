from datetime import datetime

from django.db import models

from ..model import BaseModel


class VideoClip(BaseModel):
    cabinet = models.ForeignKey("game.Cabinet", on_delete=models.CASCADE)
    game = models.ForeignKey("game.Game", null=True, on_delete=models.SET_NULL)
    created_by = models.ForeignKey("user.User", null=True, on_delete=models.SET_NULL)
    twitch_clip_id = models.CharField(max_length=100)
    created_date = models.DateTimeField(default=datetime.utcnow)
