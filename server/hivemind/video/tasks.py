from celery import shared_task

import hivemind.twitch

from .models import VideoClip


@shared_task
def create_clip(clip_id):
    clip = VideoClip.objects.get(id=clip_id)

    clip.twitch_clip_id = hivemind.twitch.create_clip(clip.cabinet.twitch_channel_name)
    clip.save()
