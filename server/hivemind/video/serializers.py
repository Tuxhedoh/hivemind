from rest_framework import serializers

from ..game.serializers import CabinetSerializer
from .models import VideoClip


class VideoClipSerializer(serializers.ModelSerializer):
    class Meta:
        model = VideoClip
        fields = ["id", "cabinet", "game", "created_by", "twitch_clip_id", "created_date"]
        read_only_fields = ["game", "created_date", "twitch_clip_id"]
