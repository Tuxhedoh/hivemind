from rest_framework.exceptions import PermissionDenied
from rest_framework.response import Response

from ..game.models import Cabinet
from ..model import BaseViewSet
from . import permissions, serializers
from .models import VideoClip
from .tasks import create_clip


class VideoClipViewSet(BaseViewSet):
    queryset = VideoClip.objects.all().order_by("-created_date")
    serializer_class = serializers.VideoClipSerializer
    filterset_fields = ["cabinet", "created_by"]
    permission_classes = [permissions.VideoClipPermission]

    def create(self, request):
        try:
            cabinet = Cabinet.objects.get(id=request.data["cabinet"])
        except Cabinet.DoesNotExist:
            raise PermissionDenied()

        if request.data.get("token") is None:
            if request.user.is_authenticated:
                request.data["created_by"] = request.user.id
            else:
                raise PermissionDenied()

        if cabinet.twitch_channel_name is None:
            return Response({"success": False, "error": "No channel configured for this cabinet."})

        response = super().create(request)
        create_clip.delay(response.data["id"])

        return response
