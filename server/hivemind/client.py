import redis
import yaml
from django.conf import settings

redis_client = redis.StrictRedis(host=settings.REDIS_HOST, password=settings.REDIS_PASSWORD)

def redis_set(key, value, expire=None):
    redis_client.set(key, yaml.dump(value), ex=expire)

def redis_get(key):
    value = redis_client.get(key)
    if value:
        return yaml.load(value, Loader=yaml.FullLoader)

    return None
