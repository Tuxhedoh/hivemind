from celery import shared_task

from hivemind.game.models import Game
from hivemind.stats.models import SignInLog

from .models import User, UserGame


@shared_task
def reprocess_user_stats(user_id):
    user = User.objects.get(id=user_id)

    user.userstat_set.all().delete()
    user.userpositionstat_set.all().delete()
    user.playsession_set.all().delete()

    user.collect_stats()


@shared_task
def reprocess_user_games(user_id):
    user = User.objects.get(id=user_id)

    user.userstat_set.all().delete()
    user.userpositionstat_set.all().delete()
    user.playsession_set.all().delete()
    user.usergame_set.all().delete()

    for sign_in in user.signinlog_set.filter(action=SignInLog.Action.SIGN_IN).order_by("timestamp"):
        sign_out = SignInLog.objects.filter(
            cabinet=sign_in.cabinet,
            timestamp__gt=sign_in.timestamp,
            player_id=sign_in.player_id,
        ).order_by("timestamp").first()

        games = Game.objects.filter(
            cabinet=sign_in.cabinet,
            end_time__isnull=False,
            end_time__gte=sign_in.timestamp,
            end_time__lte=sign_out.timestamp,
        )

        for game in games.order_by("end_time"):
            user_game, created = UserGame.objects.get_or_create(
                game=game,
                player_id=sign_out.player_id,
                defaults={"user": user},
            )

            if not created:
                user_game.user = user
                user_game.save()

    reprocess_user_stats(user_id)
