import logging

import cachecontrol
import google.auth.transport.requests
import requests
import social_core.pipeline.social_auth
from django.contrib.auth import login
from django.db import IntegrityError, transaction
from django.shortcuts import get_object_or_404, render
from google.oauth2 import id_token
from requests.exceptions import HTTPError
from rest_framework import status, viewsets
from rest_framework.decorators import action, api_view, permission_classes
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken
from social_django.utils import psa

from ..constants import HTTPMethod
from ..game.models import Cabinet, Scene
from ..game.permissions import RequireCabinetToken
from ..game.serializers import SceneSerializer
from ..model import BaseViewSet
from ..permissions import SiteAdminPermission
from ..stats.models import NFC, SignInLog
from .models import Message, Permission, PermissionType, SceneRequest, User
from .permissions import (MessagePermission, PermissionPermission,
                          SceneRequestPermission, UserPermission)
from .serializers import (MessageSerializer, PermissionSerializer,
                          SceneRequestSerializer, SocialSerializer,
                          UserPrivateSerializer, UserSerializer)

logger = logging.getLogger(__name__)

session = requests.session()
cached_session = cachecontrol.CacheControl(session)

@api_view(["POST"])
@permission_classes([AllowAny])
@psa()
def exchange_token(request, backend=None):
    cert_req = google.auth.transport.requests.Request(session=cached_session)
    token = id_token.verify_oauth2_token(request.data["credential"], cert_req)

    if not token:
        return Response({
            "errors": {
                "token": "Invalid token",
            }
        }, status=status.HTTP_400_BAD_REQUEST)

    social = request.backend.strategy.storage.user.get_social_auth(request.backend.name, token["email"])
    if social is None:
        user = User.objects.create(email=token["email"], username=token["email"])
        social = request.backend.strategy.storage.user.create_social_auth(
            user, token["email"], request.backend.name)

    login(request, social.user, backend=backend)
    token = RefreshToken.for_user(social.user)

    return Response({
        "success": True,
        "user_id": social.user.id,
        "access_token": str(token.access_token),
        "refresh_token": str(token),
    })

@api_view(["GET"])
@permission_classes([AllowAny])
def current_user(request):
    if not request.user.is_authenticated:
        return Response({})

    serializer = UserPrivateSerializer(request.user)
    return Response(serializer.data)


class UserViewSet(BaseViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [UserPermission]

    @action(detail=True, methods=[HTTPMethod.GET], permission_classes=[SiteAdminPermission])
    def email(self, request, pk=None):
        user = get_object_or_404(User, id=pk)
        return Response({ "email": user.email })


class PermissionViewSet(BaseViewSet):
    queryset = Permission.objects.all().order_by("id")
    serializer_class = PermissionSerializer
    permission_classes = [PermissionPermission]
    filterset_fields = ["scene_id", "user_id", "permission"]

    def get_queryset(self):
        user = self.request.user
        admin_permissions = Permission.objects.filter(user=user, permission=PermissionType.ADMIN)
        scene_ids = [p.scene_id for p in admin_permissions]
        return Permission.objects.filter(scene_id__in=scene_ids).order_by("id")

    def create(self, request):
        try:
            scene = Scene.objects.get(id=request.data["scene"])
        except Scene.DoesNotExist:
            raise PermissionDenied()

        if not (request.user.is_site_admin or request.user.is_admin_of(scene)):
            raise PermissionDenied()

        return super().create(request)


class MessageViewSet(BaseViewSet):
    queryset = Message.objects.all().order_by("-timestamp")
    serializer_class = MessageSerializer
    permission_classes = [MessagePermission]

    def get_queryset(self):
        user = self.request.user
        return Message.objects.filter(user=user).order_by("-timestamp")

    @action(detail=False, methods=[HTTPMethod.POST], permission_classes=[SiteAdminPermission])
    def send(self, request):
        super().create(request)


class SceneRequestViewSet(BaseViewSet):
    queryset = SceneRequest.objects.all().order_by("-submit_date")
    serializer_class = SceneRequestSerializer
    permission_classes = [SceneRequestPermission]

    def create(self, request):
        response = super().create(request)

        message_text = 'A new scene has been registered: "{}"'.format(request.data.get("display_name"))
        for admin_user in User.objects.filter(is_site_admin=True):
            Message.objects.create(user=admin_user, message_text=message_text, url='/admin/scene-requests')

        return response

    @action(detail=True, methods=[HTTPMethod.POST])
    @transaction.atomic
    def approve(self, request, pk=None):
        scene_request = get_object_or_404(SceneRequest, id=pk)

        # Create scene from postdata in case site admin changed anything
        serializer = SceneSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        scene = serializer.instance

        # Give admin to user
        Permission.objects.create(
            user=scene_request.user,
            scene=scene,
            permission=PermissionType.ADMIN,
        )

        # Send message
        Message.objects.create(
            user=scene_request.user,
            message_text="The {} scene has been created.".format(scene.display_name),
            url="/scene/{}".format(scene.name),
        )

        # Mark request completed
        scene_request.completed = True
        scene_request.save()

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
