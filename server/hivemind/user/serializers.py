from drf_extra_fields.fields import Base64ImageField
from rest_framework import serializers

from hivemind.game.serializers import SceneSerializer

from ..game.models import Scene
from .models import Message, Permission, PlaySession, SceneRequest, User


class PermissionSerializer(serializers.ModelSerializer):
    user = serializers.CharField(source="user.email")
    scene = serializers.PrimaryKeyRelatedField(queryset=Scene.objects.all())
    permission = serializers.CharField()

    def validate_user(self, email):
        try:
            return User.objects.get(email=email)
        except User.DoesNotExist:
            raise serializers.ValidationError("User not found")

    def create(self, validated_data):
        user = User.objects.get(email=validated_data["user"]["email"])
        return Permission.objects.create(
            user=user,
            scene=validated_data["scene"],
            permission=validated_data["permission"],
        )

    class Meta:
        model = Permission
        fields = ["id", "user", "scene", "permission"]


class UserSerializer(serializers.ModelSerializer):
    stats = serializers.ListField(source="get_stats", read_only=True)
    stats_by_position = serializers.ListField(source="get_stats_by_position", read_only=True)
    last_session = PlaySession.serializer()(source="get_last_session", read_only=True)
    last_session_stats = serializers.ListField(source="get_last_session_stats", read_only=True)
    user_scenes = serializers.ListField(source="get_user_scenes", read_only=True)
    image = Base64ImageField(required=False)

    class Meta:
        model = User
        fields = ["id", "name", "stats", "stats_by_position", "last_session", "last_session_stats",
                  "user_scenes", "is_profile_public", "scene", "image", "pronouns"]


class UserPrivateSerializer(serializers.ModelSerializer):
    permissions = PermissionSerializer(read_only=True, many=True)

    class Meta:
        model = User
        fields = ["id", "name", "is_profile_public", "permissions", "last_login", "is_site_admin",
                  "scene", "image", "pronouns"]


class SocialSerializer(serializers.Serializer):
    access_token = serializers.CharField(allow_blank=False, trim_whitespace=True)


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ["id", "timestamp", "message_text", "is_read", "url"]
        read_only_fields = ["id", "timestamp", "message_text", "url"]


class SceneRequestSerializer(serializers.ModelSerializer):
    user = UserSerializer(default=serializers.CurrentUserDefault())

    class Meta:
        model = SceneRequest
        fields = "__all__"
        read_only_fields = ["user"]
