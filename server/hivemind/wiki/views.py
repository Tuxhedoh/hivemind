import logging

from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from ..constants import HTTPMethod
from ..model import BaseViewSet
from . import serializers
from .models import Page, Revision

logger = logging.getLogger(__name__)

class PageViewSet(BaseViewSet):
    queryset = Page.objects.all().order_by("slug")
    serializer_class = serializers.PageSerializer
    filterset_fields = ["slug"]


class RevisionViewSet(BaseViewSet):
    queryset = Revision.objects.all().order_by("publish_date")
    serializer_class = serializers.RevisionSerializer
    filterset_fields = ["page_id"]


@api_view(["POST"])
@permission_classes([IsAuthenticated])
def publish(request):
    page, is_new = Page.objects.get_or_create(slug=request.data["slug"])
    if is_new:
        page.save()

    serializer_data = {
        "page_id": page.id,
        "published_by": request.user.id,
        **{k: request.data[k] for k in ["title", "page_text"]},
    }

    serializer = serializers.RevisionSerializer(data=serializer_data)
    serializer.is_valid(raise_exception=True)
    serializer.save()

    return Response(serializer.data, status=status.HTTP_201_CREATED)
