from rest_framework import serializers

from ..user.serializers import UserSerializer
from .models import Page, Revision


class RevisionSerializer(serializers.ModelSerializer):
    page_id = serializers.IntegerField()

    class Meta:
        model = Revision
        fields = ["page_id", "title", "page_text", "published_by"]


class PageSerializer(serializers.ModelSerializer):
    current = RevisionSerializer(source="current_revision", read_only=True)

    class Meta:
        model = Page
        fields = ["id", "slug", "current"]
