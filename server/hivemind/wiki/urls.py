from django.urls import include, path
from rest_framework import routers, viewsets

from . import views

router = routers.DefaultRouter()
router.register(r"page", views.PageViewSet)
router.register(r"revision", views.RevisionViewSet)

urlpatterns = [
    path("publish/", views.publish),
    path("", include(router.urls)),
]
