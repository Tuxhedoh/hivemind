from django.urls import include, path
from rest_framework import routers, viewsets

from .views import UserAchievementViewSet

router = routers.DefaultRouter()
router.register(r"user-achievement", UserAchievementViewSet)

urlpatterns = [
    path("", include(router.urls)),
]
