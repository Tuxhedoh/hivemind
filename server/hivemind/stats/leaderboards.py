from datetime import datetime, timedelta

from django.db import models

from ..client import redis_get, redis_set
from ..constants import AggregateStatType, StatType
from ..user.models import PlaySessionStat, User


def get_leaderboard(stat_type):
    return redis_get("leaderboards.{}".format(stat_type))

def refresh_leaderboards(count=5, days=7):
    stats = PlaySessionStat.objects.filter(session__start_time__gt=datetime.now() - timedelta(days=days)) \
        .values(
            "session__user",
            "stat_type",
        ).annotate(models.Sum("value"))

    for stat_type, stat_title in AggregateStatType.choices + StatType.choices:
        leaders = []

        for row in stats.filter(stat_type=stat_type).order_by("-value__sum")[:count]:
            user = User.objects.get(id=row["session__user"])
            leaders.append({
                "user": user.id,
                "name": user.name,
                "scene": user.scene,
                "image": user.image.url if user.image else None,
                "value": row["value__sum"],
            })

        redis_set("leaderboards.{}".format(stat_type), leaders)
