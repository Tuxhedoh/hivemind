import json
from datetime import date, datetime, timedelta, timezone

from django.db import models
from django.db.models import Count, ExpressionWrapper, F
from django.db.models.functions import Trunc, TruncMonth
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, render
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from hivemind.game.models import Game, GameMap, Scene

from ..client import redis_client, redis_get, redis_set
from ..constants import AggregateStatType, HTTPMethod, StatType
from ..game.models import Cabinet, Game, Scene
from ..game.permissions import RequireCabinetToken
from ..model import BaseViewSet, ReadOnly
from ..permissions import SiteAdminOnly
from ..tournament.models import TournamentPlayer
from ..user.models import User, UserGame
from .leaderboards import get_leaderboard
from .models import NFC, SignInLog, UserRecap
from .permissions import NFCPermission
from .serializers import NFCSerializer
from .tasks import generate_recap


class StatsViewSet(viewsets.ViewSet):
    permission_classes = [ReadOnly]

    @action(detail=False, url_path="games-by-week")
    def games_by_week(self, request):
        WEEKS = 52

        min_date = date.today() - timedelta(weeks=WEEKS, days=date.today().weekday())
        query = Game.objects.filter(end_time__isnull=False, end_time__gte=min_date)
        query = query.annotate(week=Trunc("start_time", "week"))
        query = query.values("week", "cabinet__scene").annotate(Count("id"))

        games_by_week = {}
        scene_ids = set()
        for row in query:
            scene_id = row["cabinet__scene"]
            scene_ids.add(scene_id)
            week_start = row["week"]
            games_by_week[week_start.date()] = games_by_week.get(week_start.date(), {})
            games_by_week[week_start.date()][scene_id] = row["id__count"]

        scenes = Scene.objects.filter(id__in=scene_ids)

        labels = []
        datasets = {}
        for scene in scenes:
            datasets[scene.id] = {
                "stack": "all",
                "label": scene.display_name,
                "borderColor": "black",
                "backgroundColor": scene.background_color,
                "data": [],
            }

        week_start = date.today() - timedelta(days=date.today().weekday())
        for weeks_back in range(WEEKS):
            week = week_start - timedelta(weeks=(WEEKS-weeks_back-1))
            if week < min_date:
                continue

            labels.append(week.strftime("%Y-%m-%d"))
            for scene in scenes:
                datasets[scene.id]["data"].append(games_by_week.get(week, {}).get(scene.id, 0))

        data = {
            "labels": labels,
            "datasets": list(datasets.values()),
        }

        return JsonResponse(data)

    @action(detail=False, url_path="win-conditions")
    def win_conditions(self, request):
        map_names = [GameMap.DAY, GameMap.NIGHT, GameMap.DUSK, GameMap.TWILIGHT]
        query = Game.objects.filter(end_time__isnull=False, end_time__gt=datetime.now() - timedelta(weeks=8))
        query = query.filter(player_count__gte=8, map_name__in=map_names)
        query = query.values("map_name", "win_condition").annotate(Count("id"))

        win_cond_by_map = {i: {} for i in map_names}
        for row in query:
            win_cond_by_map[row["map_name"]][row["win_condition"]] = row["id__count"]

        data = {}
        for map_name in map_names:
            game_map = GameMap.objects.get(name=map_name)
            data[map_name] = {
                "map_name": game_map.display_name,
                "labels": ["Military", "Economic", "Snail"],
                "datasets": [{
                    "data": [win_cond_by_map[map_name].get(i, 0) for i in ["military", "economic", "snail"]],
                    "backgroundColor": ["#b00", "#0b0", "#00b"],
                    "label": game_map.display_name,
                }],
            }

        return JsonResponse(data)

    @action(detail=False)
    def leaderboards(self, request):
        leaderboards = []
        leaderboard_types = [
            (AggregateStatType.GAMES, "Most Games Played"),
            (StatType.BERRIES, "Most Berries Deposited"),
            (StatType.BERRIES_KICKED, "Most Berries Kicked In"),
            (StatType.BUMP_ASSISTS, "Most Bump Assists"),
            (StatType.SNAIL_DISTANCE, "Most Snail Distance"),
            (StatType.KILLS, "Most Total Kills"),
        ]

        for stat_type, title in leaderboard_types:
            leaderboards.append({
                "title": title,
                "leaders": get_leaderboard(stat_type),
            })

        return JsonResponse({"results": leaderboards})


class NFCViewSet(BaseViewSet):
    queryset = NFC.objects.all()
    serializer_class = NFCSerializer
    permission_classes = [NFCPermission]

    def get_queryset(self):
        user = self.request.user
        return NFC.objects.filter(user=user).order_by("date_registered")

    @action(detail=False, methods=[HTTPMethod.POST])
    def relay(self, request, pk=None, permission_classes=[SiteAdminOnly]):
        data = {
            "type": "nfc_relay",
            **{k: v for k, v in request.data.items() if k in ["reader_id", "card_id", "user_id"]},
        }

        redis_client.publish("signin", json.dumps(data))
        return Response({"success": True})

    @action(detail=False, methods=[HTTPMethod.POST], permission_classes=[IsAuthenticated])
    def register(self, request, pk=None):
        data = {
            "type": "nfc_register",
            "reader_id": request.data.get("reader"),
        }

        if "cabinet" in request.data:
            cabinet = get_object_or_404(Cabinet, id=request.data["cabinet"])
            if not cabinet.allow_nfc_signin:
                return Response({"success": False, "error": "This cabinet is not configured to allow NFC sign-in."})

            data["scene_name"] = cabinet.scene.name
            data["cabinet_name"] = cabinet.name

        if "tournament_player" in request.data:
            player = get_object_or_404(TournamentPlayer, id=request.data["tournament_player"])
            if not request.user.is_admin_of(player.team.tournament.scene):
                return Response({"success": False, "error": "You do not have permission for this."},
                                status=status.HTTP_403_FORBIDDEN)

            data["tournament_player"] = player.id

        else:
            data["user"] = request.user.id

        redis_client.publish("signin", json.dumps(data))
        return Response({"success": True})


class SignInViewSet(viewsets.ViewSet):
    queryset = SignInLog.objects.filter(id__isnull=True)
    serializer_class = SignInLog.serializer()

    @action(detail=False, methods=[HTTPMethod.POST], url_path="qr", permission_classes=[IsAuthenticated])
    def qr(self, request, pk=None):
        try:
            cabinet = Cabinet.objects.get(id=request.data["cabinet_id"])
        except Cabinet.DoesNotExist:
            return Response({"success": False, "error": "Invalid cabinet"}, status.HTTP_400_BAD_REQUEST)

        if not cabinet.allow_qr_signin:
            return Response({"success": False, "error": "Cabinet does not allow QR sign-in"},
                            status.HTTP_400_BAD_REQUEST)

        if request.data.get("action") == SignInLog.Action.SIGN_IN:
            for sign_in in SignInLog.objects.filter(user=request.user, action=SignInLog.Action.SIGN_IN,
                                                    is_current=True):
                sign_in.sign_out(method=SignInLog.SignInMethod.QR)

            sign_in = SignInLog(
                user=request.user,
                method=SignInLog.SignInMethod.QR,
                action=request.data.get("action"),
                cabinet=cabinet,
                player_id=request.data.get("player"),
            )

            sign_in.set_current()
            sign_in.save()
            sign_in.publish()

            return Response({"success": True, "id": sign_in.id})

        if request.data.get("action") == SignInLog.Action.SIGN_OUT:
            for sign_in in SignInLog.objects.filter(user=request.user, action=SignInLog.Action.SIGN_IN,
                                                    is_current=True):
                sign_in.sign_out(method=SignInLog.SignInMethod.QR)

            return Response({"success": True})

        return Response({"success": False, "error": "Unknown action."}, status.HTTP_400_BAD_REQUEST)


    @action(detail=False, methods=[HTTPMethod.POST], url_path="nfc", permission_classes=[RequireCabinetToken])
    def nfc(self, request, pk=None):
        try:
            cabinet = Cabinet.by_name(request.data["scene_name"], request.data["cabinet_name"])
        except Cabinet.DoesNotExist:
            return Response({"success": False, "error": "Invalid cabinet"})

        if not cabinet.allow_nfc_signin:
            return Response({"success": False, "error": "Cabinet does not allow NFC sign-in"})

        if request.data.get("action") == SignInLog.Action.NFC_REGISTER_TAPPED:
            nfc, created = NFC.objects.get_or_create(card_id=request.data.get("card"))

            if "user" in request.data:
                if nfc.user and not created:
                    nfc.publish_error(f"Card {nfc.card_id} is already registered.",
                                      user_id=request.data["user"])
                    return Response({"error": f"Card {nfc.card_id} is already registered."},
                                    status=status.HTTP_400_BAD_REQUEST)

                nfc.user = User.objects.get(id=request.data["user"])

                if nfc.tournament_player and not created:
                    nfc.tournament_player.user = nfc.user
                    nfc.tournament_player.save()

                    for player_game in nfc.tournament_player.tournamentplayergame_set.all():
                        try:
                            UserGame.objects.create(
                                user=nfc.user,
                                game=player_game.game,
                                player_id=player_game.player_id,
                            )
                        except UserGame.IntegrityError:
                            pass

                        nfc.user.collect_stats()

            if "tournament_player" in request.data:
                if not created:
                    nfc.publish_error(f"Card {nfc.card_id} is already registered.",
                                      tournament_player_id=request.data["tournament_player"])
                    return Response({"error": f"Card {nfc.card_id} is already registered."},
                                    status=status.HTTP_400_BAD_REQUEST)

                nfc.tournament_player = TournamentPlayer.objects.get(id=request.data["tournament_player"])

            if nfc.user is None and nfc.tournament_player is None:
                return Response({"error": "User not found"}, status=status.HTTP_400_BAD_REQUEST)

            nfc.save()
            nfc.publish_new_card()

            return Response({"success": True, "id": nfc.id})

        if request.data.get("action") == SignInLog.Action.SIGN_IN:
            nfc = get_object_or_404(NFC, card_id=request.data.get("card"))

            nfc.sign_out_all()

            if not cabinet.allow_tournament_player and nfc.user is None:
                return Response({"error": f"Card {nfc.card_id} is not registered."},
                                status=status.HTTP_400_BAD_REQUEST)

            sign_in = SignInLog(
                user=nfc.user,
                tournament_player=nfc.tournament_player,
                method=SignInLog.SignInMethod.NFC,
                action=request.data.get("action"),
                cabinet=cabinet,
                player_id=request.data.get("player"),
            )

            sign_in.set_current()
            sign_in.save()
            sign_in.publish()

            return Response({"success": True, "id": sign_in.id})

        if request.data.get("action") == SignInLog.Action.SIGN_OUT:
            try:
                sign_in = SignInLog.objects.get(cabinet=cabinet, player_id=request.data.get("player"),
                                                is_current=True)
                sign_in.sign_out()
            except SignInLog.DoesNotExist:
                pass

            return Response({"success": True})

        return Response({"success": False, "error": "Unknown action."})


class RecapViewSet(viewsets.ViewSet):
    permission_classes = [ReadOnly]

    def retrieve(self, request, pk=None):
        year = int(pk)
        if not year:
            return Response({"success": False, "error": "invalid year"})

        if year >= datetime.utcnow().year:
            return Response({"success": False, "error": "flux capacitor not installed"})

        redis_key = f"recap.{year}"
        data = redis_get(redis_key)

        if data is not None:
            return Response({"success": True, "data": data})

        year_start = datetime(year, 1, 1, tzinfo=timezone.utc)
        year_end = datetime(year+1, 1, 1, tzinfo=timezone.utc)

        data = {
            "year": year,
        }

        games = Game.objects.filter(end_time__isnull=False, start_time__gte=year_start, start_time__lt=year_end)
        user_games = UserGame.objects.filter(game__start_time__gte=year_start, game__start_time__lt=year_end)
        nfcs = NFC.objects.filter(date_registered__gte=year_start, date_registered__lt=year_end)
        all_maps = {i.name: i for i in GameMap.objects.all()}

        top_month = games.annotate(month=TruncMonth("start_time")) \
                         .values("month") \
                         .annotate(count=models.Count("id")) \
                         .order_by("-count") \
                         .first()
        top_map = games.values("map_name").annotate(count=models.Count("id")).order_by("-count").first()

        data["games"] = {
            "total": games.count(),
            "game_time_days": games.annotate(game_time=models.Sum(ExpressionWrapper((F("end_time") - F("start_time")), output_field=models.DateTimeField()))).aggregate(models.Sum("game_time"))["game_time__sum"].days,
            "scenes": games.values("cabinet__scene_id").annotate(total=models.Count("id")).count(),
            "cabinets": games.values("cabinet_id").annotate(total=models.Count("id")).count(),
            "top_month": {
                "month": top_month["month"].strftime("%B"),
                "count": top_month["count"],
            },
            "top_map": {
                "name": GameMap.objects.get(name=top_map["map_name"]).display_name,
                "count": top_map["count"],
            },
            "top_scenes": [
                {
                    "scene": i["cabinet__scene__display_name"],
                    "background_color": i["cabinet__scene__background_color"],
                    "games": i["total"],
                }
                for i in games.values("cabinet__scene__display_name", "cabinet__scene__background_color").annotate(total=models.Count("id")).order_by("-total")[:5]
            ],
        }

        position_counts = {i["player_id"]: i["total"] for i in user_games.values("player_id").annotate(total=models.Count("id"))}
        total_user_games = sum(position_counts.values())
        queen_pct = (position_counts[1] + position_counts[2]) / total_user_games
        data["logged_in"] = {
            "games": user_games.values("game_id").annotate(totals=models.Count("id")).count(),
            "users": user_games.values("user_id").annotate(totals=models.Count("id")).count(),
            "scenes": user_games.values("game__cabinet__scene_id").annotate(totals=models.Count("id")).count(),
            "queen_pct": queen_pct,
            "position_counts": position_counts,
            "nfc_tags": nfcs.count(),
        }

        tournament_game_counts = [
            {
                "name": i["tournament_match__bracket__tournament__name"],
                "scene": i["cabinet__scene__display_name"],
                "background_color": i["cabinet__scene__background_color"],
                "games": i["count"],
            }
            for i in games.filter(tournament_match__isnull=False).values(
                    "tournament_match__bracket__tournament_id",
                    "tournament_match__bracket__tournament__name",
                    "cabinet__scene__display_name",
                    "cabinet__scene__background_color",
            ).annotate(count=models.Count("id")).order_by("-count")
        ]

        data["tournaments"] = {
            "tournaments": len(tournament_game_counts),
            "games": sum([i["games"] for i in tournament_game_counts]),
            "sign_in_pct": user_games.filter(game__tournament_match__isnull=False).count() / games.filter(tournament_match__isnull=False).values("player_count").aggregate(models.Sum("player_count"))["player_count__sum"],
            "scenes": games.filter(tournament_match__isnull=False).values("cabinet__scene_id").annotate(totals=models.Count("id")).count(),
            "top_tournaments": tournament_game_counts[:5],
        }

        data["by_map"] = [
            {
                "id": all_maps[i["map_name"]].id,
                "name": all_maps[i["map_name"]].display_name or i["map_name"],
                "beta_of": all_maps[i["map_name"]].beta_of_id,
                "is_bonus": all_maps[i["map_name"]].is_bonus,
                "games": i["games"],
            }
            for i in games.values("map_name").annotate(games=models.Count("id")).order_by("-games")
        ]

        data["new_scenes"] = [
            {
                "id": i.id,
                "name": i.display_name,
                "background_color": i.background_color,
                "first_game": i.first_game.isoformat(),
            }
            for i in Scene.objects.annotate(first_game=models.Min("cabinet__game__start_time")).filter(first_game__gte=year_start, first_game__lt=year_end).order_by("first_game")
        ]

        redis_set(redis_key, data, expire=24*60*60)

        return Response({"success": True, "data": data})



class UserRecapViewSet(BaseViewSet):
    queryset = UserRecap.objects.all()
    serializer_class = UserRecap.serializer()
    permission_classes = [ReadOnly]
    filterset_fields = ["user_id", "year"]

    @action(detail=False, methods=[HTTPMethod.POST], permission_classes=[IsAuthenticated])
    def generate(self, request):
        if not request.user.is_authenticated:
            return Response({"success": False, "error": "not logged in"})
        try:
            year = int(request.data.get("year"))
        except ValueError:
            return Response({"success": False, "error": "invalid year"})

        if year >= datetime.utcnow().year:
            return Response({"success": False, "error": "flux capacitor not installed"})

        UserRecap.objects.get_or_create(user=request.user, year=year)
        generate_recap.delay(request.user.id, year)
        return Response({"success": True})
