from rest_framework.permissions import SAFE_METHODS, BasePermission

from ..constants import HTTPMethod


class NFCPermission(BasePermission):
    def has_permission(self, request, view):
        return request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        if request.user.is_authenticated and obj.user == request.user:
            return True
