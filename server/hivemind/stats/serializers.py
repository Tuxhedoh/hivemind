from rest_framework import serializers

from ..user.serializers import UserSerializer
from .models import NFC


class NFCSerializer(serializers.ModelSerializer):
    user = UserSerializer(default=serializers.CurrentUserDefault())

    class Meta:
        model = NFC
        fields = "__all__"
        read_only_fields = ["user"]
