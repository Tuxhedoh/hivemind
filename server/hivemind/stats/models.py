import json
from datetime import datetime, timedelta, timezone

from django.db import models
from django.db.models import Q

from ..client import redis_client
from ..constants import (AggregateStatType, CabinetPosition, CabinetTeam,
                         StatType)
from ..game.models import Cabinet, Game
from ..model import BaseModel
from ..tournament.models import TournamentPlayer, TournamentPlayerGame
from ..user.models import User, UserGame


class SignInLog(BaseModel):
    class SignInMethod(models.TextChoices):
        QR = "qr", "QR Code"
        NFC = "nfc", "NFC Reader"
        AUTO = "auto", "Auto Sign-Out"

    class Action(models.TextChoices):
        SIGN_IN = "sign_in", "Signed In"
        SIGN_OUT = "sign_out", "Signed Out"
        NFC_REGISTER = "nfc_register", "NFC Card Registration (Requested)"
        NFC_REGISTER_TAPPED = "nfc_register_tapped", "NFC Card Registration (Card Tapped)"
        NFC_REGISTER_SUCCESS = "nfc_register_success", "NFC Card Successfully Registered"
        NFC_REGISTER_ERROR = "nfc_register_error", "NFC Card Registration Error"

    user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    tournament_player = models.ForeignKey(TournamentPlayer, null=True, on_delete=models.SET_NULL)
    timestamp = models.DateTimeField(default=datetime.utcnow)
    method = models.CharField(max_length=30, choices=SignInMethod.choices)
    action = models.CharField(max_length=30, choices=Action.choices)
    cabinet = models.ForeignKey(Cabinet, on_delete=models.CASCADE)
    player_id = models.IntegerField(choices=CabinetPosition.choices)
    is_current = models.BooleanField(default=False)

    class Meta:
        indexes = [
            models.Index(name="current_signin_idx", fields=["cabinet_id", "player_id"],
                         condition=models.Q(is_current=True)),
        ]

    def set_current(self):
        for signin in self.__class__.objects.filter(is_current=True, cabinet_id=self.cabinet_id,
                                                    player_id=self.player_id):
            signin.is_current = False
            signin.save()

        self.is_current = True

    @classmethod
    def get_current_user(cls, cabinet_id, player_id):
        last_signin = cls.objects.filter(cabinet_id=cabinet_id, player_id=player_id, is_current=True).first()
        if last_signin is not None and last_signin.action == cls.Action.SIGN_IN:
            return last_signin.user

    @classmethod
    def get_current_users(cls, cabinet_id):
        users = {}
        for sign_in in cls.objects.filter(cabinet_id=cabinet_id, is_current=True, action=cls.Action.SIGN_IN):
            users[sign_in.player_id] = sign_in.user

        return users

    def sign_out(self, method=None):
        if method is None:
            method = self.SignInMethod.AUTO

        sign_out = SignInLog(user=self.user, tournament_player=self.tournament_player, method=method,
                             action=self.Action.SIGN_OUT, cabinet=self.cabinet, player_id=self.player_id)
        sign_out.set_current()
        sign_out.save()
        sign_out.publish()

    def save_game(self, game):
        if self.user:
            UserGame.objects.create(user=self.user, game=game, player_id=self.player_id)

            tournament_player = self.user.tournamentplayer_set.filter(team__tournament__is_active=True).first()
            if tournament_player is None and game.tournament_match is not None:
                tournament_id = game.tournament_match.bracket.tournament_id
                tournament_player = self.user.tournamentplayer_set.filter(tournament_id=tournament_id).first()

            if tournament_player:
                TournamentPlayerGame.objects.update_or_create(
                    tournament_player=tournament_player,
                    game=game,
                    defaults={ "player_id": self.player_id },
                )

        # to be removed when tournament-specific cards go away
        if self.tournament_player:
            TournamentPlayerGame.objects.update_or_create(
                tournament_player=self.tournament_player,
                game=game,
                defaults={ "player_id": self.player_id },
            )

    def get_publish_data(self):
        data = {
            "scene_name": self.cabinet.scene.name,
            "cabinet_id": self.cabinet.id,
            "cabinet_name": self.cabinet.name,
            "player_id": self.player_id,
            "position_name": self.get_player_id_display(),
            "action": self.action,
        }

        if self.user:
            data.update({
                "user_id": self.user.id,
                "user_name": self.user.name,
            })

        if self.cabinet.allow_tournament_player and self.tournament_player:
            data.update({
                "tournament_player_id": self.tournament_player.id,
                "tournament_player_name": self.tournament_player.name,
            })

            if not data.get("user_name"):
                data["user_name"] = self.tournament_player.name

        return data

    def publish(self):
        redis_client.publish("signin", json.dumps(self.get_publish_data()))


class NFC(BaseModel):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    tournament_player = models.ForeignKey("tournament.TournamentPlayer", on_delete=models.SET_NULL, null=True)
    card_id = models.CharField(max_length=30, unique=True)
    date_registered = models.DateTimeField(default=datetime.utcnow)

    def sign_out_all(self):
        for sign_in in SignInLog.objects.filter(action=SignInLog.Action.SIGN_IN, is_current=True) \
                                        .filter(Q(user=self.user, user__isnull=False) |
                                                Q(
                                                    tournament_player__isnull=False,
                                                    tournament_player=self.tournament_player,
                                                )):
            sign_in.sign_out(method=SignInLog.SignInMethod.NFC)

    def publish_new_card(self):
        data = {
            "type": SignInLog.Action.NFC_REGISTER_SUCCESS,
            "card_id": self.card_id,
            "user_id": self.user_id,
            "tournament_player_id": self.tournament_player_id,
        }

        if self.user:
            data["user_id"] = self.user.id

        if self.tournament_player:
            data["tournament_player_id"] = self.tournament_player.id
            data["tournament_player_name"] = self.tournament_player.name
            data["tournament_team_name"] = self.tournament_player.team.name

        redis_client.publish("signin", json.dumps(data))

    def publish_error(self, message, **kwargs):
        redis_client.publish("signin", json.dumps({
            "type": SignInLog.Action.NFC_REGISTER_ERROR,
            "card_id": self.card_id,
            "error": message,
            **kwargs,
        }))


class UserRecap(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    year = models.IntegerField()
    is_public = models.BooleanField(default=False)
    data = models.JSONField(null=True)

    class Meta:
        unique_together = ("user", "year")
