from datetime import datetime, timedelta, timezone

from celery import shared_task
from django.db import models
from django.db.models import F
from django.db.models.functions import TruncMonth

from ..constants import (AggregateStatType, CabinetPosition, CabinetTeam,
                         StatType)
from ..game.models import Game
from ..user.models import User, UserGame
from .models import SignInLog, UserRecap


@shared_task
def generate_recap(user_id, year):
    user = User.objects.get(id=user_id)
    all_stat_types = list(AggregateStatType) + list(StatType)
    year_start = datetime(year=year, month=1, day=1, tzinfo=timezone.utc)
    year_end = datetime(year=year+1, month=1, day=1, tzinfo=timezone.utc)
    all_user_games = user.usergame_set.filter(
        game__start_time__gte=year_start,
        game__start_time__lt=year_end,
    )

    data = {
        "year": year,
        "stat_totals": {i: 0 for i in all_stat_types},
        "by_position": {i.value: {j: 0 for j in all_stat_types} for i in CabinetPosition},
    }

    data["played_against"] = {}
    data["played_with"] = {}
    data["best_game_mil"] = {}

    for user_game in all_user_games:
        game_stats = {i: 0 for i in all_stat_types}
        game_stats[AggregateStatType.GAMES] += 1

        if CabinetPosition(user_game.player_id).team == user_game.game.winning_team:
            game_stats[AggregateStatType.WINS] += 1
        else:
            game_stats[AggregateStatType.LOSSES] += 1

        game_stats[AggregateStatType.TIME_PLAYED] += int((user_game.game.end_time - user_game.game.start_time).total_seconds() * 1000)

        for game_stat in user_game.game.gamestat_set.filter(player_id=user_game.player_id):
            game_stats[game_stat.stat_type] += game_stat.value

        for stat_type, value in game_stats.items():
            data["stat_totals"][stat_type] += value
            data["by_position"][user_game.player_id][stat_type] += value

        for other_user_game in UserGame.objects.filter(game_id=user_game.game_id).exclude(user_id=user.id):
            if CabinetPosition(user_game.player_id).team == CabinetPosition(other_user_game.player_id).team:
                target = data["played_with"]
            else:
                target = data["played_against"]

            if other_user_game.user_id not in target:
                target[other_user_game.user_id] = {
                    "id": other_user_game.user.id,
                    "name": other_user_game.user.name or "(no name set)",
                    "scene": other_user_game.user.scene,
                    "count": 0,
                    "wins": 0,
                    "losses": 0,
                }

            value = target[other_user_game.user_id]
            value["count"] += 1

            if CabinetPosition(user_game.player_id).team == user_game.game.winning_team:
                value["wins"] += 1
            else:
                value["losses"] += 1

        # Kinda arbitrary, but based on these relative totals for 2022:
        # kills = 3,117,000
        # military kills = 1,087,522
        # queen kills = 460,428
        game_score_mil = game_stats.get(StatType.KILLS, 0) \
            + 1.86 * game_stats.get(StatType.MILITARY_KILLS, 0) \
            + 3.91 * game_stats.get(StatType.QUEEN_KILLS, 0) \
            - game_stats.get(StatType.DEATHS, 0) \
            - (5.77 if user_game.player_id in [CabinetPosition.GOLD_QUEEN, CabinetPosition.BLUE_QUEEN] else 1.86) * game_stats.get(StatType.MILITARY_DEATHS, 0)

        if game_score_mil > data["best_game_mil"].get("game_score", 0):
            data["best_game_mil"] = {
                "game_id": user_game.game_id,
                "player_id": user_game.player_id,
                "start_time": user_game.game.start_time.isoformat(),
                "scene_name": user_game.game.cabinet.scene.display_name,
                "cabinet_name": user_game.game.cabinet.display_name,
                "game_score": game_score_mil,
                **game_stats,
            }

    first_game = user.usergame_set.order_by("game__start_time").first()
    if first_game.game.start_time >= year_start and first_game.game.start_time < year_end:
        data["first_game"] = {
            "id": first_game.game_id,
            "start_time": first_game.game.start_time.isoformat(),
            "scene_name": first_game.game.cabinet.scene.display_name,
            "cabinet_name": first_game.game.cabinet.display_name,
            "position": first_game.player_id,
        }

    data["games_by_scene"] = list(
        all_user_games.values(
            scene_id=F("game__cabinet__scene_id"),
            color=F("game__cabinet__scene__background_color"),
            name=F("game__cabinet__scene__display_name"),
        ) \
        .annotate(games=models.Count("id"))
    )

    data["games_by_month"] = [
        {
            "month": row["month"].strftime("%B"),
            "games": row["games"],
        }
        for row in all_user_games \
            .annotate(month=TruncMonth("game__start_time")) \
            .values("month") \
            .annotate(games=models.Count("id"))
    ]

    obj, _ = UserRecap.objects.get_or_create(user=user, year=year)
    obj.data = data
    obj.save()

    return data

@shared_task
def check_sign_in_timeouts():
    sign_ins = SignInLog.objects.filter(
        is_current=True,
        action=SignInLog.Action.SIGN_IN,
        timestamp__lt=datetime.now() - timedelta(minutes=60),
    )

    cabinet_ids = set([i.cabinet_id for i in sign_ins])
    for cabinet_id in cabinet_ids:
        game_count = Game.objects.filter(
            cabinet_id=cabinet_id,
            start_time__gt=datetime.now() - timedelta(minutes=60),
        ).count()

        if game_count > 0:
            continue

        for sign_in in SignInLog.objects.filter(
                cabinet_id=cabinet_id,
                is_current=True,
                action=SignInLog.Action.SIGN_IN,
                timestamp__lt=datetime.now() - timedelta(minutes=60),
        ):
            sign_in.sign_out()
