import logging

from rest_framework.permissions import SAFE_METHODS, BasePermission

from ..constants import HTTPMethod
from ..permissions import SceneAdminOrReadOnly
from ..user.models import PermissionType
from .models import Cabinet


class ScenePermission(BasePermission):
    def has_permission(self, request, view):
        if request.method in SAFE_METHODS or request.method == HTTPMethod.PUT:
            return True

        if request.method == HTTPMethod.POST and request.user.is_authenticated and request.user.is_site_admin:
            return True

        return False

    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True

        if not request.user.is_authenticated:
            return False

        if request.method == HTTPMethod.PUT and request.user.is_admin_of(obj):
            return True

        return False


class CabinetPermission(SceneAdminOrReadOnly):
    pass


class GamePermission(SceneAdminOrReadOnly):
    def get_scene_from_object(self, obj):
        return obj.cabinet.scene

    def get_scene_id_from_request(self, request):
        return False


class RequireCabinetToken(BasePermission):
    def has_permission(self, request, view):
        if "cabinet" in request.data:
            try:
                cabinet = Cabinet.objects.get(id=request["cabinet"])
            except Cabinet.DoesNotExist:
                return False

        elif "cabinet_name" in request.data and "scene_name" in request.data:
            try:
                cabinet = Cabinet.by_name(request.data["scene_name"], request.data["cabinet_name"])
            except Cabinet.DoesNotExist:
                return False

        else:
            return False

        return request.data.get("token") == cabinet.token
