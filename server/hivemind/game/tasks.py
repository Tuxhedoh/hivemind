import logging
from datetime import timedelta

from celery import shared_task
from django.core.management.base import BaseCommand
from django.db import transaction

import hivemind.stats.tasks
from hivemind.achievement.models import Achievement
from hivemind.client import redis_client, redis_get, redis_set
from hivemind.game.models import Cabinet, Game, GameEvent, GameMap, GameStat
from hivemind.league.models import Event
from hivemind.stats.models import SignInLog
from hivemind.tournament.models import TournamentMatch

logger = logging.getLogger(__name__)

@shared_task
def process_game(game_id):
    with transaction.atomic():
        game = Game.objects.select_for_update().get(id=game_id)

        if game.status != Game.GameStatus.READY:
            logger.info("Skipping game {} (status is {})".format(game_id, game.status))
            return

        game.status = Game.GameStatus.PROCESSING
        game.save()

    try:
        process_game_end(game)
        process_stats(game)
        process_achievements(game)

        game.status = Game.GameStatus.COMPLETE
        game.save()

    except Exception as err:
        logger.exception("Error processing game {}".format(game_id))

        game.status = Game.GameStatus.ERROR
        game.save()



def process_game_end(game):
    cabinet = game.cabinet

    logger.info("Processing game end for game {} ({}/{})".format(game.id, cabinet.scene.name, cabinet.name))

    with transaction.atomic():
        events = GameEvent.objects.select_for_update().filter(game_uuid=game.uuid)
        events.update(game_id=game.id)
        events.filter(timestamp__lt=game.start_time - timedelta(seconds=12)).delete()

    logger.info("Updated game IDs for game {} (UUID {})".format(game.id, game.uuid))

    bonus_map_name = game.detect_bonus_map()
    if bonus_map_name:
        game.map_name = bonus_map_name
        game.save()

    game_map, _ = GameMap.objects.get_or_create(name=game.map_name)

    players = game.count_players()
    game.save()

    # Process signed-in users
    for sign_in in SignInLog.objects.filter(cabinet=cabinet, is_current=True, action=SignInLog.Action.SIGN_IN):
        if str(sign_in.player_id) in players:
            sign_in.save_game(game)
        else:
            sign_in.sign_out()

    # Bonus maps don't count for league/tournament events
    if game_map.is_bonus:
        return

    # Check for active tournament on this cabinet
    tournament_match = TournamentMatch.objects.filter(active_cabinet=cabinet).first()
    if tournament_match:
        if tournament_match.is_warmup:
            tournament_match.is_warmup = False
            tournament_match.save()

            tournament_match.bracket.tournament.publish_match_state()

            return

        game.tournament_match_id = tournament_match.id
        game.save()

        if game.winning_team == "blue":
            tournament_match.blue_score += 1
        else:
            tournament_match.gold_score += 1

        tournament_match.save()
        tournament_match.bracket.tournament.publish_match_state()

        if tournament_match.round_count_reached():
            tournament_match.set_complete()
            for sign_in in SignInLog.objects.filter(cabinet_id=game.cabinet_id, is_current=True,
                                                    action=SignInLog.Action.SIGN_IN):
                sign_in.sign_out()

            tournament_match.bracket.tournament.publish_match_state()

        tournament_match.report_result()

        return

    # Check for active event on this cabinet
    event = Event.objects.filter(is_active=True, cabinet=cabinet).first()
    if event:
        if event.is_quickplay:
            qp_match = event.current_qp_match()
            if qp_match is None:
                return

            game.qp_match_id = qp_match.id
            game.save()

            if game.winning_team == "blue":
                qp_match.blue_score += 1
            else:
                qp_match.gold_score += 1

            qp_match.save()
            event.publish_match_state()

            if qp_match.round_count_reached():
                qp_match.set_complete()
                qp_match.save()
                qp_match.event.publish_match_state()

        else:
            match = event.current_match()
            if match is None:
                return

            game.match_id = match.id
            game.save()

            if game.winning_team == "blue":
                match.blue_score += 1
            else:
                match.gold_score += 1

            match.save()
            match.event.publish_match_state()

            if match.round_count_reached():
                match.set_complete()
                match.save()
                match.event.publish_match_state()

        return

    # Check if cabinet score should update
    for overlay in cabinet.overlays.all():
        if overlay.update_score:
            if game.winning_team == "blue":
                overlay.blue_score += 1
            else:
                overlay.gold_score += 1

            overlay.save()
            overlay.publish_settings()

def process_stats(game):
    game.collect_player_stats()

    for sign_in in SignInLog.objects.filter(cabinet=game.cabinet, is_current=True, action=SignInLog.Action.SIGN_IN):
        if sign_in.user:
            sign_in.user.collect_stats()
        if sign_in.tournament_player:
            sign_in.tournament_player.collect_stats()

def process_achievements(game):
    Achievement.check_all(game)
