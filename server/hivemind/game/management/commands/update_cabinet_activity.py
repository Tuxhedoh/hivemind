import json
from datetime import datetime, timedelta

from django.core.management.base import BaseCommand
from django.db import transaction
from hivemind.game.models import Cabinet, CabinetActivity, Game


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        activity = {}
        start_time = datetime.now() - timedelta(days=84)

        for game in Game.objects.filter(start_time__gt=start_time, end_time__isnull=False):
            if game.player_count is None:
                game.count_players()

            activity_value = (game.end_time - game.start_time).total_seconds() * game.player_count / 10

            activity_key = (game.cabinet_id, game.get_start_time_local().weekday(), game.get_start_time_local().hour)
            activity[activity_key] = activity.get(activity_key, 0) + activity_value

        with transaction.atomic():
            CabinetActivity.objects.all().delete()

            for k, v in activity.items():
                CabinetActivity(cabinet_id=k[0], day_of_week=k[1], hour=k[2], activity=v).save()
