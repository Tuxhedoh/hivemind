from django.core.management.base import BaseCommand
from django.db import transaction

from hivemind.game.models import Cabinet, Scene
from hivemind.overlay.models import Overlay
from hivemind.user.models import Message, Permission, User


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("--admin", help="Email address of a user to set as the admin for the test scene.")

    def handle(self, *args, **kwargs):
        scene = Scene(name="test", display_name="HiveMind Test Lab", background_color="#fa86c4")
        scene.save()

        for i in range(4):
            cabinet = Cabinet(
                name="test{}".format(i+1),
                scene=scene,
                display_name="Test Cabinet {}".format(i+1),
                token="12345",
                time_zone="UTC",
            )

            cabinet.save()

            Overlay.objects.create(
                cabinet=cabinet,
                is_default=True,
            )

        if kwargs.get("admin"):
            user = User.objects.get(email=kwargs.get("admin"))
            Permission.objects.create(user=user, scene=scene, permission=Permission.PermissionType.ADMIN)
            Message.objects.create(user=user, message_text="Test scene created.", url="/scene/test")
