import urllib
from http import HTTPStatus

import requests
from django.conf import settings

from .client import redis_get, redis_set

auth_data = {}


def do_request(path, method="GET", params=None, data=None):
    url = urllib.parse.urljoin("https://api.twitch.tv/helix/", path)

    if not auth_data.get("access_token"):
        auth_data["access_token"] = redis_get("twitch_access_token")

    headers = requests.utils.default_headers()
    headers["Authorization"] = f"Bearer {auth_data['access_token']}"
    headers["Client-Id"] = settings.TWITCH_CLIENT_ID

    request = requests.request(method, url, params=params, json=data, headers=headers)
    if request.status_code == HTTPStatus.UNAUTHORIZED:
        refresh_auth_token()
        request = requests.request(method, url, params=params, json=data, headers=headers)

    request.raise_for_status()
    return request

def refresh_auth_token():
    url = "https://id.twitch.tv/oauth2/token"
    postdata = {
        "client_id": settings.TWITCH_CLIENT_ID,
        "client_secret": settings.TWITCH_CLIENT_SECRET,
        "grant_type": "refresh_token",
        "refresh_token": redis_get("twitch_refresh_token"),
    }

    headers = {
        "Content-Type": "application/x-www-form-urlencoded",
    }

    response = requests.post(url, headers=headers, data=postdata).json()
    redis_set("twitch_access_token", response["access_token"])
    redis_set("twitch_refresh_token", response["refresh_token"])

    auth_data["access_token"] = response["access_token"]

def create_clip(channel_name):
    response = do_request("users", params={"login": channel_name})
    broadcaster_id = response.json()["data"][0]["id"]

    response = do_request("clips", method="POST", data={"broadcaster_id": broadcaster_id})
    clip_id = response.json()["data"][0]["id"]

    return clip_id
