from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from django.template import loader
from django.template.exceptions import TemplateDoesNotExist
from rest_framework.exceptions import NotFound

from ..game.models import Cabinet, CabinetLegacyName
from ..model import BaseViewSet
from ..user.models import Permission, PermissionType
from .models import Overlay, OverlayType
from .permissions import OverlayPermission
from .serializers import OverlaySerializer


class OverlayViewSet(BaseViewSet):
    queryset = Overlay.objects.all()
    serializer_class = OverlaySerializer
    filterset_fields = ["cabinet_id"]
    permission_classes = [OverlayPermission]

    def perform_update(self, serializer):
        overlay = serializer.save()
        overlay.publish_settings()
