# Generated by Django 3.2.7 on 2021-11-05 22:28

from django.db import migrations


def create_overlay(apps, schema_editor):
    Theme = apps.get_model("overlay", "Theme")
    Theme.objects.create(
        name="BumbleBash Remix",
        description="BumbleBash Remix theme",
        directory_name="bbremix",
        ingame_window={"x": 0, "y": 670, "w": 1920, "h": 410},
        postgame_window={"x": 240, "y": 0, "w": 1440, "h": 810},
        game_capture_window={"x": 240, "y": 0, "w": 1440, "h": 810},
        blue_camera_window={"x": 10, "y": 900, "w": 510, "h": 180},
        gold_camera_window={"x": 1400, "y": 900, "w": 510, "h": 180},
    )


class Migration(migrations.Migration):
    dependencies = [
        ('overlay', '0004_auto_20211019_1744'),
    ]

    operations = [
        migrations.RunPython(create_overlay),
    ]
