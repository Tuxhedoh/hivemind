import json
import logging

from drf_extra_fields.fields import Base64ImageField
from rest_framework import serializers

from ..constants import FieldType
from ..game.models import Scene
from ..user.serializers import UserSerializer
from .models import (PaymentOption, PlayerInfoField, SelectedPaymentOption,
                     Tournament, TournamentBracket, TournamentMatch,
                     TournamentPlayer, MatchQueue)

logger = logging.getLogger()

class TournamentSerializer(serializers.ModelSerializer):
    scene = serializers.PrimaryKeyRelatedField(queryset=Scene.objects.all())
    description = serializers.CharField(allow_blank=True)
    registration_banner = Base64ImageField(required=False)
    stripe_configured = serializers.SerializerMethodField()

    class Meta:
        model = Tournament
        fields = ["id", "name", "scene", "date", "is_active", "location", "description",
                  "allow_registration", "registration_close_date", "registration_banner",
                  "accept_payments", "link_type", "registration_message", "payment_message",
                  "stripe_configured", "awards", "require_player_photo"]
        read_only_fields = ["awards"]

    @staticmethod
    def get_stripe_configured(obj: Tournament):
        return True if obj.stripe_account_id else False


class PlayerInfoFieldSerializer(serializers.ModelSerializer):
    field_name = serializers.CharField(read_only=True)

    class Meta:
        model = PlayerInfoField
        fields = "__all__"


class TournamentBracketSerializer(serializers.ModelSerializer):
    class Meta:
        model = TournamentBracket
        fields = "__all__"
        read_only_fields = ["is_valid"]


class TournamentMatchSerializer(serializers.ModelSerializer):
    tournament = TournamentSerializer(read_only=True, source="bracket.tournament")

    def save(self):
        match = super().save()
        match.bracket.tournament.publish_match_state()

    class Meta:
        model = TournamentMatch
        fields = "__all__"


class MatchQueueSerializer(serializers.ModelSerializer):
    match_list = TournamentMatchSerializer(many=True, source="get_list")

    class Meta:
        model = MatchQueue
        fields = "__all__"
        read_only_fields = ["tournament_id", "cabinet_id"]


class PaymentOptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = PaymentOption
        fields = "__all__"


class SelectedPaymentOptionSerializer(serializers.ModelSerializer):
    payment_option = PaymentOptionSerializer(many=False)
    class Meta:
        model = SelectedPaymentOption
        fields = "__all__"


def handle_playerinfo_representation(representation, instance):
    for field in instance.tournament.playerinfofield_set.all():
        info = instance.playerinfo_set.filter(field=field).first()
        if field.field_type == FieldType.MULTI_CHOICE:
            representation[field.field_name] = json.loads(info.value) if info and info.value else []
        else:
            representation[field.field_name] = info.value if info and info.value else None

    return representation

class TournamentPlayerSerializer(serializers.ModelSerializer):
    stats = serializers.ListField(source="get_stats", read_only=True)
    selected_payment_options = SelectedPaymentOptionSerializer(
        required=False, allow_empty=True, many=True, read_only=True,
    )

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        return handle_playerinfo_representation(representation, instance)


    class Meta:
        model = TournamentPlayer
        fields = "__all__"
        read_only_fields = ["stats", "price", "selected_payment_options", "registration_time"]


class RegistrationSerializer(serializers.ModelSerializer):
    image = Base64ImageField(required=False)
    selected_payment_options = SelectedPaymentOptionSerializer(
        required=False, allow_empty=True, many=True, read_only=True,
    )

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        return handle_playerinfo_representation(representation, instance)

    class Meta:
        model = TournamentPlayer
        fields = ["id", "tournament", "name", "scene", "image", "do_not_display", "pronouns", "email", "tidbit",
                  "user", "paid", "team", "selected_payment_options", "price","registration_time"]
        read_only_fields = ["id", "paid", "selected_payment_options", "price","registration_time"]

    def create(self, validated_data):
        defaults = {k: v for k, v in validated_data.items() if k not in ["tournament", "user", "selected_payment_options"]}
        player, created = TournamentPlayer.objects.update_or_create(
            defaults=defaults,
            tournament=validated_data["tournament"],
            user=validated_data["user"],
        )

        player.selected_payment_options.set(validated_data.get("selected_payment_options", []))

        return player
