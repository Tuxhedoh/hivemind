import hashlib
import itertools
import json
import os
from datetime import datetime, timedelta
from urllib.parse import urlencode

from django.contrib.postgres.fields import ArrayField
from django.db import models, transaction
from django.db.models import Q
from django.utils.text import slugify

from ..client import redis_client, redis_get, redis_set
from ..constants import (AggregateStatType, CabinetPosition, FieldType,
                         HiddenStatType, StatType)
from ..game.models import Cabinet, Game, Scene
from ..model import BaseModel, Deleteable
from ..worker import app
from . import challonge

ROUND_NAMES = ("Octofinals", "Quarterfinals", "Semifinals", "Finals")

def generate_token():
    return hashlib.sha512(os.urandom(2048)).hexdigest()[:32]


class TournamentNotValidated(Exception):
    pass


class Tournament(Deleteable):
    class LinkType(models.TextChoices):
        MANUAL = "manual", "Manual"
        CHALLONGE = "challonge", "Challonge"
        SMASH = "smash", "smash.gg"

    name = models.CharField(max_length=200)
    scene = models.ForeignKey(Scene, on_delete=models.CASCADE)
    date = models.DateField(default=datetime.now)
    is_active = models.BooleanField(default=False)
    location = models.TextField(default="", null=True, blank=True)
    description = models.TextField(default="", null=True, blank=True)
    registration_message = models.TextField(default="", null=True, blank=True)
    payment_message = models.TextField(default="", null=True, blank=True)
    allow_registration = models.BooleanField(default=False)
    require_player_photo = models.BooleanField(default=False)
    registration_close_date = models.DateField(null=True)
    registration_banner = models.ImageField(null=True, blank=True, upload_to="tournament/registration-banners/")
    accept_payments = models.BooleanField(default=False)
    stripe_account_id = models.TextField(max_length=50, null=True, blank=True)
    awards = models.JSONField(null=True)

    link_type = models.TextField(max_length=20, choices=LinkType.choices, default=LinkType.CHALLONGE)

    def get_active_matches(self):
        bracket_ids = [i.id for i in self.tournamentbracket_set.all()]
        matches = TournamentMatch.objects.filter(active_cabinet__isnull=False, bracket_id__in=bracket_ids)

        return matches

    def get_active_match_by_cabinet(self, cabinet_id):
        bracket_ids = [i.id for i in self.tournamentbracket_set.all()]
        try:
            return TournamentMatch.objects.get(active_cabinet_id=cabinet_id, bracket_id__in=bracket_ids)
        except TournamentMatch.DoesNotExist:
            return None

    def get_available_matches(self):
        bracket_ids = [i.id for i in self.tournamentbracket_set.order_by("id").all()]
        matches = TournamentMatch.objects.filter(is_complete=False, bracket_id__in=bracket_ids) \
            .filter(blue_team__isnull=False, gold_team__isnull=False) \
            .order_by("round_name", "id")

        return matches

    def get_queue(self, cabinet=None):
        queue, _ = MatchQueue.objects.get_or_create(tournament=self, cabinet=cabinet)
        return queue

    @transaction.atomic
    def select_next_match(self, cabinet):
        if TournamentMatch.objects.filter(active_cabinet=cabinet).count() > 0:
            return None

        queue = self.get_queue(cabinet=cabinet)
        match = queue.get_next_match()

        if match:
            dequeued_from = MatchQueue.dequeue(match)

            # Set warmup if either team has not played yet
            if match.bracket.auto_warmup and match.bracket.tournamentmatch_set.filter(is_complete=True).filter(Q(blue_team=match.blue_team) | Q(gold_team=match.blue_team)).count() == 0:
                match.is_warmup = True
            if match.bracket.auto_warmup and match.bracket.tournamentmatch_set.filter(is_complete=True).filter(Q(blue_team=match.gold_team) | Q(gold_team=match.gold_team)).count() == 0:
                match.is_warmup = True

            active_matches = self.get_active_matches()
            active_teams = set(
                [match.blue_team_id for match in active_matches] +
                [match.gold_team_id for match in active_matches]
            )

            if match.blue_team is None or match.gold_team is None or \
               match.blue_team_id in active_teams or \
               match.gold_team_id in active_teams:

                # match is not available yet, get the next one instead
                next_match = self.select_next_match(cabinet)

                # then re-queue this one at the front
                match_list = dequeued_from.get_list()
                match_list.insert(0, match)
                dequeued_from.set_list(match_list)

                return next_match

            if match.active_cabinet is not None or match.is_complete:
                # match already started somewhere, get the next one instead
                # it's already dequeued so just run the function again
                return self.select_next_match(cabinet)

            match.active_cabinet = cabinet
            match.save()

        self.publish_match_state()

    def publish_match_state(self):
        for cabinet in self.scene.cabinet_set.all():
            current_match = None
            on_deck = None

            match = self.get_active_match_by_cabinet(cabinet.id)
            if match:
                current_match = {
                    "id": match.id,
                    "blue_team": match.blue_team.name,
                    "blue_score": match.blue_score,
                    "gold_team": match.gold_team.name,
                    "gold_score": match.gold_score,
                    "is_warmup": match.is_warmup,
                    "rounds_per_match": match.bracket.rounds_per_match,
                    "wins_per_match": match.bracket.wins_per_match,
                }

                if match.round_name:
                    current_match["round_name"] = "{} - {}".format(match.bracket.name, match.round_name)
                else:
                    current_match["round_name"] = match.bracket.name

            on_deck_match = self.get_queue(cabinet=cabinet).get_next_match()
            if on_deck_match:
                on_deck = {
                    "id": on_deck_match.id,
                    "blue_team": on_deck_match.blue_team.name if on_deck_match.blue_team else "(unknown team)",
                    "gold_team": on_deck_match.gold_team.name if on_deck_match.gold_team else "(unknown team)",
                }

            redis_client.publish("matchstate", json.dumps({
                "type": "match",
                "match_type": "tournament",
                "scene_name": self.scene.name,
                "cabinet_name": cabinet.name,
                "cabinet_id": cabinet.id,
                "current_match": current_match,
                "on_deck": on_deck,
            }))

    @property
    def api_module(self):
        if self.link_type == self.LinkType.CHALLONGE:
            return challonge

        if self.link_type == self.LinkType.SMASH:
            return smash


class PlayerInfoField(BaseModel):
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    field_slug = models.CharField(max_length=30, unique=True, blank=True, null=True)
    field_description = models.TextField(null=True, blank=True)
    field_type = models.CharField(max_length=30, choices=FieldType.choices)
    is_required = models.BooleanField(default=False)
    choices = ArrayField(models.CharField(max_length=120), null=True, blank=True)
    order = models.IntegerField(null=True)

    @property
    def field_name(self):
        return "question{}".format(self.id)

    class Meta:
        unique_together = (
            ("tournament", "field_description"),
        )


class PaymentOption(BaseModel):
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    price = models.DecimalField(max_digits=9, decimal_places=2, null=True)



class SelectedPaymentOption(BaseModel):
    payment_option = models.ForeignKey(PaymentOption, on_delete=models.CASCADE, null=True)
    quantity = models.IntegerField(default=1)
    custom_price = models.DecimalField(max_digits=9, decimal_places=2, null=True)


class TournamentBracket(BaseModel):
    name = models.CharField(max_length=200)
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    linked_bracket_id = models.CharField(max_length=200, null=True, db_index=True)
    link_token = models.CharField(max_length=50, default=generate_token)
    linked_org = models.CharField(max_length=50, null=True)
    is_valid = models.BooleanField(default=False)
    rounds_per_match = models.IntegerField(null=True)
    wins_per_match = models.IntegerField(null=True)
    report_as_sets = models.BooleanField(default=False)
    add_tiebreaker = models.BooleanField(default=False)
    auto_warmup = models.BooleanField(default=False)

    def get_teams(self):
        if not self.is_valid:
            return

        teams = {}
        for id, name in self.tournament.api_module.get_participants(self.linked_bracket_id, org=self.linked_org).items():
            team = self.tournament.tournamentteam_set.filter(linked_team_ids__contains=[str(id)]).first()
            if team:
                teams[id] = team
                continue

            team = self.tournament.tournamentteam_set.filter(name=name).first()
            if team is None:
                team = TournamentTeam(tournament=self.tournament, name=name, linked_team_ids=[])

            team.linked_team_ids.append(id)
            team.save()
            teams[id] = team

        return teams

    def set_teams(self, teams):
        if not self.is_valid:
            return

        for id, name in self.tournament.api_module.get_participants(self.linked_bracket_id, org=self.linked_org).items():
            team = self.tournament.tournamentteam_set.filter(linked_team_ids__contains=[str(id)]).first()
            if team:
                team.linked_team_ids.remove(str(id))

        ids = self.tournament.api_module.set_participants(self.linked_bracket_id, teams, org=self.linked_org)

        for team, id in zip(teams, ids):
            if team.linked_team_ids is None:
                team.linked_team_ids = []

            team.linked_team_ids.append(id)

            TournamentTeam.objects.bulk_update(teams, ["linked_team_ids"])

    def get_round_name(self, match, matches):
        if not hasattr(self, "_max_rounds_per_stage"):
            self._max_rounds_per_stage = {}
            for stage in set([match["stage"] for match in matches]):
                self._max_rounds_per_stage[stage] = max([match["round"] for match in matches if match["stage"] == stage])

        max_rounds_per_stage = self._max_rounds_per_stage
        num_losers_rounds = abs(min([match.get("round", 0) for match in matches])) if matches else 0

        rnd = match["round"]
        stage = match["stage"]

        if self.tournament_info["tournament_type"] == "single elimination" \
           and max_rounds_per_stage[stage] - rnd < len(ROUND_NAMES) \
           and self.tournament_info.get("state") != "group_stages_underway":
            return ROUND_NAMES[rnd - max_rounds_per_stage[stage] - 1]

        if self.tournament_info["tournament_type"] == "double elimination" \
           and self.tournament_info.get("state") != "group_stages_underway":

            if rnd == max_rounds_per_stage[stage]:
                return "Grand Finals"

            if rnd > 0 and max_rounds_per_stage[stage] - rnd < len(ROUND_NAMES):
                return "Winners' {}".format(ROUND_NAMES[rnd - max_rounds_per_stage[stage]])

            if rnd < 0 and num_losers_rounds + rnd < len(ROUND_NAMES):
                return "Losers' {}".format(ROUND_NAMES[::-1][rnd + num_losers_rounds])

            if rnd > 0:
                return "Winners' Round {}".format(rnd)

            if rnd < 0:
                return "Losers' Round {}".format(rnd)

        return "Round {}".format(rnd)

    def get_matches(self):
        if not self.is_valid:
            return

        teams_by_id = self.get_teams()
        matches = self.tournament.api_module.get_matches(self.linked_bracket_id, org=self.linked_org)

        for match in matches:
            stage = match["stage"]
            rnd = match["round"]
            if match["state"] in ["open", "pending"]:
                try:
                    tm = TournamentMatch.objects.get(linked_match_id=match["id"])
                except TournamentMatch.DoesNotExist:
                    tm = TournamentMatch()

                tm.bracket = self
                tm.linked_match_id = match["id"]

                if tm.is_flipped:
                    tm.gold_team = teams_by_id.get(match["team1"], None)
                    tm.blue_team = teams_by_id.get(match["team2"], None)
                else:
                    tm.blue_team = teams_by_id.get(match["team1"], None)
                    tm.gold_team = teams_by_id.get(match["team2"], None)

                tm.round_name = self.get_round_name(match, matches)

                if self.tournament_info.get("state") == "group_stages_underway":
                    tm.stage_name = "Group Stage"
                else:
                    tm.stage_name = "Knockout Stage".format(stage)

                tm.save()

    @property
    def tournament_info(self):
        if not hasattr(self, "_tournament_info"):
            if self.tournament.link_type == Tournament.LinkType.MANUAL:
                return True

            self._tournament_info = self.tournament.api_module.get_tournament(self.linked_bracket_id, org=self.linked_org)

        return self._tournament_info

    def check_permissions(self):
        if self.tournament.link_type == Tournament.LinkType.MANUAL:
            return True

        if self.tournament.link_type == Tournament.LinkType.CHALLONGE:
            # Get both account and org tournies
            account_ids = [i["url"] for i in self.tournament.api_module.list_tournaments(org=None)]
            org_ids = [i["url"] for i in self.tournament.api_module.list_tournaments(org=self.linked_org)]
            ids = account_ids + org_ids
            return (self.linked_bracket_id in ids)

        return False

    def validate(self):
        if self.tournament.link_type == Tournament.LinkType.MANUAL:
            return True

        if self.tournament.link_type == Tournament.LinkType.CHALLONGE:
            if not self.linked_bracket_id:
                return False

            if self.link_token in self.tournament_info["description"]:
                self.is_valid = True
                self.save()

                return True

        return False


class TournamentTeam(BaseModel):
    name = models.CharField(max_length=200)
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    linked_team_ids = ArrayField(models.CharField(max_length=200), null=False, default=list)


class HomeScene(BaseModel):
    name = models.CharField(max_length=10, unique=True)
    display_name = models.CharField(max_length=200)
    background_color = models.CharField(max_length=10)
    foreground_color = models.CharField(max_length=10)


class TournamentPlayer(BaseModel):
    name = models.CharField(max_length=200)
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    team = models.ForeignKey(TournamentTeam, null=True, blank=True, on_delete=models.SET_NULL)
    scene = models.CharField(max_length=10, null=True, blank=True)
    image = models.ImageField(null=True, blank=True, upload_to="tournament/player-images/")
    do_not_display = models.BooleanField(default=False)
    pronouns = models.CharField(max_length=30, null=True, blank=True)
    tidbit = models.TextField(null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    user = models.ForeignKey("user.User", null=True, on_delete=models.SET_NULL)
    registration_time = models.DateTimeField(null=True)
    check_in_time = models.DateTimeField(null=True)
    selected_payment_options = models.ManyToManyField(SelectedPaymentOption, blank=True)
    price = models.DecimalField(max_digits=9, decimal_places=2, null=True)
    paid = models.BooleanField(default=False)
    stripe_session_id = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return "{} [{}]".format(self.name, self.scene)

    @transaction.atomic
    def collect_stats(self):
        stats = {i: 0 for i in AggregateStatType}
        stats.update({i: 0 for i in StatType})
        stats.update({i: 0 for i in HiddenStatType})

        for player_game in self.tournamentplayergame_set.all():
            stats[AggregateStatType.GAMES] += 1

            if CabinetPosition(player_game.player_id).team == player_game.game.winning_team:
                stats[AggregateStatType.WINS] += 1
            else:
                stats[AggregateStatType.LOSSES] += 1

            stats[AggregateStatType.TIME_PLAYED] += int((player_game.game.end_time - player_game.game.start_time).total_seconds() * 1000)

        for game_stat in self.gamestat_set.all():
            stats[game_stat.stat_type] += game_stat.value

        for stat_type, value in stats.items():
            player_stat, _ = PlayerStat.objects.get_or_create(player=self, stat_type=stat_type)
            player_stat.value = value
            player_stat.save()

    def get_stats(self):
        stats_dict = {i.stat_type: i for i in self.playerstat_set.all()}
        stats = []

        for stat_type in itertools.chain(AggregateStatType, StatType):
            stats.append({
                "name": stat_type.value,
                "label": stat_type.label,
                "value": stat.value if (stat := stats_dict.get(stat_type)) else 0,
            })

        return stats


class PlayerInfo(BaseModel):
    player = models.ForeignKey(TournamentPlayer, on_delete=models.CASCADE)
    field = models.ForeignKey(PlayerInfoField, on_delete=models.CASCADE)
    value = models.CharField(max_length=200)

    class Meta:
        unique_together = ("player", "field")


class TournamentPlayerGame(BaseModel):
    tournament_player = models.ForeignKey(TournamentPlayer, on_delete=models.CASCADE)
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    player_id = models.IntegerField(choices=CabinetPosition.choices)

    def get_stats(self):
        stats_dict = {i.stat_type: i for i in self.playerstat_set.all()}
        stats = []

        for stat_type in itertools.chain(AggregateStatType, StatType, HiddenStatType):
            stats.append({
                "name": stat_type.value,
                "label": stat_type.label,
                "value": stat.value if (stat := stats_dict.get(stat_type)) else 0,
            })

        return stats

    class Meta:
        unique_together = ("tournament_player", "game")


class PlayerStat(BaseModel):
    stat_type_choices = AggregateStatType.choices + StatType.choices + HiddenStatType.choices

    player = models.ForeignKey(TournamentPlayer, on_delete=models.CASCADE)
    stat_type = models.TextField(max_length=20, choices=stat_type_choices)
    value = models.IntegerField(default=0)

    class Meta:
        unique_together = ("player", "stat_type")


class TournamentMatch(BaseModel):
    bracket = models.ForeignKey(TournamentBracket, on_delete=models.CASCADE)
    linked_match_id = models.CharField(max_length=200, unique=True, null=True)
    blue_team = models.ForeignKey(TournamentTeam, null=True, on_delete=models.SET_NULL, related_name="+")
    gold_team = models.ForeignKey(TournamentTeam, null=True, on_delete=models.SET_NULL, related_name="+")
    blue_score = models.IntegerField(default=0)
    gold_score = models.IntegerField(default=0)
    stage_name = models.CharField(max_length=50, null=True)
    round_name = models.CharField(max_length=50, null=True)
    active_cabinet = models.OneToOneField(Cabinet, null=True, on_delete=models.SET_NULL)
    is_flipped = models.BooleanField(default=False)
    is_complete = models.BooleanField(default=False)
    is_warmup = models.BooleanField(default=False)
    video_link = models.URLField(null=True)

    def blue_team_name(self):
        return self.blue_team.name

    def gold_team_name(self):
        return self.gold_team.name

    def round_count_reached(self):
        if self.bracket.add_tiebreaker and self.blue_score == self.gold_score:
            return False

        if self.bracket.rounds_per_match and self.blue_score + self.gold_score >= self.bracket.rounds_per_match:
            return True

        if self.bracket.wins_per_match and max(self.blue_score, self.gold_score) >= self.bracket.wins_per_match:
            return True

        return False

    @transaction.atomic
    def set_complete(self):
        cabinet = self.active_cabinet

        self.is_complete = True
        self.publish_match_end()
        self.active_cabinet = None
        self.save()

        self.bracket.tournament.publish_match_state()

        if cabinet:
            app.send_task(
                "hivemind.tournament.tasks.select_next_match",
                countdown=45,
                args=[self.bracket.tournament.id, cabinet.id],
            )

    def report_result(self):
        if not self.bracket.is_valid:
            return

        self.bracket.tournament.api_module.report_result(self)

    def publish_match_end(self):
        redis_client.publish("matchstate", json.dumps({
            "type": "matchend",
            "match_id": self.id,
            "cabinet_id": self.active_cabinet_id,
            "blue_team": self.blue_team.name,
            "blue_score": self.blue_score,
            "gold_team": self.gold_team.name,
            "gold_score": self.gold_score,
        }))

    def stats_cache_key(self):
        return "tournament_match_stats.{}".format(self.id)

    def get_match_stats(self, regenerate=False):
        if not regenerate:
            cached_stats = redis_get(self.stats_cache_key())
            if cached_stats:
                return cached_stats

        data = self.collect_match_stats()
        redis_set(self.stats_cache_key(), data, expire=60*60*24)
        return data

    def collect_match_stats(self):
        match_stats = {
            "length_sec": 0,
            "by_player": {},
        }

        by_player = match_stats["by_player"]

        for game in self.game_set.all():
            game_stats = game.generate_postgame_stats()

            match_stats["length_sec"] += game_stats["length_sec"]

            # Sum the by-team stats
            for stat_name in ["kills", "military_kills", "berries", "gate_control_sec"]:
                match_stats[stat_name] = match_stats.get(stat_name, {})
                for team, value in game_stats.get(stat_name, {}).items():
                    match_stats[stat_name][team] = match_stats[stat_name].get(team, 0) + value

            # Sum everything in the by_player dataset
            for stat_name, stat_values in game_stats.get("by_player", {}).items():
                by_player[stat_name] = by_player.get(stat_name, {})
                for player, value in stat_values.items():
                    by_player[stat_name][player] = by_player[stat_name].get(player, 0) + value


        # Get gate control percentage
        # This assumes each map has three warrior gates
        if "gate_control_sec" in match_stats:
            match_stats["gate_control"] = {
                k: "{:.01f}%".format(100 * v / match_stats["length_sec"] / 3)
                for k, v in match_stats["gate_control_sec"].items()
            }

        match_stats["by_player"] = by_player
        return match_stats


class MatchQueue(BaseModel):
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    cabinet = models.ForeignKey(Cabinet, null=True, on_delete=models.CASCADE)
    enabled = models.BooleanField(default=False)

    @classmethod
    def dequeue(cls, match):
        entry = MatchQueueEntry.objects.filter(match=match).first()
        if entry:
            queue = entry.queue
            entry.delete()
            return queue

    @transaction.atomic
    def add(self, match):
        last_entry = self.matchqueueentry_set.order_by("order").last()
        order = last_entry.order + 1 if last_entry else 1
        MatchQueueEntry.objects.create(queue=self, match=match, order=order)

    @transaction.atomic
    def set_list(self, match_list):
        self.matchqueueentry_set.all().delete()

        for idx, match in enumerate(match_list):
            MatchQueueEntry.objects.create(queue=self, match=match, order=idx+1)

    def get_list(self):
        return [i.match for i in self.matchqueueentry_set.order_by("order")]

    def get_next_match(self, cabinet=None):
        if cabinet is None and self.cabinet is None:
            return None

        if not self.enabled:
            return None

        if cabinet is None:
            cabinet = self.cabinet

        next_entry = self.matchqueueentry_set.order_by("order").first()
        if next_entry is not None:
            return next_entry.match

        # check the next-available queue if this was an empty cab-specific queue
        if self.cabinet is not None:
            try:
                next_queue = MatchQueue.objects.get(tournament=self.tournament, cabinet__isnull=True)
                return next_queue.get_next_match(cabinet=self.cabinet)
            except MatchQueue.DoesNotExist:
                pass

        return None

    class Meta:
        unique_together = ("tournament_id", "cabinet_id")


class MatchQueueEntry(BaseModel):
    queue = models.ForeignKey(MatchQueue, on_delete=models.CASCADE)
    match = models.OneToOneField(TournamentMatch, on_delete=models.CASCADE)
    order = models.IntegerField(null=False)

    class Meta:
        unique_together = ("queue", "order")


class Video(BaseModel):
    class VideoSite(models.TextChoices):
        YOUTUBE = "youtube", "YouTube"

    tournament = models.ForeignKey(Tournament, null=True, on_delete=models.SET_NULL)
    cabinet = models.ForeignKey(Cabinet, on_delete=models.CASCADE)
    service = models.TextField(max_length=20, choices=VideoSite.choices, null=True)
    video_id = models.CharField(max_length=200)
    start_time = models.DateTimeField()
    length = models.DurationField()

    class Meta:
        unique_together = ("service", "video_id")

    def games(self):
        return Game.objects.filter(
            cabinet=self.cabinet,
            start_time__gte=self.start_time,
            start_time__lt=self.start_time + self.length,
        ).order_by("start_time")

    def get_match_data(self):
        matches = {}

        for game in self.games():
            if game.tournament_match:
                match = game.tournament_match
                matches[match.id] = {
                    "match": match,
                    "start_time": min(
                        matches.get(match.id, {}).get("start_time", timedelta.max),
                        game.start_time - self.start_time - timedelta(seconds=5),
                    ),
                    "end_time": max(
                        matches.get(match.id, {}).get("end_time", timedelta.min),
                        game.end_time - self.start_time + timedelta(seconds=5),
                    ),
                }

        return matches

    def tag_matches(self):
        matches = self.get_match_data()

        for match in matches.values():
            match["match"].video_link = self.get_embed_url(match["start_time"], match["end_time"])
            match["match"].save()

    def match_timestamps(self):
        matches = self.get_match_data()

        return os.linesep.join([
            "{} - {}: {} vs. {}".format(
                str(m["start_time"]).split(".")[0],
                m["match"].round_name,
                m["match"].blue_team.name,
                m["match"].gold_team.name,
            )
            for m in sorted(matches.values(), key=lambda m: m["start_time"])
        ])

    def url(self):
        if self.service == self.VideoSite.YOUTUBE:
            return "https://www.youtube.com/watch?v={}".format(self.video_id)

        return self.video_id

    def get_embed_url(self, start_time, end_time=None):
        query = {
            "start": int(start_time.total_seconds()),
        }

        if end_time:
            query["end"] = int(end_time.total_seconds())

        if self.service == self.VideoSite.YOUTUBE:
            return "https://www.youtube.com/embed/{}?{}".format(self.video_id, urlencode(query))
