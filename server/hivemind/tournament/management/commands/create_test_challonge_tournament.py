import random
from datetime import datetime, timedelta

import lorem
from django.core.management.base import BaseCommand

from hivemind.game.models import Scene
from hivemind.tests import HiveMindTest
from hivemind.tournament.models import (HomeScene, Tournament,
                                        TournamentBracket, TournamentPlayer,
                                        TournamentTeam)


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        test_case = HiveMindTest()

        scene = Scene.objects.get(name="test")
        tournament = Tournament.objects.create(
            name="HiveMind Test",
            scene=scene,
            link_type=Tournament.LinkType.CHALLONGE,
            date=datetime.now() + timedelta(days=7),
            allow_registration=True,
            registration_close_date=datetime.now() + timedelta(days=7),
        )

        teams = [TournamentTeam.objects.create(
            tournament=tournament,
            name=i,
        ) for i in test_case.get_team_names(12)]

        group_a = TournamentBracket.objects.create(
            name="Group A",
            tournament=tournament,
            linked_bracket_id="HiveMindTestGroupA",
            is_valid=True,
            rounds_per_match=4,
            report_as_sets=True,
            auto_warmup=True,
        )

        group_a.set_teams(teams[0:4])

        group_b = TournamentBracket.objects.create(
            name="Group B",
            tournament=tournament,
            linked_bracket_id="HiveMindTestGroupB",
            is_valid=True,
            rounds_per_match=4,
            report_as_sets=True,
            auto_warmup=True,
        )

        group_b.set_teams(teams[4:8])

        group_c = TournamentBracket.objects.create(
            name="Group C",
            tournament=tournament,
            linked_bracket_id="HiveMindTestGroupC",
            is_valid=True,
            rounds_per_match=4,
            report_as_sets=True,
            auto_warmup=True,
        )

        group_c.set_teams(teams[8:12])

        elim_stage = TournamentBracket.objects.create(
            name="Double Elimination",
            tournament=tournament,
            linked_bracket_id="HiveMindTestElim",
            is_valid=True,
            wins_per_match=2,
            report_as_sets=True,
            auto_warmup=True,
        )

        names = test_case.get_player_names(len(teams) * 5)
        for team in teams:
            for i in range(5):
                TournamentPlayer.objects.create(
                    name=names.pop(),
                    tournament=tournament,
                    team=team,
                    scene=HomeScene.objects.order_by("?").first().name,
                    pronouns=random.choice(["he/him", "she/her", "they/them", "he/they", "she/they"]),
                    tidbit=lorem.sentence(),
                )
