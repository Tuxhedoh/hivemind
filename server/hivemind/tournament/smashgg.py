from django.conf import settings
from smashggpy.models.Event import Event
from smashggpy.util import Initializer

Initializer.initialize(settings.SMASH_API_KEY)
