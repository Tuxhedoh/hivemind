import http.client
import json
from datetime import datetime, timedelta

import requests.exceptions
import stripe
from django.conf import settings
from django.core.exceptions import BadRequest, ValidationError
from django.db import transaction
from django.shortcuts import get_object_or_404, render
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response

from ..constants import FieldType, HTTPMethod
from ..game.models import Cabinet, Game, timedelta_parse
from ..model import BaseViewSet
from ..viewsets import DeleteableMixin
from .filters import PlayerInfoFilter, TournamentMatchFilter
from .models import (HomeScene, MatchQueue, PaymentOption, PlayerInfo,
                     PlayerInfoField, PlayerStat, SelectedPaymentOption,
                     Tournament, TournamentBracket, TournamentMatch,
                     TournamentPlayer, TournamentTeam, Video)
from .permissions import (MatchQueuePermission, PaymentOptionPermission,
                          PlayerInfoFieldPermission,
                          TournamentBracketPermission,
                          TournamentMatchPermission, TournamentPermission,
                          TournamentPlayerPermission, TournamentTeamPermission,
                          VideoCreatePermission, VideoPermission)
from .serializers import (MatchQueueSerializer, PlayerInfoFieldSerializer,
                          RegistrationSerializer, TournamentBracketSerializer,
                          TournamentMatchSerializer,
                          TournamentPlayerSerializer, TournamentSerializer)
from .tasks import (get_available_matches, publish_matches, publish_queue,
                    select_next_match)


class TournamentViewSet(BaseViewSet):
    queryset = Tournament.objects.order_by("-date", "-id").all()
    serializer_class = TournamentSerializer
    filterset_fields = ["scene_id"]
    permission_classes = [TournamentPermission]

    @action(detail=True, methods=[HTTPMethod.GET], url_path="available-matches")
    def available_matches(self, request, pk=None):
        tournament = get_object_or_404(Tournament, id=pk)
        matches = tournament.get_available_matches()

        get_available_matches.delay(pk)

        return Response(TournamentMatchSerializer(matches, many=True).data)

    @action(detail=True, methods=[HTTPMethod.PUT], url_path="select-next")
    def select_next(self, request, pk=None):
        tournament = get_object_or_404(Tournament, id=pk)
        cabinet = get_object_or_404(Cabinet, id=request.data.get("cabinet"))

        if tournament.scene.id != cabinet.scene.id:
            raise BadRequest("Invalid cabinet")

        select_next_match.delay(tournament.id, cabinet.id)
        return Response({})

    @action(detail=True, methods=[HTTPMethod.GET], url_path="registration")
    def get_registration(self, request, pk=None):
        if not request.user.is_authenticated:
            return Response({})

        tournament = get_object_or_404(Tournament, id=pk)

        try:
            player = TournamentPlayer.objects.get(tournament=tournament, user=request.user)
        except TournamentPlayer.DoesNotExist:
            return Response({})

        return Response(RegistrationSerializer(player).data)

    @action(detail=True, methods=[HTTPMethod.PUT])
    def register(self, request, pk=None):
        tournament = get_object_or_404(Tournament, id=pk)
        if not tournament.allow_registration or (
                tournament.registration_close_date and
                tournament.registration_close_date < datetime.utcnow().date()
        ):
            return Response({"error": "This tournament is not accepting registrations."})

        data = request.data
        data["tournament"] = tournament.id
        data["user"] = request.user.id

        missing_fields = []
        for field in tournament.playerinfofield_set.filter(is_required=True):
            if not data.get(field.field_name):
                missing_fields.append(field)

        if missing_fields:
            raise ValidationError({
                f.field_name: "This field is required" for f in missing_fields
            })

        if request.data.get("team_type") == "free-agent":
            data["team"] = None
        if request.data.get("team_type") == "create-team":
            data["team"] = TournamentTeam.objects.create(tournament=tournament, name=request.data["team_name"]).id

        serializer = RegistrationSerializer(data=data, context={"request": request})
        serializer.is_valid(raise_exception=True)

        player = serializer.save()

        if request.user.image and not player.image:
            player.image = request.user.image
            player.save()

        for field in tournament.playerinfofield_set.all():
            player_info, _ = PlayerInfo.objects.get_or_create(player=player, field=field)
            if request.data.get(field.field_name) and field.field_type == FieldType.MULTI_CHOICE:
                player_info.value = json.dumps(request.data.get(field.field_name))
            else:
                player_info.value = request.data.get(field.field_name)

            player_info.save()

        return Response(TournamentPlayerSerializer(player).data)

    @action(detail=True, methods=[HTTPMethod.PUT], url_path="create-queues")
    def create_queues(self, request, pk=None):
        tournament = get_object_or_404(Tournament, id=pk)
        queues = []

        queue, _ = MatchQueue.objects.get_or_create(tournament=tournament, cabinet=None)
        queues.append(MatchQueueSerializer(queue).data)

        for cabinet in tournament.scene.cabinet_set.order_by("id"):
            queue, _ = MatchQueue.objects.get_or_create(tournament=tournament, cabinet=cabinet)
            queues.append(MatchQueueSerializer(queue).data)

        return Response({"queues": queues})

    @action(detail=True, methods=[HTTPMethod.PUT])
    def stripe(self, request, pk=None):
        tournament = get_object_or_404(Tournament, id=pk)

        if tournament.stripe_account_id is None:
            account = stripe.Account.create(type="standard")
            tournament.stripe_account_id = account["id"]
            tournament.save()

        account_link = stripe.AccountLink.create(
            account=tournament.stripe_account_id,
            refresh_url=settings.PUBLIC_URL("/stripe/refresh"),
            return_url=settings.PUBLIC_URL("/stripe/return"),
            type="account_onboarding",
        )

        return Response({"url": account_link["url"]})

    @action(detail=True, methods=[HTTPMethod.PUT], url_path="question-order")
    def update_order(self, request, pk=None):
        tournament = get_object_or_404(Tournament, id=pk)
        try:
            questions = PlayerInfoField.objects.filter(tournament=tournament)
            with transaction.atomic():
                for index, id in enumerate(request.data.get("order")):
                    questions.filter(id=id).update(order=index+1)
                return Response({"success": True})
        except PlayerInfoField.DoesNotExist:
            return Response({"error": "This tournament has no questions."})



class PlayerInfoFieldViewSet(BaseViewSet):
    queryset = PlayerInfoField.objects.order_by("order", "id").all()
    serializer_class = PlayerInfoFieldSerializer
    filterset_fields = ["tournament_id"]
    permission_classes = [PlayerInfoFieldPermission]


class PaymentOptionViewSet(BaseViewSet):
    queryset = PaymentOption.objects.order_by("price").all()
    serializer_class = PaymentOption.serializer()
    filterset_fields = ["tournament_id"]
    permission_classes = [PaymentOptionPermission]


class TournamentBracketViewSet(BaseViewSet):
    queryset = TournamentBracket.objects.order_by("id").all()
    serializer_class = TournamentBracketSerializer
    filterset_fields = ["tournament_id"]
    permission_classes = [TournamentBracketPermission]

    @action(detail=True, methods=[HTTPMethod.PUT], permission_classes=[TournamentBracketPermission])
    def validate(self, request, pk=None):
        bracket = get_object_or_404(TournamentBracket, id=pk)
        errors = []

        try:
            if not bracket.validate():
                errors.append("Token not found in tournament description.")
            if not bracket.check_permissions():
                errors.append("Tournament admin access not shared with HiveMind user.")
        except requests.exceptions.HTTPError as err:
            if err.response is not None and err.response.status_code == http.client.NOT_FOUND:
                errors.append("Tournament not found. Check the organization and tournament ID and try again.")
            else:
                errors.append("Error validating tournament: {}".format(err))

        if errors:
            return Response({"success": False, "errors": errors})

        return Response({"success": True})

    @action(detail=True, methods=[HTTPMethod.PUT], url_path="set-teams",
            permission_classes=[TournamentBracketPermission])
    def set_teams(self, request, pk=None):
        bracket = get_object_or_404(TournamentBracket, id=pk)
        errors = []

        if not bracket.check_permissions():
            errors.append("Tournament admin access not shared with HiveMind user.")
        if not bracket.is_valid:
            errors.append("Bracket not yet validated.")

        if errors:
            return Response({"success": False, "errors": errors})

        teams = [
            get_object_or_404(TournamentTeam, id=i, tournament=bracket.tournament)
            for i in request.data.get("teams")
        ]

        bracket.set_teams(teams)

        return Response({"success": True})


class TournamentMatchViewSet(BaseViewSet):
    queryset = TournamentMatch.objects.order_by("id").all()
    serializer_class = TournamentMatchSerializer
    filterset_class = TournamentMatchFilter
    permission_classes = [TournamentMatchPermission]

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)

        match = self.get_object()
        publish_matches.delay(match.bracket.tournament_id)

        return response

    @action(detail=True)
    def stats(self, request, pk=None):
        match = TournamentMatch.objects.get(id=pk)
        if not match.is_complete:
            return Response({"error": "Match is not complete."})

        return Response(match.get_match_stats())

    @action(detail=True, methods=[HTTPMethod.PUT])
    def report(self, request, pk=None):
        match = get_object_or_404(TournamentMatch, id=pk)
        match.report_result()
        return Response({"success": True})


class MatchQueueViewSet(BaseViewSet):
    queryset = MatchQueue.objects.order_by("id")
    serializer_class = MatchQueueSerializer
    filterset_fields = ["tournament_id", "cabinet_id"]
    permission_classes = [MatchQueuePermission]

    def create(self, request):
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)

        queue = self.get_object()
        publish_queue.delay(queue.id)

        return response

    @action(detail=True, methods=[HTTPMethod.PUT], url_path="set-queue")
    def set_queue(self, request, pk=None):
        queue = get_object_or_404(MatchQueue, id=pk)
        matches = [TournamentMatch.objects.get(id=i) for i in request.data.get("matches", [])]

        if any([i.bracket.tournament_id != queue.tournament_id for i in matches]):
            raise BadRequest("Invalid match")

        queue.set_list(matches)
        publish_queue.delay(queue.id)
        queue.tournament.publish_match_state()

        return Response(MatchQueueSerializer(queue).data)


class TournamentTeamViewSet(BaseViewSet):
    queryset = TournamentTeam.objects.order_by("name").all()
    serializer_class = TournamentTeam.serializer()
    filterset_fields = ["tournament_id"]
    permission_classes = [TournamentTeamPermission]


class HomeSceneViewSet(BaseViewSet):
    queryset = HomeScene.objects.order_by("name").all()
    serializer_class = HomeScene.serializer()


class TournamentPlayerViewSet(BaseViewSet):
    queryset = TournamentPlayer.objects.order_by("name").all()
    serializer_class = TournamentPlayerSerializer
    filterset_fields = ["tournament_id", "team_id"]
    permission_classes = [TournamentPlayerPermission]

    @action(detail=True, methods=[HTTPMethod.PUT], url_path="begin-checkout")
    def begin_checkout(self, request, pk=None):
        player = get_object_or_404(TournamentPlayer, id=pk)
        tournament = player.tournament
        payment_options = request.data.get("payment_options")

        if not tournament.accept_payments:
            return Response({ "error": "This tournament is not accepting payments." })
        if not tournament.stripe_account_id:
            return Response({ "error": "Stripe has not yet been configured for this tournament." })
        if not payment_options:
            return Response({ "error": "No payment options selected." })
        if player.paid:
            return Response({ "error": "Registration is already paid." })


        selected_payment = [SelectedPaymentOption.objects.create(
            payment_option=PaymentOption.objects.get(id=po.get("payment_option")),
            quantity=po.get("quantity"),
            custom_price=po.get("custom_price"),
        ) for po in payment_options]

        player.selected_payment_options.set(selected_payment)
        player.save()

        price = int(100 * sum([(i.payment_option.price or i.custom_price) * i.quantity for i in player.selected_payment_options.all()]))

        line_items = [{
            "name": "{} - {}".format(tournament.name, po.payment_option.name),
            "amount": int(100 * (po.payment_option.price or po.custom_price)),
            "currency": "usd",
            "quantity": po.quantity,
        } for po in player.selected_payment_options.all()]

        session = stripe.checkout.Session.create(
            line_items=line_items,
            stripe_account=tournament.stripe_account_id,
            mode="payment",
            success_url=settings.PUBLIC_URL(
                "/tournament/{}/paid".format(tournament.id),
            ),
            cancel_url=settings.PUBLIC_URL(
                "/tournament/{}/register".format(tournament.id),
            ),
        )

        player.price = price
        player.stripe_session_id = session.id
        player.save()
        return Response({"url": session.url})

    @action(detail=True, methods=[HTTPMethod.PUT], url_path="check-payment")
    def check_payment(self, request, pk=None):
        player = get_object_or_404(TournamentPlayer, id=pk)

        if not player.stripe_session_id:
            return Response({ "error": "No session found." })

        session = stripe.checkout.Session.retrieve(
            player.stripe_session_id,
            expand=["line_items"],
            stripe_account=player.tournament.stripe_account_id,
        )


        if not session:
            return Response({ "error": "No session found." })

        if session["status"] == "complete":
            player.paid = True
            player.registration_time = datetime.fromtimestamp(session['created'])
            player.save()

        return Response({})


class VideoViewSet(BaseViewSet):
    queryset = Video.objects.order_by("id").all()
    serializer_class = Video.serializer()
    permission_classes = [VideoPermission]

    @action(detail=False, methods=[HTTPMethod.POST], permission_classes=[VideoCreatePermission])
    def add(self, request):
        game = get_object_or_404(Game, id=request.data.get("game"))
        tournament = get_object_or_404(Tournament, id=request.data.get("tournament"))

        start_time = game.end_time - timedelta_parse(request.data.get("game_end_time")) + timedelta(seconds=10)
        video = Video.objects.create(
            tournament=tournament,
            cabinet=game.cabinet,
            service=Video.VideoSite.YOUTUBE,
            video_id=request.data.get("video_id"),
            start_time=start_time,
            length=timedelta_parse(request.data.get("length")),
        )

        video.tag_matches()
        return Response(Video.serializer()(video).data)
