# Generated by Django 3.2.7 on 2021-12-01 16:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tournament', '0014_auto_20211121_1936'),
    ]

    operations = [
        migrations.AlterField(
            model_name='video',
            name='service',
            field=models.TextField(choices=[('youtube', 'YouTube')], max_length=20, null=True),
        ),
    ]
