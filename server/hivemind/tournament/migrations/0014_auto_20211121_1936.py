# Generated by Django 3.2.7 on 2021-11-21 19:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0014_alter_cabinet_time_zone'),
        ('tournament', '0013_tournamentteam_linked_team_ids'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tournamentmatch',
            name='video_link',
            field=models.URLField(null=True),
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('service', models.TextField(choices=[('youtube', 'YouTube'), ('twitch', 'Twitch')], max_length=20, null=True)),
                ('video_id', models.CharField(max_length=200)),
                ('start_time', models.DateTimeField()),
                ('length', models.DurationField()),
                ('cabinet', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='game.cabinet')),
            ],
            options={
                'unique_together': {('service', 'video_id')},
            },
        ),
    ]
