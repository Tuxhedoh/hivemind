from django.apps import apps
from django.conf import settings
from django.db import models
from rest_framework import serializers, status, viewsets
from rest_framework.authentication import (SessionAuthentication,
                                           TokenAuthentication)
from rest_framework.permissions import (SAFE_METHODS, BasePermission,
                                        IsAuthenticated)


class ReadOnly(BasePermission):
    def has_permission(self, request, view):
        return request.method in SAFE_METHODS


class BaseSerializer(serializers.HyperlinkedModelSerializer):
    pass


class BaseViewSet(viewsets.ModelViewSet):
    permission_classes = [ReadOnly]

    def admin_scene_ids(self):
        Permission = apps.get_model("user", "Permission")
        admin_permissions = Permission.objects.filter(
            user=self.request.user,
            permission=Permission.PermissionType.ADMIN,
        )

        scene_ids = [p.scene_id for p in admin_permissions]
        return scene_ids


class BaseModel(models.Model):
    class Meta:
        abstract = True

    @classmethod
    def default_serializer(cls):
        class Serializer(serializers.ModelSerializer):
            class Meta:
                model = cls
                fields = "__all__"
                ref_name = cls.__name__ + "Serializer"

        Serializer.__name__ = cls.__name__ + "Serializer"
        return Serializer

    @classmethod
    def serializer(cls):
        if not hasattr(cls, "_serializer"):
            cls._serializer = cls.default_serializer()

        return cls._serializer

    @classmethod
    def default_viewset(cls):
        class ViewSet(BaseViewSet):
            queryset = cls.objects.all()
            serializer_class = cls.serializer()

        ViewSet.__name__ = cls.__name__ + "ViewSet"
        return ViewSet

    @classmethod
    def viewset(cls):
        if not hasattr(cls, "_viewset"):
            cls._viewset = cls.default_viewset()

        return cls._viewset


class Deleteable(BaseModel):
    class Meta:
        abstract = True

    deleted = models.BooleanField(default=False)
    deleted_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)
    deleted_at = models.DateTimeField(null=True)
