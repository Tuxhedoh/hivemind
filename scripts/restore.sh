#!/bin/bash

backupdate="$1"
if [ -z "$backupdate" ]; then
    backupdate=$( ls -t ./backups | head -1 )
fi

basedir="/data/backups"
volumes="-v $(pwd)/backups:${basedir}"
outputdir="${basedir}/$( date +%Y%m%d_%H%M%S )"

source app.env
docker-compose run $volumes --rm -e "PGPASSWORD=${POSTGRES_PASSWORD}" db \
               psql -h db --user postgres --dbname postgres \
               -c 'DROP SCHEMA public CASCADE; CREATE SCHEMA public;'
docker-compose run $volumes --rm -e "PGPASSWORD=${POSTGRES_PASSWORD}" db \
               pg_restore -h db --user postgres --dbname postgres \
               --format directory --no-acl --no-owner "${basedir}/${backupdate}"

# prod:
# sudo -u postgres psql hivemind -c 'DROP SCHEMA public CASCADE; CREATE SCHEMA public;'
# sudo -u postgres pg_restore -v -d hivemind --format directory "${basedir}/$1"
