DO $$
DECLARE maxid int;
DECLARE row record;
BEGIN
    INSERT INTO hivemind_user(account_id, email)
    SELECT account_id, email
    FROM kqb_adminuser
    WHERE account_id NOT IN (SELECT account_id FROM hivemind_user);

    INSERT INTO hivemind_user_scenes(user_id, scene_id)
    SELECT u.id, s.id
    FROM hivemind_user u, hivemind_scene s
    WHERE u.account_id IN (SELECT account_id FROM kqb_adminuser)
    AND s.name IN ('BMORE', 'PHIL')
    AND NOT EXISTS (SELECT 1 FROM hivemind_user_scenes WHERE user_id = u.id AND scene_id = s.id);

    INSERT INTO hivemind_season(id, name, start_date, end_date, drop_lowest_scores, match_count_base, scene_id, season_type)
    SELECT id, name, start_date, end_date, drop_lowest_scores, match_count_base, 1, 'shuffle'
    FROM kqb_season;

    SELECT coalesce(max(id), 0) FROM hivemind_season INTO maxid;
    EXECUTE 'ALTER SEQUENCE hivemind_season_id_seq RESTART WITH ' || maxid+1;

    INSERT INTO hivemind_event(id, event_date, is_complete, is_active, is_quickplay, points_per_round, points_per_match, rounds_per_match,
        qp_matches_per_player, cabinet_id, season_id)
    SELECT id, event_date, is_complete, is_active, is_quickplay, points_per_round, points_per_match, rounds_per_match,
        qp_matches_per_player, cabinet_id, season_id
    FROM kqb_event;

    SELECT coalesce(max(id), 0) FROM hivemind_event INTO maxid;
    EXECUTE 'ALTER SEQUENCE hivemind_event_id_seq RESTART WITH ' || maxid+1;

    INSERT INTO hivemind_team(id, name, byoteam_id, event_id)
    SELECT id, name, byoteam_id, event_id
    FROM kqb_team;

    SELECT coalesce(max(id), 0) FROM hivemind_team INTO maxid;
    EXECUTE 'ALTER SEQUENCE hivemind_team_id_seq RESTART WITH ' || maxid+1;

    INSERT INTO hivemind_player(id, name, scene_id)
    SELECT id, name, 1
    FROM kqb_player;

    SELECT coalesce(max(id), 0) FROM hivemind_player INTO maxid;
    EXECUTE 'ALTER SEQUENCE hivemind_player_id_seq RESTART WITH ' || maxid+1;

    INSERT INTO hivemind_team_players(id, team_id, player_id)
    SELECT id, team_id, player_id
    FROM kqb_team_players;

    SELECT coalesce(max(id), 0) FROM hivemind_team_players INTO maxid;
    EXECUTE 'ALTER SEQUENCE hivemind_team_players_id_seq RESTART WITH ' || maxid+1;

    INSERT INTO hivemind_match(id, blue_score, gold_score, is_complete, blue_team_id, gold_team_id, event_id)
    SELECT id, blue_score, gold_score, is_complete, blue_team_id, gold_team_id, event_id
    FROM kqb_match;

    SELECT coalesce(max(id), 0) FROM hivemind_match INTO maxid;
    EXECUTE 'ALTER SEQUENCE hivemind_match_id_seq RESTART WITH ' || maxid+1;

    INSERT INTO hivemind_qpmatch(id, blue_score, gold_score, is_complete, event_id)
    SELECT id, blue_score, gold_score, is_complete, event_id
    FROM kqb_qpmatch;

    SELECT coalesce(max(id), 0) FROM hivemind_qpmatch INTO maxid;
    EXECUTE 'ALTER SEQUENCE hivemind_qpmatch_id_seq RESTART WITH ' || maxid+1;

    INSERT INTO hivemind_qpplayer(id, team, player_id, qp_match_id)
    SELECT id, team, player_id, qp_match_id
    FROM kqb_qpplayer;

    SELECT coalesce(max(id), 0) FROM hivemind_qpplayer INTO maxid;
    EXECUTE 'ALTER SEQUENCE hivemind_qpplayer_id_seq RESTART WITH ' || maxid+1;

    FOR row IN SELECT k.id AS k_id, h.id AS h_id, k.match_id, k.qp_match_id
    FROM kqb_game k
      JOIN hivemind_game h ON (k.start_time = h.start_time and k.end_time = h.end_time and k.win_condition = h.win_condition and k.winning_team = h.winning_team)
    WHERE coalesce(k.match_id, k.qp_match_id) IS NOT NULL
    LOOP
        UPDATE hivemind_game SET match_id = row.match_id, qp_match_id = row.qp_match_id WHERE id = row.h_id;
    END LOOP;

END;
$$ LANGUAGE plpgsql;
