#!/bin/bash

for domain in kqhivemind.com baltimorekillerqueen.com; do
    /usr/bin/letsencrypt certonly -n --webroot --webroot-path /data/certbotroot --renew-by-default -d "${domain},www.${domain}" -m contact@kqhivemind.com --agree-tos
done

rsync -avrL /etc/letsencrypt/live/* /data/certs

cd /data/hivemind
/usr/bin/docker-compose exec web nginx -s reload

/usr/bin/letsencrypt certonly -n --webroot --webroot-path /data/certbotroot --renew-by-default -d mail.kqhivemind.com -m contact@kqhivemind.com --agree-tos
systemctl restart dovecot
systemctl restart postfix

cd /etc/letsencrypt/live
for dir in *; do
    find "$dir" -type f,l | xargs -I{} /usr/local/bin/b2 upload-file kqhivemind-certs "{}" "{}"
done

