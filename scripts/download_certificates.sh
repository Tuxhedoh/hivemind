#!/bin/bash
b2="/usr/local/bin/b2"

for dir in $( "${b2}" ls kqhivemind-certs ); do
    domain=$( basename "$dir" )
    mkdir -p "/data/certs/${domain}"
    for fullpath in $( "b2" ls kqhivemind-certs "${dir}" ); do
        fn=$( basename "$fullpath" )
        "${b2}" download-file-by-name kqhivemind-certs "$fullpath" "/data/certs/${domain}/${fn}"
    done
done
