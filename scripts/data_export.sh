#!/bin/bash
source app.env

basedir="/data"
volumes="-v $(pwd):${basedir}"

if [ -z "$2" ]; then
    outputdir="export_$( date +%Y%m%d_%H%M%S )"
    outputfile="export_$( date +%Y%m%d_%H%M%S ).zip"
else
    outputdir="export_$( date +%Y%m%d_%H%M%S )_$2"
    outputfile="export_$( date +%Y%m%d_%H%M%S )_$2.zip"
fi

mkdir "$outputdir"

docker-compose run $volumes --rm -e "PGPASSWORD=${POSTGRES_PASSWORD}" db \
               psql -h db --user postgres --dbname postgres \
               -c "\\COPY (SELECT g.id, start_time, end_time, win_condition, winning_team, map_name,
                                player_count, cabinet_id, c.name AS cabinet_name,
                                tournament_match_id, ttb.name AS blue_team, ttg.name AS gold_team
                         FROM game_game g
                           JOIN game_cabinet c ON (g.cabinet_id = c.id)
                           JOIN game_scene s ON (c.scene_id = s.id)
                           LEFT JOIN tournament_tournamentmatch tm ON (g.tournament_match_id = tm.id)
                           LEFT JOIN tournament_tournamentbracket tb ON (tm.bracket_id = tb.id)
                           LEFT JOIN tournament_tournament t ON (tb.tournament_id = t.id)
                           LEFT JOIN tournament_tournamentteam ttb ON (tm.blue_team_id = ttb.id)
                           LEFT JOIN tournament_tournamentteam ttg ON (tm.gold_team_id = ttg.id)
                         WHERE $1
                           AND EXISTS (SELECT 1 FROM game_gameevent ge1 WHERE g.id = ge1.game_id AND ge1.event_type = 'gamestart')
                           AND EXISTS (SELECT 1 FROM game_gameevent ge2 WHERE g.id = ge2.game_id AND ge2.event_type = 'victory')
                         )
                   TO '${basedir}/${outputdir}/game.csv' DELIMITER ',' CSV HEADER"

docker-compose run $volumes --rm -e "PGPASSWORD=${POSTGRES_PASSWORD}" db \
               psql -h db --user postgres --dbname postgres \
               -c "\\COPY (SELECT ge.id, timestamp, event_type, values, game_id
                         FROM game_gameevent ge
                           JOIN game_game g ON (ge.game_id = g.id)
                           JOIN game_cabinet c ON (g.cabinet_id = c.id)
                           JOIN game_scene s ON (c.scene_id = s.id)
                           LEFT JOIN tournament_tournamentmatch tm ON (g.tournament_match_id = tm.id)
                           LEFT JOIN tournament_tournamentbracket tb ON (tm.bracket_id = tb.id)
                           LEFT JOIN tournament_tournament t ON (tb.tournament_id = t.id)
                           LEFT JOIN tournament_tournamentteam ttb ON (tm.blue_team_id = ttb.id)
                           LEFT JOIN tournament_tournamentteam ttg ON (tm.gold_team_id = ttg.id)
                         WHERE $1
                           AND EXISTS (SELECT 1 FROM game_gameevent ge1 WHERE g.id = ge1.game_id AND ge1.event_type = 'gamestart')
                           AND EXISTS (SELECT 1 FROM game_gameevent ge2 WHERE g.id = ge2.game_id AND ge2.event_type = 'victory')
                         )
                   TO '${basedir}/${outputdir}/gameevent.csv' DELIMITER ',' CSV HEADER"

docker-compose run $volumes --rm -e "PGPASSWORD=${POSTGRES_PASSWORD}" db \
               psql -h db --user postgres --dbname postgres \
               -c "\\COPY (SELECT ug.id, ug.game_id, ug.player_id AS position_id, ug.user_id, u.name, u.scene
                         FROM user_usergame ug
                           JOIN user_user u ON (ug.user_id = u.id)
                           JOIN game_game g ON (ug.game_id = g.id)
                         WHERE $1
                           AND EXISTS (SELECT 1 FROM game_gameevent ge1 WHERE g.id = ge1.game_id AND ge1.event_type = 'gamestart')
                           AND EXISTS (SELECT 1 FROM game_gameevent ge2 WHERE g.id = ge2.game_id AND ge2.event_type = 'victory')
                         )
                   TO '${basedir}/${outputdir}/usergame.csv' DELIMITER ',' CSV HEADER"

zip -r "$outputfile" "$outputdir"
