import { Link, Paper, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { format, parseISO } from 'date-fns';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import React from 'react';

const useStyles = makeStyles(theme => ({
  container: {
    padding: theme.spacing(2),
  },
  title: {
    fontWeight: 'bold',
  },
  info: {
    fontSize: '12px',
    color: theme.palette.grey['500'],
    fontStyle: 'italic',
  },
}));

export default function Achievement({ achievement }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.container, 'flex flex-col justify-between rounded bg-gray-900 text-white')}>
      <Typography variant="h4" className={clsx(classes.title, 'text-lg')}>{achievement.title}</Typography>
      <Typography className={clsx(classes.description, 'text-sm flex-grow text-gray-300')}>{achievement.description}</Typography>
      <Typography className={clsx(classes.info,'mt-2')} suppressHydrationWarning>
        Earned on {format(parseISO(achievement.timestamp), 'MMM d, yyyy')} in <Link href={`/game/${achievement.game}/`}>Game {achievement.game}</Link>
      </Typography>
    </div>
  );
}

Achievement.propTypes = {
  achievement: PropTypes.shape({
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    timestamp: PropTypes.string.isRequired,
    game: PropTypes.number.isRequired,
  }).isRequired,
};
