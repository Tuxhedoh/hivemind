import { Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Card, CircularProgress, Grid, Menu, MenuItem, Typography } from '@material-ui/core';
import { Droppable } from 'react-beautiful-dnd';
import { bindMenu, bindTrigger, usePopupState } from 'material-ui-popup-state/hooks';
import MenuIcon from '@material-ui/icons/Menu';
import clsx from 'clsx';
import { toast } from 'react-toastify';

import { getAxios } from 'util/axios';
import { TOURNAMENT_LINK_TYPES } from 'util/constants';
import { useTournamentAdmin } from 'providers/TournamentAdmin';
import TournamentQueueMatch from 'components/TournamentQueueMatch';

const useStyles = makeStyles(theme => ({
  card: {
    backgroundColor: 'white',
  },
  row: {
    padding: theme.spacing(1, 2),
    alignItems: 'center',
    '&:first-child': {
      borderWidth: 0,
    },
  },
  stageNameRow: {
    display: 'flex',
    justifyContent: 'space-between',
    backgroundColor: theme.palette.gold.light3,
    '.online &': {
      backgroundColor: theme.palette.gold.light3,
    },
  },
  stageName: {
    fontWeight: 'bold',
  },
  menuIcon: {
    cursor: 'pointer',
  },
}));

export function AssignAllMenu({ bracket, menuState, filteredMatches }) {
  const axios = getAxios({ authenticated: true });
  const classes = useStyles();
  const { cabinets, matches, queues, setQueue } = useTournamentAdmin();

  const assignAll = cabinetId => {
    const queue = queues.filter(q => q.cabinet === cabinetId).shift();
    queue.matchList = queue.matchList.concat(filteredMatches);

    axios
      .put(`/api/tournament/queue/${queue.id}/set-queue/`, {
        matches: queue.matchList.map(m => m.id),
      })
      .then(response => setQueue(response.data))
      .catch(err => toast.error('Could not assign matches to queue.'));

    setQueue(queue);
    menuState.close();
  };

  return (
    <Menu
      getContentAnchorEl={null}
      anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
      transformOrigin={{ vertical: 'top', horizontal: 'center' }}
      className={classes.menu}
      {...bindMenu(menuState)}
    >
      <MenuItem className={classes.menuItem} onClick={() => assignAll(null)}>
        Assign All to First Available
      </MenuItem>
      {cabinets.map(cabinet => (
        <MenuItem
          key={cabinet.id}
          className={classes.menuItem}
          onClick={() => assignAll(cabinet.id)}
        >
          Assign All to {cabinet.displayName}
        </MenuItem>
      ))}
    </Menu>
  );
}

export function TournamentQueueBracket({ bracket, queuedIds }) {
  const menuState = usePopupState({ popupId: `assign-all-menu-${bracket.id}` });
  const classes = useStyles();
  const { tournament, matches } = useTournamentAdmin();

  if (matches === null) return <></>;
  const filteredMatches = matches.filter(
    m => m.bracket === bracket.id && !queuedIds.has(m.id) && !m.activeCabinet && !m.isComplete,
  );

  if (tournament.linkType == TOURNAMENT_LINK_TYPES.CHALLONGE) {
    filteredMatches.sort((a, b) => parseInt(a.linkedMatchId) - parseInt(b.linkedMatchId));
  }

  if (tournament.linkType == TOURNAMENT_LINK_TYPES.MANUAL) {
    filteredMatches.sort((a, b) => a.id - b.id);
  }

  return (
    <Fragment key={bracket.id}>
      <Grid key={bracket.id} item className={clsx(classes.row, classes.stageNameRow)}>
        <Typography className={classes.stageName}>{bracket.name}</Typography>
        <MenuIcon className={classes.menuIcon} {...bindTrigger(menuState)} />
        <AssignAllMenu menuState={menuState} bracket={bracket} filteredMatches={filteredMatches} />
      </Grid>

      {filteredMatches.map((match, idx) => (
        <TournamentQueueMatch key={match.id} className={classes.row} match={match} index={idx} />
      ))}
    </Fragment>
  );
}

export default function TournamentQueueMatchList({}) {
  const classes = useStyles();
  const { tournament, queues } = useTournamentAdmin();
  const queuedIds = new Set();

  if (queues === null) {
    return <CircularProgress />;
  }

  for (const queue of queues) {
    for (const match of queue.matchList) {
      queuedIds.add(match.id);
    }
  }

  return (
    <Grid item>
      <Card className={classes.card}>
        <Droppable droppableId="unassigned">
          {(provided, snapshot) => (
            <Grid
              container
              ref={provided.innerRef}
              direction="column"
              className={classes.container}
            >
              {tournament?.brackets.map(bracket => (
                <TournamentQueueBracket key={bracket.id} bracket={bracket} queuedIds={queuedIds} />
              ))}
              {provided.placeholder}
            </Grid>
          )}
        </Droppable>
      </Card>
    </Grid>
  );
}
