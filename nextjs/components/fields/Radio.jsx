import React, { useContext } from 'react';
import { FormControl, FormLabel, FormControlLabel, FormHelperText,
         Radio as MuiRadio, RadioGroup as MuiRadioGroup } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import { useForm } from '../forms/Form';

export const RadioContext = React.createContext({});

const useStyles = makeStyles(theme => ({
  radioGroup: {
    display: 'flex',
    flexDirection: 'row',
    gap: '24px'
  },
  formLabel: {
    color: theme.palette.text.primary,
  },
  formControl: {
    flexBasis: 0,
    flexGrow: 1,
    margin: theme.spacing(1.5, 0),
    padding: theme.spacing(1),
    border: `1px solid ${theme.palette.divider}`,
    borderRadius: '4px',
    cursor: 'pointer',
    '&.selected': {
      backgroundColor: theme.palette.highlight.main,
    },
  },
  label: {
    margin: theme.spacing(0, 0, 1, 0),
    textTransform: 'uppercase',
    '& .MuiTypography-root': {
      fontSize: '.875rem',
    },
    '&.selected .MuiTypography-root': {
      fontWeight: 'bold',
    },
  },
  radio: {
    display: 'none',
  },
}));

export function RadioGroup({ name, label, children, error, fullWidth, helperText, ...props }) {
  const classes = useStyles();

  return (
    <FormControl fullWidth={fullWidth} error={error}>
      <FormLabel className={classes.formLabel}>{label}</FormLabel>
      <FormHelperText>{helperText}</FormHelperText>

      <MuiRadioGroup className={classes.radioGroup} name={name} {...props}>
        <RadioContext.Provider value={name}>
          {children}
        </RadioContext.Provider>
      </MuiRadioGroup>
    </FormControl>
  );
}

RadioGroup.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  error: PropTypes.bool,
  fullWidth: PropTypes.bool,
  helperText: PropTypes.string,
};

RadioGroup.defaultProps = {
  children: (<></>),
  error: false,
  fullWidth: true,
  helperText: '',
};

export function Radio({ value, children, fullWidth, helperText, ...props }) {
  const classes = useStyles();
  const formik = useForm();
  const radioGroupName = useContext(RadioContext);
  const selected = formik.values[radioGroupName] == value;

  const onClick = () => {
    formik.setFieldValue(radioGroupName, value);
  };

  const control = (
    <MuiRadio className={clsx(classes.radio, { selected })} />
  );

  return (
    <FormControl className={clsx(classes.formControl, { selected })} onClick={onClick}>
      <FormControlLabel value={value} className={clsx(classes.label, { selected })} control={control} {...props} />
      <FormHelperText className={classes.helperText}>{helperText}</FormHelperText>
    </FormControl>
  );
}

