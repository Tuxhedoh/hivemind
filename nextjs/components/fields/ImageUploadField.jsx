import { Button, FormControl, FormHelperText, InputLabel } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import UserIcon from '@material-ui/icons/Person';
import { useForm } from 'components/forms/Form';
import { useState } from 'react';

const useStyles = makeStyles(theme => ({
  label: {
    transform: 'translate(12px, 10px) scale(0.75)',
  },
  button: {
    cursor: 'pointer',
  },
  input: {
    display: 'none',
  },
  inputContainer: {
    borderRadius: '4px',
    paddingTop: theme.spacing(4),
    minHeight: '40px',
    textAlign: 'center',
  },
  imageContainer: {
    borderRadius: '4px',
    border: `1px solid ${theme.palette.divider}`,
    marginBottom: theme.spacing(2),
  },
  image: {
    maxWidth: `calc(100% - ${theme.spacing(4)}px)`,
    margin: theme.spacing(2),
    height: 'auto',
  },
}));
export const imageURLRegex =
  /^(?:([a-z0-9+.-]+):\/\/)(?:\S+(?::\S*)?@)?(?:(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/;

export default function ImageUploadField({ name, label, helperText, required }) {
  const classes = useStyles();
  const formik = useForm();

  const [imageURL, setImageURL] = useState(formik?.values[name]);

  const uploadFile = e => {
    if (e.target.files.length === 0) return;

    const reader = new FileReader();
    reader.readAsDataURL(e.target.files[0]);

    reader.onloadend = e => {
      setImageURL(reader.result);
      formik.setFieldValue(name, reader.result);
    };
  };

  const imageStyles = imageURL ? { backgroundImage: `url(${imageURL})` } : {};
  return (
    <FormControl fullWidth>
      <div className="flex md:items-center md:gap-8 md:flex-row flex-col ">
        <div>
          <div className="space-y-2">
            <InputLabel variant="standard" className=" text-black relative m-0 p-0 translate-y-0">
              {label}
              {required && (
                <span className="text-red-500">
                  {' '}
                  * <small>Required</small>
                </span>
              )}
            </InputLabel>
            <FormHelperText className={classes.helperText}>{helperText}</FormHelperText>

            <Button
              variant="outlined"
              color="secondary"
              size="small"
              component="label"
              className={classes.button}
              onClick={() => {
                formik.setFieldTouched(name);
              }}
            >
              {imageURL ? 'Change' : 'Upload a File'}
              <input
                name={name}
                type="file"
                accept="image/*"
                className={classes.input}
                onChange={uploadFile}
              />
            </Button>

            {formik.touched[name] && formik.errors[name] && (
              <FormHelperText className="text-red-500">{formik.errors[name]}</FormHelperText>
            )}
          </div>
        </div>
        <div
          className="flex-shrink-0 -order-1 md:order-2 border-[4px] border-solid border-gray-300 bg-gray-100 h-36 w-36 rounded-full flex justify-center items-center mx-auto mb-4 overflow-hidden relative bg-cover"
          style={imageStyles}
        >
          {!Boolean(imageURL) && <UserIcon className="opacity-40 h-24 w-24" />}
        </div>
      </div>
    </FormControl>
  );
}
