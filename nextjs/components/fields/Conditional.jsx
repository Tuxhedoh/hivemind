import React, { useState, useEffect } from 'react';
import { useForm } from '../forms/Form';

export default function Conditional({ test, children }) {
  const formik = useForm();
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    setVisible(test(formik.values));
  }, [formik.values]);

  return (
    <>
      {visible && (
        <>
          {children}
        </>
      )}
    </>
  );
}
