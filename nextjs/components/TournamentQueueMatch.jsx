import { Grid, IconButton } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Icon from '@mdi/react';
import { mdiPlaylistPlay } from '@mdi/js';
import { Draggable } from 'react-beautiful-dnd';
import clsx from 'clsx';

import { getAxios } from 'util/axios';
import { useTournamentAdmin } from 'providers/TournamentAdmin';

const useStyles = makeStyles(theme => ({
  row: {},
}));

export default function TournamentQueueMatch({ match, cabinet, className, index }) {
  const axios = getAxios({ authenticated: true });
  const classes = useStyles();
  const { tournament, teamsById } = useTournamentAdmin();

  const selectNext = () => {
    axios.put(`/api/tournament/tournament/${tournament.id}/select-next/`, { cabinet: cabinet.id });
  };

  const getTeamName = teamId => (teamId ? teamsById[teamId].name : '(unknown team)');

  if (!match?.id) {
    return (
      <Grid
        item
        container
        direction="row"
        justifyContent="space-between"
        className={clsx(classes.row, classes.noMatch, className)}
      >
        No current match
        <IconButton onClick={selectNext} size="small">
          <Icon path={mdiPlaylistPlay} size={1} />
        </IconButton>
      </Grid>
    );
  }

  return (
    <Draggable draggableId={`${match.id}`} index={index}>
      {(provided, snapshot) => (
        <Grid
          item
          ref={provided.innerRef}
          className={clsx(classes.row, className)}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          {match.roundName}: {getTeamName(match.blueTeam)} vs. {getTeamName(match.goldTeam)}
          {match.activeCabinet && (
            <>
              {' '}
              ({match.blueScore}-{match.goldScore})
            </>
          )}
        </Grid>
      )}
    </Draggable>
  );
}
