import React from 'react';
import { useRouter } from 'next/router';
import { makeStyles } from '@material-ui/core/styles';
import { Avatar, Link } from '@material-ui/core';
import Icon from '@mdi/react';
import { mdiAccountCircle } from '@mdi/js';
import { MessageListIcon } from './Message';

import { UserConsumer } from 'util/auth';
import LoginButtons from 'components/LoginButtons';

const useStyles = makeStyles(theme => ({
  avatar: {
    height: '32px',
    width: '32px',
  },
}));

const UserLoggedIn = UserConsumer(({ user }) => {
  const router = useRouter();
  const classes = useStyles();

  const profileText = user?.name ? user.name : 'My Profile';
  return (
    <div className="flex gap-4 justify-end items-center">
      <MessageListIcon className="text-white" />
      {user?.id ? (
        <span>
          {user?.name && <>Hi,&nbsp;</>}
          <Link
            className="text-white inline-flex gap-2 items-center font-semibold"
            href={`/user/${user.id}`}
          >
            {profileText}
            {user?.image && <Avatar src={user.image} alt={user.name} className={classes.avatar} />}
            {!user?.image && <Icon path={mdiAccountCircle} className="w-6 h-6" />}
          </Link>
        </span>
      ) : (
        <LoginButtons onSuccess={router.reload} />
      )}
    </div>
  );
});

export default UserLoggedIn;
