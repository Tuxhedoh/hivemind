import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Grid, Paper, List, ListItem } from '@material-ui/core';
import Icon from '@mdi/react';
import { mdiHexagonMultipleOutline } from '@mdi/js';
import Carousel from 'react-material-ui-carousel';
import clsx from 'clsx';
import { Pie } from 'react-chartjs-2';

const useStyles = makeStyles(theme => ({
  recapItem: {
    padding: theme.spacing(2),
    margin: '0 auto',
    maxWidth: '620px',
    height: '550px',
    background: `linear-gradient(150deg, ${theme.palette.gold.light5}, ${theme.palette.gold.light4})`,
  },
  itemContainer: {
    maxHeight: '534px',
  },
  largeText: {
    fontSize: '24px',
    '& b': {
      fontSize: '36px',
      fontWeight: 'bold',
    },
    marginBottom: theme.spacing(2),
    textAlign: 'center',
  },
  subItem: {
    fontSize: '16px',
    '& b': {
      fontSize: '18px',
    },
  },
  subItemIcon: {
    verticalAlign: 'middle',
    marginRight: theme.spacing(0.5),
    color: theme.palette.text.secondary,
  },
  listTitle: {
    textAlign: 'center',
    fontSize: '18px',
    fontWeight: 'bold',
    marginTop: theme.spacing(1),
    paddingTop: theme.spacing(1),
    borderTop: `1px solid ${theme.palette.divider}`,
  },
  list: {
    maxWidth: '520px',
    margin: '0 auto',
    display: 'flex',
    flexWrap: 'wrap',
  },
  listItem: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  listItemName: {
    fontSize: '16px',
    lineHeight: '24px',
  },
  listItemValue: {
    fontSize: '16px',
    fontWeight: 'bold',
    textAlign: 'right',
  },
  chartContainer: {
    height: '400px',
    display: 'flex',
    flexDirection: 'column',
  },
  chart: {
    flexGrow: 1,
  },
}));

export function Recap({ title, children }) {
  return (
    <>
      <Typography variant="h1" element="h1" className="flex-grow">
        {title}
      </Typography>

      <Carousel autoPlay={false}>{children}</Carousel>
    </>
  );
}

export function RecapItem({ children }) {
  const classes = useStyles();

  return (
    <Paper className={classes.recapItem}>
      <Grid container className={classes.itemContainer} spacing={2}>
        {children}
      </Grid>
    </Paper>
  );
}

export function RecapMainItem({ children }) {
  const classes = useStyles();

  return (
    <Grid item xs={12} className={classes.gridItem}>
      <Typography variant="h3" className={classes.largeText}>
        {children}
      </Typography>
    </Grid>
  );
}

export function RecapSubItem({ children }) {
  const classes = useStyles();

  return (
    <Grid item xs={12} className={classes.gridItem}>
      <Typography className={classes.subItem}>
        <Icon path={mdiHexagonMultipleOutline} size={1} className={classes.subItemIcon} />
        {children}
      </Typography>
    </Grid>
  );
}

export function RecapList({ title, children, ...params }) {
  const classes = useStyles();

  return (
    <Grid item xs={12} className={classes.gridItem}>
      <Typography className={classes.listTitle}>{title}</Typography>
      <List className={classes.list} {...params}>
        {children}
      </List>
    </Grid>
  );
}

export function RecapListItem({ name, value, backgroundColor, ...params }) {
  const classes = useStyles();

  return (
    <ListItem className={classes.listItem} {...params}>
      <Typography className={classes.listItemName}>{name}</Typography>
      <Typography className={classes.listItemValue}>{value.toLocaleString()}</Typography>
    </ListItem>
  );
}

export function RecapPieChart({ title, data, getLabel, getValue, getColor }) {
  const classes = useStyles();

  const options = {
    title: {
      display: false,
    },
    animation: { duration: 0 },
    maintainAspectRatio: false,
    plugins: {
      legend: {
        position: 'right',
      },
    },
    segmentShowStroke: false,
  };

  const chartData = {
    labels: data.map(getLabel),
    datasets: [
      {
        data: data.map(getValue),
        borderColor: '#111111',
        borderWidth: 0.5,
        backgroundColor: data.map(getColor),
      },
    ],
  };

  return (
    <Grid item xs={12} className={clsx(classes.gridItem, classes.chartContainer)}>
      <Typography className={classes.listTitle}>{title}</Typography>
      <Grid item className={classes.chart}>
        <Pie options={options} data={chartData} />
      </Grid>
    </Grid>
  );
}
