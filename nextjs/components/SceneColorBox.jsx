import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles(theme => ({
  svg: {
    height: '1.5em',
    width: '1.5em',
    verticalAlign: 'middle',
  },
}));

export default function SceneColorBox({ color }) {
  const classes = useStyles();

  return (
    <svg
      id="Layer_2"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 60 60"
      className={classes.svg}
    >
      <g id="Layer_1-2">
        <polygon points="15,4 45,4 60,30 45,56 15,56 0,30" fill={color} />
      </g>
    </svg>
  );
}
