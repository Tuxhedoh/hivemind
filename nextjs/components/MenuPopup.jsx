import React, { useState } from 'react';
import { useRouter } from 'next/router';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  CircularProgress,
  Divider,
  Link,
  Menu,
  MenuItem as MuiMenuItem,
} from '@material-ui/core';
import { bindMenu } from 'material-ui-popup-state/hooks';
import Icon from '@mdi/react';
import { mdiPatreon, mdiInformation } from '@mdi/js';

import { onLoginSuccess, logOut, UserConsumer } from 'util/auth';
import LoginButtons from 'components/LoginButtons';

const useStyles = makeStyles(theme => ({
  menu: {},
  item: {},
  link: {
    color: 'black',
    padding: theme.spacing(1, 4),
  },
  linkIcon: {
    verticalAlign: 'middle',
    marginRight: theme.spacing(1),
    color: theme.palette.text.secondary,
  },
  loginButtons: {
    padding: theme.spacing(1, 4),
  },
}));

const MenuPopup = UserConsumer(({ user, menuState }) => {
  const classes = useStyles();
  const router = useRouter();

  const onLogoutClick = evt => {
    evt.preventDefault();
    logOut();
    router.reload();
  };

  const MenuItem = ({ href, onClick, icon, children }) => {
    return (
      <MuiMenuItem className={classes.item}>
        <Link className={classes.link} href={href} onClick={onClick}>
          {icon && <Icon className={classes.linkIcon} path={icon} size={1} />}

          {children}
        </Link>
      </MuiMenuItem>
    );
  };

  return (
    <Menu
      getContentAnchorEl={null}
      anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
      transformOrigin={{ vertical: 'top', horizontal: 'center' }}
      className={classes.menu}
      {...bindMenu(menuState)}
    >
      {user?.id && (
        <MenuItem href="#" onClick={onLogoutClick}>
          Log Out
        </MenuItem>
      )}

      {!user?.id && (
        <MuiMenuItem className={classes.item}>
          <Box className={classes.loginButtons}>
            <LoginButtons onSuccess={router.reload} />
          </Box>
        </MuiMenuItem>
      )}

      <Divider />

      {user?.id && <MenuItem href={`/user/${user.id}`}>User Profile</MenuItem>}

      <MenuItem href="/about">About HiveMind</MenuItem>
      <MenuItem href="/scene-request">Register a New Scene</MenuItem>
      <MenuItem href="/wiki">HiveMind Wiki</MenuItem>
      <MenuItem href="/privacy">Privacy Policy</MenuItem>
      <MenuItem href="https://www.patreon.com/kqhivemind" icon={mdiPatreon}>
        Support HiveMind
      </MenuItem>
    </Menu>
  );
});

export default MenuPopup;
