import PropTypes from 'prop-types';

import Markdown from './Markdown';

export default function WikiMarkdown({ contents, ...props }) {
  const newContents = contents.replace(
    /\[\[([\w\s]+)\]\]/g,
    (match, title) => `[${title}](${title.replace(/\s+/g, '_')})`,
  );

  return (
    <Markdown contents={newContents} {...props} />
  );
}

WikiMarkdown.propTypes = {
  contents: PropTypes.string.isRequired,
};

WikiMarkdown.defaultProps = {
};
