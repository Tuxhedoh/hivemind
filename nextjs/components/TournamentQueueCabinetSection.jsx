import { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Card, Grid, Switch, Typography } from '@material-ui/core';
import { Droppable } from 'react-beautiful-dnd';
import clsx from 'clsx';

import { getAxios } from 'util/axios';
import { useTournamentAdmin } from 'providers/TournamentAdmin';
import TournamentQueueMatch from 'components/TournamentQueueMatch';

const useStyles = makeStyles(theme => ({
  card: {
    backgroundColor: 'white',
  },
  row: {
    padding: theme.spacing(1, 2),
    alignItems: 'center',
    '&:first-child': {
      borderWidth: 0,
    },
  },
  cabinetNameRow: {
    display: 'flex',
    justifyContent: 'space-between',
    backgroundColor: theme.palette.gold.light3,
    '.online &': {
      backgroundColor: theme.palette.gold.light3,
    },
  },
  cabinetName: {
    fontWeight: 'bold',
  },
  activeMatch: {
    '&.active': {
      fontWeight: 'bold',
    },
    '&:not(.active)': {
      fontStyle: 'italic',
      color: theme.palette.grey['800'],
    },
  },
  activeMatchContainer: {
    backgroundColor: theme.palette.grey['300'],
    marginBottom: theme.spacing(2),
  },
  queuedMatches: {
    minHeight: theme.spacing(2),
  },
}));

export default function TournamentQueueCabinetSection({ cabinet }) {
  const axios = getAxios({ authenticated: true });
  const classes = useStyles();
  const { tournament, queues, matches } = useTournamentAdmin();
  const [isLoading, setIsLoading] = useState(false);

  const queue = queues?.filter(q => q.cabinet === (cabinet?.id ?? null)).shift();
  const activeMatch = cabinet?.id && matches?.filter(m => m.activeCabinet === cabinet.id).shift();

  if (queue === undefined) return <></>;

  const toggleEnabled = () => {
    setIsLoading(true);
    axios.patch(`/api/tournament/queue/${queue.id}/`, { enabled: !queue.enabled }).then(() => {
      setIsLoading(false);
    });
  };

  return (
    <Grid item>
      <Card className={classes.card}>
        <Grid container direction="column" className={classes.container}>
          <Grid item className={clsx(classes.row, classes.cabinetNameRow)}>
            <Typography className={classes.cabinetName}>
              {cabinet?.displayName ?? 'First Available'}
            </Typography>
            <Switch
              className={classes.queueSwitch}
              disabled={isLoading}
              checked={queue?.enabled ?? false}
              onChange={toggleEnabled}
            />
          </Grid>

          {cabinet && (
            <Droppable
              droppableId={`cabinet-${cabinet.id}`}
              isDropDisabled={Boolean(activeMatch?.id)}
            >
              {(provided, snapshot) => (
                <Grid
                  item
                  container
                  direction="column"
                  className={classes.activeMatchContainer}
                  ref={provided.innerRef}
                >
                  <TournamentQueueMatch
                    match={activeMatch}
                    cabinet={cabinet}
                    index={0}
                    className={clsx(classes.row, classes.activeMatch, { active: activeMatch })}
                  />
                  {provided.placeholder}
                </Grid>
              )}
            </Droppable>
          )}

          <Droppable droppableId={`queue-${queue.id}`}>
            {(provided, snapshot) => (
              <Grid item className={classes.queuedMatches} ref={provided.innerRef}>
                {queue?.matchList?.map((match, idx) => (
                  <TournamentQueueMatch
                    key={match.id}
                    match={match}
                    index={idx}
                    className={clsx(classes.row, classes.queuedMatch)}
                  />
                ))}
                {provided.placeholder}
              </Grid>
            )}
          </Droppable>
        </Grid>
      </Card>
    </Grid>
  );
}
