import { Scatter } from 'react-chartjs-2';
import { Typography } from '@material-ui/core';
import cloneDeep from 'lodash/cloneDeep';
import { colors } from 'theme/colors';

import { getBaseOptions } from './GameChart';

export default function WarriorChart({ game }) {
  const datasetProps = {
    pointRadius: 7,
    hoverRadius: 7,
    borderWidth: 1,
  };

  const data = {
    datasets: [
      {
        label: 'Blue Queen Deaths',
        pointBackgroundColor: colors.blue.light1,
        pointBorderColor: colors.blue.dark2,
        pointBorderWidth: 1,
        data: game.queenDeathData.blue,
        ...datasetProps,
      },
      {
        label: 'Gold Queen Deaths',
        pointBackgroundColor: colors.gold.light1,
        pointBorderColor: colors.gold.dark3,
        data: game.queenDeathData.gold,
        ...datasetProps,
      },
    ],
  };

  const options = getBaseOptions(game);
  options.scales.y = {
    display: false,
    type: 'linear',
    min: -4,
    max: 4,
    scaleLabel: {
      display: false,
      labelString: 'Queen Deaths',
    },
  };

  return (
    <Scatter options={options} data={data} />
  );
}
