import { Typography } from '@material-ui/core';

import GameChart from './GameChart';
import BaseWinProbabilityChart from './BaseWinProbabilityChart';

export default function WinProbailityChart({ game, datasetProps }) {
  if (!game.winProbability || game.winProbability.length === 0) return null;

  return (
    <GameChart title="KQuity Win Probability">
      <BaseWinProbabilityChart game={game} datasetProps={{ pointRadius: 0 }} />
    </GameChart>
  );
}
