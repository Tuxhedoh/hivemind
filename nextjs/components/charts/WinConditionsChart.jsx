import { makeStyles } from '@material-ui/core/styles';
import { useState } from 'react';
import { Pie } from 'react-chartjs-2';
import { Grid, CircularProgress, Typography } from '@material-ui/core';
import clsx from 'clsx';

import { getAxios } from 'util/axios';

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: theme.spacing(1),
    marginBottom: '4px'
  },
  legend: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  legendItem: {
    flexBasis: 0,
    flexGrow: 1,
    textAlign: 'center',
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'center'
  },
  legendColor: {
    // border: `1px solid ${theme.palette.divider}`,
    height: '14px',
    width: '14px',
    marginRight: theme.spacing(1),
  },
  legendText: {
    fontSize: '12px',
    textTransform: 'uppercase',
    lineHeight: '0.5',
    display: 'block',
    letterSpacing: '1px',
    color: theme.palette.grey['600']

  },
}));

const colors = [
  { title: 'Military', color: '#C52233' },
  { title: 'Economic', color: '#3DA35D' },
  { title: 'Snail', color: '#4062BB' },
];

function Chart({ data }) {
  const pctFormat = Intl.NumberFormat('en-US', { minimumFractionDigits: 1, maximumFractionDigits: 1 });
  const total = data.datasets[0].data.reduce((a, b) => a + b, 0);

  const options = {
    title: {
      display: false,
    },
    animation: {duration: 0},
    maintainAspectRatio: true,
    plugins: {
      legend: {
        display: false,
      },
      tooltip: {
        callbacks: {
          label: ctx => `${ctx.label}: ${ctx.parsed} (${pctFormat.format(100 * ctx.parsed / total)}%)`,
        },
      },
    },
  };

  for (const dataset of data.datasets) {
    dataset.backgroundColor = colors.map(c => c.color);
  }

  return (
    <Pie options={options} data={data} />
  );
}

export default function WinConditionsChart() {
  const classes = useStyles();
  const axios = getAxios();
  const [data, setData] = useState(null);

  if (data === null) {
    axios.get("/api/stats/win-conditions").then((response) => {
      setData(response.data);
    });

    return (<CircularProgress />);
  }

  return (
    <div className="border border-solid border-gray-300 rounded p-3">
      <div className="">
        <Typography align="center" variant="h3">Win Conditions by Map</Typography>
        <Typography align="center" variant="subtitle1" className="text-gray-500">In games over the past 8 weeks with at least 8 human players.</Typography>
      </div>

      <Grid className={classes.container} container spacing={3}>
        {Object.keys(data).map(mapName => (
          <Grid item key={mapName} xs={3}>
            <Typography align="center" variant="h4" className="mb-2">{data[mapName].mapName}</Typography>
            <Chart data={data[mapName]} />
          </Grid>
        ))}
      </Grid>

      <Grid className={clsx(classes.legend, 'w-[calc(100% + 28px)] -mx-3 mt-4 pt-2 px-3 border-0 border-t border-solid border-gray-300')} container spacing={2}>
        {colors.map((color, idx) => (
          <Grid item className={classes.legendItem} key={idx}>
            <span className={classes.legendColor} style={{ backgroundColor: color.color }} />
            <Typography className={classes.legendText}>{color.title}</Typography>
          </Grid>
        ))}
      </Grid>
    </div>
  );
}
