import GameChart from './GameChart';
import BaseQueenDeathsChart from './BaseQueenDeathsChart';

export default function QueenDeathsChart({ game }) {
  return (
    <GameChart title="Queen Deaths">
      <BaseQueenDeathsChart game={game} />
    </GameChart>
  );
}
