import { Line } from 'react-chartjs-2';
import { Typography } from '@material-ui/core';
import cloneDeep from 'lodash/cloneDeep';

import GameChart, { getBaseOptions } from './GameChart';

export default function BerriesRemainingChart({ game }) {
  const data = {
    datasets: [
      {
        label: 'Berries Remaining',
        borderColor: 'rgba(189, 66, 216, 1)',
        backgroundColor: 'rgba(189, 66, 216, 0.4)',
        pointRadius: 5,
        hitRadius: 5,
        hoverRadius: 5,
        borderWidth: 1,
        lineTension: 0,
        stepped: true,
        fill: 'origin',
        data: game.berriesRemainingData,
      },
    ],
  };

  const options = getBaseOptions(game);
  options.scales.y = {
    display: false,
    type: 'linear',
    min: 0,
    max: 70,
    scaleLabel: {
      display: false,
      labelString: 'Berries',
    },
  };

  return (
    <GameChart title='Berries Remaining'>
      <Line options={options} data={data} />
    </GameChart>
  );
}

