import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Button, Grid, Typography } from '@material-ui/core';
import PropTypes from 'prop-types';
import Icon from '@mdi/react';
import { mdiPlus, mdiMinus, mdiNull } from '@mdi/js';

const useStyles = makeStyles(theme => ({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(2),
  },
  value: {
    fontSize: '54px',
    fontWeight: 'bold',
    minWidth: '64px',
    textAlign: 'center',
    cursor: 'default',
    '&:hover': {
      backgroundColor: 'none',
    },
  },
  buttonScoreUpdater: {
    margin: theme.spacing(1),
    maxHeight: '32px',
  },
  title: {
    fontSize: '18px',
    fontWeight: 'bold',
  },
  titleBox: {
    overflow: 'hidden',
  },
}));

export default function ScoreUpdater({ title, value, onChange, disabled }) {
  const classes = useStyles();
  const [isLoading, setisLoading] = useState(false);
  const disableButtons = Boolean(isLoading || disabled);

  const handleChange = newValue => {
    if (disabled) {
      return;
    }
    setisLoading(true);
    onChange(newValue).then(setisLoading(false));
  };

  const ScoreButton = ({ iconPath, ...props }) => (
    <Button
      variant="outlined"
      className={classes.buttonScoreUpdater}
      disabled={disabled}
      {...props}
    >
      <Icon size={1} path={iconPath} />
    </Button>
  );

  return (
    <Grid container direction="column" className={classes.container}>
      {title && (
        <Grid item className={classes.titleBox}>
          <Typography className={classes.title}>{title}</Typography>
        </Grid>
      )}
      <Grid item container direction="row" alignItems="center" justify="space-between">
        <ScoreButton iconPath={mdiMinus} onClick={() => handleChange(value - 1)} />
        <Box className={classes.value}>{value}</Box>
        <ScoreButton iconPath={mdiPlus} onClick={() => handleChange(value + 1)} />
      </Grid>
    </Grid>
  );
}

ScoreUpdater.propTypes = {
  title: PropTypes.string,
  value: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
};

ScoreUpdater.defaultProps = {
  title: null,
};
