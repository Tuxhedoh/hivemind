import React, { useState } from 'react';
import { useRouter } from 'next/router';
import { makeStyles } from '@material-ui/core/styles';
import { Badge, IconButton, Menu, MenuItem, Typography, Grid, ListItemIcon, ListItemText } from '@material-ui/core';
import { usePopupState, bindTrigger, bindMenu } from 'material-ui-popup-state/hooks';
import clsx from 'clsx';
import { format } from 'date-fns';
import MailIcon from '@material-ui/icons/Mail';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';

import { getAxios } from 'util/axios';
import { UserConsumer } from 'util/auth';
import isServer from 'util/isServer';

const useStyles = makeStyles(theme => ({
  iconButton: {
    cursor: 'pointer',
    padding: '4px',
  },
  icon: {
    opacity: props => props.newMessageCount > 0 ? 1 : 0.5,
  },
  messageText: {
    opacity: 0.5,
  },
  messageTextUnread: {
    opacity: 1,
  },
  messageTime: {
    fontSize: '12px',
    fontStyle: 'italic',
    opacity: 0.5,
  },
  messageTimeUnread: {
    opacity: 0.8,
  },
  messageIcon: {
    color: theme.palette.primary.main,
  },
}));

export function MessageList({ user, className }) {
  const [messages, setMessages] = useState(null);
  const menuState = usePopupState({popupId: 'messages'});
  const axios = getAxios({ authenticated: true });
  const router = useRouter();

  const newMessageCount = messages ? messages.filter(message => !message.isRead).length : 0;
  const classes = useStyles({ newMessageCount });
  const browserPermission = (!isServer() && 'Notification' in window) ? Notification.permssion : null;

  const Message = React.forwardRef(({ message }, ref) => {
    const onClick = () => {
      menuState.close();

      if (!message.isRead) {
        message.isRead = true;
        axios.put(`/api/user/message/${message.id}/`, { isRead: true });
      }

      if (message.url) {
        router.push(message.url);
      }
    };

    const messageTextClass = clsx(classes.messageText, { [classes.messageTextUnread]: !message.isRead });
    const messageTimeClass = clsx(classes.messageTime, { [classes.messageTimeUnread]: !message.isRead });

    return (
      <MenuItem className={classes.message} onClick={onClick} ref={ref}>
        <ListItemIcon>
          {!message.isRead && (
            <ArrowRightIcon className={classes.messageIcon} />
          )}
        </ListItemIcon>

        <ListItemText>
          <Grid container direction="column">
            <Typography variant="h4" className={messageTextClass}>{message.messageText}</Typography>
            <Typography className={messageTimeClass}>{format(new Date(message.timestamp), 'MMM d - hh:mm aa')}</Typography>
          </Grid>
        </ListItemText>
      </MenuItem>
    );
  });

  if (!user?.id)
    return (<></>);

  if (messages === null) {
    axios.get('/api/user/message/').then(response => {
      setMessages(response.data.results);
    });

    return (<></>);
  }

  return (
    <>
      <IconButton aria-label="messages" className={classes.iconButton} {...bindTrigger(menuState)}>
        <Badge badgeContent={newMessageCount} max={99} color="secondary">
          <MailIcon className={clsx(className, classes.icon)} />
        </Badge>
      </IconButton>

      <Menu
        className={classes.menu}
        getContentAnchorEl={null}
        {...bindMenu(menuState)}
      >
        {messages.length === 0 && (
          <MenuItem className={classes.noMessages}>No messages.</MenuItem>
        )}

        {messages.map(message => (
          <Message key={message.id} message={message} />
        ))}
      </Menu>
    </>
  );
}

export const MessageListIcon = UserConsumer(MessageList);
