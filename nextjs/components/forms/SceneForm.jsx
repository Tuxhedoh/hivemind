import React, { useState } from 'react';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import { toast } from 'react-toastify';

import Form from './Form';
import List from './List';
import Field, { TextField } from '../fields/Field';
import { isAdminOf } from 'util/auth';
import { getAxios } from 'util/axios';
import { PERMISSION_TYPES } from 'util/constants';

const SceneSchema = Yup.object().shape({
  displayName: Yup.string().max(30, 'Too long'),
});

export default function SceneForm({ scene }) {
  const axios = getAxios({ authenticated: true });
  const [admins, setAdmins] = useState(null);
  const isAdmin = isAdminOf(scene.id);
  const props = {
    buttonText: 'Edit',
    buttonVariant: 'contained',
    dialogTitle: `Editing Scene: ${scene.displayName}`,
    canEdit: scene => isAdminOf(scene.id),
    object: scene,
    validationSchema: SceneSchema,
    url: `/api/game/scene/${scene.id}/`,
  };

  const getAdminList = async () => {
    if (isAdmin) {
      const response = await axios.get(
        `/api/user/permission/`,
        { params: { sceneId: scene.id, permission: PERMISSION_TYPES.ADMIN } },
      );

      setAdmins(response.data.results);
    }
  };

  if (admins === null) {
    getAdminList();
  }

  const addAdmin = async (email) => {
    const postdata = { user: email, scene: scene.id, permission: PERMISSION_TYPES.ADMIN }

    try {
      const response = await axios.post(`/api/user/permission/`, postdata);
      setAdmins([...admins, response.data]);
    } catch (error) {
      if (error.response?.data?.user) {
        const errorText = error.response.data.user.join(', ');
        toast.error(`Couldn't add new admin user: ${errorText}`);
        return errorText;
      }

      throw(error);
    }
  };

  const removeAdmin = admin => {
    axios.delete(`/api/user/permission/${admin.id}/`).then(response => {
      getAdminList();
    });
  };

  return (
    <Form {...props}>
      <Field name='displayName' label='Display Name' component={TextField} />
      <Field name='description' label='Description/Info' component={TextField} multiline rows={10} />
      <List title='Scene Admins'
        className="scene-admins-list"
        isLoading={admins === null}
        getItemText={item => item.user}
        items={admins}
        onAdd={addAdmin}
        onDelete={removeAdmin}
        newItemPlaceholderText='New Admin email'
      />
    </Form>
  );
}

SceneForm.propTypes = {
  scene: PropTypes.shape({
    displayName: PropTypes.string.isRequired,
  }).isRequired,
};
