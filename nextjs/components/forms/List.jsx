import {
  CircularProgress,
  FormControl,
  IconButton,
  Link,
  List as MuiList,
  ListItem as MuiListItem,
  ListItemSecondaryAction,
  ListItemText,
  Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import DeleteIcon from '@material-ui/icons/Delete';
import { TextField } from 'components/fields/Field';
import { useFormik } from 'formik';
import PropTypes from 'prop-types';
import { useRef } from 'react';

const useStyles = makeStyles(theme => ({
  progress: {
    display: 'block',
    margin: '1.5rem auto',
    color: '#bbb',
  },
}));

export function ListItem({ text, href, onDelete, className }) {
  const classes = useStyles();
  return (
    <MuiListItem className={className}>
      {href === null && <ListItemText primary={text} />}

      {href !== null && (
        <Link href={href}>
          <ListItemText primary={text} />
        </Link>
      )}

      <ListItemSecondaryAction>
        <IconButton edge="end" aria-label="delete" onClick={onDelete}>
          <DeleteIcon />
        </IconButton>
      </ListItemSecondaryAction>
    </MuiListItem>
  );
}

ListItem.propTypes = {
  text: PropTypes.string.isRequired,
  href: PropTypes.string,
  onDelete: PropTypes.func.isRequired,
};

ListItem.defaultProps = {
  href: null,
};

export default function List({
  title,
  items,
  fields,
  fieldsSchema,
  getItemText,
  getHref,
  noItemsText,
  newItemPlaceholderText,
  newItemFieldType,
  isLoading,
  onClick,
  onAdd,
  onDelete,
  newItemField,
  buttons,
  className,
  listItemClass,
}) {
  const classes = useStyles();

  const handleSubmit = async (values, formik) => {
    onAdd(values.newItem, { formik, values }).then(errorText => {
      if (errorText) {
        formik.setFieldError('newItem', errorText);
      } else {
        formik.resetForm();
      }
    });
  };
  const firstFieldRef = useRef();

  const initialValues = {
    newItem: '',
  };

  for (const field of fields) {
    initialValues[field.name] = '';
  }

  const formik = useFormik({
    onSubmit: handleSubmit,
    validationSchema: fieldsSchema,
    initialValues,
  });
  const handleEnterKey = e => {
    if (e.key === 'Enter') {
      formik.handleSubmit();
      firstFieldRef?.current?.focus();
    }
  };
  return (
    <FormControl className="flex flex-col">
      <Typography variant="h6" className={classes.title}>
        {title}
      </Typography>

      {isLoading && <CircularProgress color="inherit" className={classes.progress} />}

      {!isLoading && (
        <MuiList className={className}>
          {items.length === 0 && <Typography className={classes.noItems}>{noItemsText}</Typography>}

          {items.map((item, idx) => (
            <ListItem
              key={idx}
              text={getItemText(item)}
              href={getHref === null ? null : getHref(item)}
              onClick={() => onClick(item)}
              onDelete={() => onDelete(item)}
              className={listItemClass}
            />
          ))}

          {onAdd && (
            <MuiListItem className="py-0 pl-0 my-4">
              <ListItemText>
                {newItemField === null && !fields?.length && (
                  <TextField
                    name="newItem"
                    type={newItemFieldType}
                    placeholder={newItemPlaceholderText}
                    value={formik.values.newItem}
                    onChange={formik.handleChange}
                    className={classes.newItemTextField}
                    error={formik.errors.newItem}
                    helperText={formik.errors.newItem}
                    fullWidth
                  />
                )}
                {fields?.length > 0 && (
                  <div className="flex gap-4 ">
                    {fields.map((field, index) => (
                      <TextField
                        inputRef={index === 0 ? firstFieldRef : null}
                        name={field.name}
                        key={field.name}
                        type={field.type ?? 'text'}
                        placeholder={field.placeholder}
                        value={formik.values[field.name]}
                        onChange={formik.handleChange}
                        fullWidth
                        error={Boolean(formik.errors[field.name])}
                        helperText={formik.errors[field.name]}
                        required={field?.required}
                        onKeyDown={index === fields.length - 1 ? e => handleEnterKey(e) : null}
                      />
                    ))}
                  </div>
                )}

                {newItemField !== null && <>{newItemField}</>}
              </ListItemText>

              <ListItemSecondaryAction>
                {buttons}

                <IconButton edge="end" aria-label="add" onClick={formik.handleSubmit}>
                  <AddCircleIcon />
                </IconButton>
              </ListItemSecondaryAction>
            </MuiListItem>
          )}
        </MuiList>
      )}
    </FormControl>
  );
}

List.propTypes = {
  title: PropTypes.node.isRequired,
  items: PropTypes.array,
  fields: PropTypes.array,
  getItemText: PropTypes.func.isRequired,
  getHref: PropTypes.func,
  noItemsText: PropTypes.string,
  newItemPlaceholderText: PropTypes.string,
  newItemFieldType: PropTypes.string,
  isLoading: PropTypes.bool,
  onClick: PropTypes.func,
  onAdd: PropTypes.func,
  onDelete: PropTypes.func,
  newItemField: PropTypes.node,
  buttons: PropTypes.array,
  className: PropTypes.string,
};

List.defaultProps = {
  items: [],
  fields: [],
  getHref: null,
  noItemsText: 'No items found.',
  newItemPlaceholderText: 'New item',
  newItemFieldType: 'text',
  isLoading: false,
  onClick: () => {},
  onAdd: null,
  onDelete: () => {},
  newItemField: null,
  buttons: [],
  className: null,
};
