import React from 'react';
import { Grid, IconButton, Tooltip } from '@material-ui/core';
import SwapHorizIcon from '@material-ui/icons/SwapHoriz';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { useForm } from './Form';

const useStyles = makeStyles(theme => ({
  fieldContainer: {
    margin: theme.spacing(2, 0),
  },
}));

export default function SwapFieldsButton({ field1, field2, ...props }) {
  const classes = useStyles();
  const { values, setFieldValue } = useForm();

  const swapValues = () => {
    const tempField1 = values[field1];
    setFieldValue(field1, values[field2]);
    setFieldValue(field2, tempField1);
  }

  return (
    <Grid item md={"auto"} className={classes.fieldContainer}>
      <Tooltip title="Swap Names">
        <IconButton aria-label="Swap team names" onClick={() => swapValues()}>
          <SwapHorizIcon />
        </IconButton>
      </Tooltip>
      <span className="md:hidden"  onClick={() => swapValues()}>Swap team names</span>
    </Grid>
  );
}

SwapFieldsButton.propTypes = {
  field1: PropTypes.string.isRequired,
  field2: PropTypes.elementType.isRequired,
};
