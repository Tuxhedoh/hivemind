import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Box, Typography, Link, Chip, Button } from '@material-ui/core';
import * as Yup from 'yup';
import { toast } from 'react-toastify';
import { useRouter } from 'next/router';
import Icon from '@mdi/react';
import { mdiAccount } from '@mdi/js';

import Form from './Form';
import List from './List';
import TournamentBracketTeamsForm from './TournamentBracketTeamsForm';
import Field, { TextField, Switch } from '../fields/Field';
import DeleteButton from 'components/DeleteButton';
import InlineCode from 'components/InlineCode';
import { TOURNAMENT_LINK_TYPES } from 'util/constants';
import { isAdminOf } from 'util/auth';
import { getAxios } from 'util/axios';

const TournamentBracketSchema = Yup.object().shape({
  name: Yup.string().required('Stage name is required').max(100, 'Too long'),
  roundsPerMatch: Yup.number().default(0),
  winsPerMatch: Yup.number().default(0),
});

export default function TournamentBracketForm({ bracket, tournament, onClose, ...props }) {
  const axios = getAxios({ authenticated: true });
  const [valid, setValid] = useState(false);
  const [isValidating, setIsValidating] = useState(false);
  const [errors, setErrors] = useState(null);
  const router = useRouter();

  const formProps = {
    buttonText: bracket.id ? 'Edit' : 'Create Stage',
    dialogTitle: bracket.id ? `Editing Tournament Stage: ${bracket.name}` : 'Create New Stage',
    canEdit: bracket => isAdminOf(tournament.scene.id),
    object: bracket,
    validationSchema: TournamentBracketSchema,
    url: bracket.id ? `/api/tournament/bracket/${bracket.id}/` : '/api/tournament/bracket/',
    method: bracket.id ? 'PUT' : 'POST',
    enableReinitialize: true,
    getPostData: {
      linkedOrg: values => (values.linkedOrg === '' ? null : values.linkedOrg),
    },
    onClose,
    ...props,
  };

  const validateBracket = () => {
    setIsValidating(true);
    axios.put(`/api/tournament/bracket/${bracket.id}/validate/`, bracket).then(response => {
      setIsValidating(false);
      if (response.data.success) {
        setValid(true);
      } else {
        toast.error(response.data?.errors?.join('\n'));
        setErrors(response.data.errors);
      }
    });
  };

  const deleteBracket = () => {
    axios.delete(`/api/tournament/bracket/${bracket.id}/`).then(response => {
      toast(`${bracket.name} deleted.`);
      onClose();
    });
  };

  if (bracket.id) {
    formProps.dialogActions = (
      <>
        {tournament.linkType == TOURNAMENT_LINK_TYPES.CHALLONGE && !bracket.isValid && (
          <Button
            variant="outlined"
            color="primary"
            onClick={validateBracket}
            disabled={isValidating}
          >
            Validate
          </Button>
        )}

        {tournament.linkType == TOURNAMENT_LINK_TYPES.CHALLONGE &&
          bracket.isValid &&
          tournament.teams?.length > 0 && (
            <TournamentBracketTeamsForm
              bracket={bracket}
              tournament={tournament}
              onClose={onClose}
            />
          )}

        <DeleteButton onConfirm={deleteBracket} confirmText={`Delete "${bracket.name}"?`} />
      </>
    );
  }

  return (
    <Form {...formProps}>
      <Field name="name" label="Stage Name" component={TextField} />
      <Field
        name="roundsPerMatch"
        label="Rounds per Match"
        component={TextField}
        type="number"
        helperText="The match will end when this number of rounds have been played. Set to 0 to disable."
      />
      <Field
        name="winsPerMatch"
        label="Wins per Match"
        component={TextField}
        type="number"
        helperText="The match will end when one team has won this many rounds. Set to 0 to disable."
      />
      <Field
        name="addTiebreaker"
        label="Add Tiebreaker"
        component={Switch}
        helperText="If the match is tied after reaching the maximum number of games to play, add another game."
      />
      <Field
        name="autoWarmup"
        label="Warm-Up Games"
        component={Switch}
        helperText="Automatically enable a warm-up game for a queued match if either team has not played yet in this stage."
      />

      {tournament.linkType == TOURNAMENT_LINK_TYPES.CHALLONGE && (
        <>
          <Field
            name="linkedOrg"
            label="Challonge Organization"
            component={TextField}
            disabled={bracket.isValid || valid}
            helperText="Leave blank if not hosted by an organization."
          />
          <Field
            name="linkedBracketId"
            label="Challonge ID"
            component={TextField}
            disabled={bracket.isValid || valid}
            helperText={`The part of the URL after "challonge.com/"`}
          />
          <Field
            name="reportAsSets"
            label="Report Results as Sets"
            component={Switch}
            helperText="If selected, the results will be reported to Challonge as a series of 1-0 sets. Select this if awarding points per game/set win."
          />
          {!bracket.isValid && !valid && (
            <Box>
              <Typography>
                To use Challonge integration, share tournament admin access with
                <Chip
                  href="https://challonge.com/users/kq_hivemind"
                  component="a"
                  variant="outlined"
                  clickable
                  label="kq_hivemind"
                  icon={<Icon path={mdiAccount} size={1} />}
                />
              </Typography>
              <Typography>
                The Permissions section can be found at "Settings" &rarr; "Advanced Options" &rarr;
                "Permissions".
              </Typography>

              {bracket.linkToken && bracket.linkedBracketId && (
                <Typography>
                  In order to automatically report scores, please confirm that this tournament
                  belongs to you by adding the following value into your tournament's description
                  and clicking "Validate":
                  <InlineCode>{bracket.linkToken}</InlineCode>
                </Typography>
              )}

              {errors && errors.map((error, idx) => <Typography key={idx}>{error}</Typography>)}
            </Box>
          )}

          {valid && (
            <Box>
              <Typography>
                Challonge integration is active for this bracket. You can remove the validation
                token from your tournament description.
              </Typography>
            </Box>
          )}
        </>
      )}
    </Form>
  );
}

TournamentBracketForm.propTypes = {
  bracket: PropTypes.object.isRequired,
  tournament: PropTypes.object.isRequired,
};
