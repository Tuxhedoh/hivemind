import { Typography } from '@material-ui/core';
import { useRouter } from 'next/router';

import { SEASON_TYPES } from 'util/constants';
import Form from './Form';

export default function EventDeleteTeamsForm({ event, onSave }) {
  const router = useRouter();
  const postdata = { event: event.id, numTeams: Math.ceil(event.unassignedPlayers.length / 5) };
  const props = {
    buttonText: 'Delete Teams and Matches',
    buttonColor: 'default',
    dialogTitle: 'Delete Teams and Matches',
    canEdit: async () => true,
    object: postdata,
    url: `/api/league/event/${event.id}/delete-teams/`,
    method: 'PUT',
    closeButtonText: 'Cancel',
    saveButtonText: 'Delete',
    onSave: router.reload,
  };

  return (
    <Form {...props}>
      {(event.season.seasonType == SEASON_TYPES.SHUFFLE) && (
        <Typography variant="subtitle1">This will delete all teams and matches that have been generated for this event. All players will be returned to the unassigned player list.</Typography>
      )}

      {(event.season.seasonType == SEASON_TYPES.BYOT) && (
        <Typography variant="subtitle1">This will delete all matches that have been generated for this event. You will be able to select teams again and regenerate the match list.</Typography>
      )}
    </Form>
  );
}
