import {
  Button,
  Card,
  Chip,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  FormControlLabel,
  FormGroup,
  Grid,
  Switch,
  Typography,
  useMediaQuery,
  useTheme,
} from '@material-ui/core';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import {
  mdiBookOpenVariant,
  mdiCheck,
  mdiClose,
  mdiFruitGrapes,
  mdiMap,
  mdiPause,
  mdiPlay,
  mdiRefresh,
  mdiSwapHorizontalBold,
  mdiThemeLightDark,
  mdiTimer,
  mdiTimerPauseOutline,
  mdiTools,
  mdiWeatherNight,
  mdiWeatherSunny,
  mdiWeatherSunset,
  mdiWifiOff,
} from '@mdi/js';
import Icon from '@mdi/react';
import ScoreUpdater from 'components/ScoreUpdaterReferee';
import TournamentGameSelector from 'components/tables/TournamentGameSelectorReferee';
import TournamentQueueTable from 'components/tables/TournamentQueueTable';
import { useTournamentAdmin } from 'providers/TournamentAdmin';
import React, { useEffect, useMemo, useState } from 'react';
import { toast } from 'react-toastify';
import { getAxios } from 'util/axios';
import { MAP_NAMES, TOURNAMENT_LINK_TYPES } from 'util/constants';
import { formatUTC } from 'util/dates';

const useStyles = makeStyles(theme => ({
  cabStatus: {
    marginLeft: theme.spacing(2),
  },
  dialogTitle: {
    backgroundColor: theme.palette.gold.light3,
    padding: theme.spacing(1, 2),
    textAlign: 'center',
  },
  scoreUpdaterBlue: {
    backgroundColor: theme.palette.blue.light1,
    textAlign: 'center',
  },
  scoreUpdaterGold: {
    backgroundColor: theme.palette.gold.light1,
    textAlign: 'center',
  },
  gameStats: {
    backgroundColor: theme.palette.action.selected,
    margin: theme.spacing(2, 5),
    borderRadius: 5,
  },
  subtitleItem: {
    flexBasis: 0,
    flexGrow: 1,
    textAlign: 'center',
    '& svg': {
      width: '24px',
      height: '24px',
      verticalAlign: 'middle',
    },
  },
  subtitleItemMap: {
    '& svg': {
      verticalAlign: 'top',
    },
  },
  warmupSwitch: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  dialogActions: {
    backgroundColor: theme.palette.grey['300'],
  },
}));

export default function TournamentMatchForm({
  cabinet,
  activeMatch,
  open,
  onClose,
  stats,
  ...props
}) {
  const classes = useStyles();
  const axios = getAxios({ authenticated: true });
  const [isLoading, setIsLoading] = useState(false);
  const { cabinets, loading, loadAvailableMatches, matches, teamsById, tournament } =
    useTournamentAdmin();

  const disableButtons = Boolean(isLoading || !activeMatch?.id || loading || cabinet.clientStatus == 'offline');

  const availableMatches = matches?.filter(
    match => match.blueTeam !== null && match.goldTeam !== null,
  );

  useEffect(() => {
    if (matches === null) {
      loadAvailableMatches();
    }
  }, [matches]);

  // Pulls team names for active match
  const activeBlueTeam = teamsById[activeMatch.blueTeam];
  const activeGoldTeam = teamsById[activeMatch.goldTeam];

  // Swap blue/gold button
  const flip = async () => {
    if (isLoading) {
      return;
    }

    setIsLoading(true);
    const postdata = {
      isFlipped: !activeMatch.isFlipped,
      blueTeam: activeMatch.goldTeam,
      goldTeam: activeMatch.blueTeam,
    };

    await axios
      .patch(`/api/tournament/match/${activeMatch.id}/`, postdata)
      .catch(err => toast.error('Could not swap team sides'));
    setIsLoading(false);
  };

  // Toggle warm-up button
  const toggleWarmup = async () => {
    if (isLoading) {
      return;
    }

    setIsLoading(true);
    const response = await axios
      .get(`/api/tournament/match/${activeMatch.id}/`)
      .catch(err => toast.error('Could not set game as warm-up'));
    const postdata = {
      isWarmup: !response.data.isWarmup,
    };

    await axios
      .patch(`/api/tournament/match/${activeMatch.id}/`, postdata)
      .catch(err => toast.error('Could not set game as warm-up'));
    setIsLoading(false);
  };

  // Update scores
  const updateBlueScore = async value => {
    await axios
      .patch(`/api/tournament/match/${activeMatch.id}/`, { blueScore: value })
      .catch(err => toast.error('Could not update Blue team score'));
  };

  const updateGoldScore = async value => {
    await axios
      .patch(`/api/tournament/match/${activeMatch.id}/`, { goldScore: value })
      .catch(err => toast.error('Could not update Gold team score'));
  };

  // Pause match and remove from queue
  const pauseMatch = async match => {
    setIsLoading(true);
    await axios
      .patch(`/api/tournament/match/${activeMatch.id}/`, { activeCabinet: null })
      .catch(err => toast.error('Could not unassign match'));
    setIsLoading(false);
  };

  // Start next match from queue (cab queue first then any available)
  const nextMatch = async match => {
    setIsLoading(true);
    await axios
      .put(`/api/tournament/tournament/${tournament.id}/select-next/`, { cabinet: cabinet.id })
      .catch(err => toast.error('Could not start match in queue'));
    setIsLoading(false);
  };

  // Used to set full-screen on ref dash dialog modal for mobile/smaller screens
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('lg'));

  // Dialog box and button
  const [openDialog, setOpenDialog] = useState(false);
  const onConfirmDialog = useState(false);
  const handleOpenDialog = () => setOpenDialog(true);
  const handleCloseDialog = () => setOpenDialog(false);
  const handleConfirmDialog = () => {
    pauseMatch();
    setOpenDialog(false);
  };

  const mapIcons = {
    Day: <Icon size={0.8} path={mdiWeatherSunny} />,
    Night: <Icon size={0.8} path={mdiWeatherNight} />,
    Dusk: <Icon size={0.8} path={mdiWeatherSunset} />,
    Twilight: <Icon size={0.8} path={mdiThemeLightDark} />,
  };

  return (
    <Dialog open={open} onClose={onClose} fullScreen={fullScreen} maxWidth="lg">
      <DialogTitle className={classes.dialogTitle}>
        <Grid container direction="row" alignItems="center" justifyContent="space-between">
          <Grid item>Referee</Grid>
          <Grid item>{cabinet.displayName}</Grid>
          <Grid item>
          {cabinet.clientStatus == 'online' && (
            <Chip
              className={classes.cabStatus}
              label="Online"
              color="secondary"
              icon=<Icon path={mdiCheck} size={1} />
            />
          )}
          {cabinet.clientStatus == 'offline' && (
            <Chip
              className={classes.cabStatus}
              label="Offline"
              color="info"
              icon=<Icon path={mdiWifiOff} size={1} />
            />
          )}
          </Grid>
        </Grid>
      </DialogTitle>

      {/* Current game stats */}
      <DialogContent>
        <Grid container justifyContent="center" direction="row" spacing={2}>
          <Grid item container sm={8} direction="row" className={classes.gameStats}>
            <Grid item className={ clsx(classes.subtitleItem, classes.subtitleItemMap)}>
              {mapIcons[MAP_NAMES[stats?.mapName]]} {MAP_NAMES[stats?.mapName] ?? stats?.mapName}
            </Grid>
            <Grid item className={classes.subtitleItem}>
              <Icon path={mdiTimer} /> {formatUTC((stats?.gameTime ?? 0) * 1000, 'm:ss')}
            </Grid>
            <Grid item className={classes.subtitleItem}>
              <Icon path={mdiFruitGrapes} /> {stats?.berriesRemaining}
            </Grid>
          </Grid>

          {/* Score and game ref controls (swap and warm-up) */}
          <Grid
            item
            container
            sx={{ flexDirection: { xs: 'column', lg: 'row' } }}
            xs={12}
            spacing={2}
            justifyContent="center"
            alignItems="center"
          >
            <Grid item>
              <Card className={classes.scoreUpdaterBlue}>
                <ScoreUpdater
                  title={activeBlueTeam?.name ?? ''}
                  value={activeMatch.blueScore}
                  onChange={updateBlueScore}
                  disabled={disableButtons}
                />
              </Card>
            </Grid>

            <Grid item>
              <Button
                variant="outlined"
                color="secondary"
                onClick={flip}
                disabled={disableButtons}
                startIcon={<Icon size={1} path={mdiSwapHorizontalBold} />}
              >
                Swap
              </Button>
            </Grid>

            <Grid item>
              <Card className={classes.scoreUpdaterGold}>
                <ScoreUpdater
                  title={activeGoldTeam?.name ?? ''}
                  value={activeMatch.goldScore}
                  onChange={updateGoldScore}
                  disabled={disableButtons}
                />
              </Card>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <FormGroup className={classes.warmupSwitch}>
              <FormControlLabel
                label="Warm-up Game"
                control={
                  <Switch
                    onChange={toggleWarmup}
                    checked={activeMatch?.isWarmup}
                    disabled={disableButtons}
                  />
                }
              />
            </FormGroup>
          </Grid>

          {/* Game selector (add/remove games to current set) */}
          <Grid item container spacing={2} direction="row">
            <Grid item sx={12} md={6}>
              <TournamentGameSelector
                className={classes.tableGameQueue}
                tournament={tournament}
                cabinet={cabinet}
                activeMatch={activeMatch}
              />
            </Grid>

            {/* Current cab and any available queues */}
            <Grid item xs={12} md={6}>
              <TournamentQueueTable
                className={classes.tableGameQueue}
                title="Cab Queue"
                cabinet={cabinet}
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <TournamentQueueTable
                className={classes.tableGameQueue}
                title="Any Available Queue"
                cabinet={null}
              />
            </Grid>
          </Grid>
        </Grid>
      </DialogContent>

      <DialogActions className={classes.dialogActions}>
        <Grid
          container
          direction="row"
          alignItems="center"
          spacing={1}
          justifyContent="space-evenly"
        >
          <Grid item>
            <Button
              variant="outlined"
              color="secondary"
              onClick={activeMatch?.id ? handleOpenDialog : nextMatch}
              startIcon={<Icon size={1} path={activeMatch?.id ? mdiPause : mdiPlay} />}
              disabled={isLoading || loading || cabinet.clientStatus == 'offline'}
            >
              Match
            </Button>
            <Dialog open={openDialog} onClose={handleCloseDialog}>
              <DialogContent>
                <DialogContentText>Pause Match and remove from queue?</DialogContentText>
              </DialogContent>
              <DialogActions>
                <Grid
                  container
                  direction="row"
                  alignItems="center"
                  spacing={1}
                  justifyContent="space-evenly"
                >
                  <Grid item>
                    <Button variant="outlined" color="default" onClick={handleCloseDialog}>
                      Cancel
                    </Button>
                  </Grid>
                  <Grid item>
                    <Button variant="contained" color="secondary" onClick={handleConfirmDialog}>
                      Confirm
                    </Button>
                  </Grid>
                </Grid>
              </DialogActions>
            </Dialog>
          </Grid>
          <Grid item>
            <Button
              variant="outlined"
              color="secondary"
              onClick={loadAvailableMatches}
              startIcon={<Icon size={1} path={mdiRefresh} />}
              disabled={loading}
            >
              Refresh
            </Button>
          </Grid>
          <Grid item>
            <Button
              variant="outlined"
              color="secondary"
              onClick={onClose}
              startIcon={<Icon size={1} path={mdiClose} />}
              disabled={loading}
            >
              Close
            </Button>
          </Grid>
        </Grid>
      </DialogActions>
    </Dialog>
  );
}