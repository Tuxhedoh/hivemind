import { Grid, MenuItem, Typography } from '@material-ui/core';

import * as Yup from 'yup';

import { allIngameThemes } from 'overlay/ingame/themes';
import { allMatchThemes } from 'overlay/match/themes';
import { allMatchPreviewThemes } from 'overlay/matchPreview/themes';
import { allPostgameThemes } from 'overlay/postgame/themes';
import { isAdminOf } from 'util/auth';
import { getAxios } from 'util/axios';
import Field, { Divider, Select, Switch, TextField } from '../fields/Field';
import Form from './Form';
import SwapFieldsButton from './SwapFieldsButton';

const OverlaySchema = Yup.object().shape({});

export default function OverlayForm({ overlay }) {
  const axios = getAxios();

  const props = {
    dialog: false,
    canEdit: overlay => isAdminOf(overlay.cabinet.scene.id),
    object: overlay,
    validationSchema: OverlaySchema,
    url: `/api/overlay/overlay/${overlay.id}/`,
    closeOnSave: false,
    getPostData: {
      cabinet: values => overlay.cabinet.id,
      theme: values => values.themeId,
    },
  };

  return (
    <Form
      {...props}
      className="relative overflow-visible"
      buttonClass={`absolute bottom-full right-0 mb-6 before:inline-block`}
      buttonVariant="contained"
    >
      <Grid container direction="row" spacing={0}>
        <Grid item xs={12} md>
          <Field name="blueTeam" label="Blue Team Display Name" component={TextField} />
          <Field name="blueScore" label="Blue Score" component={TextField} type="number" />
        </Grid>
        <Grid item md={'auto'}>
          <SwapFieldsButton field1="blueTeam" field2="goldTeam" />
        </Grid>
        <Grid item xs={12} md>
          <Field name="goldTeam" label="Gold Team Display Name" component={TextField} />
          <Field name="goldScore" label="Gold Score" component={TextField} type="number" />
        </Grid>
      </Grid>

      <Divider />
      <Typography variant="h3" className="mb-4">
        Series Settings
      </Typography>
      <Grid container direction="row" spacing={2}>
        <Grid item xs={12} md={6}>
          <Field
            name="showScore"
            label="Show Scores"
            component={Switch}
            helperText="Show these scores on the overlay when a league event or tournament is not active."
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <Field
            name="updateScore"
            label="Auto-Update Score"
            component={Switch}
            helperText="If checked, scores will automatically update when a game ends."
          />
        </Grid>
      </Grid>
      <Grid container direction="row" spacing={2}>
        <Grid item xs={12} md={6}>
          <Field
            name="matchWinMax"
            label="How many games to win the series?"
            component={TextField}
            type="number"
            helperText="Sets the number of empty match markers displayed if the chosen overlay supports it."
          />
        </Grid>
      </Grid>

      <Divider />

      <Typography variant="h3">Advanced Settings</Typography>

      <Field name="name" label="Overlay Name" component={TextField} />

      <Grid container direction="row" spacing={2}>
        <Grid item xs={12} md={6}>
          <Field name="ingameTheme" label="In-Game Theme" component={Select}>
            {allIngameThemes.map(theme => (
              <MenuItem key={theme.themeProps.name} value={theme.themeProps.name}>
                {theme.themeProps.displayName}
              </MenuItem>
            ))}
          </Field>
        </Grid>
        <Grid item xs={12} md={6}>
          <Field name="postgameTheme" label="Postgame Theme" component={Select}>
            {allPostgameThemes.map(theme => (
              <MenuItem key={theme.themeProps.name} value={theme.themeProps.name}>
                {theme.themeProps.displayName}
              </MenuItem>
            ))}
          </Field>
        </Grid>
        <Grid item xs={12} md={6}>
          <Field name="matchTheme" label="Match Summary Theme" component={Select}>
            {allMatchThemes.map(theme => (
              <MenuItem key={theme.themeProps.name} value={theme.themeProps.name}>
                {theme.themeProps.displayName}
              </MenuItem>
            ))}
          </Field>
        </Grid>
        <Grid item xs={12} md={6}>
          <Field name="matchPreviewTheme" label="MatchPreview Theme" component={Select}>
            {allMatchPreviewThemes.map(theme => (
              <MenuItem key={theme.themeProps.name} value={theme.themeProps.name}>
                {theme.themeProps.displayName}
              </MenuItem>
            ))}
          </Field>
        </Grid>
      </Grid>

      <Grid container direction="row" spacing={2}>
        <Grid item xs={12} md={6}>
          <Field name="showPlayers" label="Show Player Names" component={Switch} />
        </Grid>
        <Grid item xs={12} md={6}>
          <Field
            name="showChat"
            label="Show Twitch Chat"
            component={Switch}
            helperText="Show Twitch chat in stream, if supported by the chosen overlay. Channel name must be set in the cabinet settings menu."
          />
        </Grid>
      </Grid>
      <Grid container direction="row" spacing={2}>
        <Grid item xs={12} md={6}>
          <Field
            name="goldOnLeft"
            label="Gold on Left"
            component={Switch}
            helperText="Reverse the overlay, moving the gold team to the left side."
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <Field
            name="additionalFonts"
            label="Additional Fonts"
            helperText="must be a url that works in a link tag rel='stylesheet'"
            component={TextField}
          />
        </Grid>
      </Grid>

      <Typography variant="h3">Auto Scene Settings</Typography>
      <p className="px-4 py-2 text-sm font-sans rounded bg-gold-light4">
        ⚠️ This is an advanced section, proceed with caution. These parameters support an instant
        replay configuration with OBS. Changing these values will require you to refresh and clear
        the cache of the Auto OBS browser source.{' '}
        <a href="/wiki/instant_replay">See the wiki for instructions</a>
      </p>
      <Grid container direction="row" spacing={2}>
        <Grid item xs={12} md={6}>
          <Field
            name="enableAutoscene"
            label="Enable Auto Scene"
            component={Switch}
            helperText="AutoScene triggers OBS to change scenes based on the game state."
          />

          <Field
            name="enableAutosceneAlways"
            label="Always Switch to Game Scene"
            component={Switch}
            helperText="Upon Game Start, always switch to the Game scene regardless of the active OBS scene. If disabled, AutoScene will only switch between Game and PostGame scenes when they are the active scene."
          />
        </Grid>

        <Grid item xs={12} md={6}>
          <Field
            name="postgameDelayDurationMs"
            label="Post-Game Delay Duration in MS"
            component={TextField}
            type="number"
            helperText="Delay duration in milliseconds between the Game End and showing post-game stats AND autoswitching to post-game scene."
          />
        </Grid>
      </Grid>
      <Grid container direction="row" spacing={2}>
        <Grid item xs={12} md={6}>
          {' '}
          <Field
            name="enablePostgameSceneDuration"
            label="Enable Post-game Scene Duration"
            component={Switch}
            helperText="ON: Auto returns to Game scene from Post-game scene after set duration. OFF: You will be responsible for configuring OBS to return to the Game scene"
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <Field
            name="postgameSceneDurationMs"
            label="Post-game Scene Duration in MS"
            component={TextField}
            type="number"
            helperText="The length of time in milliseconds to show Post-game scene and stats. Should be the sum of instant replay and how long you want the post-game stats to show after the replay ends."
          />
        </Grid>
      </Grid>
      <Grid container direction="row" spacing={2}>
        <Grid item xs={12} md={6}>
          {' '}
          <Field
            name="enableMatchSummaryDuration"
            label="Enable Match Summary Duration"
            component={Switch}
            helperText="ON: Auto will hide the Match Summary overlay after a duration. OFF: Match Summary will remain on screen until the next map start"
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <Field
            name="matchSummaryDurationMs"
            label="Match Summary Duration in MS"
            component={TextField}
            type="number"
            helperText="The length of time in milliseconds to show the Match Summary. For scene switching, this will be summed with the post-game scene duration and wont return to the game scene until after."
          />
        </Grid>
      </Grid>

      <Divider />
    </Form>
  );
}
