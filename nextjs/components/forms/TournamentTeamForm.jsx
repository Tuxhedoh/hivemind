import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';
import * as Yup from 'yup';

import DeleteButton from 'components/DeleteButton';
import { isAdminOf } from 'util/auth';
import { getAxios } from 'util/axios';
import Field, { TextField } from '../fields/Field';
import Form, { useForm } from './Form';

const TournamentTeamSchema = Yup.object().shape({
  name: Yup.string().required('Team name is required').max(100, 'Too long'),
});

export default function TournamentTeamForm({ team, tournament, onClose, children, ...props }) {
  const axios = getAxios({ authenticated: true });
  const router = useRouter();

  const formProps = {
    buttonText: team.id ? 'Edit' : 'Quick Add Team',
    buttonColor: 'default',
    dialogTitle: team.id ? `Editing Team: ${team.name}` : 'Create New Team',
    canEdit: team => isAdminOf(tournament.scene.id),
    object: team,
    validationSchema: TournamentTeamSchema,
    url: team.id ? `/api/tournament/team/${team.id}/` : '/api/tournament/team/',
    method: team.id ? 'PUT' : 'POST',
    ...props,
  };

  const deleteTeam = () => {
    axios.delete(`/api/tournament/team/${team.id}/`).then(response => {
      toast(`${team.name} deleted.`);
      onClose();
    });
  };

  if (team.id) {
    formProps.dialogActions = (
      <>
        <DeleteButton onConfirm={deleteTeam} confirmText={`Delete "${team.name}"?`} />
      </>
    );
  }

  const TeamField = () => {
    const formik = useForm();
    return (
      <Field
        name="name"
        label="Team Name"
        autoFocus={true}
        component={TextField}
        onKeyDown={e => e.code === 'Enter' && formik.handleSubmit()}
      />
    );
  };

  const handleOnClose = () => {};
  return (
    <Form onClose={handleOnClose} {...formProps}>
      <TeamField />
    </Form>
  );
}

TournamentTeamForm.propTypes = {
  team: PropTypes.object.isRequired,
  tournament: PropTypes.object.isRequired,
};
