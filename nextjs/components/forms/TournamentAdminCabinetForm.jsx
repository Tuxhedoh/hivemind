import React, { useState } from 'react';
import { createFilterOptions } from '@material-ui/lab/Autocomplete';

import Form, { useForm } from './Form';
import Field, { Autocomplete } from '../fields/Field';
import { TOURNAMENT_LINK_TYPES } from 'util/constants';
import { isAdminOf } from 'util/auth';
import { getAxios } from 'util/axios';

const filter = createFilterOptions();

export default function TournamentAdminCabinetForm({ tournament, cabinet, activeMatch, open, onClose }) {
  const axios = getAxios({ authenticated: true });
  const formik = useForm();

  const filterOptions = (options, params) => {
    const filtered = filter(options, params);
    const { inputValue } = params;

    const isExisting = options.some((option) => inputValue === option.name);
    if (inputValue !== '' && !isExisting) {
      filtered.push({ name: `New Team: "${inputValue}"`, value: inputValue });
    }

    return filtered;
  };

  const onSave = async (response) => {
    onClose();
  };

  const props = {
    showButton: false,
    open,
    onClose,
    onSave,
    object: {},
    canEdit: overlay => isAdminOf(tournament.scene.id),
  };

  if (tournament.linkType == TOURNAMENT_LINK_TYPES.MANUAL) {
    return (
      <Form {...props}>
        <Field
          component={Autocomplete}
          name="blueTeam"
          label="Blue Team"
          freeSolo
          options={tournament.teams}
          filterOptions={filterOptions}
          handleHomeEndKeys
          getOptionLabel={opt => opt?.name ?? ''}
          renderOption={opt => opt?.name ?? ''}
          clearOnBlur
        />

        <Field
          component={Autocomplete}
          name="goldTeam"
          label="Gold Team"
          freeSolo
          options={tournament.teams}
          filterOptions={filterOptions}
          handleHomeEndKeys
          getOptionLabel={opt => opt?.name ?? ''}
          renderOption={opt => opt?.name ?? ''}
          clearOnBlur
        />
      </Form>
    );
  }

  if (tournament.linkType == TOURNAMENT_LINK_TYPES.CHALLONGE) {
    return (
      <>
      </>
    );
  }
}
