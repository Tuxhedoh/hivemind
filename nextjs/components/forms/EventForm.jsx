import React, { useState } from 'react';
import { MenuItem, Grid } from '@material-ui/core';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import { toast } from 'react-toastify';
import { useRouter } from 'next/router';

import { SEASON_TYPES } from 'util/constants';
import Form from './Form';
import List from './List';
import Field, { DateField, Divider, Select, Switch, TextField } from '../fields/Field';
import { isAdminOf } from 'util/auth';
import { getAxios } from 'util/axios';

const EventSchema = Yup.object().shape({
  name: Yup.string().nullable(true).transform(v => v || null),
  eventDate: Yup.string()
    .required('Event Date is required'),
  cabinet: Yup.number().nullable(true).transform(v => v || null),
  isPlayoff: Yup.bool().default(false).transform(v => Boolean(v)),
  isQuickplay: Yup.bool().default(false).transform(v => Boolean(v)),
  roundsPerMatch: Yup.number().default(0),
  winsPerMatch: Yup.number().default(0),
  matchesPerOpponent: Yup.number().default(0),
});

export default function EventForm({ event, onSave }) {
  const axios = getAxios({ authenticated: true });
  const [cabinets, setCabinets] = useState(null);
  const router = useRouter();

  if (cabinets === null) {
    axios.get('/api/game/cabinet/', { params: { sceneId: event.season.scene.id } }).then(response => {
      setCabinets(response.data.results);
    });
  }

  const props = {
    buttonText: event.id ? 'Edit' : 'Create Event',
    dialogTitle: event.id ? `Editing Event #${event.eventNumber} of ${event.season.name}` : 'Create New Event',
    canEdit: event => isAdminOf(event.season.scene.id),
    object: event,
    validationSchema: EventSchema,
    url: event.id ? `/api/league/event/${event.id}/` : '/api/league/event/',
    method: event.id ? 'PUT' : 'POST',
    getPostData: {
      season: values => values.season.id,
      unassignedPlayers: values => undefined,
    },
    onSave: result => {
      if (!event.id) {
        router.push(`/event/${result.id}`);
      }

      if (onSave !== null) {
        onSave();
      }
    },
  };

  return (
    <Form {...props}>
      <Field name="name" label="Event Name" component={TextField} />
      <Field name="eventDate" label="Event Date" component={DateField} />
      <Field name="cabinet" label="Cabinet" component={Select}>
        <MenuItem value={null}>None</MenuItem>
        {(cabinets !== null) && cabinets.map(cabinet => (
          <MenuItem key={cabinet.id} value={cabinet.id}>{cabinet.displayName}</MenuItem>
        ))}
      </Field>

      {event.season.seasonType == SEASON_TYPES.SHUFFLE && (
        <>
          <Field name="isQuickplay" label="Quickplay Event" component={Switch}
                 helperText="Randomly generate teams at the start of each match." />
          <Grid container spacing={2}>
            <Grid item xs={12} md={6}>

              <Field name="qpMatchesPerPlayer" label="Matches per Player" component={TextField} type="number"
                    helperText="Number of matches per player during a quickplay event. Must be an even number. Has no effect if this is not a quickplay event." />
            </Grid>
            <Grid item xs={12} md={6}>
              <Field name="pointsPerRound" label="Points per Map" component={TextField} type="number"
                 helperText="Teams/players are awarded this many points for winning each map." />
            </Grid>
          </Grid>
          <Field name="pointsPerMatch" label="Points per Match" component={TextField} type="number"
                 helperText="Teams/players are awarded this many points for winning each match." />
        </>
      )}

      {event.season.seasonType == SEASON_TYPES.BYOT && (
        <Field name="isPlayoff" label="Playoffs" component={Switch} />
      )}

      <Grid container spacing={2}>
        <Grid item xs={12} md={6}>
          <Field name="roundsPerMatch" label="Rounds per Match" component={TextField} type="number"
                helperText="The match will end when this number of rounds have been played. Set to 0 to disable." />
        </Grid>
        <Grid item xs={12} md={6}>
          <Field name="winsPerMatch" label="Wins per Match" component={TextField} type="number"
             helperText="The match will end when one team has won this many rounds. Set to 0 to disable." />
        </Grid>
      </Grid>

      <Field name="matchesPerOpponent" label="Matches per Opponent" component={TextField} type="number"
             helperText="Number of times each team plays each other team. Has no effect on quickplay events or playoffs." />


      {(event.canStart && !event.isComplete) && (
        <>
        <Divider />
        <Field name="isActive" label="Event Active" component={Switch}
               helperText="When the event is active, games on the selected cabinet will automatically update the current match." />
        </>
      )}
    </Form>
  );
}

EventForm.propTypes = {
  event: PropTypes.object,
  onSave: PropTypes.func,
};

EventForm.defaultProps = {
  event: null,
  onSave: null,
};
