import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { format, formatDistance } from 'date-fns';
import { Button, Dialog, DialogTitle, Typography } from '@material-ui/core';
import { getAxios } from 'util/axios';
import clsx from 'clsx';

const useStyles = makeStyles(theme => ({
  container: {
    padding: '0 1.5rem 1.5rem',
  },
  sceneButtons: {
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    gap: '20px'
  },
  dialog: {}
}));
export default function SceneSelector({ title, filters }) {
  const axios = getAxios();
  const [open, setOpen] = useState(false);
  const [data, setData] = useState(null);
  const classes = useStyles();

  useEffect(() => {
    if (data === null) {
      axios.getAllPages('/api/game/scene/').then(response => {
        const scenes = response
          // .filter(scene => scene.totalGames > 0)
          .sort((a, b) => a.name.toUpperCase() < b.name.toUpperCase());

        setData(scenes);
      });
    }
  }, []);

  const sceneButtons =  (
    <div className={classes.sceneButtons}>
      {data?.map(row => {
        const style = {};
        if (row.backgroundColor) {
          style.backgroundColor = `${row.backgroundColor}3f`;
          style.borderColor = `${row.backgroundColor}3f`;
        }

        return (
          <Button href={`/scene/${row.name.toLowerCase()}`} key={row.id} variant="outlined" color="inherit" className="scene-button" style={style}>
            <span className="basis-full" >{row.displayName}</span>
          </Button>
        );
      })}
    </div>
  );

  return (
    <>
      <Button variant="contained" color="secondary" onClick={() => setOpen(true)}>Select Scene</Button>
      <Dialog
          open={open}
          onClose={() => setOpen(false)}
          className={clsx(classes.dialog)}
          fullWidth
          maxWidth='md'
          title="Select a Scene"
      >
          <DialogTitle variant="h3">Select a Scene</DialogTitle>
        <div className={clsx(classes.container)}>
          {sceneButtons}
        </div>
      </Dialog>
    </>
  );

}

SceneSelector.propTypes = {
  title: PropTypes.string,
  filters: PropTypes.object,
};

SceneSelector.defaultProps = {
  title: 'Select a Scene',
  filters: {},
};
