import { Checkbox, FormControlLabel, MenuItem } from '@material-ui/core';
import { CheckboxGroup } from 'components/fields/Checkbox';
import Field, { Select, Switch, TextField } from 'components/fields/Field';
import { useState } from 'react';
import { Remark } from 'react-remark';
import { useForm } from './Form';
const getFieldProps = (
  { fieldName, fieldType, choices, isRequired },
  [value, setValue],
  formik,
) => {
  switch (fieldType) {
    case 'text':
    default:
      return {
        component: TextField,
        required: isRequired,
        children: null,
      };
    case 'single_choice':
      return {
        component: Select,
        children: choices.map(choice => (
          <MenuItem key={choice} value={choice}>
            {choice}
          </MenuItem>
        )),
        required: isRequired,
      };
    case 'checkbox':
      const valBoolean = typeof value === 'string' ? JSON.parse(value?.toLowerCase()) : value;
      return {
        component: Switch,
        required: isRequired,
        children: null,
        checked: valBoolean,
        value: valBoolean,
        onChange: e => {
          setValue(e.target.checked);
          formik.setFieldValue(fieldName, e.target.checked);
        },
      };
  }
};

export default function RegistrationQuestionsFields({ questions, initialData }) {
  return questions?.map(question => {
    const initializedValue =
      question.fieldType == 'multi_choice' ? [] : initialData[question.fieldName];
    const valueState = useState(initializedValue);
    const formik = useForm();
    const { children, ...props } = getFieldProps(question, valueState, formik);

    const returnField = (
      <div>
        {(question.fieldType === 'text' || question.fieldType === 'single_choice') && (
          <label htmlFor="question.fieldName" className="mb-1 block font-bold text-base">
            <Remark
              rehypeReactOptions={{
                components: {
                  p: props => <span className="my-0 inline-block" {...props} />,
                  a: props => <a target="_blank" {...props} />,
                },
              }}
            >
              {question.fieldDescription}
            </Remark>
          </label>
        )}
        <Field
          key={`field-${question.fieldName}`}
          name={question.fieldName}
          noGrid
          label={
            question.fieldType !== 'text' &&
            question.fieldType !== 'single_choice' && (
              <span className="text-[15px]">
                <Remark
                  rehypeReactOptions={{
                    components: {
                      p: props => <span className="my-0 inline-block" {...props} />,
                      a: props => <a target="_blank" {...props} />,
                    },
                  }}
                >
                  {question.fieldDescription}
                </Remark>
              </span>
            )
          }
          {...props}
        >
          {children}
        </Field>
      </div>
    );

    if (question.fieldType == 'multi_choice') {
      return (
        <Field
          key={`field-${question.fieldName}`}
          component={CheckboxGroup}
          name={question.fieldName}
          label={<strong>{question.fieldDescription}</strong>}
          multiple={true}
          className="rounded bg-gray-100 px-4 py-2 mt-2"
        >
          <>
            {question?.choices.map(choice => (
              <FormControlLabel
                className="flex items-center checkbox-label"
                key={`fcl-${choice}`}
                control={
                  <Checkbox
                    name={question.fieldName}
                    value={choice}
                    checked={formik.values[question.fieldName]?.includes(choice)}
                    onChange={e => {
                      const { checked, value: checkedValue } = e.target;
                      const values = formik.values[question.fieldName];

                      formik.setFieldValue(question.fieldName, [...values, checkedValue]);
                    }}
                  />
                }
                label={choice}
              />
            ))}
          </>
        </Field>
      );
    }
    return returnField;
  });
}
