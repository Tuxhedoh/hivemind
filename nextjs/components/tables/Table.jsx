import {
  CircularProgress,
  Grid,
  Link,
  Paper,
  Table as MuiTable,
  TableBody,
  TableCell as MuiTableCell,
  TableHead,
  TablePagination,
  TableRow as MuiTableRow,
  Toolbar,
  Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import React, { useContext } from 'react';

export const TableContext = React.createContext({});
export const TableRowContext = React.createContext({});

const useStyles = makeStyles(theme => ({
  paper: {
    margin: theme.spacing(2, 0, 4, 0),
    background: theme.palette.grey['100'],
    borderRadius: '5px',
    paddingBottom: '8px',
    overflow: 'hidden',
  },
  toolbar: {
    backgroundColor: theme.palette.gold.light2,
    // backgroundColor: theme.palette.grey['300'],
    borderBottom: `2px solid ${theme.palette.grey['800']}`,
    padding: theme.spacing(2),
    minHeight: 'unset',
  },
  titleContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  titleButton: {
    '& .MuiButtonBase-root': {
      margin: theme.spacing(-1, 0),
      // color: theme.palette.text.default,
      // borderColor: theme.palette.primary.light,
      '&:hover': {
        // backgroundColor: theme.palette.primary.main,
      },
    },
  },
  table: {
    tableLayout: 'fixed',
  },
  tableBody: {
    position: 'relative',
  },
  tableBodyLoading: {
    '& .MuiTableRow-root': {
      opacity: 0.3,
    },
  },
  loadingRow: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    opacity: 1,
  },
  loadingGrid: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerRow: {
    background: theme.palette.grey['800'],
  },
  headerCell: {
    padding: theme.spacing(1, 1.5),
    color: 'white',
  },
  toolbarTitle: {
    color: theme.palette.text.default,
    fontWeight: 'bold',
    marginTop: 0,
  },
  toolbarSubtitle: {
    color: theme.palette.grey['600'],
    marginTop: '.25rem',
    fontStyle: 'italic',
  },
  subtitleRow: {},
  subtitleCell: {
    fontWeight: 'bold',
    background: theme.palette.grey['800'],
    color: 'white',
  },
  row: {
    '&:nth-child(even)': {
      backgroundColor: theme.palette.grey['200'],
    },
  },
  rowWithLink: {
    '&:hover': {
      backgroundColor: theme.palette.highlight.main,
    },
    cursor: 'pointer',
  },
  cell: {
    padding: theme.spacing(1, 1.5),
    borderColor: theme.palette.grey['300'],
  },
  cellWithLink: {
    padding: 0,
    borderBottomWidth: '1px',
    borderBottomStyle: 'solid',
    '.MuiTableRow-root:hover &': {
      borderBottomWidth: '2px',
      // borderBottomColor: theme.palette.text.primary,
    },
  },
  link: {
    color: 'unset',
    display: 'block',
    height: '100%',
    width: '100%',
    padding: theme.spacing(1, 1.5),
    marginBottom: '1px',
    '&:hover': {
      textDecoration: 'unset',
    },
    '.MuiTableRow-root:hover &': {
      marginBottom: 0,
    },
  },
  emptyTableCell: {
    fontStyle: 'italic',
    padding: theme.spacing(2),
    textAlign: 'center',
  },
}));

export function TableCell({ children, className, linkClassName = '', ...props }) {
  const classes = useStyles();
  const tableRowCtx = useContext(TableRowContext);
  const { href, onClick } = tableRowCtx;

  if (href === undefined && onClick === undefined) {
    return (
      <MuiTableCell className={clsx(classes.cell, className)} {...props}>
        {children}
      </MuiTableCell>
    );
  } else {
    return (
      <MuiTableCell className={clsx(classes.cell, classes.cellWithLink, className)} {...props}>
        <Link href={href} onClick={onClick} className={clsx(classes.link, linkClassName)}>
          {children}
        </Link>
      </MuiTableCell>
    );
  }
}

TableCell.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]),
  className: PropTypes.string,
};

TableCell.defaultProps = {
  children: '',
  className: null,
};

export function TableRow({ children, className, href, onClick, row }) {
  const tableCtx = useContext(TableContext);
  const { getRowCells, getCellAttributes, cellClassNames, getCellStyle } = tableCtx;
  const classes = useStyles();

  const rowClassName = clsx(classes.row, className, { [classes.rowWithLink]: href || onClick });
  const getClassName = idx =>
    clsx({
      [cellClassNames && cellClassNames[idx] ? cellClassNames[idx] : null]: true,
    });

  const cellStyles = getCellStyle(row) ?? getRowCells(row).map(() => ({}));

  const tableRowCtx = { href, onClick };

  return (
    <TableRowContext.Provider value={tableRowCtx}>
      <MuiTableRow className={rowClassName}>
        {children}

        {!children &&
          getRowCells(row).map((cell, idx) => (
            <TableCell
              key={idx}
              className={getClassName(idx)}
              style={cellStyles[idx]}
              {...getCellAttributes(row, idx)}
            >
              {cell}
            </TableCell>
          ))}
      </MuiTableRow>
    </TableRowContext.Provider>
  );
}

TableRow.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)]),
  row: PropTypes.object,
  href: PropTypes.string,
  onClick: PropTypes.func,
  className: PropTypes.string,
};

TableRow.defaultProps = {
  children: undefined,
  row: undefined,
  href: undefined,
  className: undefined,
  onClick: undefined,
};

export function TableSubtitle({ href, children }) {
  const tableCtx = useContext(TableContext);
  const { columnHeaders } = tableCtx;
  const classes = useStyles({ link: href });

  return (
    <TableRow className={classes.subtitleRow} href={href}>
      <TableCell className={classes.subtitleCell} colSpan={columnHeaders.length}>
        {children}
      </TableCell>
    </TableRow>
  );
}

TableSubtitle.propTypes = {
  href: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]),
};

TableSubtitle.defaultProps = {
  href: null,
  children: '',
};

export default function Table({
  children,
  title,
  titleButton,
  tableSubtitle,
  showColumnHeaders,
  columnHeaders,
  data,
  rowElement,
  getRowCells,
  getSubtitle,
  getSectionRows,
  getCellAttributes,
  getCellStyle,
  cellClassNames,
  className,
  tableClassName,
  isLoading,
  link,
  subtitleLink,
  emptyText,
  page,
  rowsPerPage,
  totalCount,
  onPageChange,
  onRowClick,
}) {
  const classes = useStyles({ link });

  const getClassName = idx => {
    if (cellClassNames && cellClassNames[idx]) return clsx(classes.headerCell, cellClassNames[idx]);

    return classes.headerCell;
  };

  const tableCtx = {
    getRowCells,
    getSubtitle,
    getSectionRows,
    getCellAttributes,
    cellClassNames,
    getCellStyle,
    link,
    subtitleLink,
    columnHeaders,
  };

  return (
    <TableContext.Provider value={tableCtx}>
      <div className={clsx(classes.paper, className)}>
        {title && (
          <div className={classes.toolbar}>
            <Grid container className={classes.titleContainer} spacing={2}>
              <Grid item className={classes.titleText}>
                <Typography variant="h3" className={classes.toolbarTitle}>
                  {title}
                </Typography>
                {tableSubtitle && (
                  <Typography variant="h4" className={classes.toolbarSubtitle}>
                    {tableSubtitle}
                  </Typography>
                )}
              </Grid>
              <Grid item className={classes.titleButton}>
                {titleButton}
              </Grid>
            </Grid>
          </div>
        )}

        <MuiTable className={clsx(classes.table, tableClassName)}>
          {cellClassNames && (
            <colgroup>
              {cellClassNames.map((className, idx) => (
                <col key={idx} className={className} />
              ))}
            </colgroup>
          )}

          <TableHead>
            <MuiTableRow className={classes.headerRow}>
              {showColumnHeaders &&
                columnHeaders.map((header, idx) => (
                  <TableCell className={getClassName(idx)} key={idx}>
                    {header}
                  </TableCell>
                ))}
            </MuiTableRow>
          </TableHead>

          <TableBody className={clsx(classes.tableBody, { [classes.tableBodyLoading]: isLoading })}>
            {isLoading && (
              <MuiTableRow className={classes.loadingRow}>
                <TableCell>
                  <Grid container direction="row" className={classes.loadingGrid}>
                    <Grid item>
                      <CircularProgress />
                    </Grid>
                  </Grid>
                </TableCell>
              </MuiTableRow>
            )}

            {children}

            {children === undefined && data !== null && data.length > 0 && (
              <>
                {rowElement &&
                  data.map((row, key) => React.createElement(rowElement, { row, key }))}

                {!rowElement &&
                  getSubtitle &&
                  data.map((section, sectionIdx) => (
                    <React.Fragment key={sectionIdx}>
                      <TableSubtitle href={subtitleLink ? subtitleLink(section) : undefined}>
                        {getSubtitle(section)}
                      </TableSubtitle>

                      {getSectionRows(section).map((row, rowIdx) => (
                        <TableRow
                          key={rowIdx}
                          row={row}
                          href={link ? link(row) : undefined}
                          onClick={
                            onRowClick
                              ? () => {
                                  onRowClick(row);
                                }
                              : null
                          }
                        />
                      ))}
                    </React.Fragment>
                  ))}

                {!rowElement &&
                  !getSubtitle &&
                  data.map((row, rowIdx) => (
                    <TableRow
                      key={rowIdx}
                      row={row}
                      href={link ? link(row) : undefined}
                      onClick={
                        onRowClick
                          ? () => {
                              onRowClick(row);
                            }
                          : null
                      }
                    />
                  ))}
              </>
            )}

            {(data === null || data.length == 0) &&
              emptyText &&
              !isLoading &&
              children === undefined && (
                <MuiTableRow className={classes.emptyTableRow}>
                  <TableCell className={classes.emptyTableCell} colSpan={columnHeaders.length}>
                    {emptyText}
                  </TableCell>
                </MuiTableRow>
              )}

            {totalCount > 0 && onPageChange && (
              <MuiTableRow className={classes.pageLinksRow}>
                <TablePagination
                  colSpan={columnHeaders.length}
                  page={page}
                  rowsPerPage={rowsPerPage}
                  rowsPerPageOptions={[rowsPerPage]}
                  count={totalCount}
                  onPageChange={onPageChange}
                />
              </MuiTableRow>
            )}
          </TableBody>
        </MuiTable>
      </div>
    </TableContext.Provider>
  );
}

Table.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)]),
  title: PropTypes.string,
  titleButton: PropTypes.node,
  showColumnHeaders: PropTypes.bool,
  columnHeaders: PropTypes.array.isRequired,
  data: PropTypes.array,
  rowElement: PropTypes.func,
  getRowCells: PropTypes.func,
  getSubtitle: PropTypes.func,
  getSectionRows: PropTypes.func,
  getCellAttributes: PropTypes.func,
  getCellStyle: PropTypes.func,
  cellClassNames: PropTypes.array,
  className: PropTypes.string,
  tableClassName: PropTypes.string,
  isLoading: PropTypes.bool,
  link: PropTypes.func,
  subtitleLink: PropTypes.func,
  emptyText: PropTypes.string,
  page: PropTypes.number,
  rowsPerPage: PropTypes.number,
  totalCount: PropTypes.number,
  onPageChange: PropTypes.func,
};

Table.defaultProps = {
  children: undefined,
  title: null,
  titleButton: null,
  tableSubtitle: null,
  showColumnHeaders: true,
  data: [],
  rowElement: null,
  getSubtitle: null,
  getSectionRow: null,
  className: null,
  tableClassName: null,
  isLoading: false,
  link: null,
  subtitleLink: null,
  emptyText: 'No data.',
  getCellAttributes: () => ({}),
  getCellStyle: () => [],
  cellClassNames: null,
  page: 0,
  rowsPerPage: 20,
  totalCount: null,
  onPageChange: null,
  onRowClick: null,
};
