import { useMemo } from 'react';
import { formatTime } from 'util/dates';
import NamedPlayerStatsRow, { NamedPlayerStatsContainer, NamedPlayerStatsHeaderRow } from './NamedPlayerStatsRow';
import PropTypes from 'prop-types';

export default function NamedPlayerStatsTable({ stats }) {
  if (stats === undefined || stats.length === 0) return null;

  const statsByPlayer = useMemo(() => {
    const byPlayer = {};
    for (const player of stats) {
      byPlayer[player.id] = {};
      for (const stat of player.stats) {
        byPlayer[player.id][stat.name] = stat.value;
      }
    }

    return byPlayer;
  }, [stats]);

  console.log(statsByPlayer);

  function getPlayerStat(key, playerId, defaultValue) {
    if (statsByPlayer[playerId] !== undefined && statsByPlayer[playerId][key] !== undefined) {
      return statsByPlayer[playerId][key];
    }

    if (defaultValue !== undefined) return defaultValue;

    return '';
  }

  return (
    <NamedPlayerStatsContainer stats={stats}>
      <NamedPlayerStatsHeaderRow />

      <NamedPlayerStatsRow
        label="K/D"
        getValue={playerId => `${getPlayerStat('kills', playerId, 0)}-${getPlayerStat('deaths', playerId, 0)}`}
      />
      <NamedPlayerStatsRow
        label="Military K/D"
        getValue={playerId =>
          `${getPlayerStat('military_kills', playerId, 0)}-${getPlayerStat('military_deaths', playerId, 0)}`
        }
      />
      <NamedPlayerStatsRow label="Queen Kills" getValue={playerId => getPlayerStat('queen_kills', playerId)} />
      <NamedPlayerStatsRow label="Bump Assists" getValue={playerId => getPlayerStat('bump_assists', playerId)} />
      <NamedPlayerStatsRow
        label="Warrior Uptime"
        getValue={playerId => formatTime(getPlayerStat('warrior_uptime', playerId))}
      />
      <NamedPlayerStatsRow label="Berries Deposited" getValue={playerId => getPlayerStat('berries', playerId)} />
      <NamedPlayerStatsRow
        label="Berries Kicked In"
        getValue={playerId => getPlayerStat('berries_kicked', playerId)}
      />
      <NamedPlayerStatsRow label="Snail Meters" getValue={playerId => getPlayerStat('snail_distance', playerId)} />
    </NamedPlayerStatsContainer>
  );
}
