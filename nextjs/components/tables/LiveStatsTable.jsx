import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { Grid, Typography } from '@material-ui/core';
import Icon from '@mdi/react';
import { mdiTimer, mdiMap, mdiFruitGrapes, mdiRunFast, mdiSword } from '@mdi/js';

import { TEAMS, BLUE_TEAM, GOLD_TEAM, POSITIONS_BY_TEAM, MAP_NAMES } from 'util/constants';
import { getAxios } from 'util/axios';
import { formatUTC } from 'util/dates';
import { useWebSocket } from 'util/websocket';
import Table from './Table';

const useStyles = makeStyles(theme => ({
  icon: {
    marginTop: '12px',
    width: '20px',
    [theme.breakpoints.up('md')]: {
      width: '40px',
    },
  },
  statusRow: {
    marginTop: theme.spacing(-1),
    textAlign: 'center',
    overflow: 'visible',
    [theme.breakpoints.up('md')]: {
      marginTop: '-20px',
    },
  },
  statusIcon: {
    width: '10px',
    height: '10px',
    borderRadius: '5px',
    background: 'transparent',
    color: 'transparent',
    margin: '0 1px',
    [theme.breakpoints.up('md')]: {
      margin: '0 2px',
      width: '24px',
      height: '24px',
      borderRadius: '12px',
    },
  },
  statusIconOn: {
    background: theme.palette.text.secondary,
    color: theme.palette.background.default,
  },
  blueTeam: {
    textAlign: 'center',
    backgroundColor: theme.palette.blue.light5,
    fontSize: '12px',
    [theme.breakpoints.up('md')]: {
      fontSize: '18px',
    },
  },
  goldTeam: {
    textAlign: 'center',
    backgroundColor: theme.palette.gold.light5,
    fontSize: '12px',
    [theme.breakpoints.up('md')]: {
      fontSize: '18px',
    },
  },
  statName: {
    textAlign: 'center',
    backgroundColor: theme.palette.background.paper,
    fontWeight: 'bold',
    borderLeft: `1px solid ${theme.palette.divider}`,
    borderRight: `1px solid ${theme.palette.divider}`,
    overflow: 'hidden',
    width: '80px',
    fontSize: '12px',
    [theme.breakpoints.up('md')]: {
      fontSize: '18px',
      width: '200px',
    },
  },
  subtitle: {
    background: theme.palette.grey['800'],
    color: 'white',
  },
  subtitleItem: {
    flexBasis: 0,
    flexGrow: 1,
    textAlign: 'center',
    color: 'white',
    '& svg': {
      width: '24px',
      height: '24px',
      verticalAlign: 'middle',
    },
  },
}));

export default function LiveStatsTable({ cabinet }) {
  const classes = useStyles();
  const [stats, setStats] = useState(null);

  const title = 'Live Stats';
  const columnHeaders = [
    ...POSITIONS_BY_TEAM[BLUE_TEAM].map(pos => (<img className={classes.icon} src={pos.ICON} />)),
    '',
    ...POSITIONS_BY_TEAM[GOLD_TEAM].map(pos => (<img className={classes.icon} src={pos.ICON} />)),
  ];

  const getRowCells = row => [
    ...POSITIONS_BY_TEAM[BLUE_TEAM].map(pos => row.getValue(pos) ?? ''),
    row.statName,
    ...POSITIONS_BY_TEAM[GOLD_TEAM].map(pos => row.getValue(pos) ?? ''),
  ];

  const cellClassNames = [
    ...POSITIONS_BY_TEAM[BLUE_TEAM].map(pos => `${classes.blueTeam} px-1 md:px-3`),
    `${classes.statName} w-[72px] md:w-[150px] px-1 md:px-4 text-[10px] md:text-base`,
    ...POSITIONS_BY_TEAM[GOLD_TEAM].map(pos => classes.goldTeam),
  ];

  const onOpen = () => {
    ws.sendJsonMessage({ type: 'subscribe', cabinet_id: cabinet.id });
  };

  const ws = useWebSocket('/ws/ingame_stats', { onOpen });
  ws.onJsonMessage(message => {
    setStats(message);
  });

  const getSubtitle = data => (
    <Grid container direction="row" className={classes.subtitle}>
      <Grid item className={classes.subtitleItem}>
        <Icon path={mdiMap} /> {MAP_NAMES[stats?.mapName] ?? stats?.mapName}
      </Grid>
      <Grid item className={classes.subtitleItem}>
        <Icon path={mdiTimer} /> {formatUTC((stats?.gameTime ?? 0) * 1000, 'm:ss')}
      </Grid>
      <Grid item className={classes.subtitleItem}>
        <Icon path={mdiFruitGrapes} /> {stats?.berriesRemaining}
      </Grid>
    </Grid>
  );

  const data = [
    {statName: '', getValue: pos => (
      <Grid container direction="column">
        <Grid item><img className={classes.icon} src={pos.ICON} /></Grid>
        <Grid item className={classes.statusRow}>
          <Icon path={mdiRunFast} className={clsx(classes.statusIcon, { [classes.statusIconOn]: stats?.speed.includes(pos.ID.toString()) })} />
          <Icon path={mdiSword} className={clsx(classes.statusIcon, { [classes.statusIconOn]: stats?.warriors.includes(pos.ID.toString()) })} />
        </Grid>
      </Grid>
    )},
    {statName: 'Total Kills', getValue: pos =>(<strong>{stats?.kills[pos.ID]}</strong>) ?? ''},
    {statName: (<span className="font-normal">Military Kills</span>), getValue: pos => stats?.militaryKills[pos.ID] ?? ''},
    {statName: (<span className="font-normal">Drone Kills</span>), getValue: pos => stats?.workerKills[pos.ID] ?? ''},
    {statName: 'Queen Kills', getValue: pos => stats?.queenKills[pos.ID] ?? ''},
    {statName: 'Total Deaths', getValue: pos => (<strong>{stats?.deaths[pos.ID]}</strong>) ?? ''},
    {statName: (<span className="font-normal">Military Deaths</span>), getValue: pos => stats?.militaryDeaths[pos.ID] ?? ''},
    {statName: (<span className="font-normal">Drone Deaths</span>), getValue: pos => stats?.workerDeaths[pos.ID] ?? ''},
    {statName: 'Berries', getValue: pos => stats?.berriesDeposited[pos.ID] ?? ''},
    {statName: 'Snail Distance', getValue: pos => Math.floor((stats?.snailDistance[pos.ID] ?? 0) / 21) || ''},
  ];

  return (
    <Table
      title={title}
      columnHeaders={columnHeaders}
      showColumnHeaders={false}
      getSubtitle={getSubtitle}
      getSectionRows={() => data}
      data={[[]]}
      getRowCells={getRowCells}
      isLoading={stats === null}
      cellClassNames={cellClassNames}
    />
  );
}
