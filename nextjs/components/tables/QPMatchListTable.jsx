import PropTypes from 'prop-types';
import { Grid, Typography, Link } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import Table from './Table';

const useStyles = makeStyles(theme => ({
  blue: {
    background: theme.gradients.blue.light,
  },
  gold: {
    background: theme.gradients.gold.light,
  },
  score: {
    ...theme.mixins.tableCell.numeric(theme),
  },
}));

export default function QPMatchListTable({ title, data }) {
  const classes = useStyles();

  const columnHeaders = ['Blue Team', 'Blue Score', 'Gold Team', 'Gold Score'];
  const getRowCells = row => [
    row.players.blue.map(player => player.name).join(", "),
    row.blueScore,
    row.players.gold.map(player => player.name).join(", "),
    row.goldScore,
  ];

  const cellClassNames = [
    classes.blue,
    clsx(classes.blue, classes.score),
    classes.gold,
    clsx(classes.gold, classes.score),
  ];

  const getCellStyle = row => {
    const bold = { fontWeight: 'bold' };

    if (row.isComplete && row.blueScore > row.goldScore)
      return [ bold, bold, {}, {} ];
    if (row.isComplete && row.goldScore > row.blueScore)
      return [ {}, {}, bold, bold ];

    return [ {}, {}, {}, {} ];
  };

  const rowLink = row => `/qpmatch/${row.id}`;

  return (
    <Table title={title} showColumnHeaders={false} columnHeaders={columnHeaders} data={data} getRowCells={getRowCells} isLoading={false} link={rowLink} getCellStyle={getCellStyle} cellClassNames={cellClassNames} />
  );
}

QPMatchListTable.propTypes = {
  title: PropTypes.string,
  data: PropTypes.array.isRequired,
};

QPMatchListTable.defaultProps = {
  title: 'Matches',
};
