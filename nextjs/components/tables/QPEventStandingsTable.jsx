import { useRouter } from 'next/router';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { NumericFormat } from 'react-number-format';

import Table from './Table';

const useStyles = makeStyles(theme => ({
  teamName: {
    fontWeight: 'bold',
  },
  numeric: {
    ...theme.mixins.tableCell.numeric(theme),
  },
}));

export default function QPEventStandingsTable({ title, season, data }) {
  const classes = useStyles();
  const columnHeaders = ['Player Name', 'Points', 'Rounds', 'Matches'];
  const getRowCells = row => [
    row.player.name,
    <NumericFormat
      value={row.points}
      displayType="text"
      decimalScale={2}
      fixedDecimalScale={true}
    />,
    `${row.roundsWon}-${row.roundsLost}`,
    `${row.matchesWon}-${row.matchesLost}`,
  ];

  const cellClassNames = [classes.teamName, classes.numeric, classes.numeric, classes.numeric];
  const rowLink = row => `/player/${row.player.id}`;

  return (
    <Table
      title={title}
      columnHeaders={columnHeaders}
      data={data}
      getRowCells={getRowCells}
      isLoading={false}
      link={rowLink}
      cellClassNames={cellClassNames}
    />
  );
}

QPEventStandingsTable.propTypes = {
  title: PropTypes.string,
  data: PropTypes.array.isRequired,
};

QPEventStandingsTable.defaultProps = {
  title: 'Standings',
};
