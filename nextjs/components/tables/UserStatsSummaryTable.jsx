import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Box, Typography, Link } from '@material-ui/core';
import { NumericFormat } from 'react-number-format';

import Table from './Table';
import { formatInterval } from 'util/dates';

const useStyles = makeStyles(theme => ({
  label: {},
  numeric: {
    ...theme.mixins.tableCell.numeric(theme),
    width: '300px',
  },
}));

export default function UserStatsSummaryTable({ stats, title, titleButton, subtitle, className }) {
  const classes = useStyles();

  const formatValue = row => {
    if (row.name == 'warrior_uptime' || row.name == 'time_played') {
      return formatInterval(row.value);
    } else {
      return <NumericFormat value={row.value} displayType="text" thousandSeparator="," />;
    }
  };

  const columnHeaders = ['Label', 'Value'];
  const getRowCells = row => [row.label, formatValue(row)];

  const cellClassNames = [classes.label, classes.numeric];

  return (
    <Table
      title={title}
      titleButton={titleButton}
      tableSubtitle={subtitle}
      columnHeaders={columnHeaders}
      data={stats}
      getRowCells={getRowCells}
      showColumnHeaders={false}
      cellClassNames={cellClassNames}
      className={className}
    />
  );
}

UserStatsSummaryTable.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  stats: PropTypes.array.isRequired,
};

UserStatsSummaryTable.defaultProps = {
  title: 'Total Stats',
  subtitle: null,
};
