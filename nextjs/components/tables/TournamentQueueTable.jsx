import PropTypes from 'prop-types';
import Table, { TableRow, TableCell } from 'components/tables/Table';
import { useTournamentAdmin } from 'providers/TournamentAdmin';

export function TournamentQueueMatch({ match }) {
  const { teamsById } = useTournamentAdmin();

  const getTeamName = teamId => (teamId ? teamsById[teamId].name : '(unknown team)');

  return (
    <TableRow key={match.id}>
      <TableCell>{getTeamName(match.blueTeam)}</TableCell>
      <TableCell>{match.roundName}</TableCell>
      <TableCell>{getTeamName(match.goldTeam)}</TableCell>
    </TableRow>
  );
}

export default function TournamentQueueTable({ title, cabinet }) {
  const { queues } = useTournamentAdmin();

  const queue = queues?.filter(q => q.cabinet === (cabinet?.id ?? null)).shift();

  const columnHeaders = ['Blue', 'Round', 'Gold'];

  const props = {
    title,
    columnHeaders,
  };

  if (queue === undefined) return <></>;

  return (
    <Table {...props}>
      {queue?.matchList?.map(match => (
        <TournamentQueueMatch match={match} />
      ))}
    </Table>
  );
}

TournamentQueueTable.defaultProps = {
  title: 'Cabinet Queue',
};
