import { makeStyles } from '@material-ui/core/styles';
import { Box, Typography, Link } from '@material-ui/core';
import { mdiTrophy, mdiMap, mdiTimer, mdiStarThreePointsOutline, mdiContentSave, mdiChevronRight } from '@mdi/js';
import { format } from 'date-fns';
import Icon from '@mdi/react';

import { SummaryTable, SummaryTableRow } from 'components/tables/SummaryTable';

const useStyles = makeStyles(theme => ({
  linkedTable: ({ game }) => ({
    borderRadius: '5px',
    background: theme.palette[game?.winningTeam]?.light3,
    transition: 'background 150ms ease-in-out',
    '& .MuiTypography-colorPrimary': {
      color: theme.palette.text.primary,
      textDecoration: 'none',
    },
    '&:hover': {
      background: theme.palette[game?.winningTeam]?.light2,
    }
  }),
  linkedTableTitle: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: theme.spacing(2),
  },
  link: {
    display: 'block',
  },
}));

export default function GameSummaryTable({ game, showGameVersion, className }) {
  return (
    <div className={`p-4 ${className}`}>
      <SummaryTableRow icon={mdiTrophy} label="Winner">
        <Typography><b>{game.winningTeamDisplay}</b></Typography>
      </SummaryTableRow>
      <SummaryTableRow icon={mdiMap} label="Map">
        <Typography>{game.map}</Typography>
      </SummaryTableRow>
      <SummaryTableRow icon={mdiStarThreePointsOutline} label="Win Condition">
        <Typography>{game.winConditionDisplay}</Typography>
      </SummaryTableRow>
      <SummaryTableRow icon={mdiTimer} label="Game Time">
        <Typography>{game.length}</Typography>
      </SummaryTableRow>
      {showGameVersion && (
        <SummaryTableRow icon={mdiContentSave} label="Game Version">
          <Typography>{game.cabinetVersion ?? 'Unknown (pre-17.14)'}</Typography>
        </SummaryTableRow>
      )}
    </div>
  );
}

export function LinkedGameSummaryTable({ game, round }) {
  const classes = useStyles({ game });

  return (
    <Box className={classes.linkedTable}>
      <Link className={classes.link} href={`/game/${game.gameId}`}>
        <Typography suppressHydrationWarning className={classes.linkedTableTitle} variant="h3">
          Game {round} - {format(new Date(game.startTime), 'h:mm a')}
          <Icon className="w-6 h-6" path={mdiChevronRight} />
        </Typography>
        <div className="divider"></div>
        <GameSummaryTable game={game} />
      </Link>
    </Box>
  );
}

export function GameSummaryTableList({ games }) {
  return (
    <div className="grid grid-cols-3 gap-6">
      {games
        .sort((a, b) => new Date(a.endTime) - new Date(b.endTime))
        .map((game, i) => (
        <LinkedGameSummaryTable key={game.gameId} game={game} round={i+1} />
      ))}
    </div>
  );
}


