import { useRouter } from 'next/router';
import PropTypes from 'prop-types';

import { getAxios } from 'util/axios';
import Table from './Table';

export default function BYOTeamFinalStandingsTable({ title, season, data }) {
  const columnHeaders = ['Place', 'Team Name'];
  const getRowCells = (row, idx) => [
    row.place,
    row.team.name,
  ];

  const rowLink = row => `/season-team/${row.team.id}`;

  return (
    <Table title={title} columnHeaders={columnHeaders} data={data} getRowCells={getRowCells} isLoading={false} link={rowLink} />
  );
}

BYOTeamFinalStandingsTable.propTypes = {
  title: PropTypes.string,
  data: PropTypes.array.isRequired,
};

BYOTeamFinalStandingsTable.defaultProps = {
  title: 'Final Standings',
};
