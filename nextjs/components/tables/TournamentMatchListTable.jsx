import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import React, { useState } from 'react';

import TournamentMatchEditForm from 'components/forms/TournamentMatchEditForm';
import { TOURNAMENT_LINK_TYPES } from 'util/constants';
import Table, { TableCell, TableRow, TableSubtitle } from './Table';

const useStyles = makeStyles(theme => ({
  blue: {
    width: '40%',
    '&.winner': {
      fontWeight: 'bold',
      background: theme.gradients.blue.light,
    },
  },
  gold: {
    width: '40%',
    '&.winner': {
      fontWeight: 'bold',
      background: theme.gradients.gold.light,
    },
  },
  teamName: {
    lineHeight: '1.15',
  },
  score: {
    ...theme.mixins.tableCell.numeric(theme),
    lineHeight: '1.15',
  },
  roundName: {
    width: '3rem',
    flexBasis: '3rem',
    textAlign: 'center',
    background: 'white',
    whiteSpace: 'nowrap',
  },
  winner: {
    fontWeight: 'bold',
  },
}));

function Divider({}) {
  return (
    <div className="md:hidden w-full border-0 border-b border-b-gray-300 md:border-0 border-solid my-1 mix-blend-multiply"></div>
  );
}

function MatchRow({ tournament, match, teamNames, editable }) {
  const classes = useStyles();
  const [isOpen, setIsOpen] = useState(false);

  return (
    <React.Fragment>
      <TableRow
        onClick={editable ? () => setIsOpen(true) : undefined}
        href={editable ? undefined : `/tournament-match/${match.id}`}
      >
        <TableCell
          className={clsx(classes.blue, {
            ['winner']: match.isComplete && match.blueScore > match.goldScore,
          })}
        >
          <div className="flex justify-between md:flex-row flex-col items-center">
            <div className={clsx(classes.teamName, 'md:max-w-[40%] text-center md:text-left')}>
              {teamNames[match.blueTeam]}
            </div>

            <Divider />

            <div className={clsx(classes.score, 'w-full md:w-[100px] text-center md:text-right')}>
              {match.blueScore}
            </div>
          </div>
        </TableCell>

        <TableCell className={classes.roundName}>{match.roundName}</TableCell>
        <TableCell
          className={clsx(classes.gold, classes.teamName, {
            ['winner']: match.isComplete && match.goldScore > match.blueScore,
          })}
        >
          <div className="flex justify-between  md:flex-row flex-col">
            <div
              className={clsx(
                classes.teamName,
                'md:max-w-[40%] md:order-2 text-center md:text-right',
              )}
            >
              {teamNames[match.goldTeam]}
            </div>

            <Divider />

            <div className={clsx(classes.score, 'w-full md:w-[100px] text-center md:text-left ')}>
              {match.goldScore}
            </div>
          </div>
        </TableCell>
      </TableRow>

      {editable && (
        <TournamentMatchEditForm
          tournament={tournament}
          match={match}
          isOpen={isOpen}
          onClose={() => setIsOpen(false)}
        />
      )}
    </React.Fragment>
  );
}

export default function TournamentMatchListTable({ tournament, title, editable, filter }) {
  const classes = useStyles();
  const columnHeaders = ['Blue Team & Score', '', 'Gold Team & Score'];

  const teamNames = {};
  for (const team of tournament.teams) {
    teamNames[team.id] = team.name;
  }

  const getSubtitleLink = bracket => {
    if (tournament.linkType == TOURNAMENT_LINK_TYPES.MANUAL) {
      return undefined;
    }

    if (tournament.linkType == TOURNAMENT_LINK_TYPES.CHALLONGE) {
      if (bracket.linkedOrg) {
        return `https://${bracket.linkedOrg}.challonge.com/${bracket.linkedBracketId}`;
      }

      return `https://challonge.com/${bracket.linkedBracketId}`;
    }
  };

  const divider = (
    <div className="md:hidden w-full border-0 border-b border-b-gray-300 md:border-0 border-solid my-1 mix-blend-multiply"></div>
  );
  const bracketMatches = tournament.brackets
    .filter(b => b.matches.length > 0)
    .sort((a, b) => b.id - a.id);

  return bracketMatches.length ? (
    <Table
      title={title}
      showColumnHeaders={false}
      columnHeaders={columnHeaders}
      isLoading={false}
      tableClassName="table-auto"
    >
      {bracketMatches.map(bracket => (
        <React.Fragment key={bracket.id}>
          <TableSubtitle href={getSubtitleLink(bracket)}>{bracket.name}</TableSubtitle>

          {bracket.matches
            .filter(filter)
            .sort((a, b) => b.id - a.id)
            .map(match => (
              <MatchRow
                key={match.id}
                tournament={tournament}
                match={match}
                teamNames={teamNames}
                editable={editable}
              />
            ))}
        </React.Fragment>
      ))}
    </Table>
  ) : null;
}

TournamentMatchListTable.propTypes = {
  title: PropTypes.string,
  editable: PropTypes.bool,
  filter: PropTypes.func,
  tournament: PropTypes.object.isRequired,
};

TournamentMatchListTable.defaultProps = {
  title: 'Matches',
  editable: false,
  filter: i => true,
};
