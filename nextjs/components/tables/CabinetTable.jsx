import { Button } from '@material-ui/core';
import { useState } from 'react';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { format } from 'date-fns';

import { getAxios } from 'util/axios';
import Table from './Table';
import { isAdminOf } from 'util/auth';
import CabinetForm from 'components/forms/CabinetForm';

export default function CabinetTable({ title, scene }) {
  const axios = getAxios();
  const [data, setData] = useState(null);

  const columnHeaders = ['Name', 'Total Games', 'Last Game'];
  const getRowCells = row => [
    row.displayName,
    row.totalGames,
    row.lastGameTime ? format(new Date(row.lastGameTime), 'MMM d, yyyy - hh:mm aa') : '',
  ];

  const rowLink = row => `/cabinet/${scene.name}/${row.name}`;

  const [showOld, setShowOld] = useState(false);

  if (data === null) {
    axios.get('/api/game/cabinet/', { params: { sceneId: scene.id } }).then((response) => {
      const cabinets = response.data.results.sort((a, b) =>
        a.displayName.localeCompare(b.displayName)
      );

      setData(cabinets);
    });
  }

  const props = {
    title,
    columnHeaders,
    data: data?.filter(cabinets => showOld || new Date(cabinets.lastGameTime) > new Date(new Date().setMonth(new Date().getMonth() - 6))),
    getRowCells,
    isLoading: data === null,
    link: rowLink,
  };

  if (isAdminOf(scene.id)) {
    props.titleButton = (<CabinetForm scene={scene} />);
  }

  props.titleButton = (
    <>
      <div className="flex gap-2 items-center">
        <Button
          className=" ml-auto inline-block"
          size="small"
          variant="outlined"
          onClick={() => setShowOld(!showOld)}
        >
          {showOld ? 'Hide' : 'Show'} old
        </Button>
        {props?.titleButton}
      </div>
    </>
  );

  return (
    <Table {...props} />
  );
}

CabinetTable.propTypes = {
  title: PropTypes.string,
  scene: PropTypes.object.isRequired,
};

CabinetTable.defaultProps = {
  title: 'Cabinets',
};
