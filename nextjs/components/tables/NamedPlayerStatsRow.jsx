import { useContext, createContext } from 'react';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { BLUE_TEAM, GOLD_TEAM, POSITIONS_BY_TEAM } from 'util/constants';

export const PlayerStatsContext = createContext({});

const useStyles = makeStyles(theme => ({
  backgroundBlue: {
    background: theme.gradients.blue.light,
  },
  backgroundGold: {
    background: theme.gradients.gold.light,
  },
  row: {
    flexDirection: 'column',
    flexBasis: 0,
    flexGrow: 1,
  },
  playerCell: {
    textAlign: 'center',
    verticalAlign: 'middle',
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'center',
    background: 'transparent',
    flexBasis: '36px',
    height: '36px',
    lineHeight: '36px',
    flexGrow: 0,
    fontSize: '12px',
    [theme.breakpoints.up('lg')]: {
      fontSize: '16px',
    },
    alignItems: 'center'
  },
  value: {
    fontSize: 'inherit',
  },
  labelCell: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(0.5, 1),
    textAlign: 'center',
    flexGrow: 0,
    fontWeight: 'bold',
    flexBasis: '60px',
    verticalAlign: 'middle',
    fontSize: '12px',
    lineHeight: '14px',
    [theme.breakpoints.up('lg')]: {
      fontSize: '16px',
      lineHeight: '18px',
    },
    '& .MuiTypography-root': {
      fontWeight: 'bold',
    },
  },
  headerRow: {
    [theme.breakpoints.down('sm')]: {
      width: '144px',
      flexBasis: '144px',
    },
    [theme.breakpoints.up('md')]: {
      width: '192px',
      flexBasis: '192px',
    },
    flexGrow: 0,
  },
  icon: {
    padding: '12px 6px',
    padding: 0,
    width: '48px',
    '& img': {
      margin: '-8px 0 -8px -8px'
    },
  },
  iconGold: {
    '& span': {
      transform: 'scaleX(-1)',
    },
  },
  playerIcon: {
    display: 'block',
    height: '48px',
    width: '48px',
    backgroundSize: '48px 48px',
    backgroundPosition: 'center',
    padding: 0,
    height: '37px',
  }
}));

export function NamedPlayerStatsContainer({ stats, children }) {
  const classes = useStyles();
  if (stats === undefined) {
    return <></>;
  }

  return (
    <PlayerStatsContext.Provider value={{ stats }}>
      <div className="overflow-scroll rounded">
        <Grid container className={clsx(classes.container, 'flex-nowrap lg:flex-wrap')}>{children}</Grid>
      </div>
    </PlayerStatsContext.Provider>
  );
}

export function NamedPlayerStatsHeaderRow() {
  const classes = useStyles();
  const { stats } = useContext(PlayerStatsContext);

  return (
    <Grid item container className={clsx(classes.row, classes.headerRow, 'sticky left-0 md:relative odd:bg-gray-100 bg-gray-200')}>
      <Grid item className={clsx(classes.headerCell, classes.playerCell, classes.labelCell)} />

      {stats && stats.map(player => (
        <Grid key={player.id} item className={clsx(classes.headerCell, classes.playerCell)}>
          <span>{player.name}</span>

          {(player.positions) && player.positions.map(pos => (
            <span className={clsx(classes.playerIcon)} style={{ backgroundImage: `url(${POSITIONS_BY_ID[pos].IMAGE})` }}></span>
          ))}
        </Grid>

      ))}
    </Grid>
  )
}

export default function NamedPlayerStatsRow({ label, values, format, defaultValue, getValue = null }) {
  const classes = useStyles();
  const { stats } = useContext(PlayerStatsContext);
  const getValueDefault =  position => {
    const value = values[position] ?? defaultValue ?? 0;
    return format ? format(value) : value;
  };

  const getValueFn = getValue || getValueDefault;

  return (
    <Grid item container className={clsx(classes.row, classes.statRow, 'odd:bg-gray-100 bg-gray-200')}>
      <Grid item className={classes.labelCell}>
        <span className={classes.label}>{label}</span>
      </Grid>

      {stats && stats.map(player => (
        <Grid item className={clsx(classes.valueCell, classes.playerCell, classes.backgroundBlue)} key={player.id}>
          <Typography className={classes.value}>{getValueFn(player.id)}</Typography>
        </Grid>
      ))}
    </Grid>
  );
}
