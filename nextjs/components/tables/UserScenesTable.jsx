import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import { formatDistance } from 'date-fns';
import clsx from 'clsx';

import Table, { TableRow, TableCell } from './Table';
import { SceneTableDisplayName } from './SceneTable';

const useStyles = makeStyles(theme => ({
  sceneName: {
    fontWeight: 'bold',
  },
  description: {
    fontStyle: 'italic',
  },
}));

export default function UserScenesTable({ title, scenes }) {
  const classes = useStyles();
  const columnHeaders = [''];

  return (
    <Table title={title} columnHeaders={columnHeaders} showColumnHeaders={false} isLoading={false} className="mt-0">
      {scenes.map(scene => (
        <TableRow href={`/scene/${scene.name.toLowerCase()}`} key={scene.name} className="grid grid-cols-1 md:grid-cols-2 gap-y-2">
            <SceneTableDisplayName scene={scene} />
            <TableCell
              className={clsx(classes.description,' flex items-center text-xs -mt-3 md:mt-0  pl-10')}
              linkClassName="flex items-center">
              {scene.lastPlayed ?
                `You played here ${formatDistance(new Date(), new Date(scene.lastPlayed))} ago` :
                (scene.isAdmin ? 'You are an admin' : '')
              }
            </TableCell>
        </TableRow>
      ))}
    </Table>
  );
}
