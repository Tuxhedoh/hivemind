import { useRouter } from 'next/router';
import PropTypes from 'prop-types';

import { getAxios } from 'util/axios';
import Table from './Table';

export default function PlayerListTable({ title, data }) {
  const columnHeaders = ['Player Name'];
  const getRowCells = row => [
    row.name,
  ];

  const rowLink = row => `/player/${row.id}`;

  return (
    <Table title={title} columnHeaders={columnHeaders} data={data} getRowCells={getRowCells} isLoading={false} link={rowLink} />
  );
}

PlayerListTable.propTypes = {
  title: PropTypes.string,
  data: PropTypes.array.isRequired,
};

PlayerListTable.defaultProps = {
  title: 'Players',
};
