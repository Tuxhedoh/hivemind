import { Button, Grid, Link } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { useState } from 'react';

import TournamentTeamForm from 'components/forms/TournamentTeamForm';
import { isAdminOf } from 'util/auth';
import Table, { TableCell, TableRow } from './Table';

const useStyles = makeStyles(theme => ({}));

export default function TournamentTeamTable({ title, tournament, onSave }) {
  const classes = useStyles();
  const [formOpen, setFormOpen] = useState(false);
  const [selectedTeam, setSelectedTeam] = useState({});

  const columnHeaders = ['Team Name'];
  const openForm = (e, team) => {
    e.preventDefault();

    setSelectedTeam(team);
    setFormOpen(true);
  };

  const closeForm = () => {
    setFormOpen(false);
    onSave();
  };

  const props = {
    title,
    columnHeaders,
    isLoading: false,
    showColumnHeaders: false,
  };

  if (isAdminOf(tournament.scene.id)) {
    const blankTeam = { tournament: tournament.id };
    props.titleButton = (
      <Grid container direction="row" spacing={2} className="mt-2 md:-mt-2">
        <Grid item>
          <TournamentTeamForm team={blankTeam} tournament={tournament} onSave={onSave} />
        </Grid>
        <Grid item>
          <Button
            variant="outlined"
            color="secondary"
            href={`/tournament/${tournament.id}/players`}
          >
            Manage Teams
          </Button>
        </Grid>
      </Grid>
    );
  }

  return (
    <>
      <Table {...props}>
        <TableRow>
          <TableCell>
            <div className="grid grid-cols-1  lg:grid-cols-3 gap-0 -m-2 -mx-3">
              {tournament.teams.map(team => (
                <Link
                  className="px-4 py-4 text-gray-800 even:bg-gray-200"
                  href={`/tournament-team/${team.id}`}
                  key={team.id}
                >
                  {team.name}
                </Link>
              ))}
            </div>
          </TableCell>
        </TableRow>
      </Table>
      <TournamentTeamForm
        team={selectedTeam}
        tournament={tournament}
        showButton={false}
        open={formOpen}
        onClose={closeForm}
        enableReinitialize={true}
      />
    </>
  );
}

TournamentTeamTable.propTypes = {
  title: PropTypes.string,
  tournament: PropTypes.object.isRequired,
  onSave: PropTypes.func,
};

TournamentTeamTable.defaultProps = {
  title: 'Teams',
  onSave: () => null,
};
