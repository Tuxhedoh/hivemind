import GoogleLogin from './GoogleLogin';

export default function LoginButtons({ ...props }) {
  return (
    <GoogleLogin {...props} />
  );
}
