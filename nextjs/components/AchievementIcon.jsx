import { makeStyles } from '@material-ui/core/styles';
import { alpha } from '@material-ui/core/styles/colorManipulator';
import { Box, Chip, Typography, Tooltip } from '@material-ui/core';
import Icon from '@mdi/react';
import { mdiMedal } from '@mdi/js';

const useStyles = makeStyles(theme => ({
  container: {
  },
  icon: {
    verticalAlign: 'middle',
    '&:first-of-type': {
      marginLeft: theme.spacing(2),
    },
  },
  title: {
    fontWeight: 'bold',
    fontSize: '1.125rem',
    marginBottom: theme.spacing(1),
  },
  tooltipBlue: {
    backgroundColor: alpha(theme.palette.blue.dark5, 0.8),
    color: theme.palette.common.white,
  },
  tooltipGold: {
    backgroundColor: alpha(theme.palette.gold.dark5, 0.8),
    color: theme.palette.common.white,
  },
}));

export default function AchievementIcon({ achievement, team }) {
  const classes = useStyles({ achievement });
  const tooltipClass = { 'blue': classes.tooltipBlue, 'gold': classes.tooltipGold };

  const tooltip = (
    <Box className={classes.container}>
      <Typography className={classes.title}>{achievement.title}</Typography>
      <Typography className={classes.description}>{achievement.description}</Typography>
    </Box>
  );

  return (
    <Tooltip classes={{ tooltip: tooltipClass[team] }} title={tooltip} placement="right">
      <Icon path={mdiMedal} size={1} className={classes.icon} />
    </Tooltip>
  );
}
