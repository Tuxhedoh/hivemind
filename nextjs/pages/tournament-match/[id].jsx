import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Breadcrumbs, Link, Grid, Paper } from '@material-ui/core';
import { getAxios } from 'util/axios';
import { format } from 'date-fns';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import MatchResultTable from 'components/tables/MatchResultTable';
import TeamStatsTable from 'components/tables/TeamStatsTable';
import PlayerStatsTable from 'components/tables/PlayerStatsTable';
import { GameSummaryTableList } from 'components/tables/GameSummaryTable';
import YouTube from 'components/video/YouTube';

const useStyles = makeStyles(theme => ({
  video: {
  },
  matchStatsPaper: {
    padding: theme.spacing(1),
    marginTop: theme.spacing(3),
  },
  sectionTitle: {
    margin: theme.spacing(2, 0),
  },
}));

export default function TournamentMatchPage({ match }) {
  const classes = useStyles({ match });
  const [stats, setStats] = useState(null);
  const axios = getAxios({ authenticated: true });

  if (match.isComplete && stats === null) {
    axios.get(`/api/tournament/match/${match.id}/stats/`).then(response => {
      setStats(response.data);
    });
  }

  return (
    <div className="space-y-8">
      <Breadcrumbs>
        <Link href={`/tournament/${match.tournament.id}`}>{match.tournament.name}</Link>
      </Breadcrumbs>

      <Typography variant="h1">{match.blueTeam.name} vs. {match.goldTeam.name}</Typography>

      <MatchResultTable match={match} teamLinkPath="tournament-team" />


      <Grid container direction="row" spacing={2}>

        {stats && Object.values(stats?.byPlayer).length !== 0 && (
          <Grid item xs={12} lg={6}>
            <Typography variant="h2" className={classes.sectionTitle}>Match Stats</Typography>
            <TeamStatsTable stats={stats} xs={12} />
          </Grid>
        )}

          <Grid item xs={12} lg={6}>
            {match.videoLink && (<>
              <Typography variant="h2" className={classes.sectionTitle}>Match Video</Typography>
              <YouTube url={match.videoLink} />
              </>
            )}
          </Grid>
      </Grid>


{stats && Object.values(stats?.byPlayer).length !== 0 && (
        <>
          <Typography variant="h2" className={classes.sectionTitle}>Player Stats</Typography>
          <PlayerStatsTable game={stats} />
        </>
      )}

      <div className="space-y-4">

      <Typography variant="h2" className={classes.sectionTitle}>Game Results</Typography>

      {match.games.length === 0
        ? <Typography className={classes.noGames}>Game details not available for this match.</Typography>
        : <GameSummaryTableList games={match.games} />
      }
      </div>
    </div>
  );
}

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/tournament/match/${params.id}`);
  const match = response.data;

  if (!match) return { notFound: true };

  response = await axios.get(`/api/tournament/team/${match.blueTeam}/`)
  match.blueTeam = response.data;

  response = await axios.get(`/api/tournament/team/${match.goldTeam}/`)
  match.goldTeam = response.data;

  match.games = [];
  response = await axios.get('/api/game/game/', { params: { tournamentMatchId: match.id } });

  for (const game of response.data.results) {
    response = await axios.get(`/api/game/game/${game.id}/stats`);
    match.games.push(response.data);
  }

  return {
    props: {
      match,
      title: `${match.blueTeam.name} vs. ${match.goldTeam.name}`,
      description: `${match.blueTeam.name} vs. ${match.goldTeam.name} in ${match.tournament.name} in ${match.tournament.scene.displayName}.`,
    },
  };
}

