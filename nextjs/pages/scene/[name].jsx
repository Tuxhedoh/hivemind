import { makeStyles } from '@material-ui/core/styles';
import { Typography, Paper } from '@material-ui/core';
import PropTypes from 'prop-types';
import { format, parseISO } from 'date-fns'

import { getAxios } from 'util/axios';
import Markdown from 'components/Markdown';
import Title from 'components/Title';
import SeasonsTable from 'components/tables/SeasonsTable';
import TournamentTable from 'components/tables/TournamentTable';
import CabinetTable from 'components/tables/CabinetTable';
import GameTable from 'components/tables/GameTable';
import SceneForm from 'components/forms/SceneForm';

const useStyles = makeStyles(theme => ({
  description: {
    padding: theme.spacing(1, 2),
    margin: theme.spacing(2, 0),
    borderRadius: '5px',
    background: theme.palette.grey['100']
  },
}));

export default function ScenePage({ scene }) {
  const classes = useStyles();
  const form = (<SceneForm scene={scene} />);

  return (
    <div className="">
      <header className="mb-12">

        <Title title={scene.displayName} form={form} />

        { scene.description && (

          <div className={classes.description}>
            <Markdown contents={scene.description} />
          </div>
          )}

      </header>
      <CabinetTable scene={scene} />
      <div className="grid grid-cols-1 lg:grid-cols-2 lg:gap-12 ">

      <TournamentTable scene={scene} />
      <SeasonsTable scene={scene} />
      </div>
      <GameTable filters={{scene_id: scene.id}} />
    </div>
  );
}

ScenePage.propTypes = {
  scene: PropTypes.object.isRequired,
};

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  const response = await axios.get(`/api/game/scene/`, { params: { name: params.name } });
  const scene = response.data.results[0];

  if (!scene) return { notFound: true };

  return {
    props: { scene, title: scene.displayName, description: scene.description },
  };
}

