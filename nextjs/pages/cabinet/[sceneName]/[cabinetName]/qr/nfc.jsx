import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Paper, Grid, Typography } from '@material-ui/core';
import QRCode from 'qrcode';

import { getAxios } from 'util/axios';

const useStyles = makeStyles(theme => ({
  image: {
    marginBottom: theme.spacing(3),
  },
  paper: {
    padding: theme.spacing(2),
    maxWidth: '400px',
    margin: `${theme.spacing(4)}px auto ${theme.spacing(2)}px auto`,
  },
  paperGrid: {
    alignItems: 'center',
  },
  paperSection: {
    textAlign: 'center',
  },
  text: {
    fontFamily: 'Roboto Slab',
  },
  name: {
    textAlign: 'center',
  },
}));

export default function QRPage({ cabinet, images }) {
  const classes = useStyles();

  return (
    <>
      <Grid container direction="column">
        {images.map(image => (
          <>
            <Paper className={classes.paper}>
              <Grid container item direction="column" key={image.name} className={classes.paperGrid}>
                <Grid item className={classes.paperSection}>
                  <img src={image.url} />
                </Grid>

                <Grid item className={classes.paperSection}>
                  <Typography className={classes.text}>Scan to register your NFC tag.</Typography>
                </Grid>
              </Grid>
            </Paper>

            <Typography className={classes.name}>{image.name}</Typography>
          </>
        ))}
      </Grid>
    </>
  );
}

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/game/scene/`, { params: { name: params.sceneName } });
  const scene = response.data.results[0];

  if (!scene) return { notFound: true };

  response = await axios.get(`/api/game/cabinet/`, { params: { sceneId: scene.id, name: params.cabinetName }});
  const cabinet = response.data.results[0];

  if (!cabinet) return { notFound: true };

  cabinet.scene = scene;

  const images = [];
  for (const readerId of ['blue', 'gold']) {
    images.push({
      name: readerId,
      url: await QRCode.toDataURL(`https://kqhivemind.com/cabinet/${scene.name}/${cabinet.name}/register-nfc/${readerId}`),
    });
  }

  return {
    props: { cabinet, images },
  };
}

