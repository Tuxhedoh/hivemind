import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Breadcrumbs, Link, Paper, Button, CircularProgress } from '@material-ui/core';
import { useRouter } from 'next/router';
import { mdiAccount, mdiCity, mdiGamepadVariant } from '@mdi/js';

import { getAxios } from 'util/axios';
import { useWebSocket } from 'util/websocket';
import { onLoginSuccess, UserConsumer } from 'util/auth';
import LoginButtons from 'components/LoginButtons';
import { SummaryTable, SummaryTableRow } from 'components/tables/SummaryTable';

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(3),
  },
  button: {
    margin: `${theme.spacing(1)}px auto`,
  },
}));

const RegisterNfcPage = UserConsumer(({ user, cabinet, readerId }) => {
  const classes = useStyles();
  const router = useRouter();
  const axios = getAxios({ authenticated: true });
  const [status, setStatus] = useState({ step: 0 });

  const sendRegRequest = () => {
    setStatus({ step: 1 });

    const postdata = {
      reader: readerId,
      cabinet: cabinet.id,
    };

    axios
      .post('/api/stats/nfc/register/', postdata)
      .then(response => {
        if (response.data.success) {
          setStatus({ step: 2 });
        } else {
          setStatus({ step: 0, error: response.data.error });
        }
      })
      .catch(err => {
        setStatus({ step: 0, error: 'An unknown error occurred. Please try again.' });
      });
  };

  const ws = useWebSocket('/ws/signin');
  ws.onJsonMessage(
    message => {
      if (message.userId != user?.id) {
        return;
      }

      if (message.type == 'nfc_register_error') {
        setStatus({ step: 0, error: message.error });
      }

      if (message.type == 'nfc_register_success') {
        setStatus({ step: 3, cardId: message.cardId });
      }
    },
    [user?.id],
  );

  return (
    <div className="space-y-4">
      <Breadcrumbs>
        <Link href={`/scene/${cabinet.scene.name}`}>{cabinet.scene.displayName}</Link>
        <Link href={`/cabinet/${cabinet.scene.name}/${cabinet.name}`}>{cabinet.displayName}</Link>
      </Breadcrumbs>
      <Typography variant="h1">Stats Tracking Setup</Typography>

      {user?.id && (
        <>
          <SummaryTable>
            <SummaryTableRow icon={mdiAccount} label="Your Name">
              <Typography>
                <b>{user.name}</b>
              </Typography>
            </SummaryTableRow>

            <SummaryTableRow icon={mdiCity} label="Scene">
              <Typography>
                <b>{cabinet.scene.displayName}</b>
              </Typography>
            </SummaryTableRow>

            <SummaryTableRow icon={mdiGamepadVariant} label="Cabinet">
              <Typography>
                <b>{cabinet.displayName}</b>
              </Typography>
            </SummaryTableRow>

            {status.error && (
              <SummaryTableRow>
                <Typography className={classes.error}>{status.error}</Typography>
              </SummaryTableRow>
            )}

            {status.step == 0 && (
              <SummaryTableRow>
                <Button
                  variant="outlined"
                  color="primary"
                  className={classes.button}
                  onClick={sendRegRequest}
                >
                  Register Tag
                </Button>
              </SummaryTableRow>
            )}

            {status.step == 1 && (
              <SummaryTableRow>
                <CircularProgress />
              </SummaryTableRow>
            )}

            {status.step == 2 && (
              <SummaryTableRow>
                <Typography>
                  Tap your card or device on the <b>{readerId}</b> NFC reader to register.
                </Typography>
              </SummaryTableRow>
            )}

            {status.step == 3 && (
              <SummaryTableRow>
                <Typography>
                  NFC tag <b>{status.cardId}</b> successfully registered.
                </Typography>
              </SummaryTableRow>
            )}
          </SummaryTable>
        </>
      )}

      {!user?.id && (
        <>
          <Typography>Log in with your Google account to continue.</Typography>
          <LoginButtons onSuccess={router.reload} />
        </>
      )}
    </div>
  );
});

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/game/scene/`, { params: { name: params.sceneName } });
  const scene = response.data.results[0];

  if (!scene) return { notFound: true };

  response = await axios.get(`/api/game/cabinet/`, {
    params: { sceneId: scene.id, name: params.cabinetName },
  });
  const cabinet = response.data.results[0];

  if (!cabinet) return { notFound: true };

  cabinet.scene = scene;

  return {
    props: { cabinet, readerId: params.readerId, title: 'Player Card Registration' },
  };
}

export default RegisterNfcPage;
