import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Icon from '@mdi/react';
import { mdiTimer, mdiMap, mdiFruitGrapes, mdiRunFast, mdiSword, mdiSnail } from '@mdi/js';

import { TEAMS, BLUE_TEAM, GOLD_TEAM, POSITIONS_BY_TEAM, MAP_NAMES } from 'util/constants';
import { getAxios } from 'util/axios';
import LiveStatsTable from 'components/tables/LiveStatsTable';
import { useWebSocket } from 'util/websocket';
import { formatUTC } from 'util/dates';

const useStyles = makeStyles(theme => ({
  container: {
    margin: theme.spacing(-1),
  },
  topBar: {
    display: 'flex',
    flexDirection: 'row',
    padding: theme.spacing(2, 1),
  },
  topBarItem: {
    flexBasis: 0,
    flexGrow: 1,
    textAlign: 'center',
    fontSize: '24px',
    '& svg': {
      width: '36px',
      height: '36px',
      verticalAlign: 'middle',
    },
  },
  teams: {
    display: 'flex',
    flexDirection: 'row',
    height: 'calc(100vh - 124px)',
  },
  blueTeam: {
    flexGrow: 1,
    flexBasis: 0,
    display: 'flex',
    flexDirection: 'column',
    background: `linear-gradient(to left, black, ${theme.palette.blue.dark5})`,
    borderRight: '1px solid #777',
  },
  goldTeam: {
    flexGrow: 1,
    flexBasis: 0,
    display: 'flex',
    flexDirection: 'column',
    background: `linear-gradient(to right, black, ${theme.palette.gold.dark5})`,
  },
  cabinetPosition: {
    flexGrow: 1,
    flexBasis: 0,
    display: 'flex',
    flexDirection: 'row',
    borderBottom: '1px solid #777',
    '&.blue': {
      paddingRight: '8px',
    },
    '&.gold': {
      flexDirection: 'row-reverse',
      paddingLeft: '8px',
    },
  },
  icon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(0, 1),
  },
  statsCell: {
    flexGrow: 1,
    flexBasis: 0,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  kd: {
    color: 'white',
    fontSize: '36px',
  },
  otherStats: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    padding: theme.spacing(0, 1),
  },
  otherStatsEntry: {
    flexGrow: 1,
    flexBasis: 0,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    color: 'white',
    '& svg': {
      width: '28px',
      height: '28px',
      marginRight: '4px',
    },
    '& span': {
      fontSize: '24px',
      whiteSpace: 'nowrap',
    },
  },
}));

export default function LiveStatsPage({ cabinet }) {
  const classes = useStyles();
  const [stats, setStats] = useState(null);

  const onOpen = () => {
    ws.sendJsonMessage({ type: 'subscribe', cabinet_id: cabinet.id });
  };

  const ws = useWebSocket('/ws/ingame_stats', { onOpen });
  ws.onJsonMessage(message => {
    setStats(message);
  });

  const CabinetPosition = ({ pos }) => {
    return (
      <div className={`${classes.cabinetPosition} ${pos.TEAM}`}>
        <div className={classes.icon}>
          <img src={pos.ICON} />
        </div>
        <div className={classes.statsCell}>
          <div className={classes.kd}>
            {stats?.kills[pos.ID] ?? 0}-{stats?.deaths[pos.ID] ?? 0}
          </div>
          <div className={classes.otherStats}>
            <div className={classes.otherStatsEntry}>
              <Icon path={mdiSnail} />
              <span>{Math.floor((stats?.snailDistance[pos.ID] ?? 0) / 21)}</span>
            </div>
            <div className={classes.otherStatsEntry}>
              <Icon path={mdiFruitGrapes} />
              <span>{stats?.berriesDeposited[pos.ID] ?? 0}</span>
            </div>
          </div>
        </div>
      </div>
    );
  };

  return (
    <div className={classes.container}>
      <div className={classes.topBar}>
        <div className={classes.topBarItem}>
          <Icon path={mdiMap} /> {MAP_NAMES[stats?.mapName] ?? stats?.mapName}
        </div>

        <div className={classes.topBarItem}>
          <Icon path={mdiTimer} />
          <span>{formatUTC((stats?.gameTime ?? 0) * 1000, 'm:ss')}</span>
        </div>

        <div className={classes.topBarItem}>
          <Icon path={mdiFruitGrapes} />
          <span>{stats?.berriesRemaining}</span>
        </div>
      </div>

      <div className={classes.teams}>
        <div className={classes.blueTeam}>
          {POSITIONS_BY_TEAM[BLUE_TEAM].map(pos => (
            <CabinetPosition pos={pos} />
          ))}
        </div>

        <div className={classes.goldTeam}>
          {POSITIONS_BY_TEAM[GOLD_TEAM].map(pos => (
            <CabinetPosition pos={pos} />
          ))}
        </div>
      </div>
    </div>
  );
}

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/game/scene/`, { params: { name: params.sceneName } });
  const scene = response.data.results[0];

  if (!scene) return { notFound: true };

  response = await axios.get(`/api/game/cabinet/`, {
    params: { sceneId: scene.id, name: params.cabinetName },
  });
  const cabinet = response.data.results[0];

  if (!cabinet) return { notFound: true };

  cabinet.scene = scene;

  return {
    props: { cabinet },
  };
}
