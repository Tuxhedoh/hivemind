import { makeStyles } from '@material-ui/core/styles';
import { Typography, Paper, Chip, Breadcrumbs, Link, Grid, Button } from '@material-ui/core';
import { getAxios } from 'util/axios';
import PropTypes from 'prop-types';
import { format, parseISO } from 'date-fns';
import CheckIcon from '@material-ui/icons/Check';
import WifiOffIcon from '@material-ui/icons/WifiOff';
import { isAdminOf } from 'util/auth';

import Markdown from 'components/Markdown';
import Title from 'components/Title';
import LiveStatsTable from 'components/tables/LiveStatsTable';
import VideoClipsTable from 'components/tables/VideoClipsTable';
import CabinetActiveTimeTable from 'components/tables/CabinetActiveTimeTable';
import GameTable from 'components/tables/GameTable';
import CabinetForm from 'components/forms/CabinetForm';

const useStyles = makeStyles(theme => ({
  description: {
    padding: theme.spacing(1, 2),
    margin: theme.spacing(2, 0),
  },
  clientStatus: {
    marginLeft: theme.spacing(2),
    verticalAlign: 'baseline',
  },
}));

export default function CabinetPage({ cabinet, cabinetActivity }) {
  const classes = useStyles();
  const form = isAdminOf(cabinet?.scene?.id) ? (
    <Grid container direction="row" spacing={2}>
      <Grid item>
        <Button variant="contained" color="secondary" href={`/overlay/${cabinet.overlay.id}`}>
          Edit Overlay
        </Button>
      </Grid>
      <Grid item>
        <CabinetForm cabinetId={cabinet.id} scene={cabinet.scene} />
      </Grid>
    </Grid>
  ) : null;

  const title = (
    <>
      {cabinet.displayName}
      {cabinet.clientStatus == 'online' && (
        <Chip
          className={classes.clientStatus}
          label="Online"
          color="primary"
          icon={<CheckIcon />}
        />
      )}
      {cabinet.clientStatus == 'offline' && (
        <Chip
          className={classes.clientStatus}
          label="Offline"
          color="info"
          icon={<WifiOffIcon />}
        />
      )}
    </>
  );

  return (
    <div className="space-y-8">
      <Breadcrumbs>
        <Link href={`/scene/${cabinet.scene.name}`}>{cabinet.scene.displayName}</Link>
        <Link href={`/cabinet/${cabinet.scene.name}/${cabinet.name}`}>{cabinet.displayName}</Link>
      </Breadcrumbs>
      <Title title={title} form={form} />

      {cabinet.clientStatus == 'online' && <LiveStatsTable cabinet={cabinet} />}
      <GameTable filters={{ cabinet_id: cabinet.id }} showCabName={false} />
      <VideoClipsTable filters={{ cabinet: cabinet.id }} />
      <CabinetActiveTimeTable cabinet={cabinet} cabinetActivity={cabinetActivity} />
    </div>
  );
}

CabinetPage.propTypes = {
  cabinet: PropTypes.object.isRequired,
  cabinetActivity: PropTypes.object.isRequired,
};

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/game/scene/`, { params: { name: params.sceneName } });
  const scene = response.data.results[0];

  if (!scene) return { notFound: true };

  response = await axios.get(`/api/game/cabinet/`, {
    params: { sceneId: scene.id, name: params.cabinetName },
  });
  const cabinet = response.data.results[0];

  if (!cabinet) return { notFound: true };

  cabinet.scene = scene;

  response = await axios.get(`/api/overlay/overlay/`, { params: { cabinetId: cabinet.id } });
  cabinet.overlay = response.data.results[0];

  const activityResponse = await axios.get(`/api/game/cabinet-activity/`, {
    params: { cabinetId: cabinet.id },
  });
  const cabinetActivity = activityResponse.data;

  return {
    props: {
      cabinet,
      cabinetActivity,
      title: `${scene.displayName} - ${cabinet.displayName}`,
      description: cabinet.description,
    },
  };
}
