import React, { createElement } from 'react';
import PropTypes from 'prop-types';
import checkPropTypes from 'check-prop-types';
import { useRouter } from 'next/router';

import App from 'overlay/App';

import { POSITIONS_BY_ID } from 'util/constants';
import { GameStatsProvider } from 'overlay/GameStats';
import { SignInProvider } from 'overlay/SignIn';
import { MatchProvider } from 'overlay/Match';
import { OverlaySettingsProvider } from 'overlay/Settings';

import BerriesRemainingBox from 'overlay/ingame/default/BerriesRemainingBox';
import GameTimeBox from 'overlay/ingame/default/GameTimeBox';
import HiveMindLogo from 'overlay/ingame/default/HiveMindLogo';
import KillFeed from 'overlay/ingame/default/KillFeed';
import KillsDeaths from 'overlay/ingame/default/KillsDeaths';
import PlayerNames from 'overlay/ingame/default/PlayerNames';
import PlayerStats from 'overlay/ingame/default/PlayerStats';
import QueenKillIcons from 'overlay/ingame/default/QueenKillIcons';
import StatusIcons from 'overlay/ingame/default/StatusIcons';
import TeamNameBox from 'overlay/ingame/default/TeamNameBox';

import GdcPlayerIcon from 'overlay/ingame/gdc/PlayerIcon';
import GdcPlayerIconsRow from 'overlay/ingame/gdc/PlayerIconsRow';
import GdcSnailPercentageRow from 'overlay/ingame/gdc/SnailPercentageRow';
import GdcStatsRow from 'overlay/ingame/gdc/StatsRow';
import GdcStatsSection from 'overlay/ingame/gdc/StatsSection';
import GdcTeamSection from 'overlay/ingame/gdc/TeamSection';

import ChiBerriesRemainingBox from 'overlay/ingame/chicago-league/BerriesRemainingBox';
import ChiComboStatRow from 'overlay/ingame/chicago-league/ComboStatRow';
import ChiEconomicVictoryAnimation from 'overlay/ingame/chicago-league/EconomicVictoryAnimation';
import ChiGameTimeBox from 'overlay/ingame/chicago-league/GameTimeBox';
import ChiKillFeed from 'overlay/ingame/chicago-league/KillFeed';
import ChiMilitaryVictoryAnimation from 'overlay/ingame/chicago-league/MilitaryVictoryAnimation';
import ChiPlayerIconsRow from 'overlay/ingame/chicago-league/PlayerIconsRow';
import ChiPlayerNames from 'overlay/ingame/chicago-league/PlayerNames';
import ChiPlayersLoggedIn from 'overlay/ingame/chicago-league/PlayersLoggedIn';
import ChiPlayerStats from 'overlay/ingame/chicago-league/PlayerStats';
import ChiQueenKillIconContainer from 'overlay/ingame/chicago-league/QueenKillIconContainer';
import ChiQueenKillIcons from 'overlay/ingame/chicago-league/QueenKillIcons';
import ChiSnailVictoryAnimation from 'overlay/ingame/chicago-league/SnailVictoryAnimation';
import ChiStatsRow from 'overlay/ingame/chicago-league/StatsRow';
import ChiStatsSection from 'overlay/ingame/chicago-league/StatsSection';
import ChiTeamSection from 'overlay/ingame/chicago-league/TeamSection';

const components = {
  default: {
    'berries-remaining': BerriesRemainingBox,
    'game-time': GameTimeBox,
    'hivemind-logo': HiveMindLogo,
    'kill-feed': KillFeed,
    'kills-deaths': KillsDeaths,
    'player-names': PlayerNames,
    'player-stats': PlayerStats,
    'queen-kill-icons': QueenKillIcons,
    'status-icons': StatusIcons,
    'team-name-box': TeamNameBox,
  },
  gdc: {
    'player-icon': GdcPlayerIcon,
    'player-icons-row': GdcPlayerIconsRow,
    'snail-percentage-row': GdcSnailPercentageRow,
    'stats-row': GdcStatsRow,
    'stats-section': GdcStatsSection,
    'team-section': GdcTeamSection,
  },
  'chicago-league': {
    'berries-remaining': ChiBerriesRemainingBox,
    'combo-stat-row': ChiComboStatRow,
    'economic-victory-animation': ChiEconomicVictoryAnimation,
    'game-time': ChiGameTimeBox,
    'kill-feed': ChiKillFeed,
    'military-victory-animation': ChiMilitaryVictoryAnimation,
    'player-icons-row': ChiPlayerIconsRow,
    'player-names': ChiPlayerNames,
    'players-logged-in': ChiPlayersLoggedIn,
    'player-stats': ChiPlayerStats,
    'queen-kill-icon-container': ChiQueenKillIconContainer,
    'queen-kill-icons': ChiQueenKillIcons,
    'snail-victory-animation': ChiSnailVictoryAnimation,
    'stats-row': ChiStatsRow,
    'stats-section': ChiStatsSection,
    'team-section': ChiTeamSection,
  },
};

export default function ComponentOverlayPage({}) {
  const router = useRouter();
  const { id, theme, componentName, ...componentProps } = router.query;
  if (!theme || !componentName) {
    return <></>;
  }

  const componentClass = components[theme][componentName];

  if (componentClass === undefined) {
    return (
      <div>
        Could not find component: <b>{componentName}</b>.
      </div>
    );
  }

  if (typeof componentProps?.position === 'string') {
    componentProps.position = POSITIONS_BY_ID[componentProps.position];
  }

  if (componentClass.propTypes) {
    const propErrors = checkPropTypes(
      componentClass.propTypes,
      componentProps,
      'property',
      componentClass.name,
    );

    if (propErrors !== undefined) {
      return <>{propErrors}</>;
    }
  }

  return (
    <OverlaySettingsProvider id={id}>
      <GameStatsProvider>
        <SignInProvider>
          <MatchProvider>
            <div className="overlay-component">
              {createElement(componentClass, { className: 'component', ...componentProps })}
            </div>
          </MatchProvider>
        </SignInProvider>
      </GameStatsProvider>
    </OverlaySettingsProvider>
  );
}

ComponentOverlayPage.App = App;
