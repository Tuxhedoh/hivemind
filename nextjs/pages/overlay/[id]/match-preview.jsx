import React from 'react';
import PropTypes from 'prop-types';

import App from 'overlay/App';
import theme from 'theme/overlay';
import { OverlaySettingsProvider, MatchPreviewOverlay } from 'overlay/Settings';
import { MatchStatsProvider } from 'overlay/MatchStats';
import { MatchProvider } from 'overlay/Match';

export default function MatchPreviewOverlayPage({ id }) {
  return (
    <OverlaySettingsProvider id={id}>
      <MatchProvider>
        <MatchPreviewOverlay />
      </MatchProvider>
    </OverlaySettingsProvider>
  );
}

MatchPreviewOverlayPage.propTypes = {
  id: PropTypes.string.isRequired,
};

MatchPreviewOverlayPage.App = App;

export async function getServerSideProps({ params }) {
  return {
    props: params,
  };
}

