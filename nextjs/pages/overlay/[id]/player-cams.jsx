import React from 'react';
import PropTypes from 'prop-types';

import App from 'overlay/App';
import { GameStatsProvider } from 'overlay/GameStats';
import { OverlaySettingsProvider, PlayerCamsOverlay } from 'overlay/Settings';
import { SignInProvider } from 'overlay/SignIn';
import { MatchProvider } from 'overlay/Match';

export default function PlayerCamsOverlayPage({ id }) {
  return (
    <OverlaySettingsProvider id={id}>
      <GameStatsProvider>
        <SignInProvider>
          <MatchProvider>
            <PlayerCamsOverlay />
          </MatchProvider>
        </SignInProvider>
      </GameStatsProvider>
    </OverlaySettingsProvider>
  );
}

PlayerCamsOverlayPage.propTypes = {
  id: PropTypes.string.isRequired,
};

PlayerCamsOverlayPage.App = App;

export async function getServerSideProps({ params }) {
  return {
    props: params,
  };
}

