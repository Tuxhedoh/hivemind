import { Breadcrumbs, Grid, Link, List, ListItem, Typography } from '@material-ui/core';
import Title from 'components/Title';
import OverlayForm from 'components/forms/OverlayForm';
import fileDownload from 'js-file-download';
import Head from 'next/head';
import { getConfigJson } from 'overlay/obs';

import { isAdminOf } from 'util/auth';
import { getAxios } from 'util/axios';

export default function OverlayPage({ overlay }) {
  const downloadConfig = evt => {
    evt.preventDefault();
    try {
      const config = getConfigJson(overlay);
      fileDownload(config, `HiveMind_${overlay.cabinet.name}.json`);
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <>
      <Head>
        <title>{`Edit Overlay: ${overlay.cabinet.displayName}`}</title>
      </Head>
      <Breadcrumbs>
        <Link href={`/scene/${overlay.cabinet.scene.name}`}>
          {overlay.cabinet.scene.displayName}
        </Link>
        <Link href={`/cabinet/${overlay.cabinet.scene.name}/${overlay.cabinet.name}`}>
          {overlay.cabinet.displayName}
        </Link>
        <Link href={`/overlay/${overlay.id}`}>{overlay.name}</Link>
      </Breadcrumbs>

      <Title
        title={
          <div className="flex flex-col md:flex-row justify-between items-center w-full gap-8">
            <div>Edit Overlay: {overlay.cabinet.displayName}</div>
            <div
              className="px-4 py-2 text-sm font-sans rounded bg-gold-light4 w-fit mt-1 -mb-1.5 -md:mb-1 mr-16"
              title="OK you can but know it will override the tournament settings until the next game starts"
            >
              ⚠️ Do Not Edit While Running A Tournament
            </div>
          </div>
        }
      />

      <OverlayForm overlay={overlay} />

      {isAdminOf(overlay?.cabinet?.scene?.id) && (
        <>
          <Typography variant="h3">Overlay Resources</Typography>

          <Grid container direction="row" spacing={2} className="mt-6">
            <Grid item xs={12} md={6}>
              <Typography variant="h4">Setup</Typography>
              <List>
                <ListItem>
                  <Link href="/wiki/OBS_Configuration">OBS Setup Instructions</Link>
                </ListItem>
                <ListItem>
                  <Link href="#" onClick={downloadConfig}>
                    Download OBS Scene Collection
                  </Link>
                </ListItem>
              </List>
            </Grid>
            <Grid item xs={12} md={6}>
              <Typography variant="h4">Direct Links</Typography>
              <List className="md:columns-2">
                <ListItem>
                  <Link href={`/overlay/${overlay.id}/ingame`}>In-Game Overlay</Link>
                </ListItem>
                <ListItem>
                  <Link href={`/overlay/${overlay.id}/postgame`}>Postgame Overlay</Link>
                </ListItem>
                <ListItem>
                  <Link href={`/overlay/${overlay.id}/auto`}>Auto-Switch Overlay</Link>
                </ListItem>
                <ListItem>
                  <Link href={`/overlay/${overlay.id}/match`}>Match Summary Overlay</Link>
                </ListItem>
                <ListItem>
                  <Link href={`/overlay/${overlay.id}/match-preview`}>Match Preview Overlay</Link>
                </ListItem>
                <ListItem>
                  <Link href={`/overlay/${overlay.id}/player-cams`}>Player Cameras Overlay</Link>
                </ListItem>
              </List>
            </Grid>
          </Grid>
        </>
      )}
    </>
  );
}

const loadOverlay = async id => {
  const axios = getAxios();
  let response = await axios.get(`/api/overlay/overlay/${id}/`);
  const overlay = response.data;

  response = await axios.get(`/api/game/cabinet/${overlay.cabinet}/`);
  overlay.cabinet = response.data;

  response = await axios.get(`/api/game/scene/${overlay.cabinet.scene}/`);
  overlay.cabinet.scene = response.data;

  return overlay;
};

export async function getServerSideProps({ params }) {
  const overlay = await loadOverlay(params.id);

  return {
    props: { overlayId: params.id, overlay },
  };
}
