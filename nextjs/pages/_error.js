import NextErrorComponent from 'next/error';
import gelf from 'gelf-pro';

const ErrorPage = ({ statusCode, hasGetInitialPropsRun, err }) => {
  return (
    <NextErrorComponent statusCode={statusCode} />
  );
};

ErrorPage.getInitialProps = async ({ res, err, asPath }) => {
  const errorInitialProps = await NextErrorComponent.getInitialProps({res, err});
  errorInitialProps.hasGetInitialPropsRun = true;
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;

  if (err) {
    gelf.setConfig({
      adapterOptions: { host: process.env.GRAYLOG_HOST, port: 12201 },
    });

    gelf.error(err.stack);
  }

  return { statusCode };
};

export default ErrorPage;
