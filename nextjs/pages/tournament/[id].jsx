import { Breadcrumbs, Button, IconButton, Link, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import SettingsIcon from '@material-ui/icons/Settings';
import { mdiCalendar } from '@mdi/js';
import PropTypes from 'prop-types';
import { getAxios } from 'util/axios';

import Markdown from 'components/Markdown';
import { SummaryTable, SummaryTableRow } from 'components/tables/SummaryTable';
import TournamentMatchListTable from 'components/tables/TournamentMatchListTable';
import TournamentTeamTable from 'components/tables/TournamentTeamTable';
import Title from 'components/Title';
import { isAdminOf } from 'util/auth';
import { formatUTC } from 'util/dates';

const useStyles = makeStyles(theme => ({
  bracket: {
    padding: theme.spacing(1),
  },
  description: {
    padding: theme.spacing(1, 2),
    margin: theme.spacing(2, 0),
    background: theme.palette.grey['100'],
    boxShadow: 'none',
    fontSize: '18px',
  },
}));

export default function TournamentPage({ tournament }) {
  const classes = useStyles({ tournament });
  const buttons = (
    <div className="space-x-4">
      <Button variant="contained" color="secondary" href={`/tournament/${tournament.id}/register`}>
        Register
      </Button>
      <Button variant="contained" color="secondary" href={`/tournament/${tournament.id}/dashboard`}>
        Dashboard
      </Button>
      {tournament.awards && (
        <Button variant="contained" color="secondary" href={`/tournament/${tournament.id}/awards`}>
          Awards
        </Button>
      )}
      {isAdminOf(tournament.scene.id) && (
        <>
          <Button
            variant="contained"
            color="secondary"
            href={`/tournament/${tournament.id}/manage`}
          >
            Manage
          </Button>
          <Button
            variant="contained"
            color="secondary"
            href={`/tournament/${tournament.id}/referee`}
          >
            Referee (Beta)
          </Button>
          <Button variant="contained" color="secondary" href={`/tournament/${tournament.id}/queue`}>
            Match Queue
          </Button>
          <IconButton
            variant="contained"
            color="secondary"
            href={`/tournament/${tournament.id}/admin`}
          >
            <SettingsIcon />
          </IconButton>
        </>
      )}
    </div>
  );

  return (
    <div className="space-y-8">
      <Breadcrumbs>
        <Link href={`/scene/${tournament.scene.name}`}>{tournament.scene.displayName}</Link>
      </Breadcrumbs>

      <Title title={tournament.name} form={buttons} />

      <Paper className={classes.description}>
        <SummaryTable>
          <SummaryTableRow icon={mdiCalendar} label="Start Date">
            {formatUTC(tournament.date, 'MMM d, yyyy')}
          </SummaryTableRow>
        </SummaryTable>

        <Markdown contents={tournament.description} />
      </Paper>

      {tournament.teams.length > 0 && <TournamentTeamTable title="Teams" tournament={tournament} />}

      {tournament.brackets.some(b => b.matches.length > 0) && (
        <TournamentMatchListTable tournament={tournament} />
      )}
    </div>
  );
}

TournamentPage.propTypes = {
  tournament: PropTypes.object.isRequired,
};

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/tournament/tournament/${params.id}/`);
  const tournament = response.data;

  if (!tournament) return { notFound: true };

  response = await axios.get(`/api/game/scene/${tournament.scene}/`);
  tournament.scene = response.data;

  tournament.teams = await axios.getAllPages(`/api/tournament/team/`, {
    params: { tournamentId: tournament.id },
  });

  response = await axios.get(`/api/tournament/bracket/`, {
    params: { tournamentId: tournament.id },
  });
  tournament.brackets = [];

  for (const bracket of response.data.results) {
    bracket.matches = await axios.getAllPages(`/api/tournament/match/`, {
      params: { bracketId: bracket.id },
    });
    bracket.matches.sort((a, b) => a?.roundName?.localeCompare(b?.roundName) || a.id - b.id);
    tournament.brackets.push(bracket);
  }

  return {
    props: { tournament, title: tournament.name, description: tournament.description },
  };
}
