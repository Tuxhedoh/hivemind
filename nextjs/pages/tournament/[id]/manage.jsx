import {
  Breadcrumbs,
  Button,
  CircularProgress,
  Grid,
  IconButton,
  Link,
  Typography,
} from '@material-ui/core';
import SettingsIcon from '@material-ui/icons/Settings';
import TournamentMatchListTable from 'components/tables/TournamentMatchListTable';
import TournamentTeamTable from 'components/tables/TournamentTeamTable';
import Title from 'components/Title';
import TournamentAdminCabinet from 'components/TournamentAdminCabinet';
import { TournamentAdminProvider, useTournamentAdmin } from 'providers/TournamentAdmin';

const TournamentAdminContents = () => {
  const { tournament, cabinets, reloadTournament, hideInactiveCabs, setHideInactiveCabs } =
    useTournamentAdmin();
  if (tournament === null || cabinets === null) {
    return <CircularProgress />;
  }

  const form = (
    <div className="space-x-4">
      <Button variant="contained" color="secondary" href={`/tournament/${tournament.id}/queue`}>
        Match Queue
      </Button>
      <IconButton variant="contained" color="secondary" href={`/tournament/${tournament.id}/admin`}>
        <SettingsIcon />
      </IconButton>
    </div>
  );

  return (
    <div className="space-y-8">
      <Breadcrumbs>
        <Link href={`/scene/${tournament.scene.name}`}>{tournament.scene.displayName}</Link>
        <Link href={`/tournament/${tournament.id}`}>{tournament.name}</Link>
      </Breadcrumbs>

      <Title title={`Manage: ${tournament.name}`} form={form} />
      <div className="space-y-0 py-4 px-4 pb-2 bg-gray-100 rounded-lg">
        <div className="flex justify-between items-center">
          <Typography variant="h2" className="m-0">
            Cabinets
          </Typography>
          <Button variant="outlined" color="default" onClick={() => setHideInactiveCabs(i => !i)}>
            {hideInactiveCabs ? 'Show' : 'Hide'} Inactive Cabinets
          </Button>
        </div>
        <Grid container direction="row" spacing={4}>
          {cabinets.map(cabinet => (
            <TournamentAdminCabinet key={cabinet.id} cabinet={cabinet} tournament={tournament} />
          ))}
        </Grid>
      </div>
      <Grid container direction="row" spacing={4}>
        <Grid item xs={12}>
          <TournamentMatchListTable
            title="Current and Completed Matches"
            filter={i => i.isComplete || i.blueScore > 0 || i.goldScore > 0 || i.activeCabinetId}
            editable={true}
            tournament={tournament}
          />
        </Grid>
      </Grid>
      <TournamentTeamTable tournament={tournament} onSave={reloadTournament} />
    </div>
  );
};

export default function TournamentAdminPage({ id }) {
  return (
    <TournamentAdminProvider id={id}>
      <TournamentAdminContents />
    </TournamentAdminProvider>
  );
}

export async function getServerSideProps({ params }) {
  return {
    props: { id: params.id, title: 'Tournament Admin' },
  };
}
