import {
  Breadcrumbs,
  Button,
  CircularProgress,
  Grid,
  IconButton,
  Link,
  Typography,
} from '@material-ui/core';
import SettingsIcon from '@material-ui/icons/Settings';
import TournamentMatchListTable from 'components/tables/TournamentMatchListTable';
import TournamentTeamTable from 'components/tables/TournamentTeamTable';
import Title from 'components/Title';
import { isAdminOf } from 'util/auth';
import TournamentRefereeCabinet from 'components/TournamentRefereeCabinet';
import TournamentQueueTable from 'components/tables/TournamentQueueTable';
import { TournamentAdminProvider, useTournamentAdmin } from 'providers/TournamentAdmin';

const TournamentAdminContents = () => {
  const { tournament, cabinets, reloadTournament, hideInactiveCabs, setHideInactiveCabs } =
    useTournamentAdmin();
  if (tournament === null || cabinets === null) {
    return <CircularProgress />;
  }

  const form = (
    <Grid container direction="row" alignItems="center" spacing={2}>
      <Grid item>
        <Button variant="contained" color="secondary" href={`/tournament/${tournament.id}/manage`}>
          Manage
        </Button>
      </Grid>
      <Grid item>
        <Button variant="contained" color="secondary" href={`/tournament/${tournament.id}/queue`}>
          Match Queue
        </Button>
      </Grid>
      <Grid item>
        <IconButton
          variant="contained"
          color="secondary"
          href={`/tournament/${tournament.id}/admin`}
        >
          <SettingsIcon />
        </IconButton>
      </Grid>
    </Grid>
  );

  return (
    <div className="space-y-8">
      <Breadcrumbs>
        <Link href={`/scene/${tournament.scene.name}`}>{tournament.scene.displayName}</Link>
        <Link href={`/tournament/${tournament.id}`}>{tournament.name}</Link>
      </Breadcrumbs>

      <Title title={`Referee Dashboard`} form={form} />
      <div className="space-y-0 py-4 px-4 pb-2 bg-gray-100 rounded-lg">
        <div className="flex justify-between items-center">
          <Typography variant="h2" className="m-0">
            Cabinets
          </Typography>
          <Button variant="outlined" color="default" onClick={() => setHideInactiveCabs(i => !i)}>
            {hideInactiveCabs ? 'Show' : 'Hide'} Inactive Cabinets
          </Button>
        </div>
        <Grid container direction="row" spacing={4}>
          {cabinets.map(cabinet => (
            <TournamentRefereeCabinet cabinet={cabinet} manageButton={true} />
          ))}
          <Grid item xs={12}>
            <TournamentQueueTable title="First Cab Available Queue" cabinet={null} />
          </Grid>
          {cabinets
            .filter(c => c.clientStatus === 'online')
            .map(cabinet => (
              <Grid item xs={12} md={6}>
                <TournamentQueueTable title={cabinet.displayName + ' Queue'} cabinet={cabinet} />
              </Grid>
            ))}
        </Grid>
      </div>
    </div>
  );
};

export default function TournamentAdminPage({ id }) {
  return (
    <TournamentAdminProvider id={id}>
      <TournamentAdminContents />
    </TournamentAdminProvider>
  );
}

export async function getServerSideProps({ params }) {
  return {
    props: { id: params.id, title: 'Referee Dashboard' },
  };
}
