import { useEffect } from 'react';
import { TournamentAdminProvider, useTournamentAdmin } from 'providers/TournamentAdmin';
import { Breadcrumbs, CircularProgress, Link, Grid, IconButton } from '@material-ui/core';
import SettingsIcon from '@material-ui/icons/Settings';
import Title from 'components/Title';

import TournamentQueueContainer from 'components/TournamentQueueContainer';
import TournamentQueueCabinetSection from 'components/TournamentQueueCabinetSection';
import TournamentQueueMatchList from 'components/TournamentQueueMatchList';

function TournamentAdminContents() {
  const { tournament, cabinets, loadAvailableMatches } = useTournamentAdmin();

  useEffect(() => {
    loadAvailableMatches();
  }, [tournament?.id]);

  if (tournament === null || cabinets === null) {
    return <CircularProgress />;
  }

  const form = (
    <Grid container direction="row" spacing={2}>
      <Grid item>
        <IconButton
          variant="contained"
          color="secondary"
          href={`/tournament/${tournament.id}/admin`}
        >
          <SettingsIcon />
        </IconButton>
      </Grid>
    </Grid>
  );

  return (
    <div className="space-y-8">
      <Breadcrumbs>
        <Link href={`/scene/${tournament.scene.name}`}>{tournament.scene.displayName}</Link>
        <Link href={`/tournament/${tournament.id}`}>{tournament.name}</Link>
      </Breadcrumbs>

      <Title title={`Match Queue: ${tournament.name}`} form={form} />

      <TournamentQueueContainer>
        <Grid container direction="row" spacing={2}>
          <Grid container item direction="column" spacing={2} xs={12} md={6}>
            <TournamentQueueCabinetSection tournament={tournament} cabinet={null} />

            {cabinets.map(cabinet => (
              <TournamentQueueCabinetSection
                key={cabinet.id}
                cabinet={cabinet}
                tournament={tournament}
              />
            ))}
          </Grid>

          <Grid container item direction="column" spacing={2} xs={12} md={6}>
            <TournamentQueueMatchList />
          </Grid>
        </Grid>
      </TournamentQueueContainer>
    </div>
  );
}

export default function TournamentQueuePage({ id }) {
  return (
    <TournamentAdminProvider id={id}>
      <TournamentAdminContents />
    </TournamentAdminProvider>
  );
}

export async function getServerSideProps({ params }) {
  return {
    props: { id: params.id, title: 'Match Queue' },
  };
}
