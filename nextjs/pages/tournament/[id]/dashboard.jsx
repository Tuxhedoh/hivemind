import {
  Breadcrumbs,
  Button,
  CircularProgress,
  Grid,
  IconButton,
  Link,
  Typography,
} from '@material-ui/core';
import SettingsIcon from '@material-ui/icons/Settings';
import TournamentMatchListTable from 'components/tables/TournamentMatchListTable';
import TournamentTeamTable from 'components/tables/TournamentTeamTable';
import Title from 'components/Title';
import { isAdminOf } from 'util/auth';
import TournamentRefereeCabinet from 'components/TournamentRefereeCabinet';
import TournamentQueueTable from 'components/tables/TournamentQueueTable';
import { TournamentAdminProvider, useTournamentAdmin } from 'providers/TournamentAdmin';

const TournamentAdminContents = () => {
  const { tournament, cabinets, reloadTournament, hideInactiveCabs, setHideInactiveCabs } =
    useTournamentAdmin();
  if (tournament === null || cabinets === null) {
    return <CircularProgress />;
  }

  return (
    <div className="space-y-8">
      <Breadcrumbs>
        <Link href={`/scene/${tournament.scene.name}`}>{tournament.scene.displayName}</Link>
        <Link href={`/tournament/${tournament.id}`}>{tournament.name}</Link>
      </Breadcrumbs>

      <Title title={tournament.name + ' Dashboard (Beta)'} />
      <Grid container direction="row" spacing={2}>
        {cabinets.map(cabinet => (
          <TournamentRefereeCabinet cabinet={cabinet} />
        ))}
        <Grid item xs={12}>
          <TournamentQueueTable title="First Cab Available Queue" cabinet={null} />
        </Grid>
        {cabinets
          .filter(c => c.clientStatus === 'online')
          .map(cabinet => (
            <Grid item xs={12} md={6}>
              <TournamentQueueTable title={cabinet.displayName + ' Queue'} cabinet={cabinet} />
            </Grid>
          ))}
      </Grid>
    </div>
  );
};

export default function TournamentAdminPage({ id }) {
  return (
    <TournamentAdminProvider id={id}>
      <TournamentAdminContents />
    </TournamentAdminProvider>
  );
}

export async function getServerSideProps({ params }) {
  return {
    props: { id: params.id, title: 'Tournament Dashboard' },
  };
}
