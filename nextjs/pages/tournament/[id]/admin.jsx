import { Breadcrumbs, CircularProgress, Grid, Link } from '@material-ui/core';
import TournamentForm from 'components/forms/TournamentForm';

import TournamentBracketTable from 'components/tables/TournamentBracketTable';
import TournamentTeamTable from 'components/tables/TournamentTeamTable';
import { TournamentAdminProvider, useTournamentAdmin } from 'providers/TournamentAdmin';

const TournamentAdminContents = () => {
  const { tournament, reloadTournament } = useTournamentAdmin();
  if (tournament === null) {
    return <CircularProgress />;
  }

  return (
    <div className="space-y-8">
      <Breadcrumbs>
        <Link href={`/scene/${tournament.scene.name}`}>{tournament.scene.displayName}</Link>
        <Link href={`/tournament/${tournament.id}`}>{tournament.name}</Link>
      </Breadcrumbs>

      <TournamentForm tournament={tournament} onSave={reloadTournament} />

      <Grid container direction="row" spacing={4}>
        <Grid item xs={12} md={6}>
          <TournamentTeamTable tournament={tournament} onSave={reloadTournament} />
        </Grid>
        <Grid item xs={12} md={6}>
          <TournamentBracketTable tournament={tournament} onSave={reloadTournament} />
        </Grid>
      </Grid>
    </div>
  );
};

export default function TournamentAdminPage({ id }) {
  // @TODO: Admin provider is overkill now. Split into manage and admin
  return (
    <TournamentAdminProvider id={id}>
      <TournamentAdminContents />
    </TournamentAdminProvider>
  );
}

export async function getServerSideProps({ params }) {
  return {
    props: { id: params.id, title: 'Tournament Admin' },
  };
}

