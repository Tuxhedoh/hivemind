import { Breadcrumbs, CircularProgress, FormControlLabel, Link, Switch } from '@material-ui/core';
import Title from 'components/Title';
import TournamentConfigureRegistrationForm, {
  AddRegistrationQuestionForm,
} from 'components/forms/TournamentConfigureRegistrationForm';
import { useRouter } from 'next/router';
import { TournamentAdminProvider, useTournamentAdmin } from 'providers/TournamentAdmin';
import React, { useState } from 'react';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import { toast } from 'react-toastify';
import { getAxios } from 'util/axios';

const getQuestions = async (tournamentId = 6) => {
  const axios = getAxios();
  return await axios.get(`/api/tournament/player-info-field/?tournament_id=${tournamentId}`);
};

const updateOrder = async (order = [10, 8, 7, 9, 11], tournamentId = 6) => {
  const axios = getAxios({ authenticated: true });

  const response = await axios.put(`/api/tournament/tournament/${tournamentId}/question-order/`, {
    order,
  });
};

const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const RequirePhotoSwitch = ({ tournament, reloadTournament }) => {
  const axios = getAxios({ authenticated: true });
  const handlePhotoRequirementChange = async e => {
    const response = await axios.patch(`/api/tournament/tournament/${tournament.id}/`, {
      requirePlayerPhoto: e.target.checked,
    });
    if (response.status === 200) {
      reloadTournament();
      toast.success('Photo requirement updated', { autoClose: 1500 });
    }
    // I dont think this is properly handling errors
    if (response.error) toast.error(response.error);
  };
  return (
    <FormControlLabel
      label="Require Player Photo"
      control={
        <Switch
          name="requirePlayerPhoto"
          checked={tournament.requirePlayerPhoto}
          onChange={handlePhotoRequirementChange}
        />
      }
    />
  );
};

const TournamentAdminContents = ({ questions: questionsSource }) => {
  const { tournament, reloadTournament } = useTournamentAdmin();
  // const [addNewQuestion, setAddNewQuestion] = useState(false);
  const [questions, setQuestions] = useState(questionsSource);
  const [disableDrop, setDisableDrop] = useState(false);

  const router = useRouter();

  if (tournament === null) {
    return <CircularProgress />;
  }

  const dragEnd = async ({ source, destination }) => {
    if (!destination) return;
    if (destination.index === source.index) return;
    setDisableDrop(true);
    const newOrder = reorder(questions, source.index, destination.index);
    setQuestions(newOrder);
    const orderIds = newOrder.map(q => q.id);
    await updateOrder(orderIds, tournament.id);
    const response = await getQuestions(tournament.id);
    setQuestions(response?.data?.results || questions);
    setDisableDrop(false);
  };

  const QuestionList = React.memo(function QuestionList({ questions }) {
    return questions?.map((question, index) => (
      <TournamentConfigureRegistrationForm
        key={question.id}
        tournament={tournament}
        question={question}
        index={index}
      />
    ));
  });

  return (
    <div className="space-y-8">
      <Breadcrumbs>
        <Link href={`/scene/${tournament.scene.name}`}>{tournament.scene.displayName}</Link>
        <Link href={`/tournament/${tournament.id}`}>{tournament.name}</Link>
        <Link href={`/tournament/${tournament.id}/admin`}>Admin</Link>
      </Breadcrumbs>
      <Title
        title={
          <>
            <span>{tournament.name} Registration Questions</span>
          </>
        }
        form={
          <AddRegistrationQuestionForm
            key="new"
            title="Add new question"
            tournament={tournament}
            question={{ order: questions.length + 1 }}
            onSave={() => setAddNewQuestion(false)}
            onCancel={() => setAddNewQuestion(false)}
            dialog={true}
            buttonText="Add New Question"
            buttonVariant="contained"
            dialogTitle="Add New Question"
          />
        }
      />
      <p>Forms by default will require a Name, email, and scene. </p>
      <div>
        <RequirePhotoSwitch tournament={tournament} reloadTournament={reloadTournament} />
      </div>

      <h2 className="text-2xl">Registration Questions</h2>
      <p>
        Add additional questions for the registration sign up. Form will always include name,
        pronouns, photo, team select, tidbit info, and optional payment. Use this form to add
        additional questions such as Covid agreements, T-Shirt size, volunteering, etc.
      </p>
      <DragDropContext onDragEnd={dragEnd}>
        <Droppable droppableId="list" isDropDisabled={disableDrop}>
          {(provided, snapsnot) => (
            <div
              className="grid grid-cols-1 gap-4"
              ref={provided.innerRef}
              {...provided.droppableProps}
            >
              <QuestionList questions={questions} />

              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>
    </div>
  );
};

export default function TournamentAdminPage({ id, questions }) {
  return (
    <TournamentAdminProvider id={id}>
      <TournamentAdminContents questions={questions} />
    </TournamentAdminProvider>
  );
}

export async function getServerSideProps({ params }) {
  const questions = await getQuestions(params.id);

  return {
    props: {
      id: params.id,
      title: 'Tournament Configure Registration',
      questions: questions?.data?.results,
    },
  };
}
