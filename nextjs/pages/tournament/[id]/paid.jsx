import { CircularProgress } from '@material-ui/core';
import { useRouter } from 'next/router';
import { useEffect } from 'react';

import { getAxios } from 'util/axios';

export default function TournamentRegistrationPaidPage({ tournament }) {
  const axios = getAxios({ authenticated: true });
  const router = useRouter();

  useEffect(() => {
    const checkPayment = async () => {
      let response = await axios.get(`/api/tournament/tournament/${tournament.id}/registration/`);
      const player = response.data;

      response = await axios.put(`/api/tournament/player/${player.id}/check-payment/`);

      router.push(`/tournament/${tournament.id}/register`);
    };
    checkPayment();
  }, []);

  return <CircularProgress />;
}

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/tournament/tournament/${params.id}/`);
  const tournament = response.data;

  if (!tournament) return { notFound: true };

  response = await axios.get(`/api/game/scene/${tournament.scene}/`);
  tournament.scene = response.data;

  return {
    props: {
      tournament,
      title: `${tournament.name} - Tournament Registration`,
      description: tournament.description,
    },
  };
}
