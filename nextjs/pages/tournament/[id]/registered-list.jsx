import { Breadcrumbs, Button, Link, Switch } from '@material-ui/core';
import BlockIcon from '@material-ui/icons/Block';
import CheckIcon from '@material-ui/icons/Check';
import { mdiAccountEditOutline } from '@mdi/js';
import {
  flexRender,
  getCoreRowModel,
  getSortedRowModel,
  useReactTable,
} from '@tanstack/react-table';
import Title from 'components/Title';
import TournamentPlayerAdminForm from 'components/forms/TournamentPlayerAdminForm';

import { ExportToCsv } from 'export-to-csv';
import { Fragment, useMemo, useState } from 'react';
import { isAdminOf } from 'util/auth';
import { getAxios } from 'util/axios';

const csvOptions = {
  fieldSeparator: ',',
  quoteStrings: '"',
  decimalSeparator: '.',
  showLabels: true,
  useBom: true,
  useKeysAsHeaders: true,
};
const csvExporter = new ExportToCsv(csvOptions);

const parsePrice = value =>
  value
    ? (parseFloat(value) / 100).toLocaleString('en-US', {
        currency: 'USD',
        style: 'currency',
      })
    : '--';

export default function TeamSheetPage({ tournament, questions, questionsKeyMap }) {
  if (isAdminOf(tournament.scene.id) === false) return <div>Not Authorized</div>;
  const { players } = tournament;
  const [sorting, setSorting] = useState([]);

  const columnHandlers = (key, value) => {
    if (key.includes('question')) {
      const returnVal = (function () {
        if (!value) return '--';
        const { fieldType } = questions.find(q => q.fieldName === key);
        if (fieldType === 'multi_choice') {
          return typeof value === 'string' ? JSON.parse(value)?.join(', ') : value?.join(', ');
        }
        return value;
      })();
      return <span className="text-xs whitespace-nowrap">{returnVal}</span>;
    }
    switch (key) {
      case 'price':
        return <span className="font-mono">{parsePrice(value)}</span>;
      case 'team':
        return tournament?.teams[value]?.name || 'Free Agent';
      case 'registrationTime':
        return value ? new Date(value).toLocaleString('en-US') : '--';

      default:
        return typeof value === 'object' ? JSON.stringify(value) : value;
    }
  };

  const defaultColumns = useMemo(() => {
    return [
      {
        id: 'checkedIn',
        header: 'Checked In',
        accessorFn: row => {
          return <CheckInSwitch player={row} />;
        },
        cell: ({ getValue }) => <>{getValue()}</>,
      },
      {
        id: 'player',
        accessorFn: row => {
          return (
            <div className="flex gap-6 items-center">
              <div className="w-12 h-12 rounded-lg bg-gray-300 overflow-hidden object-cover flex-shrink-0 relative">
                {row?.doNotDisplay ? (
                  <BlockIcon className="text-xl opacity-40 absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2" />
                ) : (
                  row?.image && (
                    <img src={row?.image} className="block w-full h-full object-cover" />
                  )
                )}
              </div>
              <div className="flex gap-3 text-base font-bold whitespace-nowrap">
                <div>{row?.name}</div>
                <div>
                  <span className="rounded-sm bg-indigo-400/40 p-1 text-[10px] tracking-wider uppercase opacity-50">
                    {row?.pronouns}
                  </span>
                </div>
              </div>
            </div>
          );
        },
        cell: ({ getValue }) => <>{getValue()}</>,
      },
      {
        id: 'paid',

        accessorFn: row => {
          return row?.paid ? (
            <div className="group relative">
              <span className="rounded-full w-8 h-8 bg-green-200 text-lg flex items-center justify-center tracking-wider uppercase whitespace-nowrap">
                <CheckIcon className="inline text-base" />
              </span>
              <div className="group-hover:block hidden absolute top-6 h-center p-3 rounded-lg bg-white shadow-lg z-50">
                {row.selectedPaymentOptions?.map(po => (
                  <div
                    key={po.paymentOption?.name}
                    className="flex justify-between gap-6 text-xs font-mono whitespace-nowrap"
                  >
                    <span>{po.paymentOption?.name}</span>
                    <span>
                      {po.quantity} &times; {po.paymentOption?.price || po.customPrice}
                    </span>
                  </div>
                ))}
              </div>
            </div>
          ) : (
            <span className="rounded-full w-8 h-8 bg-red-200 text-lg flex items-center justify-center tracking-wider uppercase whitespace-nowrap">
              &times;
            </span>
          );
        },
        sortingFn: a => (a.original.paid ? 1 : -1),
        cell: ({ getValue }) => <>{getValue()}</>,
      },

      ...['price', 'email', 'scene', 'team'].map(key => ({
        accessorKey: key,
        header: key,
        cell: ({ getValue }) => columnHandlers(key, getValue()),
      })),

      ...questions.map(q => ({
        accessorKey: q.fieldName,
        header: q?.fieldSlug || q?.fieldDescription || q.fieldName,
        cell: ({ getValue }) => columnHandlers(q.fieldName, getValue()),
      })),
      {
        id: 'registrationTime',
        accessorKey: 'registrationTime',
        header: 'Registration Time',
        cell: ({ getValue }) => columnHandlers('registrationTime', getValue()),
      },
      {
        id: 'edit',
        accessorFn: row => (
          <TournamentPlayerAdminForm
            player={row}
            team={tournament?.teams[row.team]}
            tournament={tournament}
            buttonIconPath={mdiAccountEditOutline}
          />
        ),
        cell: ({ getValue }) => <>{getValue()}</>,
      },
    ];
  });

  const table = useReactTable({
    data: players,
    columns: defaultColumns,
    getCoreRowModel: getCoreRowModel(),
    state: {
      sorting,
    },
    onSortingChange: setSorting,
    getSortedRowModel: getSortedRowModel(),
  });

  const { getHeaderGroups, getRowModel } = table;

  // @TODO Make this work better
  const handleExportData = () => {
    const remap = players.map(player => {
      Object.keys(player).forEach((key, index) => {
        if (key === 'selectedPaymentOptions') {
          player.selectedPaymentOptions = player.selectedPaymentOptions
            ?.map(po => `${po.paymentOption?.name}: ${po.quantity} x ${po.paymentOption?.price}`)
            .join('\n');
          return;
        }
        if (key === 'team') {
          player.team = tournament?.teams[player.team]?.name || '--';
          return;
        }
        if (key === 'price') {
          player.price = parsePrice(player.price);
          return;
        }

        if (Array.isArray(player[key])) {
          player[questionsKeyMap[key] || key] = player[key].join(', ');
        } else if (typeof player[key] === 'object') {
          player[questionsKeyMap[key] || key] = JSON.stringify(player[key]);
        } else {
          player[questionsKeyMap[key] || key] = player[key];
        }
      });
      return player;
    });
    csvExporter.generateCsv(remap);
  };
  return (
    <>
      <Breadcrumbs>
        <Link href={`/scene/${tournament.scene.name}`}>{tournament.scene.displayName}</Link>
        <Link href={`/tournament/${tournament.id}`}>{tournament.name}</Link>
      </Breadcrumbs>

      <Title
        title={`${tournament.name} - Registered Players`}
        form={
          <Button variant="contained" color="secondary" onClick={handleExportData}>
            Export CSV
          </Button>
        }
      />

      <div className="rounded bg-blue-light4 p-6">
        <div className="grid grid-cols-2 md:grid-cols-4 gap-6">
          <h2 className="text-lg font-bold my-0">Summary</h2>
          <div>
            <h3 className="text-sm uppercase my-0 font-bold">Total Players</h3>
            <div className="text-2xl font-bold">{tournament.summary.totalPlayers}</div>
          </div>
          <div>
            <h3 className="text-sm uppercase my-0 font-bold">Players Paid</h3>
            <div className="text-2xl font-bold">{tournament.summary.playersPaid}</div>
          </div>
          <div>
            <h3 className="text-sm uppercase my-0 font-bold">Total Paid</h3>
            <div className="text-2xl font-bold">
              {tournament?.summary?.totalPaid &&
                (parseFloat(tournament.summary.totalPaid) / 100).toLocaleString('en-US', {
                  currency: 'USD',
                  style: 'currency',
                })}
            </div>
          </div>
        </div>
      </div>
      <div className="relative">
        <div className="overflow-auto w-full max-w-full h-[fit-content] max-h-[80vh] md:max-h-[60vh] md:relative sticky top-0">
          <table className="w-full border-collapse border-0 border-spacing-0">
            <thead>
              {getHeaderGroups().map(headerGroup => (
                <Fragment key={headerGroup.id}>
                  <tr className="bg-gray-800 text-gray-100">
                    {headerGroup.headers.map(header => (
                      <th
                        key={header.id}
                        className="p-2 px-3 text-left uppercase text-xs sticky top-0 z-50 whitespace-nowrap bg-gray-800 text-gray-100"
                      >
                        <div
                          {...{
                            className: header.column.getCanSort()
                              ? 'cursor-pointer select-none'
                              : '',
                            onClick: header.column.getToggleSortingHandler(),
                          }}
                        >
                          {flexRender(header.column.columnDef.header, header.getContext())}
                          {{
                            asc: ' 🔼',
                            desc: ' 🔽',
                          }[header.column.getIsSorted()] ?? null}
                        </div>
                      </th>
                    ))}
                  </tr>
                </Fragment>
              ))}
            </thead>
            <tbody>
              {getRowModel().rows.map(row => (
                <tr key={row.id} className="even:bg-gray-100">
                  {row.getVisibleCells().map(cell => (
                    <td key={cell.id} className="px-3 py-2 text-sm">
                      {flexRender(cell.column.columnDef.cell, cell.getContext())}
                    </td>
                  ))}
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
}

const CheckInSwitch = ({ player }) => {
  const [checkedIn, setSetCheckedIn] = useState(!!player.checkInTime);
  return (
    <div className="flex items-center">
      <Switch
        name="checkInTime"
        checked={checkedIn}
        onChange={async e => {
          const axios = getAxios({ authenticated: true });
          setSetCheckedIn(!checkedIn);
          const response = await axios.patch(`/api/tournament/player/${player.id}/`, {
            checkInTime: e.target.checked ? new Date() : null,
          });
          if (response.status !== 200) {
            setSetCheckedIn(!checkedIn);
          }
        }}
      />
    </div>
  );
};

export async function getServerSideProps({ params }) {
  const axios = getAxios({ authenticated: true });
  let response = await axios.get(`/api/tournament/tournament/${params.id}/`);
  const tournament = response.data;

  if (!tournament) return { notFound: true };

  response = await axios.get(`/api/game/scene/${tournament.scene}/`);
  tournament.scene = response.data;

  tournament.teams = {};
  const teams = await axios.getAllPages(`/api/tournament/team/`, {
    params: { tournamentId: tournament.id },
  });
  teams.forEach(team => (tournament.teams[team.id] = team));
  const players = await axios.getAllPages(`/api/tournament/player/`, {
    params: { tournamentId: tournament.id },
  });

  const ignorekeys = ['stats', 'stripeSessionId', 'tournament'];
  const playerKeysFilter = key => !ignorekeys.includes(key);
  tournament.players = [];
  tournament.summary = {
    playersPaid: 0,
    totalPlayers: 0,
    totalPaid: 0,
  };
  players.forEach(player => {
    const playerData = {};
    tournament.summary.playersPaid += player.paid ? 1 : 0;
    tournament.summary.totalPlayers += 1;
    tournament.summary.totalPaid += parseFloat(player.price) ? parseFloat(player.price) : 0;
    Object.keys(player)
      .filter(playerKeysFilter)
      .forEach(key => (playerData[key] = player[key]));
    tournament.players.push(playerData);
  });
  response = await axios.get(`/api/tournament/player-info-field/?tournament_id=${params.id}`);
  const questions = response?.data?.results;
  const questionsKeyMap = {};
  questions.forEach(q => (q.fieldSlug ? (questionsKeyMap[q.fieldName] = q.fieldSlug) : undefined));
  console.log(questionsKeyMap);

  return {
    props: {
      tournament,
      title: tournament.name,
      description: tournament.description,
      questions,
      questionsKeyMap,
    },
  };
}
