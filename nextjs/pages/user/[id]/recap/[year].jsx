import React, { useState, useEffect, useMemo } from 'react';
import { useRouter } from 'next/router';
import { makeStyles } from '@material-ui/core/styles';
import { CircularProgress, Grid, Link } from '@material-ui/core';
import { format } from 'date-fns';

import { useUser } from 'util/auth';
import { getAxios } from 'util/axios';
import {
  Recap,
  RecapItem,
  RecapMainItem,
  RecapSubItem,
  RecapList,
  RecapListItem,
  RecapPieChart,
} from 'components/Recap';
import { CABINET_POSITIONS, POSITIONS_BY_ID } from 'util/constants';
import { formatInterval } from 'util/dates';
import { formatPercent } from 'util/formatters';

const useStyles = makeStyles(theme => ({}));

export default function UserRecapPage() {
  const router = useRouter();
  const requestUser = useUser();
  const axios = getAxios({ authenticated: true });
  const classes = useStyles();

  const [user, setUser] = useState(null);
  const [error, setError] = useState(null);
  const [generating, setGenerating] = useState(false);
  const [recapData, setRecapData] = useState(null);

  const getUserInfo = async () => {
    if (!router.isReady) return;

    let response = await axios.get(`/api/user/user/${router.query.id}/`);
    setUser(response.data);

    response = await axios.get(`/api/stats/user-recap/`, {
      params: { userId: router.query.id, year: router.query.year },
    });

    if (!response?.data?.count) {
      if (generating) {
        setTimeout(getUserInfo, 1000);
        return;
      }

      if (requestUser?.id == router.query.id) {
        response = await axios.post(`/api/stats/user-recap/generate/`, { year: router.query.year });
        if (response?.data?.success) {
          setError(null);
          setGenerating(true);
          setTimeout(getUserInfo, 1000);
        } else {
          setError(response?.data?.error ?? 'unknown error');
        }

        return;
      }

      setError('recap not found');
      return;
    }

    if (response.data.results[0] && !response.data.results[0].data) {
      setGenerating(true);
      setTimeout(getUserInfo, 1000);
      return;
    }

    setError(null);
    setGenerating(false);
    setRecapData(response.data.results[0].data);
  };

  useEffect(getUserInfo, [router.isReady, requestUser?.id]);

  const byPosition = useMemo(() => {
    if (!recapData?.byPosition) return {};

    const positionStats = {};
    for (const pos of Object.values(CABINET_POSITIONS)) {
      if (pos.ID in recapData.byPosition) {
        positionStats[pos.POSITION] = positionStats[pos.POSITION] ?? {};
        for (const [k, v] of Object.entries(recapData.byPosition[pos.ID])) {
          positionStats[pos.POSITION][k] = v + (positionStats[pos.POSITION][k] ?? 0);
        }
      }
    }

    return positionStats;
  }, [recapData]);

  const allDroneStats = useMemo(() => {
    if (!recapData?.statTotals || !byPosition?.queen) return {};

    const droneStats = {};
    for (const [k, v] of Object.entries(recapData.statTotals)) {
      droneStats[k] = v - (byPosition?.queen[k] ?? 0);
    }

    return droneStats;
  }, [recapData, byPosition]);

  const favoritePosition = useMemo(() => {
    if (!byPosition) return null;
    return Object.keys(byPosition).sort((a, b) => byPosition[b].games - byPosition[a].games)[0];
  }, [byPosition]);

  const favoriteDronePosition = useMemo(() => {
    if (!byPosition) return null;
    return Object.keys(byPosition)
      .filter(i => i !== 'queen')
      .sort((a, b) => byPosition[b].games - byPosition[a].games)[0];
  }, [byPosition]);

  const mostPlayedWith = useMemo(() => {
    if (!recapData) return null;
    return Object.values(recapData.playedWith)
      .sort((a, b) => b.count - a.count)
      .slice(0, 5);
  }, [recapData?.playedWith]);

  const mostPlayedAgainst = useMemo(() => {
    if (!recapData) return null;
    return Object.values(recapData.playedAgainst).sort((a, b) => b.count - a.count)[0];
  }, [recapData?.playedAgainst]);

  const nemesis = useMemo(() => {
    if (!recapData) return null;
    return Object.values(recapData.playedAgainst)
      .filter(i => i.count >= 10)
      .sort((a, b) => b.losses / b.count - a.losses / a.count)[0];
  }, [recapData?.playedAgainst]);

  const combo = useMemo(() => {
    if (!recapData) return null;
    return Object.values(recapData.playedWith)
      .filter(i => i.count >= 10)
      .sort((a, b) => b.wins / b.count - a.wins / a.count)[0];
  }, [recapData?.playedWith]);

  if (error !== null) {
    return <>Could not load recap: {error}.</>;
  }

  if (user === null) {
    return <CircularProgress />;
  }

  if (generating) {
    return (
      <Recap title={`${user.name}'s ${router.query.year} Recap`}>
        <RecapItem>
          <RecapMainItem>
            <CircularProgress /> Generating your recap...
          </RecapMainItem>
        </RecapItem>
      </Recap>
    );
  }

  if (recapData === null) {
    return <CircularProgress />;
  }

  return (
    <Recap title={`${user.name}'s ${recapData.year} Recap`}>
      <RecapItem>
        <RecapMainItem>
          You played <b>{recapData.statTotals.games.toLocaleString()}</b> total games in{' '}
          {recapData.year}
        </RecapMainItem>

        <RecapSubItem>
          Your team won <b>{recapData.statTotals.wins.toLocaleString()}</b> games and lost{' '}
          <b>{recapData.statTotals.losses.toLocaleString()}</b>.
        </RecapSubItem>

        <RecapSubItem>
          That's a total of <b>{formatInterval(recapData.statTotals.timePlayed)}</b> of game time.
        </RecapSubItem>

        <RecapSubItem>
          You spent <b>{formatInterval(recapData.statTotals.warriorUptime)}</b> of that time as a
          warrior.
        </RecapSubItem>

        <RecapSubItem>
          Your favorite cab position was <b>{favoritePosition}</b>, accounting for{' '}
          <b>{formatPercent(byPosition[favoritePosition].games / recapData.statTotals.games)}</b> of
          your games.
        </RecapSubItem>

        <RecapList>
          <Grid item xs={6}>
            <RecapListItem name="Kills" value={recapData.statTotals.kills.toLocaleString()} />
            <RecapListItem name="Deaths" value={recapData.statTotals.deaths.toLocaleString()} />
            <RecapListItem
              name="Queen Kills"
              value={recapData.statTotals.queenKills.toLocaleString()}
            />
            <RecapListItem
              name="Military Kills"
              value={recapData.statTotals.militaryKills.toLocaleString()}
            />
            <RecapListItem
              name="Military Deaths"
              value={recapData.statTotals.militaryDeaths.toLocaleString()}
            />
          </Grid>
          <Grid item xs={6}>
            <RecapListItem
              name="Bump Assists"
              value={recapData.statTotals.bumpAssists.toLocaleString()}
            />
            <RecapListItem
              name="Berries Deposited"
              value={recapData.statTotals.berries.toLocaleString()}
            />
            <RecapListItem
              name="Berries Kicked In"
              value={recapData.statTotals.berriesKicked.toLocaleString()}
            />
            <RecapListItem
              name="Snail Distance"
              value={recapData.statTotals.snailDistance.toLocaleString()}
            />
            <RecapListItem
              name="Times Eaten By Snail"
              value={recapData.statTotals.eatenBySnail.toLocaleString()}
            />
          </Grid>
        </RecapList>
      </RecapItem>

      {recapData?.gamesByScene?.length > 1 && (
        <RecapItem>
          <RecapMainItem>
            You played games at <b>{recapData.gamesByScene.length}</b> different scenes
          </RecapMainItem>

          <RecapPieChart
            title="Where You Played"
            data={recapData.gamesByScene}
            getLabel={i => i.name}
            getValue={i => i.games}
            getColor={i => i.color}
          />
        </RecapItem>
      )}

      {recapData?.playedWith && (
        <RecapItem>
          <RecapMainItem>
            You played with <b>{Object.keys(recapData.playedWith).length}</b> other HiveMind users
          </RecapMainItem>

          {mostPlayedWith && (
            <RecapSubItem>
              You played best when paired with <b>{combo.name}</b>, winning{' '}
              <b>{formatPercent(combo.wins / combo.count, 0)}</b> of your games together.
            </RecapSubItem>
          )}

          {mostPlayedAgainst && (
            <RecapSubItem>
              Your most common opponent was <b>{mostPlayedAgainst.name}</b>. You won{' '}
              {mostPlayedAgainst.wins} of your <b>{mostPlayedAgainst.count}</b> games against them.
            </RecapSubItem>
          )}

          {nemesis && (
            <RecapSubItem>
              Your nemesis was <b>{nemesis.name}</b>, who defeated you{' '}
              <b>{formatPercent(nemesis.losses / nemesis.count, 0)}</b> of the time.
            </RecapSubItem>
          )}

          <RecapList title={`Your ${recapData.year} BeeFFs`}>
            {mostPlayedWith.map(player => (
              <RecapListItem
                key={player.playerId}
                name={`${player.name} ${player.scene ? `[${player.scene}]` : ''}`}
                value={player.count}
              />
            ))}
          </RecapList>
        </RecapItem>
      )}

      {byPosition?.queen?.games > 0 && (
        <RecapItem>
          <RecapMainItem>
            You played <b>{byPosition.queen.games.toLocaleString()}</b> games as Queen
          </RecapMainItem>

          <RecapSubItem>
            Your team won <b>{byPosition.queen.wins.toLocaleString()}</b> games and lost{' '}
            <b>{byPosition.queen.losses.toLocaleString()}</b>.
          </RecapSubItem>

          <RecapSubItem>
            That's a total of <b>{formatInterval(byPosition.queen.timePlayed)}</b> of game time.
          </RecapSubItem>

          <RecapList>
            <RecapListItem name="Kills" value={byPosition.queen.kills.toLocaleString()} />
            <RecapListItem name="Deaths" value={byPosition.queen.deaths.toLocaleString()} />
            <RecapListItem
              name="Queen Kills"
              value={byPosition.queen.queenKills.toLocaleString()}
            />
            <RecapListItem
              name="Military Kills"
              value={byPosition.queen.militaryKills.toLocaleString()}
            />

            <RecapListItem
              name="Bump Assists"
              value={byPosition.queen.bumpAssists.toLocaleString()}
            />
            <RecapListItem
              name="Berries Kicked In"
              value={byPosition.queen.berriesKicked.toLocaleString()}
            />
          </RecapList>
        </RecapItem>
      )}

      {recapData?.bestGameMil?.gameId && (
        <RecapItem>
          <RecapMainItem>Your best military game</RecapMainItem>

          <RecapSubItem>
            Your best military game was{' '}
            <Link href={`/game/${recapData.bestGameMil.gameId}`}>
              Game {recapData.bestGameMil.gameId}
            </Link>
            , played on <b>{format(new Date(recapData.bestGameMil.startTime), 'MMM d')}</b> at{' '}
            <b>{format(new Date(recapData.bestGameMil.startTime), 'h:mm a')}</b>.
          </RecapSubItem>

          <RecapSubItem>
            This game was played at {recapData.bestGameMil.cabinetName} in{' '}
            {recapData.bestGameMil.sceneName}.
          </RecapSubItem>
          <RecapSubItem>
            You played as <b>{POSITIONS_BY_ID[recapData.bestGameMil.playerId].NAME}</b>.
          </RecapSubItem>
          <RecapList>
            <RecapListItem name="Kills" value={recapData.bestGameMil.kills.toLocaleString()} />
            <RecapListItem name="Deaths" value={recapData.bestGameMil.deaths.toLocaleString()} />
            <RecapListItem
              name="Military Kills"
              value={recapData.bestGameMil.militaryKills.toLocaleString()}
            />
            <RecapListItem
              name="Military Deaths"
              value={recapData.bestGameMil.militaryDeaths.toLocaleString()}
            />
            <RecapListItem
              name="Queen Kills"
              value={recapData.bestGameMil.queenKills.toLocaleString()}
            />
          </RecapList>
        </RecapItem>
      )}

      {allDroneStats?.games > 0 && (
        <RecapItem>
          <RecapMainItem>
            You played <b>{allDroneStats.games.toLocaleString()}</b> games as a drone
          </RecapMainItem>

          <RecapSubItem>
            Your team won <b>{allDroneStats.wins.toLocaleString()}</b> games and lost{' '}
            <b>{allDroneStats.losses.toLocaleString()}</b>.
          </RecapSubItem>

          <RecapSubItem>
            That's a total of <b>{formatInterval(allDroneStats.timePlayed)}</b> of game time.
          </RecapSubItem>

          <RecapSubItem>
            You spent <b>{formatPercent(allDroneStats.warriorUptime / allDroneStats.timePlayed)}</b>{' '}
            of that time as a warrior.
          </RecapSubItem>

          <RecapSubItem>
            When playing as a drone, you picked <b>{favoriteDronePosition}</b> most often (
            <b>{formatPercent(byPosition[favoriteDronePosition].games / allDroneStats.games)}</b> of
            your drone games).
          </RecapSubItem>

          <RecapList>
            <Grid item xs={6}>
              <RecapListItem name="Kills" value={allDroneStats.kills.toLocaleString()} />
              <RecapListItem name="Deaths" value={allDroneStats.deaths.toLocaleString()} />
              <RecapListItem name="Queen Kills" value={allDroneStats.queenKills.toLocaleString()} />
              <RecapListItem
                name="Military Kills"
                value={allDroneStats.militaryKills.toLocaleString()}
              />
              <RecapListItem
                name="Military Deaths"
                value={allDroneStats.militaryDeaths.toLocaleString()}
              />
            </Grid>
            <Grid item xs={6}>
              <RecapListItem
                name="Bump Assists"
                value={allDroneStats.bumpAssists.toLocaleString()}
              />
              <RecapListItem
                name="Berries Deposited"
                value={allDroneStats.berries.toLocaleString()}
              />
              <RecapListItem
                name="Berries Kicked In"
                value={allDroneStats.berriesKicked.toLocaleString()}
              />
              <RecapListItem
                name="Snail Distance"
                value={allDroneStats.snailDistance.toLocaleString()}
              />
              <RecapListItem
                name="Times Eaten By Snail"
                value={allDroneStats.eatenBySnail.toLocaleString()}
              />
            </Grid>
          </RecapList>
        </RecapItem>
      )}
    </Recap>
  );
}
