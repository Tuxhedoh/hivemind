import { makeStyles } from '@material-ui/core/styles';
import { Typography, Grid, Paper, Link, List, ListItem, ListItemText } from '@material-ui/core';

import Title from 'components/Title';
import Code from 'components/Code';
import InlineCode from 'components/InlineCode';

const useStyles = makeStyles(theme => ({
}));

export default function SetupOBSPage() {
  const classes = useStyles();
  return (
    <>
      <Title title="OBS Setup Instructions" />

      <Typography variant="h3">Download and Install OBS</Typography>
      <List>
        <ListItem>
          <ListItemText>
            Download the latest version of OBS for your platform from <Link href="https://obsproject.com/welcome">https://obsproject.com/welcome</Link> and run the installer.
          </ListItemText>
        </ListItem>
      </List>

      <Typography variant="h3">Import the Configuration</Typography>
      <List>
        <ListItem>
          <ListItemText>
            Download your overlay's OBS configuration from the overlay admin page. The auto scene configuration will switch to the post-game stats view automatically, while the manual scene configuration will set up an in-game and a postgame scene to allow your stream tech to switch manually.
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            In OBS, click the "Scene Collection" menu bar, then click "Import". Select the <InlineCode>HiveMind_NAME.json</InlineCode> file.
          </ListItemText>
        </ListItem>
      </List>

      <Typography variant="h3">Set Up Capture Devices</Typography>
      <List>
        <ListItem>
          <ListItemText>
            Click the "Scene Collection" menu bar and select "Killer Queen - NAME".
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            In the "Scenes" list, click "Auto" (or "In-Game" if you are using the manual scene switching configuration).
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            Right-click the "Video Capture Device" source and click "Properties". Select your video capture device and click OK.
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            For each team camera, right-click the "Team Camera" source and click "Properties". Select the video capture device for the camera. Click OK.
          </ListItemText>
        </ListItem>
      </List>
    </>
  );
}

