import { makeStyles } from '@material-ui/core/styles';
import { Typography, Grid, Paper, Link, List, ListItem, ListItemText } from '@material-ui/core';

import Title from 'components/Title';
import Code from 'components/Code';
import InlineCode from 'components/InlineCode';

const useStyles = makeStyles(theme => ({
}));

export default function SetupPiPage() {
  const classes = useStyles();
  return (
    <>
      <Title title="Raspberry Pi Setup Instructions" />

      <Typography>This page contains a parts list and setup instructions for a Raspberry Pi HiveMind client, connected to the internet via WiFi.</Typography>

      <Typography>The setup script requires a Linux PC with internet access, a terminal, a MicroSD card reader, and at least 2.5 GB of free space. (It might work on MacOS but has not been tested.)</Typography>

      <Typography variant="h3">Parts List</Typography>

      <List>
        <ListItem>
          <ListItemText>
            <Link href="https://www.adafruit.com/product/3400">Raspberry Pi Zero W</Link>
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            <Link href="https://www.adafruit.com/product/3252">Adafruit Raspberry Pi Zero Case</Link>
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            <Link href="https://www.adafruit.com/product/1995">5V 2.5A MicroUSB Power Supply</Link>
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            <Link href="https://www.adafruit.com/product/1294">8 GB MicroSD Memory Card</Link>
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            <Link href="https://www.adafruit.com/product/2992">MicroUSB Ethernet Adapter</Link>
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            <Link href="https://www.adafruit.com/product/730">Ethernet Cable</Link>
          </ListItemText>
        </ListItem>
      </List>

      <Typography variant="h3">Setup Instructions</Typography>

      <List>
        <ListItem>
          <ListItemText>
            Insert the SD card into the PC.
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            Download and run the setup script:

            <Code>
              wget 'https://gitlab.com/kqhivemind/hivemind/-/raw/master/client/pi_setup.sh?inline=false' -O pi_setup.sh<br />
              chmod +x pi_setup.sh<br />
              sudo ./pi_setup.sh<br />
            </Code>
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            When prompted, enter your wireless network information, scene and cabinet name, and cabinet token.
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            Optionally, enter the path to your SSH authorized keys file. This will allow you to SSH to the Raspberry Pi.
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            The script will download a Raspberry Pi OS Lite image and install it to the SD card, along with several configuration files and setup scripts. Wait for the script to finish.
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            Remove the SD card from the PC and insert it into the Raspberry Pi.
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            Plug the ethernet adapter into the USB port on the Raspberry Pi. Connect the ethernet cable directly from the adapter to the computer's network port inside the cabinet.
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            Connect the power adapter to the PWR IN port on the Raspberry Pi and plug it in.
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            The Pi will run through its initial setup, then reboot.
          </ListItemText>
        </ListItem>
      </List>
    </>
  );
}
