import React, { useEffect, useState } from 'react';
import { Button, CircularProgress } from '@material-ui/core';
import QRCode from 'qrcode';
import crypto from 'crypto';

import { getAxios } from 'util/axios';
import { SIGN_IN_ACTIONS } from 'util/constants';
import { useWebSocket } from 'util/websocket';
import isServer from 'util/isServer';
import styles from './nfc-reg.module.css';

export default function NfcRegPage({}) {
  const [readerId, setReaderId] = useState(null);
  const [qr, setQR] = useState(null);
  const [userId, setUserId] = useState(null);
  const [userName, setUserName] = useState(null);
  const [cardId, setCardId] = useState(null);
  const [error, setError] = useState(null);
  const [debugging, setDebugging] = useState(false);
  const [debugSerial, setDebugSerial] = useState(null);
  const axios = getAxios({ authenticated: true });

  const changeReaderId = () => {
    if (!isServer() && window?.crypto) {
      const id = window.crypto.randomUUID().substring(0, 8);
      setReaderId(id);
      QRCode.toDataURL(`${window.location.origin}/register-nfc/${id}`).then(setQR);
    }
  };

  useEffect(() => {
    if (readerId === null) {
      changeReaderId();
    }
  }, [readerId]);

  const readNFC = () => {
    return new Promise((resolve, reject) => {
      const ndef = new NDEFReader();
      ndef
        .scan()
        .then(() => {
          ndef.addEventListener('readingerror', () => {
            reject('Cannot read NFC tag.');
          });

          ndef.addEventListener('reading', ({ message, serialNumber }) => {
            resolve(serialNumber);
          });
        })
        .catch(reject);
    });
  };

  const ws = useWebSocket('/ws/signin');
  ws.onJsonMessage(
    message => {
      if (message.type === SIGN_IN_ACTIONS.NFC_REGISTER && message.readerId === readerId) {
        const newUserId = message.user;
        setUserId(newUserId);
        axios.get(`/api/user/user/${newUserId}/`).then(response => {
          setUserName(response.data.name);
        });

        readNFC()
          .then(serialNumber => {
            const cardId = serialNumber.replaceAll(':', '');
            setCardId(cardId);

            axios.post('/api/stats/nfc/relay/', {
              readerId,
              cardId,
              userId: newUserId,
            });
          })
          .catch(err => {
            console.log(err);
            setError(JSON.stringify(err));
          });
      }
    },
    [readerId],
  );

  const debugRead = () => {
    setDebugging(true);
    readNFC().then(serial => {
      setDebugSerial(serial);
      setDebugging(false);
    });
  };

  return (
    <div className={styles.page}>
      <div className={styles.qrCode}>
        {!debugging && userId && (
          <div className={styles.text}>
            User: <b>{userName ?? '...'}</b>
          </div>
        )}

        {!debugging && cardId && (
          <div className={styles.text}>
            Got card ID: <b>{cardId}</b>. Continue on your device.
          </div>
        )}

        {!debugging && userId && !cardId && (
          <div className={styles.text}>Tap your card on this device to register it.</div>
        )}

        {!debugging && !userId && !cardId && <>{qr ? <img src={qr} /> : <CircularProgress />}</>}
      </div>

      <div className={styles.text}>
        Reader ID: <b>{readerId}</b>
      </div>

      {error && <div className={styles.error}>{error}</div>}

      <Button variant="outlined" className={styles.debugButton} onClick={debugRead}>
        Debug Read
      </Button>
      {debugSerial && <div className={styles.text}>{debugSerial}</div>}
    </div>
  );
}
