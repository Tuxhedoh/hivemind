import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Breadcrumbs, Link, Grid } from '@material-ui/core';

import Title from 'components/Title';
import PlayerCard from 'components/PlayerCard';
import GameTable from 'components/tables/GameTable';
import UserStatsSummaryTable from 'components/tables/UserStatsSummaryTable';
import { getAxios } from 'util/axios';

const useStyles = makeStyles(theme => ({
}));

export default function PlayerPage({ player }) {
  const classes = useStyles();

  return (
    <>
      <Breadcrumbs>
        <Link href={`/scene/${player.team.tournament.scene.name}`}>{player.team.tournament.scene.displayName}</Link>
        <Link href={`/tournament/${player.team.tournament.id}`}>{player.team.tournament.name}</Link>
        <Link href={`/tournament-team/${player.team.id}`}>{player.team.name}</Link>
        <Link href={`/tournament-player/${player.id}`}>{player.name}</Link>
      </Breadcrumbs>

      <PlayerCard player={player} />

      <Grid container direction="row" spacing={2}>
        <Grid item xs={12} lg={6}>
          <UserStatsSummaryTable stats={player.stats} />
        </Grid>
        <Grid item xs={12} lg={6}>
          <GameTable filters={{tournament_player_id: player.id}} />
        </Grid>
      </Grid>
    </>
  );
}

export async function getServerSideProps({ params }) {
  const axios = getAxios();

  let response = await axios.get(`/api/tournament/player/${params.id}/`);
  const player = response.data;

  if (!player) return { notFound: true };

  response = await axios.get(`/api/tournament/team/${player.team}`);
  player.team = response.data;

  response = await axios.get(`/api/tournament/tournament/${player.team.tournament}`);
  player.team.tournament = response.data;

  response = await axios.get(`/api/game/scene/${player.team.tournament.scene}`);
  player.team.tournament.scene = response.data;

  return {
    props: {
      player,
      title: player.scene ? `${player.name} [${player.scene}]` : player.name,
      description: `${player.name}, a player on ${player.team.name} in ${player.team.tournament.name}.`,
    },
  };
}

