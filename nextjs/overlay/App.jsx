import React from 'react';
import Head from 'next/head';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import { ThemeProvider } from '@material-ui/core/styles';
import DateFnsUtils from '@date-io/date-fns';
import CssBaseline from '@material-ui/core/CssBaseline';

import theme from 'theme/overlay';

export default function App({ children }) {
  return (
    <React.Fragment>
      <Head>
        <title>HiveMind: Stats for Killer Queen</title>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
      </Head>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          {children}
        </ThemeProvider>
      </MuiPickersUtilsProvider>
    </React.Fragment>
  );
}
