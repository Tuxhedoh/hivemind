import clsx from 'clsx';
import { Grid } from '@material-ui/core';
import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';
import PlayerNames from './PlayerNames';
import classes from './Chicago-League.module.css';
import { useOverlaySettings } from 'overlay/Settings';

export default function PlayersLoggedIn({ flipTeams, className }) {
  const settings = useOverlaySettings();

  if (!settings?.showPlayers) {
    return (<></>);
  }
  return (
    <div className={clsx(classes.players, 'players')}>
      <div className={clsx(classes.title, 'title')}>Players Signed In</div>
      <Grid container direction={flipTeams ? 'row-reverse' : 'row'} className={''}>
        <Grid item xs={6}>
          <PlayerNames team={BLUE_TEAM} />
        </Grid>
        <Grid item xs={6}>
          <PlayerNames team={GOLD_TEAM} />
        </Grid>
      </Grid>
    </div>
  )
}