import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Icon from '@mdi/react';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'inline-flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  icon: {
    verticalAlign: 'middle',
    height: '18px',
    color: 'white',
    margin: '0 4px 0 0',
  },
  text: {
    fontFamily: 'Roboto Condensed',
    fontSize: '20px',
    color: 'white',
    verticalAlign: 'middle',
  },
}));

export default function TextWithIcon({ path, className, children }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.container, className)}>
      <Icon className={clsx(classes.icon, 'icon')} path={path} />
      <div className={clsx(classes.text, 'text')}>{children}</div>
    </div>
  );
}
