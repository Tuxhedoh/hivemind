import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
  'snail': {
    backgroundImage: 'url(/static/chicago/big-snail.gif)',
    width: 840,
    height: 852,
    position: 'absolute',
    bottom: -100,
    animation: '$slide 12s both infinite ease-in-out',
    zIndex: 45,

  }
  ,
  '@keyframes slide': {
    '0%': {
      transform: 'translateX(-800px)'
    },

    '20%': { transform: 'translateX(-400px)'},
    '40%': { transform: 'translateX(0px)'},
    '60%': { transform: 'translateX(400px)'},

    '80%': { transform: 'translateX(800px)'},
    '100%': {
      transform: 'translateX(1200px)'
    }
  },
  'snailText': {
    color: 'white',
    fontSize: '64px',
    position: 'absolute',
    fontFamily: 'jaf-mashine',
    letterSpacing: '-5px',
    bottom: '45px',
    left: 300,
    zIndex: 10,
    opacity: 0,
    animation: '$delayAppear forwards 200ms 5.2s 1'
  },
  '@keyframes delayAppear': {
    '100%': { opacity: 1}
  }
}));


export default function SnailVictoryAnimation({ }) {
  const classes = useStyles();
  return (
    <div>
      <div className={classes.snail}></div>
      <div className={classes.snailText}>SNAIL VICTORY</div>
    </div>
  )
}