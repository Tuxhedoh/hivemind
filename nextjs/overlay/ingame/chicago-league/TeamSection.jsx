import React from 'react';
import Head from 'next/head';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { BLUE_TEAM, GOLD_TEAM, LEFT_TEAM, RIGHT_TEAM } from 'util/constants';
import TeamName from 'overlay/components/TeamName';
import TeamScoreCounter from 'overlay/components/TeamScoreCounter';
import { useMatch } from 'overlay/Match';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    // flexDirection: ({ team }) => team == GOLD_TEAM ? 'row-reverse' : 'row',
    alignItems: 'flex-end',
  },
  scoreCounter: {
    position: 'absolute',
    top: 0,
    zIndex: 5,
    right: 0,
    left: 0,
    padding: '0 24px',
    display: 'flex',
    justifyContent: 'flex-start',
    flexDirection: ({ side }) => side == RIGHT_TEAM ? 'row' : 'row-reverse',
  },
  marker: {
    transform: ({ side }) => side == RIGHT_TEAM && 'scaleX(-1)',
    width: '28px',
    height: '54px',
    '&:not(:last-of-type)': {
      marginLeft: ({ side }) => side == LEFT_TEAM && '-12px',
      marginRight: ({ side }) => side == RIGHT_TEAM && '-12px',
    },
  },
  markerEmpty: {
  },
  markerFilled: {
  },
  teamName: {
    flexGrow: 1,
    flexBasis: 0,
    fontFamily: 'jaf-mashine, sans-serif',
    color: ({ side }) => side == RIGHT_TEAM ? 'var(--right-team)' : 'var(--left-team)',
    fontSize: '24px',
    textTransform: 'uppercase',
    fontWeight: '400',
    lineHeight: 1,
    padding: ({ side }) => side == RIGHT_TEAM ? '0 10px 0 360px' : '0 360px 0 10px',
    textAlign: ({ side }) => side == RIGHT_TEAM ? 'right' : 'left',
    animation: '$teamNameEnter 400ms ease-in-out 1 forwards',
  },
  "@keyframes teamNameEnter": {
    "0%": {
      opacity: 0,
      transform: "translate3d(0,100%,0)"
    },
    "100%": {
      opacity: 1,
      transform: "translate3d(0,0%,0)"
    },
  }
}));

export default function TeamSection({ team, side, className }) {
  const classes = useStyles({ team, side });
  const match = useMatch();

  const MarkerFilled = () => (
    <img
      src={`/static/chicago/score-tick-on-${team}.png`}
      className={clsx(classes.marker, classes.markerEmpty)}
    />
  );

  const MarkerEmpty = () => (
    <img
      src={`/static/chicago/score-tick-off.png`}
      className={clsx(classes.marker, classes.markerFilled)}
    />
  );

  return (
    <div className={clsx(classes.container, className)}>
      <Head>
        <link rel="stylesheet" href="https://use.typekit.net/tue0thi.css" />
      </Head>

      <div className={classes.scoreCounter}>
        <TeamScoreCounter team={team} markerFilled={MarkerFilled} markerEmpty={MarkerEmpty} />
      </div>

      <div className={classes.teamName}>
        <TeamName team={team} />
      </div>
    </div>
  );
}
