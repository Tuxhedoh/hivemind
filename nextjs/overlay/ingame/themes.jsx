import ChicagoLeague from './ChicagoLeague';
import CssOverride from './CssOverride';
import Default from './Default';
import FFVII from './FFVII';
import Gdc from './Gdc';
import Manuka from './Manuka';
import Wildflower from './Wildflower';
import WildflowerDark from './WildflowerDark';

const allIngameThemes = [
  Default,
  Wildflower,
  WildflowerDark,
  Manuka,
  FFVII,
  Gdc,
  ChicagoLeague,
  CssOverride,
];

export function getIngameTheme(name) {
  for (const theme of allIngameThemes) {
    if (theme.themeProps.name == name) {
      return theme;
    }
  }
}

export { allIngameThemes };
