import GameTime from 'overlay/components/GameTime';

export default function GameTimeBox({ ...props }) {
  return (
    <div {...props}>
      <GameTime className="font-custom clock text-2xl leading-5" />
    </div>
  );
}
