import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import React from 'react';
import { POSITIONS_BY_ID, RIGHT_TEAM } from 'util/constants';

import { useGameStats } from 'overlay/GameStats';

const calcHatOffset = side => {
  const val = side == RIGHT_TEAM ? 150 : -71;
  return val + 'px';
};
const useStyles = makeStyles(theme => ({
  hattrick: {
    height: '36px',
    width: '48px',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundImage: `url(/static/chicago/hat-trick-hat.png)`,
    animation: '$slideDown 1s forwards 1 ease-in-out, huerotatefull 1s infinite',
    position: 'relative',
    margin: '0 auto',

    left: ({ side }) => calcHatOffset(side),
    top: ({ position }) => (POSITIONS_BY_ID[position.ID].POSITION === 'queen' ? '-15px' : '8px'),
  },
  '@keyframes slideDown': {
    '0%': {
      opacity: 0,
      transform: 'translateY(-100%) scale(1.25)',
    },
    '100%': {
      opacity: 1,
      transform: 'translateY(0%) scale(1)',
    },
  },
}));

export default function QueenKillIconContainer({ className, position, element, side }) {
  const stats = useGameStats();
  const numKills = parseInt(stats?.queenKills[position.ID]) ?? 0;
  const styles = useStyles({ position, side });
  return (
    <div className={className}>
      {numKills > 0 &&
        numKills < 3 &&
        [...Array(numKills)].map((i, idx) => <React.Fragment key={idx}>{element}</React.Fragment>)}
      {numKills === 3 && <div className={clsx(styles.hattrick, 'hat-trick')}></div>}
    </div>
  );
}
