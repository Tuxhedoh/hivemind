import { makeStyles } from '@material-ui/core/styles';
import { mdiSword } from '@mdi/js';
import Icon from '@mdi/react';
import clsx from 'clsx';
import { AnimatePresence, LayoutGroup, motion } from 'framer-motion';
import { partition } from 'lodash';
import { useGameStats } from 'overlay/GameStats';
import { POSITIONS_BY_ID } from 'util/constants';
import { formatUTC } from 'util/dates';

const useStyles = makeStyles(theme => ({
  killFeed: {
    position: 'absolute',
    bottom: 0,
    left: 700,
    right: 700,
    zIndex: 10,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    maxHeight: '144px',
    overflow: 'hidden',

    '& .kill-feed-entry': {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      padding: '4px',
      backgroundColor: 'currentColor',
      position: 'relative',
      height: 32,
      width: 'fit-content',
      animation: '$bloom 250ms forwards 1 ease-in',

      '&::after': {
        content: '""',
        position: 'absolute',
        top: 0,
        left: 'calc(100% - 9px)',
        width: 20,
        height: 32,
        backgroundColor: 'currentColor',
        transform: 'skew(30deg)',
        zIndex: -1,
        animation: '$bloom 250ms forwards 1 ease-in',
      },
      '& .kill-time': {
        color: 'white',
        textAlign: 'right',
        marginRight: '8px',
        width: '40px',
      },
      '& .player-icon': {
        height: '20px',
        width: '20px',
        margin: '0 5px',
        border: '1px solid black',
        '&.assist': {
          height: '14px',
          width: '14px',
          margin: '0 2px',
          border: '1px solid black',
        },
      },
      '& .sword-icon': {
        color: 'white',
        transform: 'scaleX(-1)',
      },
    },
  },
  '@keyframes bloom': {
    '0%': {
      filter: 'blur(4px) brightness(5)',
    },
    '100%': {
      filter: 'blur(0px) brightness(1)',
    },
  },
  killFeedSide: {
    display: 'flex',
    flexDirection: 'column',
    gap: '4px',
  },
  killFeedLeft: {
    alignItems: 'flex-start',
    '& .kill-feed-entry': {
      justifyContent: 'flex-end',
      marginRight: 'auto',
      '&:nth-child(2n)': {
        paddingLeft: 25,
      },
      '&:nth-child(3n)': {
        paddingLeft: 46,
      },
      '&:nth-child(4n)': {
        paddingLeft: 67,
      },
    },
  },
  killFeedRight: {
    '& .kill-feed-entry::after': {
      transform: 'skew(-30deg)',
      right: 'calc(100% - 9px)',
      left: 'auto',
    },
    '& .kill-feed-entry': {
      justifyContent: 'flex-start',
      marginLeft: 'auto',
      '&:nth-child(2n)': {
        paddingRight: 25,
      },
      '&:nth-child(3n)': {
        paddingRight: 46,
      },
      '&:nth-child(4n)': {
        paddingRight: 67,
      },
    },
  },
}));

export default function KillFeed({ className, flipTeams }) {
  const classes = useStyles();
  const stats = useGameStats();

  const [goldKills, blueKills] = partition(stats?.killFeed, kill => kill.killer % 2);

  const rightTeamKills = flipTeams ? blueKills : goldKills;
  const leftTeamKills = flipTeams ? goldKills : blueKills;

  const killFeedEntry = (kill, isOnLeft = true) => (
    <motion.div
      layout
      key={kill.id}
      className="kill-feed-entry"
      positiontransition
      initial={{ opacity: 0, x: isOnLeft ? -50 : 50, scale: 1.25, '--brightness': 3, '--blur': 10 }}
      animate={{ opacity: 1, x: 0, scale: 1, '--brightness': 1, '--blur': 0 }}
      exit={{ opacity: 0, x: isOnLeft ? -50 : 50, transition: { duration: 0.5 } }}
    >
      {kill.gameTime && (
        <div className="kill-time font-stat-numbers">{formatUTC(kill.gameTime * 1000, 'm:ss')}</div>
      )}
      <div className="killer relative">
        <div className="assists absolute top-1/3 left-1 -translate-x-1/2">
          {kill.assists.map(assist => (
            <img key={assist} className="player-icon assist" src={POSITIONS_BY_ID[assist].ICON} />
          ))}
        </div>
        <img className="player-icon block" src={POSITIONS_BY_ID[kill.killer]?.ICON} />
      </div>
      <Icon className="sword-icon" path={mdiSword} size={1} />
      <img className="player-icon victim block" src={POSITIONS_BY_ID[kill.victim]?.ICON} />
    </motion.div>
  );

  return (
    <div id="kill-feed" className={clsx(className, classes.killFeed, 'kill-feed')}>
      <div className={clsx(classes.killFeedSide, classes.killFeedLeft, 'kill-feed-left')}>
        <LayoutGroup>
          <AnimatePresence initial={false}>
            {leftTeamKills?.map(kill => killFeedEntry(kill))}
          </AnimatePresence>
        </LayoutGroup>
      </div>
      <div className={clsx(classes.killFeedSide, classes.killFeedRight, 'kill-feed-right')}>
        <LayoutGroup>
          <AnimatePresence initial={false}>
            {rightTeamKills?.map(kill => killFeedEntry(kill, false))}
          </AnimatePresence>
        </LayoutGroup>
      </div>
    </div>
  );
}
