import clsx from 'clsx';
import { useGameStats } from 'overlay/GameStats';
export default function JumbotronPredictionMeter({ flipTeams }) {
  const stats = useGameStats();
  const { prediction } = stats ?? { prediction: 0.5 };
  const [bluePrediction, goldPrediction] = [prediction, 1 - prediction];
  const fillClasses = ['fill-blue-main', 'fill-gold-main'];
  const [leftClass, rightClass] = flipTeams ? fillClasses.reverse() : fillClasses;
  const interpolateValue = val => (val <= 0.9 && val >= 0.1 ? val * 0.9 + 0.05 : val);
  const lerp = (x, y, a) => x * (1 - a) + y * a;

  const finalValue = flipTeams ? goldPrediction : bluePrediction;
  return (
    <g transform="translate(25,60)">
      <rect width="90" height="12" x="0" y="0" className={rightClass} />
      <rect
        width="90"
        height="12"
        x="0"
        y="0"
        className={clsx(leftClass, 'transition-transform duration-500 ease-in-out')}
        style={{
          transform: `scale(${interpolateValue(finalValue)},1) skewX(${lerp(
            30,
            -30,
            finalValue,
          )}deg)`,
        }}
      />
    </g>
  );
}
