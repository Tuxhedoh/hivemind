import clsx from 'clsx';
import StatIcon from 'overlay/components/StatIcons';
import StatValue from 'overlay/components/StatValue';
import { useRef } from 'react';
import { isQueen, STAT_KEYS } from 'util/constants';

export const queenIgnoreStats = ['workerDeaths', 'totalBerries', 'snailDistance'];

export default function StatsRow({ className, pos, colorOnChange, team, ...props }) {
  const keysToUse = useRef(
    isQueen(pos) ? STAT_KEYS.filter(key => !queenIgnoreStats.includes(key)) : STAT_KEYS,
  );

  return keysToUse.current.map((statKey, index) => (
    <div
      key={team + '-stat-row' + pos.ID + index}
      className={clsx('text-white   leading-[.75] flex gap-1 items-center')}
    >
      <span className="opacity-80">
        <StatIcon statKey={statKey} />
      </span>
      <StatValue
        statKey={statKey}
        position={pos}
        className={'text-base leading-[0.7] font-stat-numbers'}
        {...props}
        colorOnChange={Array.isArray(colorOnChange) ? colorOnChange[index] : colorOnChange}
      />
    </div>
  ));
}
