import Icon from '@mdi/react';
import StatValue from 'overlay/components/StatValue';
import { STAT_ICON_PATHS } from 'util/constants';
export default function BerriesRemainingBox({ ...props }) {
  return (
    <div className="flex items-center">
      <Icon path={STAT_ICON_PATHS['totalBerries']} className="text-xl h-5 text-gray-900 mr-px" />
      <StatValue
        statKey="berriesRemaining"
        className="font-stat-numbers-basic font-semibold text-lg relative top-px"
      />
    </div>
  );
}
