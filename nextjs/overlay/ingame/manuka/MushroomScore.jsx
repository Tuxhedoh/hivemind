import clsx from 'clsx';
import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';
export default function MushroomScore({ team, isActive, className, stroke }) {
  const strokeProps = stroke ? { stroke: '#ffffff66', strokeWidth: '2' } : {};
  return (
    <div
      className={clsx(
        'w-[36px] h-[50px] scale-125 mx-1.5 odd:translate-y-2 [&:nth-child(3)]:translate-y-1 [&:nth-child(4)]:translate-y-2',
        team === BLUE_TEAM
          ? 'odd:rotate-[15deg] [&:nth-child(3)]:rotate-[-18deg] [&:nth-child(3)]:translate-x-1'
          : 'odd:rotate-[-15deg] [&:nth-child(3)]:rotate-[18deg] [&:nth-child(3)]:-translate-x-1',
        team === GOLD_TEAM && '-scale-x-125',
        className,
      )}
    >
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="-1 -1 40 52.65">
        <defs>
          <clipPath id="a">
            <path
              d="M.09 13.74c1.75 9.36 33.39 12.33 36 5.4 2.8-7.41-4.18-17.72-16.51-19.02S-.97 8.03.09 13.74Z"
              fill="none"
            />
          </clipPath>
        </defs>
        <g className="mushroomgroup" {...strokeProps}>
          <path
            className="stem"
            d="m17.6,51.82c7.05-.06,3.73-7.69,3.2-10.43-.5-2.54-2.38-5.46-2.01-10.6.15-2.08.86-4.6,1.66-6.85,0,0-3.86-.22-7.26-1.02-.55,2.78-1.08,5.68-1.25,7.12-.37,3.23-3.47,21.86,5.67,21.79Z"
            fill="currentColor"
          />
          {!stroke && (
            <path
              className="stem"
              d="m17.6,51.82c7.05-.06,3.73-7.69,3.2-10.43-.5-2.54-2.38-5.46-2.01-10.6.15-2.08.86-4.6,1.66-6.85,0,0-3.86-.22-7.26-1.02-.55,2.78-1.08,5.68-1.25,7.12-.37,3.23-3.47,21.86,5.67,21.79Z"
              fill="#ffffffbb"
            />
          )}
          <path
            className="cap"
            d="M.09 13.74c1.75 9.36 33.39 12.33 36 5.4 2.8-7.41-4.18-17.72-16.51-19.02S-.97 8.03.09 13.74Z"
            fill="currentColor"
          />
          <g fill={stroke ? 'transparent' : 'white'} {...strokeProps} clipPath="url(#a)">
            <path d="M9.56 12.64c-.61 1.88-2.79 2.57-4.58 1.68-1.55-.76-2.27-2.46-1.81-3.89S5.2 8.1 6.88 8.45c1.95.41 3.3 2.31 2.69 4.19Z" />
            <ellipse
              cx="13.29"
              cy="1.05"
              rx="4.98"
              ry="2.2"
              transform="rotate(-4.21 13.311 1.034)"
            />
            <path d="M29.33 12.89c-.32 2.74-3.21 4.79-6.75 4.4-3.93-.43-6.79-3.58-6.03-6.81.72-3.05 4.35-4.6 7.82-3.69 3.15.83 5.27 3.48 4.96 6.1Z" />
            <circle cx="35.69" cy="14.11" r="2.78" />
            <path d="M31.36 5.4c.25.58-.27.79-1.64.24-2.45-.99-6.03-3.64-5.62-4.59.32-.73 3.03.63 4.92 2.02 1.23.9 2.11 1.81 2.34 2.33Z" />
          </g>
        </g>
      </svg>
    </div>
  );
}
