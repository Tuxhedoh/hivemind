import { makeStyles } from '@material-ui/core/styles';
import Icon from '@mdi/react';
import clsx from 'clsx';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'inline-flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  icon: {
    verticalAlign: 'middle',
    height: '1em',
    color: 'white',
    margin: '0 4px 0 0',
  },
  text: {
    fontFamily: 'Roboto Condensed',
    fontSize: '1.1em',
    color: 'white',
    verticalAlign: 'middle',
  },
}));

export default function TextWithIcon({ path, className, children, style }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.container, className)} style={style}>
      <Icon className={clsx(classes.icon, 'icon')} path={path} />
      <div className={clsx(classes.text, 'text')}>{children}</div>
    </div>
  );
}
