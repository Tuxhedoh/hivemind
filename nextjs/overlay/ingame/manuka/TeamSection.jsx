import clsx from 'clsx';
import { useMatch } from 'overlay/Match';
import TeamName from 'overlay/components/TeamName';
import TeamScoreCounter from 'overlay/components/TeamScoreCounter';
import WebcamPlayerIcons from 'overlay/components/WebcamPlayerIcons';
import { BLUE_TEAM, LEFT_TEAM } from 'util/constants';

export default function TeamSection({ team, side, className }) {
  const match = useMatch();
  const markerClass = 'peer h-14 w-4 mx-[2px]';
  const MarkerFilled = ({ index, total }) => (
    <div
      className={clsx(
        `${team}-score-marker-filled bg-contain`,
        markerClass,
        'relative after:content-[""] after:absolute after:-inset-0.5 after:bg-gradient-to-b after:from-white after:blur-sm afterr:z-[-1] after:mix-blend-overlay after:block',
        'last:after:opacity-100',
        {
          'after:opacity-0': index === 0,
          'after:opacity-25': index / total === 0.25,
          'after:opacity-30': index / total === 1 / 3,
          'after:opacity-50': index / total === 0.5,
          'after:opacity-60': index / total === 2 / 3,
          'after:opacity-75': index / total === 0.75,
        },
        team === BLUE_TEAM
          ? 'bg-blue-main after:to-blue-light1'
          : 'bg-gold-main after:rshadow-gold-light1',
      )}
    ></div>
  );

  const MarkerEmpty = () => (
    <div
      className={clsx(
        `${team}-score-marker-empty bg-gradient-to-b from-gray-500 to-gray-700`,
        markerClass,
      )}
    ></div>
  );

  return (
    <div
      className={clsx(
        'team-info-container',
        'team-info-container-' + team,
        'h-[213px] relative flex items-end',
        className,
      )}
    >
      <div
        className={clsx(
          `team-score-container team-score-container-${team}`,
          'absolute top-1 z-10 flex justify-start min-h-[60px]',
          'bg-gradient-to-b from-gray-700 to-gray-950 w-fit py-[3px]',
          'before:content-[""] before:absolute before:inset-0 before:bg-gradient-to-b before:from-gray-700 before:to-gray-950 before:z-[-1]',
          'after:content-[""] after:absolute after:inset-[-3px]  after:from-gray-400 after:to-white after:shadow-[3px_2px_10px_-1px_rgba(22,22,22,.5)] after:-z-10',
          side === LEFT_TEAM
            ? 'pr-[70px] pl-[2px] right-0 after:bg-gradient-to-l flex-row-reverse skew-x-[30deg]'
            : 'pl-[70px] pr-[2px] left-0 after:bg-gradient-to-r flex-row skew-x-[-30deg]',
        )}
      >
        <TeamScoreCounter team={team} markerFilled={MarkerFilled} markerEmpty={MarkerEmpty} />
      </div>
      <svg
        id="teamTriangleMarker"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 16 14"
        className={clsx(
          'absolute top-[7px] z-50',
          side === LEFT_TEAM ? 'right-[62px]' : 'left-[62px]',
        )}
        width="22"
        height="19"
      >
        <polygon
          points="8 14 16 0 0 0 8 14"
          className={team === BLUE_TEAM ? 'fill-blue-main' : 'fill-gold-main'}
        />
      </svg>
      <div
        className={clsx(
          'team-name-container',
          'team-name-container-' + team,
          'absolute top-0 px-3 py-1',
          'text-3xl font-bold',
          side === LEFT_TEAM ? 'left-0 bg-gradient-to-r' : 'right-0 bg-gradient-to-l',
        )}
      >
        <TeamName
          team={team}
          className={clsx(
            `team-name team-name-${team}`,
            'text-shadow-thicc text-white relative -top-1 z-10 block',
          )}
        />
        <svg
          id="Layer_2"
          data-name="Layer 2"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 749.98 72.64"
          className={clsx(
            'absolute -top-[6px] w-[750px] h-[73px] z-0 -scale-y-100 ',
            side === LEFT_TEAM
              ? '-left-0.5 rotate-[3deg] scale-x-[1.4]'
              : '-right-0.5 rotate-[-3deg] scale-x-[-1.4]',
            team === BLUE_TEAM ? 'text-blue-main' : 'text-gold-main',
          )}
        >
          <g>
            <polygon
              points="749.98 22.11 646.3 47.58 522.77 15.51 410.5 30.28 240 12.21 0 -10 0 56.47 216 63.54 421.79 72.64 523.66 40.94 646.3 54.53 749.98 22.11"
              fill="currentColor"
            />
          </g>
        </svg>
      </div>
      <WebcamPlayerIcons
        team={team}
        className={clsx('absolute bottom-0 w-[700px]', side === LEFT_TEAM ? 'left-0' : 'right-0')}
      />
    </div>
  );
}
