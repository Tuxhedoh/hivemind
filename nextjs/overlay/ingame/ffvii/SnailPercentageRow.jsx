import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Head from 'next/head';

import SnailPercentageValue from 'overlay/components/SnailPercentageValue';
import { POSITIONS_BY_TEAM, BLUE_TEAM, GOLD_TEAM, isQueen } from 'util/constants';

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
    borderBottom: '2px solid #ffffff19',
  },
  cell: {
    textAlign: 'center',
    padding: '3px 0 1px 0',
  },
  value: {
    width: '48px',
  },
  valueText: {
    color: 'white',
  },
  title: {
    width: '70px',
    textTransform: 'uppercase',
    color: 'white',
  },
  accurateSnailDistance: {
    display: 'none'
  },
  estimatedSnailDistance: {},
  hideQueen: {
    display: 'none',
  },
}));

export default function SnailPercentageRow({ className, useEstimatedDistance=false, ...props }) {
  const classes = useStyles();

  let estimatedOrAccurateClassNames = ["accurate-snail-distance", classes.accurateSnailDistance];
  if (useEstimatedDistance) {
    estimatedOrAccurateClassNames = ["estimated-snail-distance", classes.estimatedSnailDistance];
  }

  return (
    <div className={clsx(classes.row, className, estimatedOrAccurateClassNames, 'snailDistance-row')}>
      <Head>
        <link rel="stylesheet" type="text/css" href={`/static/ffvii/fonts/reactor7/webfont.css`} />
      </Head>

      {[...POSITIONS_BY_TEAM[BLUE_TEAM]].reverse().map(pos => (
        <div key={pos.ID} className={clsx(classes.cell, classes.value)}>
          <SnailPercentageValue position={pos} className={clsx(classes.valueText, isQueen(pos) ? classes.hideQueen : "" )} useEstimatedDistance={useEstimatedDistance} {...props} />
        </div>
      ))}

      <div className={clsx(classes.cell, classes.title, 'stat-title', 'stat-title-snailDistance')}>Snail</div>

      {[...POSITIONS_BY_TEAM[GOLD_TEAM]].reverse().map(pos => (
        <div key={pos.ID} className={clsx(classes.cell, classes.value)}>
          <SnailPercentageValue position={pos} className={clsx(classes.valueText, isQueen(pos) ? classes.hideQueen : "" )} useEstimatedDistance={useEstimatedDistance} {...props} />
        </div>
      ))}

    </div>
  );
}
