import React from 'react';
import { mdiFruitGrapes } from '@mdi/js';

import TextWithIcon from './TextWithIcon';
import StatValue from 'overlay/components/StatValue';

export default function BerriesRemainingBox({ ...props }) {
  return (
    <TextWithIcon path={mdiFruitGrapes} className="berries-remaining">
      <StatValue statKey="berriesRemaining" />
    </TextWithIcon>
  );
}
