import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { BLUE_TEAM, GOLD_TEAM, POSITIONS_BY_TEAM } from 'util/constants';
import { useOverlaySettings } from 'overlay/Settings';
import QueenKillIcons from './QueenKillIcons';
import StatusIcons from './StatusIcons';
import KillsDeaths from './KillsDeaths';

const useStyles = makeStyles(theme => ({
  block: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    position: 'absolute',
    top: '0',
    left: '0',
    width: '160px',
    height: '880px',
  },
  position: {
    display: 'flex',
    flexDirection: ({ team, settings }) => (settings?.isTeamOnLeft(team) ? 'row' : 'row-reverse'),
    margin: '6px 10px',
    alignItems: 'center',
    position: 'relative',
  },
  icon: {
    height: '30px',
    width: '30px',
    margin: '0 5px',
  },
}));

export default function PlayerStats({ team }) {
  const settings = useOverlaySettings();
  const classes = useStyles({ team, settings });

  return (
    <div className={clsx(classes.block, 'player-stats')}>
      {POSITIONS_BY_TEAM[team].map(pos => (
        <div
          className={clsx(classes.position, 'position', `player-stats-${pos.POSITION}`)}
          key={pos.ID}
        >
          <img className={classes.icon} src={pos.ICON} />
          <QueenKillIcons position={pos} />
          <StatusIcons position={pos} />
          <KillsDeaths position={pos} />
        </div>
      ))}
    </div>
  );
}

PlayerStats.propTypes = {
  team: PropTypes.string.isRequired,
};
