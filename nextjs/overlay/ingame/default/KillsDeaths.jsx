import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { useOverlaySettings } from 'overlay/Settings';
import StatValue from 'overlay/components/StatValue';
import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';

const useStyles = makeStyles(theme => ({
  container: {
    flexGrow: 1,
    alignSelf: 'center',
    color: 'white',
    fontFamily: 'Roboto',
    fontSize: '24px',
    textAlign: ({ position, settings }) =>
      settings?.isTeamOnLeft(position.TEAM) ? 'right' : 'left',
  },
}));

export default function KillsDeaths({ position, className }) {
  const settings = useOverlaySettings();
  const classes = useStyles({ position, settings });
  const killColorChange = { higher: '#0f0', duration: 1200 };
  const deathColorChange = { higher: '#f00', duration: 1200 };

  return (
    <div className={clsx(classes.container, 'kd', className)}>
      <StatValue
        className={clsx(classes.kills, 'kills')}
        statKey="kills"
        position={position}
        colorOnChange={killColorChange}
      />
      -
      <StatValue
        className={clsx(classes.deaths, 'deaths')}
        statKey="deaths"
        position={position}
        colorOnChange={deathColorChange}
      />
    </div>
  );
}

KillsDeaths.propTypes = {
  position: PropTypes.object.isRequired,
  className: PropTypes.string,
};
