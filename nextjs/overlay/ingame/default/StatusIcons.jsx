import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Icon from '@mdi/react';
import { mdiRunFast, mdiSword } from '@mdi/js';
import clsx from 'clsx';

import SpeedStatusIcon from 'overlay/components/SpeedStatusIcon';
import WarriorStatusIcon from 'overlay/components/WarriorStatusIcon';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    height: '30px',
    width: '20px',
    paddingTop: '1px',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  icon: {
    color: 'transparent',
    margin: '-3px 0',
    '&.active': {
      color: 'white',
    },
    '&.transition': {
      color: '#ff0',
    },
    '& svg': {
      height: '20px',
    },
  },
  warrior: {},
}));

export default function StatusIcons({ position }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.container, 'status-icons-container')}>
      <SpeedStatusIcon className={clsx(classes.icon, 'speed')} position={position}>
        <Icon path={mdiRunFast} />
      </SpeedStatusIcon>
      <WarriorStatusIcon className={clsx(classes.icon, 'warrior')} position={position}>
        <Icon path={mdiSword} />
      </WarriorStatusIcon>
    </div>
  );
}

StatusIcons.propTypes = {
  position: PropTypes.object.isRequired,
};
