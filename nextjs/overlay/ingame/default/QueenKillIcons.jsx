import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Icon from '@mdi/react';
import { mdiCrown } from '@mdi/js';
import clsx from 'clsx';

import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';
import { useOverlaySettings } from 'overlay/Settings';
import QueenKillIconContainer from 'overlay/components/QueenKillIconContainer';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    height: '48px',
    width: '20px',
    position: 'absolute',
    left: ({ position, settings }) => (settings?.isTeamOnLeft(position.TEAM) ? '-10px' : undefined),
    right: ({ position, settings }) =>
      settings?.isTeamOnLeft(position.TEAM) ? undefined : '-10px',
    alignItems: 'center',
    justifyContent: 'flex-start',
    overflow: 'hidden',
  },
  icon: {
    color: 'white',
    height: '20px',
    margin: '-2px 0',
  },
}));

export default function QueenKillIcons({ position }) {
  const settings = useOverlaySettings();
  const classes = useStyles({ position, settings });

  return (
    <QueenKillIconContainer
      className={clsx(classes.container, 'queen-kill')}
      position={position}
      element={<Icon className={classes.icon} path={mdiCrown} />}
    />
  );
}

QueenKillIcons.propTypes = {
  position: PropTypes.object.isRequired,
};
