import { createContext, useContext, useEffect, useState } from 'react';

import { useOverlaySettings } from 'overlay/Settings';
import { getAxios } from 'util/axios';
import { BLUE_TEAM, GOLD_TEAM, TEAMS } from 'util/constants';
import { useWebSocket } from 'util/websocket';

export const MatchContext = createContext({});
export const POST_MATCH_DELAY = 21000;

export function MatchProvider({ cabinetId, children }) {
  const axios = getAxios();
  const settings = useOverlaySettings();
  const [initialized, setInitialized] = useState(false);
  const [showScore, setShowScore] = useState(false);
  const [roundName, setRoundName] = useState(null);
  const [eventInProgress, setEventInProgress] = useState(false);
  const [resetNamesTimeout, setResetNamesTimeout] = useState(null);
  const [winsPerMatch, setWinsPerMatch] = useState(null);
  const [roundsPerMatch, setRoundsPerMatch] = useState(null);
  const [matchId, setMatchId] = useState(null);
  const [isWarmUp, setIsWarmUp] = useState(false);
  const [players, setPlayers] = useState({});

  const tournamentTeamNames = {};
  const teamName = {};
  const setTeamName = {};
  const tournamentTeamId = {};
  const setTournamentTeamId = {};
  const score = {};
  const setScore = {};

  useEffect(() => {
    if (!eventInProgress) {
      setFromOverlaySettings();
    }
  }, [settings]);

  for (const team of TEAMS) {
    [teamName[team], setTeamName[team]] = useState(null);
    [score[team], setScore[team]] = useState(null);
    [tournamentTeamId[team], setTournamentTeamId[team]] = useState(null);
  }

  const ws = useWebSocket('/ws/gamestate');

  ws.onJsonMessage(
    message => {
      if (message.type == 'match' && message.cabinetId == settings?.cabinet?.id) {
        if (message.currentMatch === null) {
          setMatchId(null);
          setIsWarmUp(false);

          // If a match ends, don't reset the names immediately
          const timeout = setTimeout(setFromOverlaySettings, POST_MATCH_DELAY);
          setResetNamesTimeout(timeout);
        } else {
          if (message.matchType == 'tournament') {
            axios.get(`/api/tournament/match/${message.currentMatch.id}/`).then(response => {
              setTournamentTeamId[BLUE_TEAM](response.data.blueTeam);
              setTournamentTeamId[GOLD_TEAM](response.data.goldTeam);

              getTournamentPlayers(BLUE_TEAM, response.data.blueTeam);
              getTournamentPlayers(GOLD_TEAM, response.data.goldTeam);
            });
          }

          if (message.matchType == 'league' && message.currentMatch.id !== matchId) {
            setPlayers({});
            const matchType = message.currentMatch.isQuickplay ? 'qpmatch' : 'match';
            axios.get(`/api/league/${matchType}/${message.currentMatch.id}/`).then(response => {
              getLeaguePlayers(response.data);
            });
          }

          const update = () => {
            setTeamName[BLUE_TEAM](message.currentMatch.blueTeam);
            setTeamName[GOLD_TEAM](message.currentMatch.goldTeam);
            setScore[BLUE_TEAM](message.currentMatch.blueScore);
            setScore[GOLD_TEAM](message.currentMatch.goldScore);
            setRoundName(message.currentMatch.roundName);
            setWinsPerMatch(message.currentMatch.winsPerMatch);
            setRoundsPerMatch(message.currentMatch.roundsPerMatch);
            setMatchId(message.currentMatch.id);
            setIsWarmUp(message.currentMatch.isWarmup || false);
            setShowScore(true);
          };

          if (!matchId || message.currentMatch.id === matchId) {
            update();
          } else {
            const timeout = setTimeout(update, POST_MATCH_DELAY);
            setResetNamesTimeout(timeout);
          }

          // If a new match gets selected before the names are reset,
          // clear the timeout so the new match stays up
          if (resetNamesTimeout) {
            clearTimeout(resetNamesTimeout);
          }

          return;
        }
      }
    },
    [settings?.cabinet?.id],
  );

  const getTournamentTeamName = async id => {
    if (!(id in tournamentTeamNames)) {
      let response = await axios.get(`/api/tournament/team/${id}/`);
      tournamentTeamNames[id] = response.data.name;
    }

    return tournamentTeamNames[id];
  };

  const getTournamentPlayers = (teamColor, team) => {
    setPlayers(players => ({ ...players, [teamColor]: undefined }));

    if (team) {
      axios.get(`/api/tournament/player/`, { params: { teamId: team } }).then(response => {
        setPlayers(players => ({ ...players, [teamColor]: response.data.results }));
      });
    }
  };

  const getLeaguePlayers = matchData => {
    setPlayers({});

    const blueTeamId = matchData.blueTeam?.id ?? matchData.blueTeamId;
    const goldTeamId = matchData.goldTeam?.id ?? matchData.goldTeamId;

    if (blueTeamId && goldTeamId) {
      axios.get(`/api/league/player/`, { params: { teamId: blueTeamId } }).then(response => {
        setPlayers(players => ({ ...players, blue: response.data.results }));
      });
      axios.get(`/api/league/player/`, { params: { teamId: goldTeamId } }).then(response => {
        setPlayers(players => ({ ...players, gold: response.data.results }));
      });
    } else {
      axios.get(`/api/league/qpmatch/${matchData.id}/players/`).then(response => {
        setPlayers(response.data);
      });
    }
  };

  const setFromOverlaySettings = () => {
    setEventInProgress(false);
    if (settings) {
      setTeamName[BLUE_TEAM](settings.blueTeam);
      setTeamName[GOLD_TEAM](settings.goldTeam);
      setScore[BLUE_TEAM](settings.blueScore);
      setScore[GOLD_TEAM](settings.goldScore);
      setShowScore(settings.showScore);
      setWinsPerMatch(settings.matchWinMax);
      setRoundsPerMatch(null);
      setRoundName('');
      setPlayers({});
    }
  };

  const loadMatchData = async () => {
    // Check for tournament match
    let response = await axios.get(`/api/tournament/match/`, {
      params: { activeCabinetId: settings.cabinet.id },
    });
    if (response.data.count > 0) {
      setEventInProgress(true);
      const matchData = response.data.results[0];

      response = await axios.get(`/api/tournament/bracket/${matchData.bracket}/`);
      const bracketData = response.data;

      const blueTeamName = await getTournamentTeamName(matchData.blueTeam);
      const goldTeamName = await getTournamentTeamName(matchData.goldTeam);

      setTeamName[BLUE_TEAM](blueTeamName);
      setTeamName[GOLD_TEAM](goldTeamName);
      setScore[BLUE_TEAM](matchData.blueScore);
      setScore[GOLD_TEAM](matchData.goldScore);
      if (matchData.roundName) {
        setRoundName(`${bracketData.name} - ${matchData.roundName}`);
      } else {
        setRoundName(bracketData.name);
      }

      setShowScore(true);
      setTournamentTeamId[BLUE_TEAM](matchData.blueTeam);
      setTournamentTeamId[GOLD_TEAM](matchData.goldTeam);

      getTournamentPlayers(BLUE_TEAM, matchData.blueTeam);
      getTournamentPlayers(GOLD_TEAM, matchData.goldTeam);

      return;
    }

    // Check for event in progress
    response = await axios.get(`/api/league/event/`, {
      params: { cabinetId: settings.cabinet.id, isActive: true },
    });
    if (response.data.count > 0) {
      const eventData = response.data.results[0];
      const params = { eventId: eventData.id, isComplete: false };
      const matchType = eventData.isQuickplay ? 'qpmatch' : 'match';
      const url = `/api/league/${matchType}/`;

      response = await axios.get(url, { params });
      if (response.data.count > 0) {
        const matchData = response.data.results[0];
        setEventInProgress(true);

        setTeamName[BLUE_TEAM](matchData.blueTeam?.name || 'Blue Team');
        setTeamName[GOLD_TEAM](matchData.goldTeam?.name || 'Gold Team');
        setScore[BLUE_TEAM](matchData.blueScore);
        setScore[GOLD_TEAM](matchData.goldScore);
        setWinsPerMatch(matchData.winsPerMatch);
        setRoundsPerMatch(matchData.roundsPerMatch);
        setRoundName(eventData.name);
        setShowScore(true);
        getLeaguePlayers(matchData);

        return;
      }
    }

    // Use overlay settings
    setFromOverlaySettings();
  };

  if (settings?.cabinet && !initialized) {
    setInitialized(true);
    loadMatchData();
  }

  const context = {
    teamName,
    score,
    showScore,
    roundName,
    winsPerMatch,
    roundsPerMatch,
    players,
    isWarmUp,
  };

  return <MatchContext.Provider value={context}>{children}</MatchContext.Provider>;
}

export function useMatch() {
  return useContext(MatchContext);
}
