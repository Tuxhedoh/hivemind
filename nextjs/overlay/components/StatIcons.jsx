import Icon from '@mdi/react';
import clsx from 'clsx';
import { STAT_ICON_PATHS, STAT_KEYS } from 'util/constants';

export default function StatIcon({ statKey, className }) {
  return STAT_KEYS.includes(statKey) ? (
    <Icon className={clsx('w-[18px] h-[18px] icon', className)} path={STAT_ICON_PATHS[statKey]} />
  ) : null;
}
