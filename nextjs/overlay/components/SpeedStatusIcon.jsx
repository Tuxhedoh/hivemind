import React, { useState } from 'react';
import clsx from 'clsx';

import { useGameStats } from 'overlay/GameStats';

export default function SpeedStatusIcon({ className, position, children }) {
  const stats = useGameStats();
  const [active, setActive] = useState(false);

  if (stats?.speed) {
    const hasSpeed = stats.speed.some(pos => parseInt(pos) == position.ID);
    if (active != hasSpeed) {
      setActive(hasSpeed);
    }
  }

  return (
    <div className={clsx(className, { active })}>
      {children}
    </div>
  );
}
