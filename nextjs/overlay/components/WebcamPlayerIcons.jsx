import clsx from 'clsx';
import { REVERSED_POSITIONS_BY_TEAM } from 'util/constants';

export default function WebcamPlayerIcons({ team, className }) {
  console.log(REVERSED_POSITIONS_BY_TEAM);
  return (
    <div className={clsx('webcam-player-icons flex justify-between gap-2', className)}>
      {REVERSED_POSITIONS_BY_TEAM[team]?.map(pos => (
        <div
          key={pos.ID}
          className="min-w-6 h-5 flex-grow bg-no-repeat bg-center opacity-75"
          style={{ backgroundImage: `url("${pos?.ICON}")` }}
        ></div>
      ))}
    </div>
  );
}
