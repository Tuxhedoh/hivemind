import clsx from 'clsx';
import { useState } from 'react';
import { isQueen } from 'util/constants';

import { useGameStats } from 'overlay/GameStats';

export default function PlayerStatusSprite({ className, position, children, speedClass }) {
  const stats = useGameStats();
  const [speed, setSpeed] = useState(false);
  const [warrior, setWarrior] = useState(false);

  if (stats?.speed) {
    const active = stats.speed.some(pos => parseInt(pos) == position.ID);
    if (active != speed) {
      setSpeed(active);
    }
  }

  if (stats?.warriors) {
    const active = stats.warriors.some(pos => parseInt(pos) == position.ID);
    if (active != warrior) {
      setWarrior(active);
    }
  }

  return (
    <div className={clsx(className, { 'group speed': speed, warrior, queen: isQueen(position) })}>
      {children}
      {speedClass && <span className={`hidden group-[.speed]:block ${speedClass}`}>⚡️</span>}
    </div>
  );
}
