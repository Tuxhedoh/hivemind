import clsx from 'clsx';
import { useMatch } from 'overlay/Match';
export default function WarmUpNotice({ className }) {
  const match = useMatch();
  return match?.isWarmUp ? (
    <div id="warmup" className={clsx(' z-50  grid', className)}>
      <div id="warmupBox" className="self-center mx-auto p-1.5">
        <span id="warmupText" className="text-5xl font-bold animate-pulse">
          WARM UP
        </span>
      </div>
    </div>
  ) : null;
}
