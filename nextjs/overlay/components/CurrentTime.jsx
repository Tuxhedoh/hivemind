import clsx from 'clsx';
import { format } from 'date-fns';
import { useEffect, useState } from 'react';

export default function CurrentTime({
  className,
  timeClass,
  dateClass,
  dateFormat = 'd MMM yyyy',
  timeFormat = 'h:mm a',
  hideDivider = false,
}) {
  const [currentTime, setCurrentTime] = useState(null);

  useEffect(() => {
    let timer;
    if (currentTime === null) {
      setCurrentTime(new Date());
      timer = setInterval(() => setCurrentTime(new Date()), 1000);
      // return <></>;
    }
    return () => clearInterval(timer);
  }, []);

  if (!currentTime) return <></>;

  return (
    <span id="clock-time" className={className}>
      <span suppressHydrationWarning className={clsx('current-date', dateClass)}>
        {format(currentTime, dateFormat)}
      </span>
      {!hideDivider && <span className={clsx("time-divider before:content-['-'] px-1")}></span>}
      <span suppressHydrationWarning className={clsx('current-time', timeClass)}>
        {format(currentTime, timeFormat)}
      </span>
    </span>
  );
}
