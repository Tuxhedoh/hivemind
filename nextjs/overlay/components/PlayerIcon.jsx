import React from 'react';
import clsx from 'clsx';

import { useSignIn } from 'overlay/SignIn';

export default function PlayerIcon({ position, className }) {
  const signIn = useSignIn();
  const name = signIn[position.ID];

  return (
    <img className={clsx(className, { active: name !== null })} src={position.ICON} />
  );
}

