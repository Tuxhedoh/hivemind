import React from 'react';
import { useOverlaySettings } from 'overlay/Settings';

export default function PlayerNamesBlock({ className, children }) {
  const settings = useOverlaySettings();

  if (!settings?.showPlayers) {
    return (<></>);
  }

  return (
    <div className={className}>
      {children}
    </div>
  );
}
