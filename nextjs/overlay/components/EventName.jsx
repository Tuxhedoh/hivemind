import clsx from 'clsx';
import { useMatch } from 'overlay/Match';

export default function EventName({ id, className }) {
  const match = useMatch();

  return match?.roundName ? (
    <span id={id} className={clsx('event-name', className)}>
      {match?.roundName ?? 'Round 1'}
    </span>
  ) : null;
}
