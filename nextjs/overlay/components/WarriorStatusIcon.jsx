import React, { useState } from 'react';
import clsx from 'clsx';

import { useGameStats } from 'overlay/GameStats';

export default function WarriorStatusIcon({ className, position, children }) {
  const stats = useGameStats();
  const [active, setActive] = useState(false);

  if (stats?.warriors) {
    const isWarrior = stats.warriors.some(pos => parseInt(pos) == position.ID);
    if (active != isWarrior) {
      setActive(isWarrior);
    }
  }

  return (
    <div className={clsx(className, { active })}>
      {children}
    </div>
  );
}
