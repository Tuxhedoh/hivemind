import { useEffect, useState } from 'react';
import tmi from 'tmi.js';

import { useOverlaySettings } from 'overlay/Settings';

export default function TwitchChat({ className }) {
  const settings = useOverlaySettings();
  const [chatLines, setChatLines] = useState([]);

  useEffect(() => {
    const client = new tmi.Client({
      channels: [settings.cabinet.twitchChannelName],
    });
    client.connect();

    client.on('message', (channel, tags, message, self) => {
      setChatLines(lines => {
        lines.push({
          id: tags['id'],
          username: tags['display-name'],
          text: message,
          color: tags['color'],
        });
        return lines.slice(-20);
      });
    });

    return () => {
      client.disconnect();
    };
  }, []);

  return (
    <div className={className}>
      {chatLines.map(line => (
        <div key={line.id} className="chat-line">
          <span className="username" style={{ color: line.color }}>
            {line.username}:
          </span>
          <span className="chat-text">{line.text}</span>
        </div>
      ))}
    </div>
  );
}
