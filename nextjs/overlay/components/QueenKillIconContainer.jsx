import React, { useState } from 'react';
import clsx from 'clsx';

import { useGameStats } from 'overlay/GameStats';

export default function QueenKillIconContainer({ className, position, element }) {
  const stats = useGameStats();
  const numKills = parseInt(stats?.queenKills[position.ID]) ?? 0;

  return (
    <div className={className}>
      {(numKills > 0) && [...Array(numKills)].map((i, idx) => (
        <React.Fragment key={idx}>
          {element}
        </React.Fragment>
      ))}
    </div>
  );
}
