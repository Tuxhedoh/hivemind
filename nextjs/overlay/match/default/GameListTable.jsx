import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { useMatchStats } from 'overlay/MatchStats';
import GameResult from './GameResult';

const useStyles = makeStyles(theme => ({
  table: {
    margin: '30px 300px',
    display: 'flex',
    flexDirection: 'column',
    padding: 0,
  },
}));

export default function GameListTable({ className }) {
  const classes = useStyles();
  const match = useMatchStats();

  if (match === null) {
    return <></>;
  }

  return (
    <div
      className={clsx(
        classes.table,
        className,
        'overflow-hidden rounded-[65px] shadow-[0_20px_60px_-30px_#00000044]',
      )}
    >
      {match?.games &&
        match.games
          .sort((a, b) => new Date(a.endTime) - new Date(b.endTime))
          .map((game, idx) => (
            <GameResult className={classes.row} key={game.id} idx={idx} game={game} />
          ))}
    </div>
  );
}
