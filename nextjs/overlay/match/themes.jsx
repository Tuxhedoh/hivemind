import Default from './Default';
import FFVII from './FFVII';

const allMatchThemes = [Default, FFVII];

export function getMatchTheme(name) {
  for (const theme of allMatchThemes) {
    if (theme.themeProps.name == name) {
      return theme;
    }
  }
}

export { allMatchThemes };
