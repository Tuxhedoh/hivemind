import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Head from 'next/head';
import { useMatchStats } from 'overlay/MatchStats';
import { useOverlaySettings } from 'overlay/Settings';
import GameListTable from './default/GameListTable';
import MatchStatsTable from './default/MatchStatsTable';
import MatchSummary from './default/MatchSummary';

const useStyles = makeStyles(theme => ({
  container: {
    opacity: 0,

    width: '1920px',
    height: '1080px',
    position: 'absolute',
    left: 0,
    top: 0,
    padding: '50px 10px',
    transition: 'opacity 1s',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    '&.active': {
      opacity: 1,
    },
  },
  boxes: {
    padding: '15px',
    borderRadius: '15px',
    boxShadow: '3px 3px 2px rgba(0, 0, 0, 0.5)',
    margin: '50px 10px',
  },
  blue: {
    background: theme.gradients.blue.light,
  },
  gold: {
    background: theme.gradients.gold.light,
  },
}));

export default function DefaultOverlay({}) {
  const classes = useStyles();
  const settings = useOverlaySettings();
  const stats = useMatchStats();

  return (
    <>
      <Head>
        {settings?.additionalFonts && <link rel="stylesheet" href={settings.additionalFonts} />}
      </Head>
      <div id="matchSummaryOverlay" className={clsx(classes.container, { active: stats?.visible })}>
        <MatchSummary className={clsx(classes.matchSummary, 'mix-blend-hard-light')} />
        <GameListTable className={clsx(classes.gameListTable, 'mix-blend-hard-light')} />
        <MatchStatsTable className={clsx(classes.matchStatsTable, 'mix-blend-hard-light')} />
        <div className="custom-color absolute -z-10  shadow-[inset_0_0_5vw_5vw_currentColor] after:content-[''] after:absolute after:inset-0 after:bg-current after:opacity-50  inset-0"></div>
      </div>
    </>
  );
}

DefaultOverlay.themeProps = {
  name: 'default',
  displayName: 'Default',
  description: `HiveMind's default match stats theme.`,
};
