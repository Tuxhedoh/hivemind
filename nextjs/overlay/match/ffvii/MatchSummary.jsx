import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { useMatchStats } from 'overlay/MatchStats';
import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'justify-between',
    gap: '1px',

    width: '100%',
    margin: '0 auto',
  },
  matchInfo: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  teamContainer: {
    display: 'flex',
    flexDirection: 'row',
    '&:last-of-type': {
      flexDirection: 'row-reverse',
    },
    flexBasis: 0,
    flexGrow: 1,
    justifyContent: 'space-between',
    padding: '5px 15px',

    // Standard border config
    border: '4px groove white',
    boxShadow: 'inset 0 0 4px #000000aa',
    borderRadius: '8px',
    color: 'white',
    textShadow: '2px 2px #212421, 1px 1px #212021',
    letterSpacing: '1px',
    // End standard border config

    '&.blue': {
      backgroundImage: 'linear-gradient(#000099, #000033)',
    },

    '&.gold': {
      backgroundImage: 'linear-gradient(#cc9933, #996633)',
    },
  },
  teamName: {
    flexGrow: 1,
    flexBasis: 0,
    fontFamily: 'OPTIEngeEtienne, Final Fantasy, Roboto, sans-serif',
    textTransform: 'uppercase',
    color: 'white',
    fontSize: '36px',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    padding: '4px',
    textOverflow: 'ellipsis',

    '&.gold': {
      textAlign: 'left',
    },

    '&.blue': {
      textAlign: 'right',
    },
  },
  scoreCounter: {
    display: 'flex',
    padding: '8px 2px',

    '&.gold': {
      flexDirection: 'row-reverse',
    },
  },
  score: {
    flexGrow: 0,
    width: '50px',
    flexBasis: '50px',
    textAlign: 'center',
  },
  blue: {
    // background: theme.palette.blue.main,
  },
  gold: {
    // background: theme.palette.gold.main,
  },
  marker: {
    height: '42px',

    // Default settings for middles
    width: '48px',
    padding: '6px 9px',
    position: 'relative',
    '&.blue': {
      backgroundImage: 'url(/static/ffvii/game-counters/blue_open_open.png)',
    },
    '&.gold': {
      backgroundImage: 'url(/static/ffvii/game-counters/gold_open_open.png)',
    },
    // end middle default settings

    '&:only-child:first-child': {
      width: '42px',
      padding: '6px',
      '&.blue': {
        backgroundImage: 'url(/static/ffvii/game-counters/blue_closed_closed.png)',
      },
      '&.gold': {
        padding: '6px',
        backgroundImage: 'url(/static/ffvii/game-counters/gold_closed_closed.png)',
      },
    },

    '&:first-child': {
      width: '45px',
      '&.blue': {
        padding: '6px',
        backgroundImage: 'url(/static/ffvii/game-counters/blue_closed_open.png)',
      },
      '&.gold': {
        padding: '6px 9px',
        backgroundImage: 'url(/static/ffvii/game-counters/gold_open_closed.png)',
      },
    },

    '&:last-child': {
      width: '45px',
      '&.blue': {
        padding: '6px 9px',
        backgroundImage: 'url(/static/ffvii/game-counters/blue_open_closed.png)',
      },
      '&.gold': {
        padding: '6px',
        backgroundImage: 'url(/static/ffvii/game-counters/gold_closed_open.png)',
      },
    },
  },
}));

// NOTE: This is partially copied from TeamScoreCounter.  I couldn't find a good way to refactor it in the GDC rush
function TeamScoreCounter({ markerEmpty, markerFilled, score, winsPerMatch, teamColor }) {
  // FIXME(chris, 2023-09-21): Adding `3` below is a fix for not knowing the numbers of winsPerMatch on this page.  Our
  // goal is to fix this post-GDC
  const numMarkers = Math.max(score, winsPerMatch, 3);

  let scoreMarkers = [];
  for (let m = 0; m < numMarkers; m++) {
    scoreMarkers.push(m < score ? markerFilled : markerEmpty);
  }

  return scoreMarkers
    .slice(0, 4)
    .map((Marker, index) => (
      <Marker key={'marker' + index} index={index} total={winsPerMatch} teamColor={teamColor} />
    ));
}

export default function MatchSummary({ className }) {
  const classes = useStyles();
  const match = useMatchStats();
  const settings = useOverlaySettings();

  if (match === null) {
    return <></>;
  }

  const blueTeam = {
    slug: BLUE_TEAM,
    name: match?.blueTeam?.name,
    score: match?.blueScore,
  };

  const goldTeam = {
    slug: GOLD_TEAM,
    name: match?.goldTeam?.name,
    score: match?.goldScore,
  };

  const teams = settings?.goldOnLeft ? [goldTeam, blueTeam] : [blueTeam, goldTeam];

  const MarkerFilled = data => (
    <div className={clsx(classes.marker, classes.markerFilled, data.teamColor)}>
      <img src={`/static/ffvii/game-counters/${data.teamColor}_materia.png`} />
    </div>
  );

  const MarkerEmpty = data => (
    <div className={clsx(classes.marker, classes.markerEmpty, data.teamColor)}></div>
  );

  return (
    <div id="matchSummaryNames" className={clsx(classes.container, className)}>
      {teams.map(team => (
        <div className={clsx(classes.teamContainer, team.slug, 'team-container')}>
          <div className={clsx(classes.scoreCounter, team.slug, 'score-counter')}>
            <TeamScoreCounter
              key={team.slug}
              markerFilled={MarkerFilled}
              markerEmpty={MarkerEmpty}
              winsPerMatch={Math.max(blueTeam.score, goldTeam.score)}
              score={team.score}
              teamColor={team.slug}
            />
          </div>
          <div className={clsx(classes.teamName, team.slug, 'team-name')}>{team.name}</div>
        </div>
      ))}
    </div>
  );
}
