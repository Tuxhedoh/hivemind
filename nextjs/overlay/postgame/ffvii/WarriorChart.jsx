import { makeStyles } from '@material-ui/core/styles';

import BaseWarriorChart from 'components/charts/BaseWarriorChart';
import { usePostgameStats } from 'overlay/PostgameStats';
import ChartContainer from './ChartContainer';

const useStyles = makeStyles(theme => ({}));

export default function WarriorChart({ className }) {
  const classes = useStyles();
  const stats = usePostgameStats();

  return (
    <ChartContainer title="Wars Up Queens Dwn" className={className}>
      {stats?.warriorData && <BaseWarriorChart
        game={stats}
        datasetProps={{ pointRadius: 0, borderWidth: 4 }}
        blueBackgroundColor={"#6CE3D1"}
        blueLineColor={"#6CE3D1"}
        blueQueenDeathColor={"#6CE3D1"}
        goldBackgroundColor={"#D79938"}
        goldLineColor={"#D79938"}
        goldQueenDeathColor={"#D79938"}
      />}
    </ChartContainer>
  );
}
