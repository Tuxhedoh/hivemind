import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { usePostgameStats } from 'overlay/PostgameStats';
import TeamStatsRow from './TeamStatsRow';

const useStyles = makeStyles(theme => ({
  container: {
    border: '4px groove white',
    boxShadow: "inset 0 0 4px #000000aa",
    borderRadius: '8px',
    marginBottom: '1px',
    background: 'rgba(0,0,0,0.8)',
  }
}));

export default function TeamStats({ className }) {
  const classes = useStyles();
  const stats = usePostgameStats();

  return (
    <div className={clsx(classes.container, className)}>
      {(stats !== null) && (
        <>
          <TeamStatsRow label="Kills" value={stats.kills} />
          <TeamStatsRow label="Military Kills" value={stats.militaryKills} />
          <TeamStatsRow label="Berries" value={stats.berries} />
          <TeamStatsRow label="Gate Control" value={stats.gateControl} />
        </>
      )}
    </div>
  );
}
