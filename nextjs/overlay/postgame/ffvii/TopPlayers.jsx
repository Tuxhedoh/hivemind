import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { usePostgameStats } from 'overlay/PostgameStats';
import TopPlayersRow from './TopPlayersRow';

const useStyles = makeStyles(theme => ({
  container: {
    border: '4px groove white',
    boxShadow: "inset 0 0 4px #000000aa",
    borderRadius: '8px',
    marginBottom: '1px',
    background: 'rgba(0,0,0,0.8)',
  }
}));

export default function TeamStats({ className }) {
  const classes = useStyles();
  const stats = usePostgameStats();

  return (
    <div className={clsx(classes.container, className)}>
      {(stats !== null) && (
        <>
          <TopPlayersRow label="Most Kills" value={stats.mostKills} showWarrior={true} />
          <TopPlayersRow label="Most Military Kills" value={stats.mostMilitaryKills} showWarrior={true} />
          <TopPlayersRow label="Most Deaths" value={stats.mostDeaths} />
          <TopPlayersRow label="Most Berries" value={stats.mostBerries} />
          <TopPlayersRow label="Most Snail Distance" from={stats.byPlayer?.snailDistance} format={v => `${Math.round(v * 1.0 / stats.snailData.length / 2.0 / 21.0 * 100)}%`} />
          <TopPlayersRow label="Best Warrior Uptime" value={stats.mostWarriorUptime} showWarrior={true} />
          <TopPlayersRow label="Most Bump Assists" from={stats.byPlayer?.bumpAssists} />
        </>
      )}
    </div>
  );
}
