import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
    margin: '0.5vw 0',
  },
  label: {
    flexGrow: 4,
    flexBasis: 0,
    whiteSpace: 'nowrap',
    margin: '0 1vw',
  },
  value: {
    textAlign: 'right',
    flexGrow: 1,
    flexBasis: 0,
    margin: '0 1vw',
    padding: '0 0.5vw',
    borderWidth: '1px',
    borderStyle: 'solid',
    borderRadius: '5px',
    background: '#ffffff7f',
  },
  blue: {
    border: '4px groove white',
    boxShadow: "inset 0 0 4px #000000aa",
    borderRadius: '8px',
    background: "linear-gradient(#000099, #000033)",
  },
  gold: {
    border: '4px groove white',
    boxShadow: "inset 0 0 4px #000000aa",
    borderRadius: '8px',
    background: "linear-gradient(#cc9933, #996633)",
  },
}));

export default function TeamStatsRow({ label, value, className }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.row, 'row', className)}>
      <div className={clsx(classes.label, 'label')}>{label}</div>
      <div className={clsx(classes.value, classes.blue, 'value', 'blue')}>{value?.blue ?? ''}</div>
      <div className={clsx(classes.value, classes.gold, 'value', 'gold')}>{value?.gold ?? ''}</div>
    </div>
  );
}
