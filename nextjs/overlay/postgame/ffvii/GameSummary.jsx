import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { usePostgameStats } from 'overlay/PostgameStats';
import GameLinkQrCode from './GameLinkQrCode';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    border: '4px groove white',
    boxShadow: "inset 0 0 4px #000000aa",
    borderRadius: '8px',
    marginBottom: '1px',
  },
  textSection: {
    flexBasis: 0,
    flexGrow: 1,
    padding: '0 1vw',
  },
  victoryImage: {
    width: '4vw',
    height: '4vw',
    marginLeft: '0.5vw',
  },
  winDescription: {
    fontSize: '2.5vw',
    fontFamily: 'OPTIEngeEtienne, Final Fantasy, Roboto, sans-serif',
    textTransform: 'uppercase',
    textShadow: '2px 2px #212421, 4px 4px #212021',
    textAlign: 'center',
    margin: '1vw 0',
  },
  gameInfo: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  summaryRows: {
    flexGrow: 1,
  },
  qrCodeStamp: {
    background: 'white',
    height: '9vh',
    marginRight: '1vw',
  }
}));

export default function GameSummary({ className }) {
  const classes = useStyles();
  const stats = usePostgameStats();

  if (stats === null) {
    return <></>;
  }

  return (
    <div className={clsx(classes.container, className)}>
      <div className={clsx(classes.textSection, 'text-section')}>
        <div
          className={clsx(
            classes.winDescription,
            'win-description cheee-lowgrav tracking-wider text-6xl bg-clip-text',
          )}
        >
          {stats?.winningTeamDisplay ?? '???'} Victory
        </div>
        <div className={clsx(classes.gameInfo, 'game-info justify-between')}>
          <div className={clsx('game-info-entry')}>{stats.map}</div>
          <div className={clsx('game-info-entry')}>
            {stats.winConditionDisplay}
          </div>
          <div className={clsx('game-info-entry')}>{stats.length}</div>
          <div className={clsx('game-info-entry')}>
            Game ID: {stats.gameId}
          </div>
        </div>
      </div>
      <GameLinkQrCode className={clsx(classes.qrCode, classes.qrCodeStamp)} />
    </div>
  );
}
