import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { POSITIONS_BY_ID } from 'util/constants';

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  label: {
    flexGrow: 3,
    flexBasis: 0,
    fontSize: '1.5vw',
    fontWeight: 'bold',
    lineHeight: '2vw',
    whiteSpace: 'nowrap',
    margin: '0 1vw',
  },
  icons: {
    flexGrow: 2,
    flexBasis: 0,
    textAlign: 'right',
  },
  icon: {
    height: '3vw',
    width: '3vw',
    marginLeft: '5px',
    marginTop: '-0.5vw',
    border: '3px solid #ffffff99',
  },
}));

export default function AwardRow({ label, players, className }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.row, className)}>
      <div className={classes.label}>{label}</div>
      <div className={classes.icons}>
        {players &&
          players.map(id => (
            <img key={id} src={POSITIONS_BY_ID[id].ICON} className={classes.icon} />
          ))}
      </div>
    </div>
  );
}
