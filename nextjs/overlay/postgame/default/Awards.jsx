import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import PropTypes from 'prop-types';

import { usePostgameStats } from 'overlay/PostgameStats';
import AwardRow from './AwardRow';

const useStyles = makeStyles(theme => ({
  container: {
    position: 'relative',
    height: '4vw',
    margin: '0 7.5vw',
  },
  placeholder: {
    opacity: 0,
  },
  row: {
    opacity: 0,
    transition: 'opacity 1.5s',
    position: 'absolute',
    width: 'calc(100% - 2vw)',
    top: '1vw',
    '&.visible': {
      opacity: 1,
    },
  },
}));

export default function Awards({ className }) {
  const classes = useStyles();
  const stats = usePostgameStats();
  const [counter, setCounter] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      if (stats?.loadedTime) {
        setCounter(Math.floor((Date.now() - stats.loadedTime) / 5000));
      }
    }, 1000);

    return () => clearInterval(interval);
  }, [stats]);

  if (!(stats?.awards?.length > 0)) {
    return <></>;
  }

  return (
    <div className={clsx(classes.container, className)}>
      <>
        {stats.awards.map((award, idx) => (
          <AwardRow
            key={award.title}
            label={award.title}
            players={award.players}
            className={clsx(classes.row, { visible: counter % stats.awards.length === idx })}
          />
        ))}
      </>
    </div>
  );
}

Awards.propTypes = {
  className: PropTypes.string,
};

Awards.defaultProps = {
  className: null,
};
