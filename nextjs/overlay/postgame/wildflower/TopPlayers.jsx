import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { usePostgameStats } from 'overlay/PostgameStats';
import TopPlayersRow from './TopPlayersRow';

const useStyles = makeStyles(theme => ({
}));

export default function TeamStats({ className }) {
  const classes = useStyles();
  const stats = usePostgameStats();

  return (
    <div className={clsx(classes.container, className)}>
      {(stats !== null) && (
        <>
          <TopPlayersRow label="Most Kills" value={stats.mostKills} />
          <TopPlayersRow label="Most Military Kills" value={stats.mostMilitaryKills} />
          <TopPlayersRow label="Most Deaths" value={stats.mostDeaths} />
          <TopPlayersRow label="Most Berries" value={stats.mostBerries} />
          <TopPlayersRow label="Most Snail Distance" from={stats.byPlayer?.snailDistance} format={v => `${v} m`} />
          <TopPlayersRow label="Best Warrior Uptime" value={stats.mostWarriorUptime} />
          <TopPlayersRow label="Most Bump Assists" from={stats.byPlayer?.bumpAssists} />
        </>
      )}
    </div>
  );
}
