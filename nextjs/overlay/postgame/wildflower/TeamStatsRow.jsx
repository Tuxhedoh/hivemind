import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
    margin: '0.5vw 0',
  },
  label: {
    fontFamily: 'Oswald',
    flexGrow: 4,
    flexBasis: 0,
    fontSize: '1.5vw',
    whiteSpace: 'nowrap',
    margin: '0 1vw',
  },
  value: {
    fontFamily: 'Oswald',
    fontSize: '1.5vw',
    textAlign: 'right',
    flexGrow: 1,
    flexBasis: 0,
    margin: '0 1vw',
    padding: '0 0.5vw',
    fontWeight: 'bold',
    borderWidth: '1px',
    borderStyle: 'solid',
    borderRadius: '5px',
    background: '#ffffff7f',
  },
  blue: {
    borderColor: theme.palette.blue.dark3,
    boxShadow: `0 0 15px 3px ${theme.palette.blue.dark3}7f`,
  },
  gold: {
    borderColor: theme.palette.gold.dark3,
    boxShadow: `0 0 15px 3px ${theme.palette.gold.dark3}7f`,
  },
}));

export default function TeamStatsRow({ label, value, className }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.row, 'row', className)}>
      <div className={clsx(classes.label, 'label')}>{label}</div>
      <div className={clsx(classes.value, classes.blue, 'value', 'blue')}>{value?.blue ?? ''}</div>
      <div className={clsx(classes.value, classes.gold, 'value', 'gold')}>{value?.gold ?? ''}</div>
    </div>
  );
}
