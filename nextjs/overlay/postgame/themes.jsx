import Default from './Default';
import FFVII from './FFVII';
import Wildflower from './Wildflower';

const allPostgameThemes = [Default, Wildflower, FFVII];

export function getPostgameTheme(name) {
  for (const theme of allPostgameThemes) {
    if (theme.themeProps.name == name) {
      return theme;
    }
  }
}

export { allPostgameThemes };
