import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import PlayerName from './PlayerName';
import TeamName from 'overlay/components/TeamName';
import { useMatch } from 'overlay/Match';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    opacity: '0.9',
    background: ({ team }) => theme.gradients[team].light,
  },
  containerLarge: {
    height: '818px',
    overflow: 'hidden',
    alignItems: 'center',
  },
  containerSmall: {
    height: '82px',
  },
  teamName: {
    padding: '10px 0',
    minWidth: '100%',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: '42px',
    whiteSpace: 'nowrap',
    '.team-container &': {
      borderBottom: '1px solid #444',
    },
  },
  playerNames: {
    margin: '0 0 10px 0',
    flexBasis: 0,
    flexGrow: 1,
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
}));

export default function TeamNameBox({ team, className }) {
  const classes = useStyles({ team });
  const match = useMatch();
  const players = match?.players[team];
  const pics = players?.some(player => player.image);

  if (players?.length > 0) {
    return (
      <div className={clsx(classes.container, classes.containerLarge, className, 'team-container')}>
        <div className={clsx(classes.teamName, 'team-name')}>
          <TeamName team={team} />
        </div>

        <div className={clsx(classes.playerNames, 'player-names', { pics })}>
          {players.map(player => (
            <PlayerName
              key={player.id}
              className={clsx(classes.playerName, 'player-name')}
              team={team}
              player={player}
            />
          ))}
        </div>

      </div>
    );
  }

  return (
    <div className={clsx(classes.container, classes.containerSmall, className, 'team-container-small')}>
      <div className={clsx(classes.teamName, 'team-name')}>
        <TeamName team={team} />
      </div>
    </div>
  );
}
