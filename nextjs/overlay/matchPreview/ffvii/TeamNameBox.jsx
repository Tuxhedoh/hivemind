import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { useMatch } from 'overlay/Match';
import TeamName from 'overlay/components/TeamName';
import PlayerName from './PlayerName';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    gap: '1px',
  },
  containerLarge: {
    overflow: 'hidden',
    alignItems: 'center',
  },
  containerSmall: {
    height: '82px',
  },
  teamName: {
    padding: '16px 0 20px',
    minWidth: '100%',
    textAlign: 'center',

    fontSize: '48px',
    lineHeight: '40px',
    whiteSpace: 'nowrap',

    fontFamily: 'OPTIEngeEtienne, Final Fantasy',
    textTransform: 'uppercase',
    letterSpacing: '1px',
  },
  playerNames: {
    margin: '0 0 0 0',
    flexGrow: 1,
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    gap: '1px',
    fontFamily: 'Reactor7, sans-serif',
  },
  insideBox: {
    border: '4px groove white',
    borderRadius: '8px',
    boxShadow: 'inset 0 0 2px #000000',
    textShadow: '1.5px 1.5px black',
    color: 'white',
    minHeight: 'fit-content',
    '&.blue': {
      backgroundImage: 'linear-gradient(#000099, #000033)',
    },

    '&.gold': {
      backgroundImage: 'linear-gradient(#cc9933, #996633)',
    },
  },
}));

export default function TeamNameBox({ team, className, teamColor }) {
  const classes = useStyles({ team });
  const match = useMatch();
  const players = match?.players[team];
  const pics = players?.some(player => player.image);

  if (players?.length > 0) {
    return (
      <div className={clsx(classes.container, classes.containerLarge, className, 'team-container')}>
        <div className={clsx(classes.teamName, 'team-name', classes.insideBox, teamColor)}>
          <TeamName team={team} />
        </div>

        <div className={clsx(classes.playerNames, 'player-names', { pics })}>
          {players.map(player => (
            <PlayerName
              key={player.id}
              className={clsx(classes.playerName, 'player-name', classes.insideBox, teamColor)}
              team={team}
              player={player}
            />
          ))}
        </div>
      </div>
    );
  }

  return (
    <div
      className={clsx(classes.container, classes.containerSmall, className, 'team-container-small')}
    >
      <div className={clsx(classes.teamName, 'team-name')}>
        <TeamName team={team} />
      </div>
    </div>
  );
}
