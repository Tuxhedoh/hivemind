import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

const useStyles = makeStyles(theme => ({
  player: ({ team }) => ({
    flexGrow: 1,
    overflow: 'hidden',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 20px',
  }),
  image: {
    width: '115px',
    height: '115px',
    overflow: 'hidden',
    flexGrow: 0,
    flexBasis: '100px',
    '& img': {
      width: '100px',
      height: '115px',
      objectFit: 'cover',
    },
    '.player-names.pics &': {
      display: 'block',
    },
  },
  text: {
    flexGrow: '1',
    flexBasis: '0',
    display: 'flex',
    flexDirection: 'column',
    padding: '0 20px',
  },
  playerAndSceneRow: {
    display: 'flex',
    direction: 'row',
  },
  playerName: {
    fontSize: '36px',
    lineHeight: '36px',
    display: 'flex',
  },
  pronouns: {
    fontSize: '36px',
    lineHeight: '36px',
    paddingLeft: '25px',
    display: 'flex',
    flexDirection: 'column',
  },
  scene: {
    fontSize: '36px',
    lineHeight: '36px',
    textAlign: 'right',
    width: '200px',
    margin: '0 0 0 auto',
    display: 'flex',
    flexDirection: 'row',
  },
  sceneExp: {
    width: '200px',
    textAlign: 'left',
  },
  sceneText: {
    width: '100px',
  },
  tidbit: {
    fontSize: '18px',
    textAlign: 'center',
    margin: '10px 0 0 0',
  },
}));

export default function PlayerName({ player, team, className }) {
  const classes = useStyles({ team });

  return (
    <div className={clsx(classes.player, 'player', className)}>
      <div className={clsx(classes.image, 'player-image')}>
        {player.image && !player.doNotDisplay ? (
          <img src={player.image} />
        ) : (
          <img src={`/static/ffvii/missing_avatar_placeholder.png`} />
        )}
      </div>
      <div className={clsx(classes.text, 'text')}>
        <div className={clsx(classes.playerAndSceneRow, 'player-scene-row')}>
          <div className={clsx(classes.playerName, 'player-name')}>{player.name}</div>
          <div className={clsx(classes.scene, 'scene')}>
            <div className={clsx(classes.sceneExp, 'scene-exxp')}>EXP:</div>
            <div className={clsx(classes.sceneText, 'scene-text')}>
              {player.scene ? player.scene : '???'}
            </div>
          </div>
        </div>
        <div className={clsx(classes.pronouns, 'pronouns')}>
          MP: {player.pronouns ? player.pronouns : '???'}
        </div>
        {/*{player.tidbit && <div className={clsx(classes.tidbit, 'tidbit')}>{player.tidbit}</div>}*/}
      </div>
    </div>
  );
}
