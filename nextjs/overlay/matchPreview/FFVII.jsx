import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Head from 'next/head';

import { useMatch } from 'overlay/Match';
import { useSceneController } from 'overlay/SceneController';
import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, GOLD_TEAM, OVERLAY_SCENES } from 'util/constants';
import EventNameBox from './ffvii/EventNameBox';
import TeamNameBox from './ffvii/TeamNameBox';

const useStyles = makeStyles(theme => ({
  container: {
    position: 'relative',
    width: 1544,
    height: 872,
    top: 0,
    left: 0,
    overflow: 'hidden',
  },
  eventName: {
    // position: 'absolute',
    // top: '50px',
    // left: '50%',
    width: '100%',
    transform: 'translateY(-150%)',
    // width: '1152px',
    // height: '114px',
    textAlign: 'center',
    fontSize: '54px',
    padding: '12px 20px',
    fontWeight: 'bold',
    fontFamily: 'Reactor7, sans-serif',
    transition: 'transform 1s ease-in-out',
    '.active &': {
      // top: '54px',
      transform: 'translateY( 0%)',
    },

    border: '4px groove white',
    borderRadius: '8px',
    boxShadow: 'inset 0 0 2px #000000',
    textShadow: '1.5px 1.5px black',
    color: 'white',
    background: 'linear-gradient(#cc3399, #993399)',
  },
  teamName: {
    width: '100%',
    transition: 'transform 1s ease-in-out',
    flexGrow: 1,
    height: '100%',
  },
  teamNameLeft: {
    transform: 'translateX(-100%)',
    '.active &': {
      transform: 'translateX(0%)',
    },
  },
  teamNameRight: {
    transform: 'translateX(100%)',
    '.active &': {
      transform: 'translateX(0%)',
    },
  },
}));

export default function FFVIIOverlay({}) {
  const classes = useStyles();
  const settings = useOverlaySettings();
  const scene = useSceneController();
  const match = useMatch();

  const active = (scene === OVERLAY_SCENES.MATCH_PREVIEW || scene === null) && match;

  const leftTeam = settings?.goldOnLeft ? GOLD_TEAM : BLUE_TEAM;
  const rightTeam = settings?.goldOnLeft ? BLUE_TEAM : GOLD_TEAM;

  return (
    <>
      <Head>
        {settings?.additionalFonts && <link rel="stylesheet" href={settings.additionalFonts} />}
        <link
          rel="stylesheet"
          type="text/css"
          href={`/static/ffvii/fonts/final_fantasy/stylesheet.css`}
        />
        <link
          rel="stylesheet"
          type="text/css"
          href={`/static/ffvii/fonts/enge-etienne/stylesheet.css`}
        />
        <link rel="stylesheet" type="text/css" href={`/static/ffvii/fonts/reactor7/webfont.css`} />
      </Head>
      <div
        id="matchPreviewOverlay"
        className={clsx(
          classes.container,
          ' flex flex-col gap-px items-center justify-evenly pt-px',
          {
            active,
          },
        )}
      >
        <EventNameBox className={clsx(classes.eventName, 'event-name')} />
        <div className="grid grid-cols-2 gap-px w-full h-full flex-grow">
          <TeamNameBox
            className={clsx(classes.teamName, classes.teamNameLeft, 'team', 'left-team')}
            team={leftTeam}
            teamColor={'blue'}
          />
          <TeamNameBox
            className={clsx(classes.teamName, classes.teamNameRight, 'team', 'right-team')}
            team={rightTeam}
            teamColor={'gold'}
          />
        </div>
      </div>
    </>
  );
}

FFVIIOverlay.themeProps = {
  name: 'ffvii',
  displayName: 'FF VII',
  description: `Match preview theme inspired by Final Fantasy VII.`,
};
