import Default from './Default';
import FFVII from './FFVII';
import Manuka from './Manuka';

const allMatchPreviewThemes = [Default, Manuka, FFVII];

export function getMatchPreviewTheme(name) {
  for (const theme of allMatchPreviewThemes) {
    if (theme.themeProps.name == name) {
      return theme;
    }
  }
}

export { allMatchPreviewThemes };
