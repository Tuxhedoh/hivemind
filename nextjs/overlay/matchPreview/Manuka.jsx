import clsx from 'clsx';
import Head from 'next/head';
import { useMatch } from 'overlay/Match';
import { useSceneController } from 'overlay/SceneController';
import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, GOLD_TEAM, OVERLAY_SCENES } from 'util/constants';
import EventNameBox from './manuka/EventNameBox';
import TeamNameBox from './manuka/TeamNameBox';

export default function KQ35Overlay({}) {
  const settings = useOverlaySettings();
  const scene = useSceneController();
  const match = useMatch();

  const active = (scene === OVERLAY_SCENES.MATCH_PREVIEW || scene === null) && match;

  const leftTeam = settings?.goldOnLeft ? GOLD_TEAM : BLUE_TEAM;
  const rightTeam = settings?.goldOnLeft ? BLUE_TEAM : GOLD_TEAM;

  return (
    <>
      <Head>
        {settings?.additionalFonts && <link rel="stylesheet" href={settings.additionalFonts} />}
      </Head>

      <div
        id="matchPreviewOverlay"
        className={clsx(
          'absolute w-[1920px] aspect-video top-0 left-0',
          active ? 'opacity-100' : 'opacity-0',
          { active },
        )}
      >
        <EventNameBox
          className={clsx(
            'font-custom text-center text-5xl text-white text-shadow-thicc absolute top-[56px] z-20 left-1/2 -translate-x-1/2 w-[1380px] h-[164px]',
            'flex items-center justify-center pb-16',
          )}
        />

        <div
          id="previewTeamBoxesContainer"
          className="grid grid-cols-2 gap-16 absolute  w-5/6 left-1/2 -translate-x-1/2 top-[200px] z-10"
        >
          <TeamNameBox
            key="blueteam"
            className={clsx(
              'preview-team-container-blue  relative !bg-blue-main',
              'before:content-[""] before:absolute before:top-[-23px] before:block before:-z-10 before:bg-blue-main before:inset-x-0 before:h-[45px] before:rounded-t-[100%]',
              'after:content-[""] after:absolute after:bottom-[-23px] after:block after:-z-10 after:bg-blue-main after:inset-x-0 after:h-[45px] after:rounded-b-[100%]',
              'left-team',
            )}
            team={leftTeam}
          />
          <TeamNameBox
            key="goldteam"
            className={clsx(
              'preview-team-container-blue  relative bg-gold-main',
              'before:content-[""] before:absolute before:top-[-23px] before:block before:-z-10 before:bg-gold-main before:inset-x-0 before:h-[45px] before:rounded-t-[100%]',
              'after:content-[""] after:absolute after:bottom-[-23px] after:block after:-z-10 after:bg-gold-main after:inset-x-0 after:h-[45px] after:rounded-b-[100%]',
              'right-team',
            )}
            team={rightTeam}
          />
        </div>
      </div>
    </>
  );
}

KQ35Overlay.themeProps = {
  name: 'manuka',
  displayName: 'Manuka',
  description: `A variation of the default theme with manuka customizations`,
};
