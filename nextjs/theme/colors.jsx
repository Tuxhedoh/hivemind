const colors = {
  blue: {
    light5: '#EBF4FF',
    light4: '#cce5ff',
    light3: '#99ccff',
    light2: '#66b2ff',
    light1: '#3299ff',
    main: '#287fff',
    dark1: '#2860e8',
    dark2: '#10489D',
    dark3: '#143366',
    dark4: '#12264D',
    dark5: '#101933',
  },
  // e0fcf9,b8f8f1,83f2e6,19e6d1,1dc5d7,21a3dd,2860e8
  gold: {
    main: '#ffb330',
    dark1: '#ffa70e',
    dark2: '#eb9500',
    dark3: '#c97f00',
    dark4: '#a76a00',
    dark5: '#855400',
    light1: '#ffbf52',
    light2: '#ffcc74',
    light3: '#ffd896',
    light4: '#ffe5b8',
    light5: '#fff1da',
  },
  secondary: {
    main: '#C856C8', //baseTheme.palette.gold.dark5,
    light: '#C58194', //baseTheme.palette.gold.main,
    dark: '#8C4056',
    lighter: '#D8ABB8'
  },
  text: {
    primary: '#212121'
  }
};

const gradients = {
  Day: 'linear-gradient(#5bacdc, #98d8fb)',
  Night: 'linear-gradient(#17113f, #6361d2)',
  Dusk: 'linear-gradient(#885527, #cb9544)',
  Twilight: 'linear-gradient(#061d33, #8d0b8f)',
};

for (const color of Object.keys(colors)) {
  gradients[color] = {
    light: `linear-gradient(135deg, ${colors[color].light3}, ${colors[color].light2})`,
    main: `linear-gradient(${colors[color].light1}, ${colors[color].dark1})`,
    dark: `linear-gradient(${colors[color].dark1}, ${colors[color].dark3})`,
  };
}

gradients.neutral = {
  light: 'linear-gradient(#eee, #ccc)',
  main: 'linear-gradient(#ccc, #aaa)',
  dark: 'linear-gradient(#aaa, #888)',
};

export { colors, gradients };
