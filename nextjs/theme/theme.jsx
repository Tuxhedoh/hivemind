import { createTheme } from '@material-ui/core/styles';

import { colors, gradients } from 'theme/colors';
import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';

const baseTheme = {
  palette: {
    type: 'light',
    ...colors,
    secondary: {
      main: '#C856C8', //baseTheme.palette.gold.dark5,
      light: '#C58194', //baseTheme.palette.gold.main,
      dark: '#8C4056',
      lighter: '#D8ABB8'
    },
    text: {
      primary: '#212121'
    }
  },
  typography: {
    fontFamily: ['Roboto', 'sans-serif'],
    fontSize: 15,
    h1: {
      fontSize: '32px',
      fontFamily: ['Roboto Slab', 'sans-serif'],
      fontWeight: 'bold',
    },
    h2: {
      marginTop: '24px',
      fontSize: '24px',
      fontFamily: ['Roboto Slab', 'sans-serif'],
      // paddingBottom: '8px',
      // borderBottom: '2px solid #9DB2D2' ,
      fontWeight: 'bold',
    },
    h3: {
      fontSize: '20px',
      fontFamily: ['Roboto', 'sans-serif'],
      marginTop: '20px',
    },
    h4: {
      fontSize: '16px',
      fontFamily: ['Roboto', 'sans-serif'],
    },
    subtitle1: {
      fontSize: '15px',
      fontFamily: ['Roboto', 'sans-serif'],
      fontStyle: 'italic',
    },
  },
  mixins: {
    shadow: {
      boxShadow: 'box-shadow: 3px 0 2px rgba(0, 0, 0, 0.5)',
    },
    tableCell: {
      numeric: theme => ({
        width: '100px',
        textAlign: 'right',
        padding: theme.spacing(0, 2),
      }),
    },
  },
};

baseTheme.gradients = {
  ...gradients,
  gray: {
    light: 'linear-gradient(#ccc, #aaa)',
  },
  primary: {
    light: `linear-gradient(${baseTheme.palette.gold.light3}, ${baseTheme.palette.gold.light1})`,
  },
  secondary: {
    light: `linear-gradient(${baseTheme.palette.secondary.lighter}, ${baseTheme.palette.secondary.light})`,
  },
  gameWinner: game => {
    if (game.winningTeam === BLUE_TEAM) return gradients.blue;
    if (game.winningTeam === GOLD_TEAM) return gradients.gold;
  },
};

const theme = createTheme(baseTheme, {
  palette: {
    primary: {
      main: baseTheme.palette.gold.dark1,
      light: baseTheme.palette.gold.main,
    },

    highlight: {
      main: baseTheme.palette.gold.light5,
    },
    divider : '#e0e0e0'
  },
});

theme.overrides.MuiCssBaseline = {
  '@global': {
    body: {
      backgroundColor: theme.palette.gold.light2,
      backgroundColor: theme.palette.grey['300'],
    },
  },
};

export default theme;
