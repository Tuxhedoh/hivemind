import React, { useState, useEffect } from 'react';
import useReactWebSocket, { ReadyState } from 'react-use-websocket';

import isServer from 'util/isServer';

export function isArray(obj) {
  return Array.isArray(obj);
}

export function isObject(obj) {
  return obj === Object(obj) && !isArray(obj) && typeof obj !== 'function';
}

export function toCamelCaseString(text) {
  return text.replace(/([-_][a-z])/ig, ($1) => {
    return $1.toUpperCase().replace('-', '').replace('_', '');
  });
}

export function toCamelCase(obj) {
  if (isObject(obj)) {
    const result = {};
    for (const [k, v] of Object.entries(obj)) {
      result[toCamelCaseString(k)] = toCamelCase(v);
    }

    return result;
  }

  if (isArray(obj)) {
    return obj.map(v => toCamelCase(v));
  }

  return obj;
}

export function useWebSocket(path, options) {
  const ws = {};
  ws.options = options ?? {};
  ws.options.shouldReconnect ??= () => true;
  ws.path = path;
  ws.jsonMessageHooks = [];

  const [hooks, setHooks] = useState([]);

  if (isServer()) {
    return {
      onJsonMessage: () => null,
    };
  }

  ws.options.onMessage = ({ data }) => {
    const message = toCamelCase(JSON.parse(data));
    for (const hook of hooks) {
      hook(message);
    }
  };

  ws.onJsonMessage = (hook, dependencies) => {
    dependencies ??= [];
    useEffect(() => {
      setHooks(v => [...v, hook]);

      return () => {
        setHooks(v => v.filter(i => i != hook));
      };
    }, dependencies);
  };

  ws.url = `${location.protocol.replace('http', 'ws')}//${location.host}/${path}`;
  ws.socket = useReactWebSocket(ws.url, ws.options);
  ws.sendJsonMessage = ws.socket.sendJsonMessage;

  return ws;
}
