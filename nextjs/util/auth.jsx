import { clearAuthTokens, isLoggedIn, setAuthTokens } from 'axios-jwt';
import PropTypes from 'prop-types';
import React, { useContext, useState } from 'react';
import { getAxios } from 'util/axios';

import { PERMISSION_TYPES } from 'util/constants';
import isServer from 'util/isServer';

export const UserContext = React.createContext({
  user: null,
});

export const useUser = () => useContext(UserContext).user;

export async function onLoginSuccess({ accessToken, tokenId }) {
  const axios = getAxios();
  const response = await axios.post('/api/user/exchange-token/google-oauth2/', {
    accessToken: accessToken,
    idToken: tokenId,
  });

  await setAuthTokens({
    accessToken: response.data.accessToken,
    refreshToken: response.data.refreshToken,
  });
}

export async function onLoginFailure({ ...props }) {
  clearAuthTokens();
}

export function logOut() {
  clearAuthTokens();
}

export async function getCurrentUser() {
  const axios = getAxios({ authenticated: true });
  try {
    const response = await axios.get('/api/user/me/');
    return response.data;
  } catch (e) {
    return null;
  }
}

export function UserProvider({ children }) {
  const [user, setUser] = useState(null);

  if (user === null) {
    getCurrentUser().then(response => {
      setUser(response);
    });
  }

  const userState = { user };

  return <UserContext.Provider value={userState}>{children}</UserContext.Provider>;
}

export function UserConsumer(component) {
  return componentProps => (
    <div suppressHydrationWarning>
      <UserContext.Consumer>
        {props => React.createElement(component, { ...props, ...componentProps })}
      </UserContext.Consumer>
    </div>
  );
}

export function isAdminOf(sceneId) {
  return hasPermission(sceneId, PERMISSION_TYPES.ADMIN);
}

export function hasPermission(sceneId, permissionType) {
  const user = useUser();
  if (user?.permissions) {
    for (const permission of user.permissions) {
      if (sceneId === permission.scene && permission.permission === permissionType) {
        return true;
      }
    }
  }

  return false;
}

export const IfLoggedIn = React.forwardRef(({ state, children }, ref) => (
  <div suppressHydrationWarning>
    {(!isServer() && isLoggedIn()) === state && <React.Fragment>{children}</React.Fragment>}
  </div>
));

IfLoggedIn.propTypes = {
  state: PropTypes.bool,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.element), PropTypes.element])
    .isRequired,
};

IfLoggedIn.defaultProps = {
  state: true,
};
