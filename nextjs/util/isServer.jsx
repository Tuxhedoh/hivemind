export default function isServer() {
  return Boolean(typeof window === 'undefined');
};
