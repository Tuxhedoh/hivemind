import React, { useEffect, useState } from 'react';

export default function usePersistentState(name, defaultValue) {
  if (typeof window === 'undefined' || !('localStorage' in window)) {
    return useState(defaultValue);
  }

  const stored = localStorage.getItem(name);
  const def = stored ? JSON.parse(stored) : defaultValue;
  const [val, setVal] = useState(def);

  useEffect(() => {
    localStorage.setItem(name, JSON.stringify(val));
  }, [val]);

  return [val, setVal];
}
