import { format, intervalToDuration, formatDuration, addMinutes, parseISO } from 'date-fns';

export function formatInterval(value) {
  return formatDuration(intervalToDuration({ start: 0, end: value }), {
    format: ['days', 'hours', 'minutes', 'seconds'],
  });
}

export function utcDateFromString(value) {
  const localDate = new Date(value);
  return addMinutes(localDate, localDate.getTimezoneOffset());
}

export function formatUTC(value, formatString) {
  return format(utcDateFromString(value), formatString);
}

export function formatTime(time) {
  if (!time) return '';

  if (time > 1000 * 60 * 60) {
    return `${Math.floor(time / 1000 / 60 / 60)}:${format(time, 'mm:ss')}`;
  }

  return format(time, 'm:ss');
}

export function formatGameTime(game) {
  if (!game?.startTime || !game?.endTime) return '';

  return formatTime(parseISO(game.endTime) - parseISO(game.startTime));
}

export function parseDuration(durationStr) {
  let sec = 0;
  for (const val of durationStr.split(':')) {
    sec *= 60;
    sec += parseInt(val);
  }

  return intervalToDuration({ start: 0, end: sec * 1000 });
}
