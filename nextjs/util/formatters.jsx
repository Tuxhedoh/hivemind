export function formatPercent(value, decimals = 2) {
  if (value === undefined) return '';
  return value.toLocaleString(undefined, {
    style: 'percent',
    minimumFractionDigits: decimals,
    maximumFractionDigits: decimals,
  });
}
