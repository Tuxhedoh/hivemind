up:
	docker compose up -d

tail: up
	docker compose logs -f

down:
	docker compose down

makemigrations: up
	docker compose exec app python /src/server/manage.py makemigrations

migrate: up
	docker compose exec app python /src/server/manage.py migrate

initial-setup:
	test -f app.env || cp app.env.dev app.env
	docker compose up -d db
	docker-compose run --rm app python manage.py migrate
	$(MAKE) up

test-scene: up
	docker compose exec app python /src/server/manage.py create_test_scene --admin $(EMAIL)

